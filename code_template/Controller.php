<?php
    
    namespace Ivory\Template;
    
    use View;

    use Format;
    use Message;
    
	class Controller extends BaseController 
	{
        // controller yg melakukan pemrosesan dari request Ajax
		public function postProcess()
		{
			$form_processor = new Repository(); // form repository

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new Processor(); // input/output class

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        // controller yg hanya me-return view
        public function getIndex()
        {
            return View::make('view.name');
        }
	}
