<?php
	
	namespace Ivory\Template;
	
	class Processor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				return TRUE;
			}
			catch(Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}