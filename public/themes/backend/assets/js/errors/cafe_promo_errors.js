function showInlineError(data)
{
    if(data.data.errors.promo_name != undefined)
    {
        $('.promo_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_name_error').removeClass('hide');
    }
    else
    {
        $('.promo_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_name_error').addClass('hide');
    }

    if(data.data.errors.promo_description != undefined)
    {
        $('.promo_description').addClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_desc_error').removeClass('hide');
    }
    else
    {
        $('.promo_description').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_desc_error').addClass('hide');
    }

    if(data.data.errors.promo_price != undefined)
    {
        $('.promo_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_price_error').removeClass('hide');
    }
    else
    {
        $('.promo_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_price_error').addClass('hide');
    }

    if(data.data.errors.promo_status != undefined)
    {
        $('.promo_status').addClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_status_error').removeClass('hide');
    }
    else
    {
        $('.promo_status').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_status_error').addClass('hide');
    }

    if(data.data.errors.promo_image != undefined)
    {
        $('.promo_image').addClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_image_error').removeClass('hide');
    }
    else
    {
        $('.promo_image').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.promo_image_error').addClass('hide');
    }
}