function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.name-error').removeClass('hide');
    }
    else
    {
        $('.name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.name-error').addClass('hide');
    }

    if(data.data.errors.phone != undefined)
    {
        $('.phone').addClass("border-red-300 bg-red-50 color-red-800");
        $('.phone-error').removeClass('hide');
    }
    else
    {
        $('.phone').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.phone-error').addClass('hide');
    }

    if(data.data.errors.email != undefined)
    {
        $('.email').addClass("border-red-300 bg-red-50 color-red-800");
        $('.email-error').removeClass('hide');
    }
    else
    {
        $('.email').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.email-error').addClass('hide');
    }

    if(data.data.errors.manager_email != undefined)
    {
        $('.manager_email').addClass("border-red-300 bg-red-50 color-red-800");
        $('.manager-email-error').removeClass('hide');
    }
    else
    {
        $('.manager_email').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.manager-email-error').addClass('hide');
    }

    if(data.data.errors.number_of_room != undefined)
    {
        $('.number_of_room').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room-error').removeClass('hide');
    }
    else
    {
        $('.number_of_room').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room-error').addClass('hide');
    }

    if(data.data.errors.number_of_floor != undefined)
    {
        $('.number_of_floor').addClass("border-red-300 bg-red-50 color-red-800");
        $('.floor-error').removeClass('hide');
    }
    else
    {
        $('.number_of_floor').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.floor-error').addClass('hide');
    }

    if(data.data.errors.tax != undefined)
    {
        $('.tax').addClass("border-red-300 bg-red-50 color-red-800");
        $('.tax-error').removeClass('hide');
    }
    else
    {
        $('.tax').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.tax-error').addClass('hide');
    }

    if(data.data.errors.service != undefined)
    {
        $('.service').addClass("border-red-300 bg-red-50 color-red-800");
        $('.service-error').removeClass('hide');
    }
    else
    {
        $('.service').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.service-error').addClass('hide');
    }

    if(data.data.errors.member_discount != undefined)
    {
        $('.member_discount').addClass("border-red-300 bg-red-50 color-red-800");
        $('.member-discount-error').removeClass('hide');
    }
    else
    {
        $('.member_discount').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.member-discount-error').addClass('hide');
    }
}