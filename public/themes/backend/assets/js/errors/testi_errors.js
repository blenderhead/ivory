function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.name_error').removeClass('hide');
    }
    else
    {
        $('.name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.name_error').addClass('hide');
    }

    if(data.data.errors.email != undefined)
    {
        $('.email').addClass("border-red-300 bg-red-50 color-red-800");
        $('.email_error').removeClass('hide');
    }
    else
    {
        $('.email').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.email_error').addClass('hide');
    }
}