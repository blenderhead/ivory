function showInlineError(data)
{
    if(data.data.errors.room_name != undefined)
    {
        $('.room_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room_name_error').removeClass('hide');
    }
    else
    {
        $('.room_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room_name_error').addClass('hide');
    }

    if(data.data.errors.room_description != undefined)
    {
        $('.room_description').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room_desc_error').removeClass('hide');
    }
    else
    {
        $('.room_description').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room_desc_error').addClass('hide');
    }

    if(data.data.errors.room_size != undefined)
    {
        $('.room_size').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room_size_error').removeClass('hide');
    }
    else
    {
        $('.room_size').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room_size_error').addClass('hide');
    }

    if(data.data.errors.number_of_beds != undefined)
    {
        $('.number_of_beds').addClass("border-red-300 bg-red-50 color-red-800");
        $('.num_beds_error').removeClass('hide');
    }
    else
    {
        $('.number_of_beds').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.num_beds_error').addClass('hide');
    }

    if(data.data.errors.publish_price != undefined)
    {
        $('.publish_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.pub_price_error').removeClass('hide');
    }
    else
    {
        $('.publish_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.pub_price_error').addClass('hide');
    }

    if(data.data.errors.member_price != undefined)
    {
        $('.member_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.member_price_error').removeClass('hide');
    }
    else
    {
        $('.member_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.member_price_error').addClass('hide');
    }
    
    if(data.data.errors.adult_price != undefined)
    {
        $('.adult_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.adult_price_error').removeClass('hide');
    }
    else
    {
        $('.adult_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.adult_price_error').addClass('hide');
    }

    if(data.data.errors.child_price != undefined)
    {
        $('.child_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.child_price_error').removeClass('hide');
    }
    else
    {
        $('.child_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.child_price_error').addClass('hide');
    }

    if(data.data.errors.adult_breakfast_price != undefined)
    {
        $('.adult_breakfast_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.adult_breakfast_error').removeClass('hide');
    }
    else
    {
        $('.adult_breakfast_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.adult_breakfast_error').addClass('hide');
    }

    if(data.data.errors.child_breakfast_price != undefined)
    {
        $('.child_breakfast_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.child_breakfast_error').removeClass('hide');
    }
    else
    {
        $('.child_breakfast_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.child_breakfast_error').addClass('hide');
    }

    if(data.data.errors.extra_bed_price != undefined)
    {
        $('.extra_bed_price').addClass("border-red-300 bg-red-50 color-red-800");
        $('.extra_bed_error').removeClass('hide');
    }
    else
    {
        $('.extra_bed_price').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.extra_bed_error').addClass('hide');
    }

    if(data.data.errors.extra_bed_number != undefined)
    {
        $('.extra_bed_number').addClass("border-red-300 bg-red-50 color-red-800");
        $('.extra_bednum_error').removeClass('hide');
    }
    else
    {
        $('.extra_bed_number').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.extra_bednum_error').addClass('hide');
    }

    if(data.data.errors.room_setup != undefined)
    {
        $('.room_setup').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room_setup_error').removeClass('hide');
    }
    else
    {
        $('.room_setup').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room_setup_error').addClass('hide');
    }

    if(data.data.errors.room_image != undefined)
    {
        $('.room_image').addClass("border-red-300 bg-red-50 color-red-800");
        $('.room_image_error').removeClass('hide');
    }
    else
    {
        $('.room_image').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.room_image_error').addClass('hide');
    }
}