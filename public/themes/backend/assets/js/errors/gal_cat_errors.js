function showInlineError(data)
{
    if(data.data.errors.gallery_name != undefined)
    {
        $('.gallery_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.gallery_name_error').removeClass('hide');
    }
    else
    {
        $('.gallery_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.gallery_name_error').addClass('hide');
    }
    
}