function showInlineError(data)
{
    if(data.data.errors.first_name != undefined)
    {
        $('.first_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.first-name-error').removeClass('hide');
    }
    else
    {
        $('.first_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.first-name-error').addClass('hide');
    }

    if(data.data.errors.email != undefined)
    {
        $('.email').addClass("border-red-300 bg-red-50 color-red-800");
        $('.email-error').removeClass('hide');
    }
    else
    {
        $('.email').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.email-error').addClass('hide');
    }

    if(data.data.errors.phone != undefined)
    {
        $('.phone').addClass("border-red-300 bg-red-50 color-red-800");
        $('.phone-error').removeClass('hide');
    }
    else
    {
        $('.phone').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.phone-error').addClass('hide');
    }

    if(data.data.errors.password != undefined)
    {
        $('.password').addClass("border-red-300 bg-red-50 color-red-800");
        $('.password-error').removeClass('hide');
    }
    else
    {
        $('.password').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.password-error').addClass('hide');
    }

    if(data.data.errors.password_confirm != undefined)
    {
        $('.password_confirm').addClass("border-red-300 bg-red-50 color-red-800");
        $('.password-confirm-error').removeClass('hide');
    }
    else
    {
        $('.password_confirm').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.password-confirm-error').addClass('hide');
    }

    if(data.data.errors.member_code != undefined)
    {
        $('.member_code').addClass("border-red-300 bg-red-50 color-red-800");
        $('.member-code-error').removeClass('hide');
    }
    else
    {
        $('.member_code').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.member-code-error').addClass('hide');
    }
}