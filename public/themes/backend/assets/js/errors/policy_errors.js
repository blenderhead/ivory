function showInlineError(data)
{
    if(data.data.errors.room_ids != undefined)
    {
        $('.room_ids_wrapper').addClass("border-red-300 bg-red-50 color-red-800");
    }
    else
    {
        $('.room_ids_wrapper').removeClass("border-red-300 bg-red-50 color-red-800");
    }

    if(data.data.errors.start_date != undefined)
    {
        $('.start_date').addClass("border-red-300 bg-red-50 color-red-800");
        $('.start_date_error').removeClass('hide');
    }
    else
    {
        $('.start_date').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.start_date_error').addClass('hide');
    }

    if(data.data.errors.start_date != undefined)
    {
        $('.end_date').addClass("border-red-300 bg-red-50 color-red-800");
        $('.end_date_error').removeClass('hide');
    }
    else
    {
        $('.end_date').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.end_date_error').addClass('hide');
    }
}