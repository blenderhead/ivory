function showInlineError(data)
{
    if(data.data.errors.start_date != undefined)
    {
        $('.start_date').addClass("border-red-300 bg-red-50 color-red-800");
        $('.start-date-error').removeClass('hide');
    }
    else
    {
        $('.start_date').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.start-date-error').addClass('hide');
    }

    if(data.data.errors.end_date != undefined)
    {
        $('.end_date').addClass("border-red-300 bg-red-50 color-red-800");
        $('.end-date-error').removeClass('hide');
    }
    else
    {
        $('.end_date').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.end-date-error').addClass('hide');
    }
}