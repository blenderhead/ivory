function showInlineError(data)
{
    if(data.data.errors.title != undefined)
    {
        $('#title').addClass("border-red-300 bg-red-50 color-red-800");
        $('.title_error').removeClass('hide');
    }
    else
    {
        $('#title').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.title_error').addClass('hide');
    }

    if(data.data.errors.url != undefined)
    {
        $('#url').addClass("border-red-300 bg-red-50 color-red-800");
        $('.urly_error').removeClass('hide');
    }
    else
    {
        $('#url').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.url_error').addClass('hide');
    }
}