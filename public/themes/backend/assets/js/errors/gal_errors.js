function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.gallery_name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.gallery_name_error').removeClass('hide');
    }
    else
    {
        $('.gallery_name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.gallery_name_error').addClass('hide');
    }

    if(data.data.errors.category != undefined)
    {
        $('.category').addClass("border-red-300 bg-red-50 color-red-800");
        $('.category_error').removeClass('hide');
    }
    else
    {
        $('.category').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.category_error').addClass('hide');
    }
    
    if(data.data.errors.publish != undefined)
    {
        $('.publish').addClass("border-red-300 bg-red-50 color-red-800");
        $('.publish_error').removeClass('hide');
    }
    else
    {
        $('.publish').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.publish_error').addClass('hide');
    }
}