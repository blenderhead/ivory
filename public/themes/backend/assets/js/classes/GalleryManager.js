var gallery = new Gallery();

$(document).ready(function() {

    var edit_form = $('#edit-gallery');
    var is_edit_page;
    var gallery_id;

    $("#selectall").removeProp("checked");
    $(".gal").removeProp("checked");

    var gal_id = [];

    /*
    function getAllExistingPhotos(id) {
        if(id) {
            var result = {};
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseUrl + '/backend/cms/gallery/get-all-photos',
                data: {id:id},
                async: false,
                success: function(data) {
                    result = data;
                }
            });
            return result;
        } else {
            return false;
        }
    }
    */

    /**
     * Dropzone files uploader
     */
	var myDropzone;
    Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        url:  baseUrl +  '/backend/cms/gallery/upload',
        previewsContainer: ".dropzone-previews",
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        dictDefaultMessage: 'Add files to upload by clicking or dropping them here.',
        addRemoveLinks: true,
        acceptedFiles: '.jpg,.pdf,.png,.bmp',
        dictInvalidFileType: 'This file type is not supported.',

        // The setting up of the dropzone
        init: function() {

            myDropzone = this;                                  
           
            // loads existing photos
            if(edit_form) {
                gallery_id = $('#gallery-id').val();
                var photos = getAllExistingPhotos(gallery_id);
                for(i in photos) {
                    var mockFile = { 
                        name: photos[i].file,
                        size: photos[i].size
                    };

                    var image_url = baseUrl + "/uploads/gallery/" + photos[i].file;
                    myDropzone.emit("addedfile", mockFile);
                    myDropzone.emit("thumbnail", mockFile, image_url);
                    myDropzone.emit("complete", mockFile); 
                    myDropzone.files.push(mockFile);
                }
            }
            this.on("successmultiple", function(files, response) {

            });

            this.on("complete", function(file) {

            });

            this.on("errormultiple", function(files, response) {

            });

            this.on("removedfile", function(file) {
                if(edit_form) {
                    var delete_photo_url = baseUrl + '/backend/cms/gallery/'+gallery_id+'/delete-photo/'+file.name;
                    $.get(delete_photo_url, function(data) {
                        bootbox.alert(data, function() {});
                    });
                }
            });
        }

    }
    
    ///
    $('#add-gallery, #edit-gallery').submit(function(e) {
        if(myDropzone !== undefined) {
            myDropzone.processQueue();
        } 
        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/cms/gallery/add';
                //var redirect_url = baseUrl + '/backend/cms/gallery';
                break;

            case 'edit':
                var url = baseUrl + '/backend/cms/gallery/edit';
                //var redirect_url = baseUrl + '/backend/cms/gallery/edit?id=' + $('#gallery-id').val();
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data)
                    showError(data);
                }
                else
                {
                    switch(op)
                    {
                        case 'add':
                            var redirect_url = baseUrl + '/backend/cms/gallery/manage_photo?id=' + data.data.gallery_id;
                            break;

                        case 'edit':
                            var redirect_url = baseUrl + '/backend/cms/gallery/edit?id=' + $('#gallery-id').val();
                            break;
                    }

                    showSuccess('Gallery is successfully saved', redirect_url);
                }
            }
        });
    });
    
    $('body').on('click', '#selectall', function() {

        $('.gal').prop('checked', this.checked);

        if(this.checked)
        {
            gal_id.splice(0,gal_id.length);

            $(".gal").each(function(index, value) {

                gal_id.push($(this).val());

            });

            console.log(gal_id);
        }
        else
        {
            gal_id.splice(0,gal_id.length);
            console.log(gal_id);
        }

    });
 
    $('body').on('click', '.gal', function() {
 
        if($(".gal").length == $(".gal:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            gal_id.push($(this).val());
            console.log(gal_id);
        }
        else
        {
            gal_id.splice( $.inArray($(this).val(), gal_id), 1 );
            console.log(gal_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/cms/gallery/delete';
        var redirect_url = baseUrl + '/backend/cms/gallery';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(gal_id.length > 0)
        {
            var id = gal_id;
            var url = baseUrl + '/backend/cms/gallery/delete';
            var redirect_url = baseUrl + '/backend/cms/gallery';
            deleteData(id, url, redirect_url);    
        }
        
    });

});