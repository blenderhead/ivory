var galleryCategories = new GalleryCategories();

$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".cat").removeProp("checked");

    var cat_id = [];
    
	$('#add-gallery-category, #edit-gallery-category').submit(function(e) {
        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/cms/gallery-categories/add';
                var redirect_url = baseUrl + '/backend/cms/gallery-categories';
                break;

            case 'edit':
                var url = baseUrl + '/backend/cms/gallery-categories/edit';
                var redirect_url = baseUrl + '/backend/cms/gallery-categories/edit?id=' + $('#gallery-category-id').val();
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Gallery Category is successfully saved', redirect_url);
                }

            }
        });
    });
    
    $('body').on('click', '#selectall', function(e) {

        $('.cat').prop('checked', this.checked);

        if(this.checked)
        {
            cat_id.splice(0,cat_id.length);

            $(".cat").each(function(index, value) {

                cat_id.push($(this).val());

            });

            console.log(cat_id);
        }
        else
        {
            cat_id.splice(0,cat_id.length);
            console.log(cat_id);
        }

    });
 
    $('body').on('click', '.cat', function(e) {
 
        if($(".cat").length == $(".cat:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            cat_id.push($(this).val());
            console.log(cat_id);
        }
        else
        {
            cat_id.splice( $.inArray($(this).val(), cat_id), 1 );
            console.log(cat_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/cms/gallery-categories/delete';
        var redirect_url = baseUrl + '/backend/cms/gallery-categories';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(cat_id.length > 0)
        {
            var id = cat_id;
            var url = baseUrl + '/backend/cms/gallery-categories/delete';
            var redirect_url = baseUrl + '/backend/cms/gallery-categories';
            deleteData(id, url, redirect_url);    
        }
        
    });
});