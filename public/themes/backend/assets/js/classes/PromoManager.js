var promo = new Promo();

$(document).ready(function() {

    /* Initial hidding field */
    $('#within_days').hide();
    $('#day_in_advance').hide();
    $('#custom_cancellations').hide();

    $("#selectall").removeProp("checked");
    $(".promo").removeProp("checked");

    var promo_id = [];

	$('#add-promo, #edit-promo').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
                var url = baseUrl + '/backend/promo/add';
                var redirect_url = baseUrl + '/backend/promo';
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/promo/edit';
        		var redirect_url = baseUrl + '/backend/promo/edit?id=' + $('#promo_id').val();
        		break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Promo is successfully saved', redirect_url);
                }

            }
        });
    });

    $('body').on('click', '#selectall', function(e) {

        $('.promo').prop('checked', this.checked);

        if(this.checked)
        {
            promo_id.splice(0,promo_id.length);

            $(".promo").each(function(index, value) {

                promo_id.push($(this).val());

            });

            console.log(promo_id);
        }
        else
        {
            promo_id.splice(0,promo_id.length);
            console.log(promo_id);
        }

    });
 
    $('body').on('click', '.promo', function(e) {
 
        if($(".promo").length == $(".promo:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            promo_id.push($(this).val());
            console.log(promo_id);
        }
        else
        {
            promo_id.splice( $.inArray($(this).val(), promo_id), 1 );
            console.log(promo_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/promo/delete';
        var redirect_url = baseUrl + '/backend/promo';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(promo_id.length > 0)
        {
            var id = promo_id;
            var url = baseUrl + '/backend/promo/delete';
            var redirect_url = baseUrl + '/backend/promo';
            deleteData(id, url, redirect_url);    
        }
        
    });
    
    /**
    * From Handle the function input EB/LM/STD
    */
    $('.promo_type').change(function(){

        switch ($(this).val()) {
            case '0' :
                $('#within_days').hide();
                $('#day_in_advance').hide();
            break;
            case '1' :
                $('#within_days').show();
                $('#day_in_advance').hide();
            break;
            case '2' :
                $('#within_days').hide();
                $('#day_in_advance').show();
            break;
            default :
                $('#within_days').hide();
                $('#day_in_advance').hide();
            break;                                  
        }
    });

    /**
    * For Handle the check box all function
    */
    $('#checkboxall').change(function(){
        if($(this).prop('checked')) {
            $('.room_ids').prop('checked', true);
        } else {
            $('.room_ids').prop('checked', false);
        };
    });

    /**
    * From handle the costum cancellation or not
    */
    $('.costum_policy').change(function(){
        if ($(this).val()==1) {
            $('#custom_cancellations').show();
        } else {
            $('#custom_cancellations').hide();
        }
    });

    /**
    * Set validation for generate name promtions
    */
    $('.bv-form').bootstrapValidator({
        fields: {
            min_stay_duration: {
                validators: {
                    notEmpty: {
                        message: "Stay at last is required and can't be empty to generate name promotion"
                    },
                    integer: {
                        message: "Stay at last must numeric value"
                    }
                }
            },
            min_book_room: {
                validators: {
                    notEmpty: {
                        message: "Book at Last is required and can't be empty to generate name promotion"
                    },
                    integer: {
                        message: "Book at last must numeric value"
                    }                        
                }
            },
            value_discount: {
                validators: {
                    notEmpty: {
                        message: "Value discount is required and can't be empty "
                    },
                    integer: {
                        message: "Value discount must numeric value"
                    }                        
                }
            }                
        }
    });

    /**
    * function generate name promotion 
    */      
    $('#generate_name_promotion').click(function(){

        /** 
        * Form must run validation for get data that needed generate promotoin name
        * all data must filled before generate promotion name
        * 
        */
        var form = $('.bv-form').data('bootstrapValidator');
        form.validate();
        if (form.isValid()) {
            
            $stay_at_least = $('#min_stay_duration').val();
            $book_at_least = $('#min_book_room').val();
            $benifit = $('#value_discount').val();
            $type_value_amount = $('#benifit_value').val();
            $type_value_amount = $('#benifit_value').val();
            $word_desc = '';
            $word_benifit = '';

            $word_desc = 'Stay at least '+$stay_at_least+' days and book more than '+$book_at_least+' rooms. ';

            switch ($type_value_amount) {
                case '0' :
                    $word_benifit = 'Get '+$benifit+'% Discount (Per Night)';
                break;
                case '1' :
                    $word_benifit = 'Get '+$benifit+'IDR Discount (Per Night)';
                break;
                case '2' :
                    $word_benifit = 'Get '+$benifit+' Free Nights';
                break;                
            }

            $('#promotion_name').val($word_desc+$word_benifit);
            form.disableSubmitButtons(true);
        
        } else {

            $('#promotion_name').val('');
            form.disableSubmitButtons(false);
        
        }
    });

    $('.save').click(function(){
        
        var form = $('.bv-form').data('bootstrapValidator');
        form.validate();
        
        if (form.isValid()) {
            form.defaultSubmit();
        }
        
    });

});