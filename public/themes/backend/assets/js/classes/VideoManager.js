$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".video").removeProp("checked");

    var video_id = [];

    $('#add-video, #edit-video').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/cms/gallery/video/add';
                break;

            case 'edit':
                var url = baseUrl + '/backend/cms/gallery/video/edit';
                break;
        }
        
        var redirect_url = baseUrl + '/backend/cms/gallery/video';

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data)
                    showError(data);
                }
                else
                {
                    showSuccess('Video is successfully saved', redirect_url);
                }
            }
        });
    });
    
    $('body').on('click', '#selectall', function() {

        $('.video').prop('checked', this.checked);

        if(this.checked)
        {
            video_id.splice(0,video_id.length);

            $(".video").each(function(index, value) {

                video_id.push($(this).val());

            });

            console.log(video_id);
        }
        else
        {
            video_id.splice(0,video_id.length);
            console.log(video_id);
        }

    });
 
    $('body').on('click', '.video', function() {
 
        if($(".video").length == $(".gal:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            video_id.push($(this).val());
            console.log(video_id);
        }
        else
        {
            video_id.splice( $.inArray($(this).val(), video_id), 1 );
            console.log(video_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/cms/gallery/video/delete';
        var redirect_url = baseUrl + '/backend/cms/gallery/video';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(video_id.length > 0)
        {
            var id = video_id;
            var url = baseUrl + '/backend/cms/gallery/video/delete';
            var redirect_url = baseUrl + '/backend/cms/gallery/video';
            deleteData(id, url, redirect_url);    
        }
        
    });

});