var policy = new Policy();

$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".policy").removeProp("checked");

    var policy_id = [];

	$('#add-policy, #edit-policy').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/backend/policy/add';
        		var redirect_url = baseUrl + '/backend/policy';
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/policy/edit';
        		var redirect_url = baseUrl + '/backend/policy/edit?id=' + $('.policy_id').val();
        		break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Policy is successfully saved', redirect_url);
                }

            }
        });
    });

    $('#checkboxall').change(function(){
        if($(this).prop('checked')) {
            $('.room_ids').prop('checked', true);
        } else {
            $('.room_ids').prop('checked', false);
        };
    });

    $('body').on('click', '#selectall', function(e) {

        $('.policy').prop('checked', this.checked);

        if(this.checked)
        {
            policy_id.splice(0,policy_id.length);

            $(".policy").each(function(index, value) {

                policy_id.push($(this).val());

            });

            console.log(policy_id);
        }
        else
        {
            policy_id.splice(0,policy_id.length);
            console.log(policy_id);
        }

    });
 
    $('body').on('click', '.policy', function(e) {
 
        if($(".policy").length == $(".policy:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            policy_id.push($(this).val());
            console.log(policy_id);
        }
        else
        {
            policy_id.splice( $.inArray($(this).val(), policy_id), 1 );
            console.log(policy_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/policy/delete';
        var redirect_url = baseUrl + '/backend/policy';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(policy_id.length > 0)
        {
            var id = policy_id;
            var url = baseUrl + '/backend/policy/delete';
            var redirect_url = baseUrl + '/backend/policy';
            deleteData(id, url, redirect_url);    
        }
        
    });

});