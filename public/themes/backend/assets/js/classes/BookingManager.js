$(document).ready(function() {

	$("#selectall").removeProp("checked");
    $(".booking").removeProp("checked");

    var booking_id = [];

    $('body').on('click', '#selectall', function(e) {

        $('.booking').prop('checked', this.checked);

        if(this.checked)
        {
            booking_id.splice(0,booking_id.length);

            $(".booking").each(function(index, value) {

                booking_id.push($(this).val());

            });

            console.log(booking_id);
        }
        else
        {
            booking_id.splice(0,booking_id.length);
            console.log(booking_id);
        }

    });
 
    $('body').on('click', '.booking', function(e) {
 
        if($(".booking").length == $(".booking:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            booking_id.push($(this).val());
            console.log(booking_id);
        }
        else
        {
            booking_id.splice( $.inArray($(this).val(), booking_id), 1 );
            console.log(booking_id);
        }
 
    });

    $('body').on('click', '.read-all', function(e) {
        e.preventDefault();

        if(booking_id.length > 0)
        {
            var id = booking_id;
            var url = baseUrl + '/backend/booking/mark_as_read';
            var redirect_url = baseUrl + '/backend/booking';
            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: url,
                data: {
                    id: id
                },
                beforeSend: function() {
                    $('.panel').waitMe({
                        effect : 'stretch',
                        text : 'Saving...',
                        bg : 'rgba(255,255,255,0.7)',
                        color : '#000',
                        sizeW : '',
                        sizeH : ''
                    });
                },
                complete: function(){
                    $('.panel').waitMe('hide');
                },
                success: function(data) {

                    if(data.error)
                    {
                        showInlineError(data);
                        showError(data);
                    }
                    else
                    {
                        showSuccess('Data is successfully saved', redirect_url);
                    }

                }
            });
        }
        
    }); 

});