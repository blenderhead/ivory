$(document).ready(function() {

	$('#edit-user').submit(function(e) {

        e.preventDefault();
        
        var form_data = new FormData(this);
        
        var url = baseUrl + '/backend/profile/edit';
        var redirect_url = baseUrl + '/backend/profile/edit?id=' + $('.user_id').val();
        var rw = $('#preview-normal').width();
        
        form_data.append('rw', rw);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Profile is successfully saved', redirect_url);
                }

            }
        });
    });

	$("#avatar").change(function(){
        
        $(".logo-container img").css('height', ''); 

        var op = $(this).data('op');	

        switch(op)
        {
            case 'add':
                var logo_preview = $('#logo-preview');
                break;

            case 'edit':
                var logo_preview = $('#preview-normal');
                break;
        }

        var JcropAPI = logo_preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }
        
        file = readURL(this, logo_preview, 'avatar', 80, 80, 0, 0, true);
          
    });

});