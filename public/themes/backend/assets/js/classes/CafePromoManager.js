$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".promo").removeProp("checked");

    var promo_id = [];

	$('#add-promo, #edit-promo').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
        	case 'add':
        		var url = baseUrl + '/backend/cafe/promo/add';
        		var redirect_url = baseUrl + '/backend/cafe/promo';
                var rw = $('#logo-preview').width();
        		break;

        	case 'edit':
        		var url = baseUrl + '/backend/cafe/promo/edit';
        		var redirect_url = baseUrl + '/backend/cafe/promo/edit?id=' + $('.promo_id').val();
                var rw = $('#preview-normal').width();
        		break;
        }
        
        form_data.append('rw', rw);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Promo is successfully saved', redirect_url);
                }

            }
        });
    });
    
    $("#promo_image").change(function(){
        
        var op = $(this).data('op');

        switch(op)
        {
            case 'add':
                var logo_preview = $('#logo-preview');
                break;

            case 'edit':
                var logo_preview = $('#preview-normal');
                break;
        }

        logo_preview.removeAttr('style');
        
        var JcropAPI = logo_preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }
        
        file = readURL(this, logo_preview, 'avatar', 0, 0, 0, 0, false);
          
    });

    $('body').on('click', '#selectall', function(e) {

        $('.promo').prop('checked', this.checked);

        if(this.checked)
        {
            promo_id.splice(0,promo_id.length);

            $(".promo").each(function(index, value) {

                promo_id.push($(this).val());

            });

            console.log(promo_id);
        }
        else
        {
            promo_id.splice(0,promo_id.length);
            console.log(promo_id);
        }

    });
 
    $('body').on('click', '.room', function(e) {
 
        if($(".promo").length == $(".promo:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            promo_id.push($(this).val());
            console.log(promo_id);
        }
        else
        {
            promo_id.splice( $.inArray($(this).val(), promo_id), 1 );
            console.log(promo_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/cafe/promo/delete';
        var redirect_url = baseUrl + '/backend/cafe/promo';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(promo_id.length > 0)
        {
            var id = promo_id;
            var url = baseUrl + '/backend/cafe/promo/delete';
            var redirect_url = baseUrl + '/backend/cafe/promo';
            deleteData(id, url, redirect_url);    
        }
        
    }); 
});