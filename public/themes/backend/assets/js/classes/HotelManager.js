var hotel = new Hotel();

$(document).ready(function() {

	$('#edit-hotel').submit(function(e) {

        e.preventDefault();
        
        var form_data = new FormData(this);
        
        var url = baseUrl + '/backend/hotel/edit';
        var redirect_url = url;
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Hotel setting is successfully saved', redirect_url);
                }

            }
        });
    });
    
    $('#add_facility').click(function(){
        
        var length = $('.facilities').length+1;

        var input = '<div class="col-lg-4  margin-bottom-5 facilities" id="fac-'+length+'"><div class="input-group">'+
        '<input name="facilities['+length+']" type="text" class="form-control no-border-right" placeholder="facility">'+
        '<span class="input-group-btn"><button data-fac-id='+length+' class="btn btn-warning delete-fac"'+ 
        'type="button"><i class="fa fa-times"></i></button></span></div></div>'; 

        $('#list_facilities').append(input);
    });

    $("#list_facilities").on("click", ".delete-fac", function(){

        var fac_id = $(this).data('fac-id');
        $('#fac-'+fac_id).remove();        

    });      

    $('#add_service').click(function(){
        
        var length = $('.services').length+1;

        var input = '<div class="col-lg-4  margin-bottom-5 services" id="service-'+length+'"><div class="input-group">'+
        '<input name="services['+length+']" type="text" class="form-control no-border-right" placeholder="service">'+
        '<span class="input-group-btn"><button data-service-id='+length+' class="btn btn-warning delete-service"'+ 
        'type="button"><i class="fa fa-times"></i></button></span></div></div>'; 

        $('#list_services').append(input);
    });

    $("#list_services").on("click", ".delete-service", function(){

        var service_id = $(this).data('service-id');
        $('#service-'+service_id).remove();        

    });      

});