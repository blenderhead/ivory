var stock = new Stock();

$(document).ready(function() {

	$('#save-stock').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
		var url = baseUrl + '/backend/stock';
		var redirect_url = baseUrl + '/backend/stock'+location.search;
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {
            	console.log(data);
                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    showSuccess('Stock is successfully saved', redirect_url);
                }

            }
        });
    });	

	$('#triger_mass_stock').click(function(){

		var stock = $('#mass_stock').val();
		var filteredDay = getFilteredDay();

		$('.value_mass_stock').each(function(index){
			var datePerRecord = $('.date').get(index).innerHTML;
			var day = new Date(datePerRecord).getDay();
			
			if (filteredDay.length > 0) {
				if (!(jQuery.inArray(day, filteredDay) == -1)) {
					$(this).val(stock);
				};
			} else {
				$(this).val(stock);
			};

		});
	});

	$('#triger_mass_price').click(function(){
		
		var price = $('#mass_price').val();
		var filteredDay = getFilteredDay();

		$('.value_mass_price').each(function(index){
			var datePerRecord = $('.date').get(index).innerHTML;
			var day = new Date(datePerRecord).getDay();
			
			if (filteredDay.length > 0) {
				if (!(jQuery.inArray(day, filteredDay) == -1)) {
					$(this).val(price);
				};
			} else {
				$(this).val(price);
			};

		});
	});

	$('#triger_mass_member_price').click(function(){
		
		var price = $('#mass_member_price').val();
		var filteredDay = getFilteredDay();

		$('.value_mass_member_price').each(function(index){
			var datePerRecord = $('.date').get(index).innerHTML;
			var day = new Date(datePerRecord).getDay();
			
			if (filteredDay.length > 0) {
				if (!(jQuery.inArray(day, filteredDay) == -1)) {
					$(this).val(price);
				};
			} else {
				$(this).val(price);
			};

		});
	});	

	$('#triger_mass_stopsell').click(function(){

		var isStopSell = $('#mass_stopsell').is(':checked');
		var filteredDay = getFilteredDay();

		$('.value_mass_stopsell').each(function(index){
			var datePerRecord = $('.date').get(index).innerHTML;
			var day = new Date(datePerRecord).getDay();
			
			if (filteredDay.length > 0) {
				if (!(jQuery.inArray(day, filteredDay) == -1)) {
					$(this).prop('checked', isStopSell);
				};
			} else {
				$(this).prop('checked', isStopSell);
			};

		});		
	});

	$('#triger_mass_stoppromo').click(function(){
		var isStopPromo = $('#mass_stoppromo').is(':checked');
		var filteredDay = getFilteredDay();

		$('.value_mass_stoppromo').each(function(index){
			var datePerRecord = $('.date').get(index).innerHTML;
			var day = new Date(datePerRecord).getDay();
			
			if (filteredDay.length > 0) {
				if (!(jQuery.inArray(day, filteredDay) == -1)) {
					$(this).prop('checked', isStopPromo);
				};
			} else {
				$(this).prop('checked', isStopPromo);
			};

		})
	});			
});

function getFilteredDay() {
	var selectionDay = [];
	var values = $('.selectpicker').val();

	if (values != null) {
		for(var i = 0; i < values.length; i += 1) {
			selectionDay.push(parseInt(values[i]));
		}
	};
	return selectionDay
}