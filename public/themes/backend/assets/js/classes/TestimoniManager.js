var testimoni = new Testimoni();

$(document).ready(function() {
    var edit_form = $('#edit-testimoni');
    var is_edit_page;
    var testimoni_id;

    $("#selectall").removeProp("checked");
    $(".testi").removeProp("checked");

    var testi_id = [];

    /*
    function getAllExistingPhotos(id) {
        if(id) {
            var result = {};
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseUrl + '/backend/cms/testimoni/get-all-photos',
                data: {id:id},
                async: false,
                success: function(data) {
                    result = data;
                }
            });
            return result;
        } else {
            return false;
        }
    }
    */

    /**
     * Dropzone files uploader
     */
	var myDropzone;
    Dropzone.options.myAwesomeDropzone = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        url:  baseUrl +  '/backend/cms/testimoni/upload',
        previewsContainer: ".dropzone-previews",
        uploadMultiple: false,
        parallelUploads: 100,
        maxFiles: 1,
        dictDefaultMessage: 'Add image to upload by clicking or dropping them here.',
        addRemoveLinks: true,
        acceptedFiles: '.jpg,.pdf,.png,.bmp',
        dictInvalidFileType: 'This file type is not supported.',

        // The setting up of the dropzone
        init: function() {

            myDropzone = this;                                  
           
            // loads existing photos
            if(edit_form) {
                testimoni_id = $('#testimoni-id').val();
                var photos = getAllExistingPhotos(testimoni_id);
                for(i in photos) {
                    var mockFile = { 
                        name: photos[i].file,
                        size: photos[i].size
                    };

                    var image_url = baseUrl + "/" +photos[i].destination + "/" + photos[i].file;
                    myDropzone.emit("addedfile", mockFile);
                    myDropzone.emit("thumbnail", mockFile, image_url);
                    myDropzone.emit("complete", mockFile); 
                    myDropzone.files.push(mockFile);
                }
            }
            this.on("maxfilesexceeded", function(file) {
                var mesg = "Only one image can accept to save in database. Are you sure?";

                alert(mesg);

                this.removeFile(file);
            });

            this.on("complete", function(file) {
            });

            this.on("errormultiple", function(files, response) {

            });

            this.on("removedfile", function(file) {
                if(edit_form) {
                    var delete_photo_url = baseUrl + '/backend/cms/testimoni/'+testimoni_id+'/delete-photo/'+file.name;
                    if(testimoni_id != undefined) {
                        $.get(delete_photo_url, function(data) {
                            bootbox.alert(data, function() {});
                        });
                    }
                }
            });
        }

    }

    ///
    $('#add-testimoni, #edit-testimoni').submit(function(e) {
        if(myDropzone !== undefined) {
            myDropzone.processQueue();
        } 
        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/cms/review/add';
                var redirect_url = baseUrl + '/backend/cms/review';
                break;

            case 'edit':
                var url = baseUrl + '/backend/cms/review/edit';
                //var redirect_url = baseUrl + '/backend/cms/testimoni/edit?id=' + $('#testimoni-id').val();
                var redirect_url = baseUrl + '/backend/cms/review';
                break;
        }
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {
                if(data.error)
                {
                    showInlineError(data)
                    showError(data);
                }
                else
                {
                    showSuccess('Review is successfully saved', redirect_url);
                }
            }
        });
    });
    
    $('body').on('click', '#selectall', function(e) {

        $('.testi').prop('checked', this.checked);

        if(this.checked)
        {
            testi_id.splice(0,testi_id.length);

            $(".testi").each(function(index, value) {

                testi_id.push($(this).val());

            });

            console.log(testi_id);
        }
        else
        {
            testi_id.splice(0,testi_id.length);
            console.log(testi_id);
        }

    });
 
    $('body').on('click', '.testi', function(e) {
 
        if($(".testi").length == $(".testi:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            testi_id.push($(this).val());
            console.log(testi_id);
        }
        else
        {
            testi_id.splice( $.inArray($(this).val(), testi_id), 1 );
            console.log(testi_id);
        }
 
    });

    $('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

        var url = baseUrl + '/backend/cms/review/delete';
        var redirect_url = baseUrl + '/backend/cms/review';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(testi_id.length > 0)
        {
            var id = testi_id;
            var url = baseUrl + '/backend/cms/review/delete';
            var redirect_url = baseUrl + '/backend/cms/review';
            deleteData(id, url, redirect_url);    
        }
        
    });
});