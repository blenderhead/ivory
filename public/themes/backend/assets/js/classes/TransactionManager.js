$(document).ready(function() {
    
    $("#selectall").removeProp("checked");
    $(".transaction").removeProp("checked");

    var transaction_id = [];

    $('body').on('change', '.update_status', function(e) {
 
        var id = $(this).data('id');
        var status = $(this).val();

        if(status)
        {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseUrl + '/backend/transaction/update_status',
                data: {
                    id: id,
                    status: status
                },
                beforeSend: function() {
                    $('.panel').waitMe({
                        effect : 'stretch',
                        text : 'Saving...',
                        bg : 'rgba(255,255,255,0.7)',
                        color : '#000',
                        sizeW : '',
                        sizeH : ''
                    });
                },
                complete: function(){
                    $('.panel').waitMe('hide');
                },
                success: function(data) {

                    if(data.error)
                    {
                        showError(data);
                    }
                    else
                    {
                        bootbox.alert('Status is successfully saved', function() {
                            window.location.reload();
                        });
                    }

                }
            });
        }
    
    });

    $('body').on('click', '#selectall', function(e) {

        $('.transaction').prop('checked', this.checked);

        if(this.checked)
        {
            transaction_id.splice(0,transaction_id.length);

            $(".transaction").each(function(index, value) {

                transaction_id.push($(this).val());

            });

            console.log(transaction_id);
        }
        else
        {
            transaction_id.splice(0,transaction_id.length);
            console.log(transaction_id);
        }

    });
 
    $('body').on('click', '.transaction', function(e) {
 
        if($(".transaction").length == $(".transaction:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            transaction_id.push($(this).val());
            console.log(transaction_id);
        }
        else
        {
            transaction_id.splice( $.inArray($(this).val(), transaction_id), 1 );
            console.log(transaction_id);
        }
 
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(transaction_id.length > 0)
        {
            var id = transaction_id;
            var url = baseUrl + '/backend/transaction/delete';
            var redirect_url = baseUrl + '/backend/transaction';
            deleteData(id, url, redirect_url);    
        }
        
    }); 

});