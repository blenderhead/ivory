var user = new User();

$(document).ready(function() {

    $("#selectall").removeProp("checked");
    $(".user").removeProp("checked");

    var user_id = [];

	$('#add-user, #edit-user').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/user/add';
                var redirect_url = url;
                var rw = $('#logo-preview').width();
                break;

            case 'edit':
                var url = baseUrl + '/backend/user/edit';
                var redirect_url = baseUrl + '/backend/user/edit?id=' + $('.user_id').val();
                var rw = $('#preview-normal').width();
                break;
        }
        
        form_data.append('rw', rw);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('User is successfully saved', redirect_url);
                }

            }
        });
    });

	$("#avatar").change(function(){
        
        $(".logo-container img").css('height', ''); 

        var op = $(this).data('op');	

        switch(op)
        {
            case 'add':
                var logo_preview = $('#logo-preview');
                break;

            case 'edit':
                var logo_preview = $('#preview-normal');
                break;
        }

        var JcropAPI = logo_preview.data('Jcrop');

        if(JcropAPI != undefined)
        {
            JcropAPI.destroy();
        }
        
        file = readURL(this, logo_preview, 'avatar', 80, 80, 0, 0, true);
          
    });

    $('body').on('click', '#selectall', function(e) {

        $('.user').prop('checked', this.checked);

        if(this.checked)
        {
            user_id.splice(0,user_id.length);

            $(".user").each(function(index, value) {

                user_id.push($(this).val());

            });

            console.log(user_id);
        }
        else
        {
            user_id.splice(0,user_id.length);
            console.log(user_id);
        }

    });
 
    $('body').on('click', '.user', function(e) {
 
        if($(".user").length == $(".user:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            user_id.push($(this).val());
            console.log(user_id);
        }
        else
        {
            user_id.splice( $.inArray($(this).val(), user_id), 1 );
            console.log(user_id);
        }
 
    });

	$('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

       	var url = baseUrl + '/backend/user/delete';
        var redirect_url = baseUrl + '/backend/user';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(user_id.length > 0)
        {
            var id = user_id;
            var url = baseUrl + '/backend/user/delete';
            var redirect_url = baseUrl + '/backend/user';
            deleteData(id, url, redirect_url);    
        }
        
    });

    $('#generate_password').click(function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/backend/user/gen_passwd',
            success: function(data) {

                if(!data.error)
                {
                    $('#password').val(data.data.password);
                    $('#password_confirm').val(data.data.password);
                }

            }
        });
    });

    $('#generate_code').click(function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/backend/user/gen_code',
            success: function(data) {

                if(!data.error)
                {
                    $('#member_code').val(data.data.code);
                }

            }
        });
    });
});