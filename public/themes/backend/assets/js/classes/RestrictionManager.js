var restriction = new Restriction();

$(document).ready(function() {

	$("#selectall").removeProp("checked");
    $(".res").removeProp("checked");

    var res_id = [];

	$('#add-res, #edit-res').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var form_data = new FormData(this);
        
        switch(op)
        {
            case 'add':
                var url = baseUrl + '/backend/restrictions/add';
                var redirect_url = url;
                break;

            case 'edit':
                var url = baseUrl + '/backend/restrictions/edit';
                var redirect_url = baseUrl + '/backend/restrictions/edit?id=' + $('.res_id').val();
                break;
        }
        
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.panel').waitMe({
                    effect : 'stretch',
                    text : 'Saving...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.panel').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                	showInlineError(data);
                    showError(data);
                }
                else
                {
                    showSuccess('Restriction is successfully saved', redirect_url);
                }

            }
        });
    });
	
	$('body').on('click', '#selectall', function(e) {

        $('.res').prop('checked', this.checked);

        if(this.checked)
        {
            res_id.splice(0,res_id.length);

            $(".res").each(function(index, value) {

                res_id.push($(this).val());

            });

            console.log(res_id);
        }
        else
        {
            res_id.splice(0,res_id.length);
            console.log(res_id);
        }

    });
 
    $('body').on('click', '.res', function(e) {
 
        if($(".res").length == $(".res:checked").length) 
        {
            $("#selectall").prop("checked", "checked");
        } 
        else 
        {
            $("#selectall").removeProp("checked");
        }

        if(this.checked)
        {
            res_id.push($(this).val());
            console.log(res_id);
        }
        else
        {
            res_id.splice( $.inArray($(this).val(), res_id), 1 );
            console.log(res_id);
        }
 
    });

	$('body').on('click', '.delete', function() {
        var id = [$(this).data('id')];

       	var url = baseUrl + '/backend/restrictions/delete';
        var redirect_url = baseUrl + '/backend/restrictions';
        
        deleteData(id, url, redirect_url);
    });

    $('body').on('click', '.delete-all', function(e) {
        e.preventDefault();

        if(res_id.length > 0)
        {
            var id = res_id;
            var url = baseUrl + '/backend/restrictions/delete';
            var redirect_url = baseUrl + '/backend/restrictions';
            deleteData(id, url, redirect_url);    
        }
        
    });
});