var confirm = '';
function showError(data)
{
	var msg = '';

	if (data.error == '9107') 
	{
	  msg = '<div><strong>' + data.message + '</strong></div>';

	  for (var x in data.data.errors) {
	    msg += '<div><strong><em>- ' + data.data.errors[x][0] + '</em></strong></div>';
	  }

	  bootbox.alert(msg, function() {});
	}
	else if (data.error == '1000') 
	{
	  msg = '<div><p><strong>' + data.message + '</strong></p></div>';
	  msg += '<div><strong><em>' + data.data.errors + '</em></strong></div>';

	  bootbox.alert(msg, function() {});
	}
	else 
	{
	  bootbox.alert(data.message, function() {});
	}
}

function showSuccess(msg,url)
{
    bootbox.alert(msg, function() {
        window.location.replace(url);
    });
}

function showConfirm(msg, get_confirm)
{
    bootbox.confirm(msg, function(result) {
        get_confirm(result);
        /*if(result)
        {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: { 
                    'id' : id,
                },
                url: url,
                beforeSend: function() {
                    $('.panel').waitMe({
                        effect : 'stretch',
                        text : 'Updating...',
                        bg : 'rgba(255,255,255,0.7)',
                        color : '#000',
                        sizeW : '',
                        sizeH : ''
                    });
                },
                success: function(data) {
                    if(data.error)
                    {
                        showError(data);
                    }
                    else
                    {
                        showSuccess('Image is successfully updated', url);
                    }
                },
                complete: function() {
                    $('.panel').waitMe('hide');
                } 
            });
           
        }*/
    });
}

function showOpTooltip()
{
    $(".edit").tooltip({
        title : 'Edit'
    });

    $(".delete").tooltip({
        title : 'Delete'
    });
}

function showEditPage(url)
{
    window.location.replace(url);
}

function readURL(input,el,type, x, y, width, height, allow_resize)
{
    if (input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e) {

            el.attr('src', e.target.result);

            allow_resize = allow_resize || false;

            el.Jcrop({
                setSelect:   [ x, x, width, height ],
                //aspectRatio: 153 / 175,
                onSelect: function(coords) {
                    updateCoords(coords, type);
                },
                onChange: function(coords) {
                    updateCoords(coords, type);
                },
                minSize: [width,height],
                maxSize: [width,height],
                boxWidth: 500,
                allowResize: allow_resize
            });
        }

        reader.readAsDataURL(input.files[0]);
        file = input.files[0];
        return file;
    }
}

function updateCoords(c,type)
{
    switch(type)
    {
        case 'avatar':
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
            break;

        case 'logo':
            $('#x_logo').val(c.x);
            $('#y_logo').val(c.y);
            $('#w_logo').val(c.w);
            $('#h_logo').val(c.h);
            break;
    }
    
};

function resetFormElement(e) 
{
      e.wrap('<form>').closest('form').get(0).reset();
      e.unwrap();
}

function deleteData(id, url, redirect_url)
{
    bootbox.confirm("Are you sure?", function(result) {
        
        if(result)
        {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: { 
                    'id' : id,
                },
                url: url,
                beforeSend: function() {
                    $('.panel').waitMe({
                        effect : 'stretch',
                        text : 'Deleting...',
                        bg : 'rgba(255,255,255,0.7)',
                        color : '#000',
                        sizeW : '',
                        sizeH : ''
                    });
                },
                success: function(data) {
                    if(data.error)
                    {
                        showError(data);
                    }
                    else
                    {
                        showSuccess('Data is successfully deleted', redirect_url);
                    }
                },
                complete: function() {
                    $('.panel').waitMe('hide');
                } 
            });
        }
    }); 
}

function cancelOp(redirect_url)
{
    window.location.replace(redirect_url);
}