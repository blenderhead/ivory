@extends('layouts.backend')

@section('title')
	Price & Stocks - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Items
@stop

@section('page_description')
	Manage room stock and price. Price is Tax and Service Charge inclusive and should be entered using IDR.		
@stop

@section('content')
<div class="row">
	<div class="col-lg-12">
		@if($errors->any())
		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Oooppss!</strong>{{ implode('', $errors->all('<li>:message</li>')) }}
		</div>
		@endif
		<div class="panel">
				<div class="panel-title bg-white no-border">
					<div class="panel-head">Stock For {{ $room->name }}</div>
				</div>
			<form action="" method="POST" id="save-stock" data-op="save">
				<input name="room_id" type="hidden" value="{{ $room->id }}">
				<div class="panel-body no-padding-top bg-white">
				
					<table class="table table-striped">
						<thead>
							<tr>
								<th colspan="2" class="vertical-middle"></th>
								<th class="vertical-middle"></th>
								<th class="vertical-middle">
									<center>
										<input name="mass_stock" class="form-control" id="mass_stock" size="2">
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<input name="mass_price" class="form-control" id="mass_price" size="10">
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<input name="mass_member_price" class="form-control" id="mass_member_price" size="10">
									</center>
								</th>								
								<th class="vertical-middle"></th>
								<th class="vertical-middle">
									<center>
										<div class="checkbox checkbox-theme no-margin">
											<input value="1" name="mass_stopsell" type="checkbox" id="mass_stopsell">
											<label for="mass_stopsell" class="no-padding">
											</label>
										</div>
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<div class="checkbox checkbox-theme no-margin">
											<input value="1" name="mass_stoppromo" type="checkbox" id="mass_stoppromo">
											<label for="mass_stoppromo" class="no-padding">
											</label>
										</div>
									</center>
								</th>
							</tr>
							<tr>
								<th colspan="2" class="vertical-middle"><button type="button" class="btn btn-primary btn-icon-left margin-right-5"data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Search</button></th>
								<th class="vertical-middle">
									<select name="filter_days" class="selectpicker" multiple data-width="100px" title="All Days">
										<option value="0" >Sunday</option>
										<option value="1" >Monday</option>
										<option value="2" >Tuesday</option>
										<option value="3" >Wednesday</option>
										<option value="4" >Thursday</option>
										<option value="5" >Friday</option>
										<option value="6" >Saturday</option>
									</select>
								</th>
								<th class="vertical-middle">
									<center>
										<button type="button" class="btn btn-primary btn-circle" id="triger_mass_stock"><i class="glyphicon glyphicon-link"></i></button>
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<button type="button" class="btn btn-primary btn-circle" id="triger_mass_price"><i class="glyphicon glyphicon-link"></i></button>
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<button type="button" class="btn btn-primary btn-circle" id="triger_mass_member_price"><i class="glyphicon glyphicon-link"></i></button>
									</center>
								</th>								
								<th class="vertical-middle"></th>
								<th class="vertical-middle">
									<center>
										<button type="button" class="btn btn-primary btn-circle" id="triger_mass_stopsell"><i class="glyphicon glyphicon-link"></i></button>
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										<button type="button" class="btn btn-primary btn-circle" id="triger_mass_stoppromo"><i class="glyphicon glyphicon-link"></i></button>
									</center>
								</th>
							</tr>
							<tr>
								<th class="vertical-middle">Room Name</th>
								<th class="vertical-middle">Day</th>
								<th class="vertical-middle">Date</th>
								<th class="vertical-middle">
									<center>
										Stock
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										Price
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										Member Price
									</center>
								</th>								
								<th class="vertical-middle">
									<center>
										Sold
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										Stop Sell
									</center>
								</th>
								<th class="vertical-middle">
									<center>
										Stop Promotion
									</center>
								</th>
							</tr>												
						</thead>
						<tbody>
						@foreach($stockDatas as $stockData)
						<tr> 
						<td class="vertical-middle">{{ $room->name }}</td>
						<td class="vertical-middle">{{ date('l', strtotime($stockData->date)) }}</td>
						<td class="vertical-middle date">{{ $stockData->date }}</td>
						<td class="vertical-middle">
								<center>
									<input name="rooms[{{ $stockData->date }}][stock]" class="form-control value_mass_stock" value="{{ $stockData->stock }}" size="2">
								</center>
							</td>
							<td class="vertical-middle">
								<center>
									<input name="rooms[{{ $stockData->date }}][price]" class="form-control value_mass_price" value="{{ $stockData->price }}" size="10">
								</center>
							</td>
							<td class="vertical-middle">
								<center>
									<input name="rooms[{{ $stockData->date }}][member_price]" class="form-control value_mass_member_price" value="{{ $stockData->member_price }}" size="10">
								</center>
							</td>							
							<td class="vertical-middle">
								<center>
									{{ $stockData->sold }}
								</center>
							</td>
							<td class="vertical-middle">
								<center>
									<div class="checkbox checkbox-theme no-margin">
										<input value="1" name="rooms[{{ $stockData->date }}][stopsell]" class="value_mass_stopsell" {{ $stockData->stop_sell==1 ? 'checked' : '' }} type="checkbox">
										<label for="value_mass_stopsell" class="no-padding">

										</label>
									</div>
								</center>						
							</td>
							<td class="vertical-middle">
								<center>
									<div class="checkbox checkbox-theme no-margin">
										<input value="1" name="rooms[{{ $stockData->date }}][stoppromo]" class="value_mass_stoppromo" {{ $stockData->stop_promotion==1 ? 'checked' : '' }} type="checkbox">
										<label for="value_mass_stoppromo" class="no-padding">

										</label>
									</div>
								</center>
							</td>
						</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<div class="panel-footer">
					<td class="vertical-middle"><button type="submit" class="btn btn-labeled btn-success"><span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>Submit</button></td>
				</div>
			</form>			
		</div>
	</div>
</div>

<!-- Modal Search -->
<form method="GET" action="" role="form">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Search Stock</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<select name="room_id" class="form-control input-sm selectpicker">
							@foreach($rooms as $x)
							@if(Input::old('room_id') == $x->id)
							<option selected="selected" value="{{ $x->id }}">{{ $x->name }}</option>
							@else
							<option value="{{ $x->id }}">{{ $x->name }}</option>			
							@endif
							@endforeach
						</select>
					</div>			
					<div class="form-group">
						<input value="{{ Input::old('start_date') }}" data-op="search" name="start_date" type="text" class="form-control from" id="from" placeholder="From">
					</div>
					<div class="form-group">
						<input value="{{ Input::old('end_date') }}" name="end_date" type="text" class="form-control to" id="to" placeholder="To">
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button name="search" value="submit" type="submit" id="#search_stock" class="btn btn-labeled btn-info"><span class="btn-label"><i class="glyphicon glyphicon-search"></i></span>Search</button>
				</div>
			</div>
		</div>
	</div>
</form>

@stop


@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ Theme::asset('assets/js/classes/stock/Stock.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/StockManager.js') }}" type="text/javascript"></script>
	<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
	<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
	<script>
		$(document).ready(function(){
			$(".from, .to").datepicker({
				'format': 'yyyy-mm-dd'
			});
			$('.selectpicker').selectpicker({title:'Fill Days', dropupAuto:true});
		});
	</script>
@stop