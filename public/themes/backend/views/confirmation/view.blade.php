@extends('layouts.backend')

@section('title')
	Confirmation - View
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	View Confirmation
@stop

@section('page_description')
	View confirmation information.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-room" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Transaction ID:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->transaction_id }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Transfer Date:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->transfer_date }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Transfer to:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ ucwords($confirmation->transfer_destination) }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Amount:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->transfer_total }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Account Number:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->transfer_account_number }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Account Name:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->transfer_account }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Account Phone:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $confirmation->phone }}</label>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<a href="{{ URL::to('/') . '/backend/transaction' }}" type="button" class="btn btn-success btn-icon-left margin-right-5 cancel"><i class="fa fa-check"></i> Back</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ Theme::asset('assets/js/classes/booking/Booking.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/BookingManager.js') }}" type="text/javascript"></script>
@stop