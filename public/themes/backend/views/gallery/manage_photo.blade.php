@extends('layouts.backend')

@section('title')
	Gallery Photos - Add
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add Photos
@stop

@section('page_description')
	Enter gallery information.
@stop

@section('content')
	
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body">

					<form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
	                	<input type="hidden" id="gallery_id" value="{{ $gallery_id }}" />

				        <!-- Redirect browsers with JavaScript disabled to the origin page -->
				        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				        <div class="row fileupload-buttonbar">
				            <div class="col-lg-5">
				                <!-- The fileinput-button span is used to style the file input field as button -->
				                <input type="file" name="files[]" multiple>
				                <!-- The global file processing state -->
				                <span class="fileupload-process"></span>
				            </div>
				            <!-- The global progress state -->
				            <div class="col-lg-5 fileupload-progress fade">
				                <!-- The global progress bar -->
				                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
				                </div>
				                <!-- The extended global progress state -->
				                <div class="progress-extended">&nbsp;</div>
				            </div>
				        </div>
				        <!-- The table listing the files available for upload/download -->
				        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
				    </form>

	                <script id="template-upload" type="text/x-tmpl">
					{% for (var i=0, file; file=o.files[i]; i++) { %}
					    <tr class="template-upload fade">
					        <td>
					            <span class="preview"></span>
					        </td>	
					        <td>
					            <p class="name">{%=file.name%}</p>
					            <strong class="error text-danger"></strong>
					        </td>
					        <td>
					            <p class="size">Processing...</p>
					            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
					        </td>
					        <td>
					            {% if (!i && !o.options.autoUpload) { %}
					                <button class="btn btn-primary start" disabled>
					                    <i class="glyphicon glyphicon-upload"></i>
					                    <span>Start</span>
					                </button>
					            {% } %}
					            {% if (!i) { %}
					                <button class="btn btn-warning cancel">
					                    <i class="glyphicon glyphicon-ban-circle"></i>
					                    <span>Cancel</span>
					                </button>
					            {% } %}
					        </td>
					    </tr>

					{% } %}
					</script>
					<!-- The template to display files available for download -->
					<script id="template-download" type="text/x-tmpl">
					{% for (var i=0, file; file=o.files[i]; i++) { %}
					    <tr class="template-download fade">
					        <td>
					            <span class="preview">
					                {% if (file.thumbnailUrl) { %}
					                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
					                {% } %}
					            </span>
					        </td>
					        <td>
					            <p class="name">
					                {% if (file.url) { %}
					                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
					                {% } else { %}
					                    <span>{%=file.name%}</span>
					                {% } %}
					            </p>
					            {% if (file.error) { %}
					                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
					            {% } %}
					        </td>
					        <td>
					        	<p class="name"><input type="text" style="background-color: #fff" class="form-control image_title_{%=file.photo_id%}" value="{%=file.photo_title%}" javascript="var photo_title = $('image_title_' + {%=file.photo_id%}).val();" /></p>
					            <p class="name"><textarea style="background-color: #fff;" rows="7" class="form-control description image_description_{%=file.photo_id%}">{%=file.photo_description%}</textarea></p>
					            <p class="name"><input onclick='updateMetadata({%=file.photo_id%})' class="btn btn-primary save-photo-metadata" type="button" value="Set Metadata" data-id="{%=file.photo_id%}"></p>
					        </td>
					        <td>
					            <span class="size">{%=o.formatFileSize(file.size)%}</span>
					        </td>
					        <td>
					            {% if (file.deleteUrl) { %}
					                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
					                    <i class="glyphicon glyphicon-trash"></i>
					                    <span>Delete</span>
					                </button>
					                <!--<input type="checkbox" name="delete" value="1" class="toggle">-->
					            {% } else { %}
					                <button class="btn btn-warning cancel">
					                    <i class="glyphicon glyphicon-ban-circle"></i>
					                    <span>Cancel</span>
					                </button>
					            {% } %}
					        </td>
					    </tr>
					{% } %}
					</script>

					<div class="margin-top-20 padding-top-20">
						<a href="{{ URL::to('/') . '/backend/cms/gallery' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Done</a>
					</div>

				</div>

			</div>

		</div>

	</div>

@stop

@section('scripts')
	
	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";

		function updateMetadata(photo_id)
		{
			var photo_title = $('.image_title_' + photo_id).val();
			var photo_description = $('.image_description_' + photo_id).val();

			$.ajax({
	            type: 'POST',
	            dataType: 'json',
	            url: baseUrl + '/backend/cms/gallery/update_metadata',
	            data: {
	                'photo_id': photo_id,
	                'photo_title': photo_title,
	                'photo_description': photo_description
	            },
	            success: function(data) {
	                if(!data.error)
	                {
	                	bootbox.alert('Metadata is successfully updated', function() {});
	                }
	            }
	        });
		}

	</script>
	
    <!-- The Templates plugin is included to render the upload/download listings -->
	<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
	<!-- blueimp Gallery script -->
	<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.iframe-transport.js') }}"></script>
	<!-- The basic File Upload plugin -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.fileupload.js') }}"></script>
	<!-- The File Upload processing plugin -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.fileupload-process.js') }}"></script>
	<!-- The File Upload image preview & resize plugin -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.fileupload-image.js') }}"></script>
	<!-- The File Upload validation plugin -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.fileupload-validate.js') }}"></script>
	<!-- The File Upload user interface plugin -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/jquery.fileupload-ui.js') }}"></script>
	<!-- The main application script -->
	<script src="{{ Theme::asset('assets/plugins/blueimp/main.js') }}"></script>

	<script src="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.min.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

    <script>
		$(document).ready(function() {
			
			$(".description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>

@stop