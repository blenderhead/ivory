@extends('layouts.backend')

@section('title')
	Gallery - Edit
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Gallery
@stop

@section('page_description')
	Edit gallery information.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-gallery" method="post" data-op="edit">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="name" class="col-lg-2 control-label">Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control gallery_name" id="gallery_name" placeholder="Gallery name" name="name" value="{{ $gallery->name }}">
								<span class="gallery_name_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="category" class="col-lg-2 control-label">Category <span class="required">*</span></label>
							<div class="col-lg-8">
								{{ Form::select('category', $categories, $gallery->gallery_category_id , array('class'=>'category')) }}
								<span class="cat_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>
						
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="description" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<textarea class="form-control description" id="description" rows="7" name="description" >{{ $gallery->description }}</textarea>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="photos" class="col-lg-2 control-label">Photos <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="dropzone dropzone-previews" id="my-awesome-dropzone"></div>
							</div>
						</div>
												
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publish" class="col-lg-2 control-label">Publish <span class="required">*</span></label>
							<div class="col-lg-8">
								{{ Form::select('publish', array('0'=>'No', '1'=>'Yes'), $gallery->is_publish , array('class'=>'publish')) }}
								<span class="publish_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_size" class="col-lg-2 control-label">Publish? <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input type="checkbox" id="checkbox4" name="publish" @if($gallery->is_publish){{'checked'}}@endif>
									<label for="checkbox4">Yes</label>
									<span class="publish_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
								</div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="gallery-id" name="id" value="{{ $gallery->id }}" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/cms/gallery' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel-gallery"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/errors/gal_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/gallery/Gallery.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/GalleryManager.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.min.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {
			
			$(".publish, .category").selectpicker()

			$(".description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>
@stop