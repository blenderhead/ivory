@extends('layouts.backend')

@section('title')
	User - Add
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add User
@stop

@section('page_description')
	Enter user informations.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-user" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="first_name" class="col-lg-2 control-label">First Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control first_name" id="first_name" placeholder="first name" name="first_name">
								<span class="first-name-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="last_name" class="col-lg-2 control-label">Last Name</label>
							<div class="col-lg-8">
								<input type="text" class="form-control last_name" id="last_name" placeholder="last name" name="last_name">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="email" class="col-lg-2 control-label">Email <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control email" id="email" placeholder="email" name="email">
								<span class="email-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="phone" class="col-lg-2 control-label">Phone <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control phone" id="phone" placeholder="phone" name="phone">
								<span class="phone-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="password" class="col-lg-2 control-label">Password <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text" class="form-control password" id="password" placeholder="password" name="password">
								<span class="password-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
							<div class="col-lg-2">
								<button id="generate_password" type="button" class="btn btn-primary btn-icon-left margin-right-5">Generate Password</button>
							</div>	
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="password_confirm" class="col-lg-2 control-label">Password Confirm <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text" class="form-control password_confirm" id="password_confirm" placeholder="confirm password" name="password_confirm">
								<span class="password-confirm-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="code" class="col-lg-2 control-label">Member Code <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text" class="form-control member_code" id="member_code" placeholder="member code" name="member_code">
								<span class="member-code-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
							<div class="col-lg-2">
								<button id="generate_code" type="button" class="btn btn-primary btn-icon-left margin-right-5">Generate Code</button>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="address" class="col-lg-2 control-label">Address</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor address" rows="7" name="address"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="avatar" class="col-lg-2 control-label">Profile Picture</label>
							<div class="col-lg-8">
								<input type="file" class="avatar" id="avatar" name="image" data-op="add">

								<div class="logo-container">
		                            <img id="logo-preview" />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="add" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/user' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/errors/user_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			resetFormElement($("#avatar"));

		});
	</script>
@stop