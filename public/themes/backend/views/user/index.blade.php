@extends('layouts.backend')

@section('title')
	Members - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Members
@stop

@section('page_description')
	Add, edit or delete your members.
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title">
					<button onClick="window.location='{{ URL::route('user.add') }}'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-user-plus"></i> Add Member</button>
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-minus-square-o"></i> Delete Selected</button>
				</div>

                <div class="panel-body padding-top-20 bg-white member_table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.member_table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/user/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null,
		          null,
		          null
		        ]
		    });

		});

	</script>

	<script src="{{ Theme::asset('assets/js/classes/user/User.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/UserManager.js') }}" type="text/javascript"></script>
@stop