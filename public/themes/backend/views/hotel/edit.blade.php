@extends('layouts.backend')

@section('title')
	Hotel Setting
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/timepicki/css/timepicki.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Hotel
@stop

@section('page_description')
	Edit hotel settings.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-hotel" method="post" enctype="multipart/form-data" data-op="edit">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="name" class="col-lg-2 control-label">Hotel Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control name" id="name" placeholder="hotel name" name="name" value="@if(isset($hotel->name)){{$hotel->name}}@endif">
								<span class="name-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="description" class="col-lg-2 control-label">Hotel Description</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor description" rows="7" name="description">@if(isset($hotel->description)){{$hotel->description}}@endif</textarea>
							</div>
						</div>
						-->

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="star" class="col-lg-2 control-label">Star</label>
							<div class="col-lg-8">
								<select class="star" name="star">
									@for($i=1;$i<=5;$i++)
										<option value="{{$i}}" 
											@if(isset($hotel->star))
												@if($hotel->star == $i)
													{{'selected'}}
												@endif
											@endif
										>{{$i}}</option>
									@endfor
								</select>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="address" class="col-lg-2 control-label">Address</label>
							<div class="col-lg-8">
								<textarea class="form-control bs-texteditor address" rows="7" name="address">@if(isset($hotel->address)){{$hotel->address}}@endif</textarea>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="type" class="col-lg-2 control-label">Type</label>
							<div class="col-lg-8">
								<select class="type" name="type">
									<option value="1" @if(isset($hotel->type)) @if($hotel->type == '1'){{'selected'}}@endif @endif>Hotel</option>
									<option value="2" @if(isset($hotel->type)) @if($hotel->type == '2'){{'selected'}}@endif @endif>Apartment</option>
									<option value="3" @if(isset($hotel->type)) @if($hotel->type == '3'){{'selected'}}@endif @endif>Vila</option>
								</select>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="phone" class="col-lg-2 control-label">Phone <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control phone" id="phone" placeholder="hotel phone" name="phone" value="@if(isset($hotel->phone)){{$hotel->phone}}@endif">
								<span class="phone-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="fax" class="col-lg-2 control-label">Fax</label>
							<div class="col-lg-8">
								<input type="text" class="form-control fax" id="fax" placeholder="hotel fax" name="fax" value="@if(isset($hotel->fax)){{$hotel->fax}}@endif">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="email" class="col-lg-2 control-label">Customer Service Email <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control email" id="email" placeholder="email" name="email" value="@if(isset($hotel->email)){{$hotel->email}}@endif">
								<span class="email-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="manager_email" class="col-lg-2 control-label">Manager Email <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control manager_email" id="manager_email" placeholder="manager_email" name="manager_email" value="@if(isset($hotel->manager_email)){{$hotel->manager_email}}@endif">
								<span class="manager-email-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="number_of_room" class="col-lg-2 control-label">Number of Rooms</label>
							<div class="col-lg-8">
								<input type="text" class="form-control number_of_room" id="number_of_room" placeholder="number of room" name="number_of_room" value="@if(isset($hotel->number_of_room)){{$hotel->number_of_room}}@endif">
								<span class="room-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="number_of_floor" class="col-lg-2 control-label">Number of Floor</label>
							<div class="col-lg-8">
								<input type="text" class="form-control number_of_floor" id="number_of_floor" placeholder="number of floor" name="number_of_floor" value="@if(isset($hotel->number_of_floor)){{$hotel->number_of_floor}}@endif">
								<span class="floor-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="facilities" class="col-lg-2 control-label">Facilities</label>
							<div class="col-lg-8" id="list_facilities">
								@if(isset($hotel->id) && count($hotel->facilities) > 0)
									@foreach($hotel->facilities as $index => $facility)
										<div class="col-lg-4  margin-bottom-5 facilities" id="fac-{{ $index }}">
											<div class="input-group">
												<input value="{{ $facility->name }}" name="facilities[{{ $index }}]" type="text" class="form-control no-border-right" placeholder="facility">
												<span class="input-group-btn"><button data-fac-id="{{ $index }}" class="btn btn-warning delete-fac" type="button"><i class="fa fa-times"></i></button></span>
											</div>
										</div>
									@endforeach		
								@endif
							</div>
							<div class="col-lg-2">
								<button type="button" id="add_facility" class="btn btn-primary btn-icon-left margin-right-5"><i class="fa fa-plus"></i>Add</button>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="services" class="col-lg-2 control-label">Services</label>
							<div class="col-lg-8" id="list_services">
								@if(isset($hotel->id) && count($hotel->services) > 0)
									@foreach($hotel->services as $index => $service)
										<div class="col-lg-4  margin-bottom-5 services" id="service-{{ $index }}">
											<div class="input-group">
												<input value="{{ $service->name }}" name="services[{{ $index }}]" type="text" class="form-control no-border-right" placeholder="service">
												<span class="input-group-btn"><button data-service-id="{{ $index }}" class="btn btn-warning delete-service" type="button"><i class="fa fa-times"></i></button></span>
											</div>
										</div>
									@endforeach		
								@endif
							</div>
							<div class="col-lg-2">
								<button type="button" id="add_service" class="btn btn-primary btn-icon-left margin-right-5"><i class="fa fa-plus"></i>Add</button>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="tax" class="col-lg-2 control-label">Hotel Tax (%)</label>
							<div class="col-lg-8">
								<input type="text" class="form-control tax" id="tax" placeholder="tax percentage" name="tax" value="@if(isset($hotel->tax_charge)){{$hotel->tax_charge}}@endif">
								<span class="tax-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="service" class="col-lg-2 control-label">Service Tax (%)</label>
							<div class="col-lg-8">
								<input type="text" class="form-control service" id="service" placeholder="service percentage" name="service" value="@if(isset($hotel->service_charge)){{$hotel->service_charge}}@endif">
								<span class="service-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="member_discount" class="col-lg-2 control-label">Member Discount</label>
							<div class="col-lg-8">
								<input type="text" class="form-control member_discount" id="member_discount" placeholder="member discount amount" name="member_discount" value="@if(isset($hotel->member_discount)){{$hotel->member_discount}}@endif">
								<span class="member-discount-error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="checkin_time" class="col-lg-2 control-label">Check-in Time</label>
							<div class="col-lg-8">
								<input type="text" class="form-control checkin_time" id="checkin_time" placeholder="check-in time" name="checkin_time" value="@if(isset($hotel->checkin_time)){{$hotel->checkin_time}}@endif">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="checkout_time" class="col-lg-2 control-label">Check-out Time</label>
							<div class="col-lg-8">
								<input type="text" class="form-control checkout_time" id="checkout_time" placeholder="check-out time" name="checkout_time" value="@if(isset($hotel->checkout_time)){{$hotel->checkout_time}}@endif">
							</div>
						</div>
						-->

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" name="hotel_id" value="@if(isset($hotel->id)){{$hotel->id}}@endif" class="hotel_id" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/timepicki/js/timepicki.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/errors/settings_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/hotel/Hotel.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/HotelManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			//resetFormElement($("#avatar"));

			$(".description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

			$(".star, .type").selectpicker();

			$(".checkin_time, .checkout_time").timepicki({
				'show_meridian': false
			});

		});
	</script>
@stop