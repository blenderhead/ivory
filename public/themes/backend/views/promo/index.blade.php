@extends('layouts.backend')

@section('title')
	Promo - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Promotions
@stop

@section('page_description')
	Add, edit or delete promotions. 
@stop

@section('content')
	<div class="row">

		<!-- Green bar means promotion is currently running and shown to the guests. Yellow bar means promotion is not running and hidden from the guests. Red bar means the promotion is not effective anymore because the booking date has passed today. -->

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title">
					<button onClick="window.location='{{ URL::route('promo.add') }}'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-gift"></i> Add Promotion</button>
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-minus-square-o"></i> Delete Selected</button>
				</div>

                <div class="panel-body padding-top-20 bg-white table_wrapper">

                	{{ $tables->render() }}

                	<!--
					<table id="example1" class="table table-bordered table-striped">

						<thead>
							<tr>
								<th><input type="checkbox" id="selectall"></th>
								<th>Promotion</th>
								<th>Room Name</th>
								<th>Booking Date</th>
								<th>Stay Date</th>
								<th>Cancellation Policy</th>
								<th>Operations</th>
							</tr>
						</thead>
						
						<tbody>
							@foreach($promotions as $promo)
								<tr>
									<td><input type="checkbox" class="promo" value="{{ $promo->id }}"></td>
									<td>{{ $promo->name }}</td>
									<td>
										@foreach($promo->rooms as $index => $room)
											{{ $index+1 }}. {{ $room->name }}<br>
										@endforeach
									</td>
									<td>{{ $promo->booking_start_date }} - {{ $promo->booking_end_date }}</td>
									<td>{{ $promo->stay_start_date }} - {{ $promo->stay_end_date }}</td>
									<td>
										@if(is_object($promo->cancellation)) 
											<a type="button" data-toggle="tooltip" data-placement="bottom" title="1. {{$promo->cancellation->level1Policy()->name}} |
												2. {{ is_object($promo->cancellation->level2Policy()) ? $promo->cancellation->level2Policy()->name : '-' }} | 
												3. {{ is_object($promo->cancellation->level3Policy()) ? $promo->cancellation->level3Policy()->name : '-' }}">
												Costum Cancellations
											</a> 
										@else 
											Defult Cancellation 
										@endif
									</td>
									<td>
										<a href="{{ URL::to('/') . '/backend/promo/edit?id=' . $promo->id }}" type="button" class="btn btn-success btn-circle edit" data-id="{{ $promo->id }}"><i class="glyphicon glyphicon-pencil"></i></a>
										<button type="button" class="btn btn-warning btn-circle delete" data-id="{{ $promo->id }}"><i class="glyphicon glyphicon-remove"></i></button>
									</td>
								</tr>
							@endforeach
						</tbody>
						
						<tfoot>
							<tr>
								<th>Promotion</th>
								<th>Room Name</th>
								<th>Booking Date</th>
								<th>Stay Date</th>
								<th>Cancellation Policy</th>
								<th>Operations</th>
							</tr>
						</tfoot>

					</table>
					-->

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/js/classes/promo/Promo.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/PromoManager.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/promo/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null,
		          null,
		          null,
		          null
		        ]
		    });

		});

	</script>
@stop