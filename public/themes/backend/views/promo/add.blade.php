@extends('layouts.backend')

@section('title')
	Promotion - Add
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add Promotion
@stop

@section('page_description')
	Add promotion for your hotels. For example staying at least 3 days and book 1 or more rooms get 20% discount.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal bv-form" id="add-promo" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_name" class="col-lg-2 control-label">Room Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input name="room_ids" value="" type="checkbox" id="checkboxall">
									<label for="checkboxall">Select All</label>
								</div>							
								@foreach($rooms as $index => $room)
								<div class="checkbox checkbox-theme">
									<input class="room_ids" name="room_ids[{{$index}}]" value="{{ $room->id }}" type="checkbox" id="checkbox{{ $room->id }}">
									<label for="checkbox{{ $room->id }}">{{ $room->name }}</label>
								</div>
								@endforeach
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="promo_type" class="col-lg-2 control-label">Type <span class="required">*</span></label>
							<div class="col-lg-8">
								<select id="promo_type" class="promo_type" name="promo_type">
									<option value="0">Standard Promotion</option>
									<option value="1">Last Minutes</option>
									<option value="2">Early Birds</option>
								</select>
							</div>
						</div>

						<div id="day_in_advance" class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="day_in_advance" class="col-lg-2 control-label">Days in Advance</label>
							<div class="col-lg-3">
								<input name="early_day" id="early_day" type="text" class="form-control" placeholder="Day in Advance">
							</div>
						</div>

						<div id="within_days" class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="within_days" class="col-lg-2 control-label">Within Days of Arrival</label>
							<div class="col-lg-3">
								<input name="whitin_day" id="whitin_day" type="text" class="form-control" placeholder="Within Days">
							</div>
						</div>													

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="end_date" class="col-lg-2 control-label">Booking Date <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-4">
										<input name="booking_start_date" type="text" class="form-control start_booking_date" placeholder="start booking date">
									</div>
									<div class="col-lg-4">
										<input name="booking_end_date" type="text" class="form-control end_booking_date" placeholder="end booking date">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="end_date" class="col-lg-2 control-label">Stay Date <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-4">
										<input name="stay_start_date" type="text" class="form-control start_stay_date" placeholder="start stay date">
									</div>
									<div class="col-lg-4">
										<input name="stay_end_date" type="text" class="form-control end_stay_date" placeholder="end stay date">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_name" class="col-lg-2 control-label">Set Days <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input name="is_sunday" value="1" checked type="checkbox" id="checkbox1">
									<label for="checkbox1">Sunday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_monday" value="1" checked type="checkbox" id="checkbox2">
									<label for="checkbox2">Monday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_tuesday" value="1" checked type="checkbox" id="checkbox3">
									<label for="checkbox3">Tuesday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_wednesday" value="1" checked type="checkbox" id="checkbox4">
									<label for="checkbox4">Wednesday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_thursday" value="1" checked type="checkbox" id="checkbox5">
									<label for="checkbox5">Thursday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_friday" value="1" checked type="checkbox" id="checkbox6">
									<label for="checkbox6">Friday</label>
								</div>
								<div class="checkbox checkbox-theme">
									<input name="is_saturday" value="1" checked type="checkbox" id="checkbox7">
									<label for="checkbox7">Saturday</label>
								</div>																																
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="min_stay_duration" class="col-lg-2 control-label">Stay at Least <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control min_stay_duration" id="min_stay_duration" placeholder="minimum stay duration" name="min_stay_duration">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="min_book_room" class="col-lg-2 control-label">Book at Least <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control min_book_room" id="min_book_room" placeholder="minimum room booking duration" name="min_book_room">
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="value_discount" class="col-lg-2 control-label">Benefit <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-2">
										<input type="text" class="form-control value_discount" id="value_discount" name="value_discount">
									</div>
									<div class="col-lg-6">
										<select class="benifit" id="benifit_value" name="benifit_value">
											<option value="0">% Discount (Per Night)</option>
											<option value="1">Discount (Per Night)</option>
											<option value="2">Free Night</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="promo_type" class="col-lg-2 control-label">Cancellation Policy <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="row margin-bottom-5">
									<div class="col-lg-5">
										<select class="form-control costum_policy" name="costum_policy">
											<option value="0">Default (Use Room Cancellation)</option>
											<option value="1">Custom Cancellation</option>
										</select>
									</div>
								</div>

								<div id="custom_cancellations">							
									<div class="row margin-bottom-5">
										<div class="col-lg-4">
											<select class="form-control level_1_policy" name="level_1_policy">
												@foreach($cancellations['level_1'] as $cancellation)
												<option value="{{ $cancellation->id }}">{{ $cancellation->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="row margin-bottom-5">
										<div class="col-lg-4">
											<select class="form-control level_2_policy" name="level_2_policy">
												<option value="">-</option>
												@foreach($cancellations['level_2'] as $cancellation)
												<option value="{{ $cancellation->id }}">{{ $cancellation->name }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="row margin-bottom-5">
										<div class="col-lg-4">
											<select class="form-control level_3_policy" name="level_3_policy">
												<option value="">-</option>
												@foreach($cancellations['level_2'] as $cancellation)
												<option value="{{ $cancellation->id }}">{{ $cancellation->name }}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="day_in_advance" class="col-lg-2 control-label">Promotion Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input id="promotion_name" name="promotion_name" type="text" class="form-control" placeholder="Promotion Name">
							</div>
							<div class="col-lg-2">
								<button id="generate_name_promotion" type="button" class="btn btn-primary btn-icon-left margin-right-5">Generate Name</button>
							</div>							
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="add" />
							<button type="button" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/policy' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/classes/promo/Promo.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/PromoManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$(".start_booking_date, .end_booking_date, .start_stay_date, .end_stay_date").datepicker({
				'format': 'yyyy-mm-dd'
			});

			$(".promo_type").selectpicker();
			$(".benifit").selectpicker();
			$(".costum_policy").selectpicker();			
		});
	</script>
@stop