@extends('layouts.backend')

@section('title')
	Policies - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Cancellation Policy
@stop

@section('page_description')
	Add, edit or delete cancellation policy. 
@stop

@section('content')
	<div class="row">

		<!-- // Green bar means cancellation policy is running. Red bar means the cancellation policy is not effective anymore because the end date has passed today. -->

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title">
					<button onClick="window.location='{{ URL::route('policy.add') }}'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-flag-o"></i> Add Policy</button>
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-minus-square-o"></i> Delete Selected</button>
				</div>

                <div class="panel-body padding-top-20 bg-white table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

		<script type="text/javascript">

		$(document).ready(function() {

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/policy/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null
		        ]
		    });

		});

	</script>

	<script src="{{ Theme::asset('assets/js/classes/policy/Policy.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/PolicyManager.js') }}" type="text/javascript"></script>
@stop