@extends('layouts.backend')

@section('title')
	Policy - Edit
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Edit Cancellation Policy
@stop

@section('page_description')
	Edit hotel cancellation policy.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-policy" method="post" enctype="multipart/form-data" data-op="edit">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_name" class="col-lg-2 control-label">Room Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input name="room_ids" value="" type="checkbox" id="checkboxall">
									<label for="checkboxall" class="room_ids_wrapper">Select All</label>
								</div>							
								@foreach($rooms as $index => $room)
								<div class="checkbox checkbox-theme">
									<input class="room_ids" @if($room->cancellations->contains($cancellation->id)) checked @endif name="room_ids[{{$index}}]" value="{{ $room->id }}" type="checkbox" id="checkbox{{ $room->id }}">
									<label for="checkbox{{ $room->id }}">{{ $room->name }}</label>
								</div>
								@endforeach
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="start_date" class="col-lg-2 control-label">Start Date <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $cancellation->start_date }}" type="text" class="form-control start_date" id="start_date" placeholder="policy start date" name="start_date">
								<span class="start_date_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="end_date" class="col-lg-2 control-label">End Date <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $cancellation->end_date }}" type="text" class="form-control end_date" id="end_date" placeholder="policy end date" name="end_date">
								<span class="end_date_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publish_price" class="col-lg-2 control-label">Cancellation Policy</label>
							<div class="col-lg-10">
								<div class="row margin-bottom-5">
									<div class="col-lg-4">
										<select class="form-control level_1_policy" name="level_1_policy">
											@foreach($cancellation_rules['level_1'] as $value)
											<option
											@if($cancellation->level_1_cancellation_rule_id == $value->id)
												selected
											@endif
											value="{{ $value->id }}">{{ $value->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="row margin-bottom-5">
									<div class="col-lg-4">
										<select class="form-control level_2_policy" name="level_2_policy">
											<option value="">-</option>
											@foreach($cancellation_rules['level_2'] as $value)
											<option
											@if($cancellation->level_2_cancellation_rule_id == $value->id)
												selected
											@endif
											value="{{ $value->id }}">{{ $value->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="row margin-bottom-5">
									<div class="col-lg-4">
										<select class="form-control level_3_policy" name="level_3_policy">
											<option value="">-</option>
											@foreach($cancellation_rules['level_2'] as $value)
											<option
											@if($cancellation->level_3_cancellation_rule_id == $value->id)
												selected
											@endif
											value="{{ $value->id }}">{{ $value->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="edit" />
							<input type="hidden" name="id" class="policy_id" value="{{$cancellation->id}}" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/policy' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Done</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/errors/policy_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/policy/Policy.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/PolicyManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$(".start_date, .end_date").datepicker({
				'format': 'yyyy-mm-dd'
			});
		});
	</script>
@stop