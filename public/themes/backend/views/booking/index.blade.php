@extends('layouts.backend')

@section('title')
	Bookings - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Bookings
@stop

@section('page_description')
	Manage your hotel bookings.
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

				<div class="panel-body">
	        		<form method="GET" action="{{ URL::to('/') . '/backend/booking' }}" id="filter_booking" class="form-inline" role="form">
						<div class="form-group">
							<input type="text" class="form-control start_date" id="start_date" placeholder="start date" name="date_start" value="@if($date_start != '0'){{ $date_start }}@endif">
						</div>
						<div class="form-group margin-left-10">
							<input type="text" class="form-control end_date" id="end_date" placeholder="end date" name="date_end" value="@if($date_end != '0'){{ $date_end }}@endif">
						</div>
					  	<button type="submit" class="btn btn-dark bg-light-blue-300 color-white margin-left-10">Filter</button>
					  	<a href="{{ URL::to('/') . '/backend/booking?date_start=0&date_end=0' }}" type="button" class="btn btn-dark bg-light-blue-300 color-white margin-left-10">Show All Dates</a>
					</form>
				</div>

				<div class="panel-title">
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 read-all"><i class="fa fa-minus-square-o"></i> Mark as Read</button>
				</div>

				<div class="panel-body padding-top-20 bg-white table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		var date_start = "{{ $date_start }}";
		var date_end = "{{ $date_end }}";

		$(document).ready(function() {

			$(".start_date, .end_date").datepicker({
				'format': 'yyyy-mm-dd'
			});

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/booking/api?date_start=' + date_start + '&date_end=' + date_end,
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		          { "bSortable": false },
		        ]
		    });

		});

	</script>

	<script src="{{ Theme::asset('assets/js/classes/booking/Booking.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/BookingManager.js') }}" type="text/javascript"></script>

@stop