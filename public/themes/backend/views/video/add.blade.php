@extends('layouts.backend')

@section('title')
	Video - Add
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Add Video
@stop

@section('page_description')
	Enter video information.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-video" method="post" data-op="add">
					<!--<form class="form-horizontal" id="add-gallery" enctype="multipart/form-data" data-op="add">-->

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="title" class="col-lg-2 control-label">Title <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control title" id="title" placeholder="Video title" name="title">
								<span class="title_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="description" class="col-lg-2 control-label">Description</label>
							<div class="col-lg-8">
								<textarea class="form-control description" id="description" rows="7" name="description"></textarea>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="url" class="col-lg-2 control-label">URL <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control url" id="url" placeholder="Video url" name="url">
								<span class="url_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_size" class="col-lg-2 control-label">Publish? <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input type="checkbox" id="checkbox4" name="publish">
									<label for="checkbox4">Yes</label>
									<span class="publish_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
								</div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="add" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/cms/gallery/video' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/js/errors/video_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/VideoManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {
			
			$(".description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>
@stop