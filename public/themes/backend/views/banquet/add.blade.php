@extends('layouts.backend')

@section('title')
	Banquet Room - Add
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <!--<link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />-->
@stop

@section('page_title')
	Add Banquet Room
@stop

@section('page_description')
	Enter banquet room information.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-room" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_name" class="col-lg-2 control-label">Room Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control room_name" id="room_name" placeholder="room name" name="room_name">
								<span class="room_name_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_description" class="col-lg-2 control-label">Room Description <span class="required">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control room_description" id="room_description" name="room_description"></textarea>
								<span class="room_desc_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>						

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_size" class="col-lg-2 control-label">Room Size <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control room_size" id="room_size" placeholder="room size" name="room_size">
								<span class="room_size_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_description" class="col-lg-2 control-label">Room Setup <span class="required">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control room_setup" id="room_setup" name="room_setup"></textarea>
								<span class="room_setup_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>		

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="profile_picture" class="col-lg-2 control-label">Room Image</label>
							<div class="col-lg-8">
								<input type="file" class="logo" id="room_image" name="room_image" data-op="add">
								<span class="room_image_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
								
								<div class="logo-container">
		                            <img id="logo-preview" />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="type" class="col-lg-2 control-label">Room Gallery</label>
							<div class="col-lg-8">
								<select class="gallery_id" name="gallery_id">
									@foreach($galleries as $gallery)
										<option value="{{ $gallery->id }}">{{ $gallery->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="op" name="op" value="add" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/banquet' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

	<!-- Currency Formater -->
	<script src="{{ Theme::asset('assets/plugins/jquery-currency/jquery.currency.js') }}" type="text/javascript"></script>

    <!--<script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>-->
    
    <script src="{{ Theme::asset('assets/js/errors/room_errors.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/BanquetManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			//resetFormElement($("#room_image"));

			$(".gallery_id").selectpicker();
			
			$(".room_description, .room_setup").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>
@stop