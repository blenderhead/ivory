@extends('layouts.backend')

@section('title')
	Banquet Rooms - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Banquet Rooms
@stop

@section('page_description')
	Add, edit or delete hotel banquet room.
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">
			@if($errors->any())
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Oooppss!</strong>{{ implode('', $errors->all('<li>:message</li>')) }}
			</div>
			@endif        
			<div class="panel no-border ">

                <div class="panel-title">
					<button onClick="window.location='{{ URL::route('banquet.add') }}'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-bed"></i> Add Banquet Room</button>
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-minus-square-o"></i> Delete Selected</button>
				</div>

                <div class="panel-body padding-top-20 bg-white table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<!-- Currency Formater -->
	<script src="{{ Theme::asset('assets/plugins/jquery-currency/jquery.currency.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/banquet/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		        ]
		    });

		});

	</script>

	<script src="{{ Theme::asset('assets/js/classes/BanquetManager.js') }}" type="text/javascript"></script>
@stop