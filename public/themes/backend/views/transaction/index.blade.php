@extends('layouts.backend')

@section('title')
	Transactions - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Transactions
@stop

@section('page_description')
	Manage your hotel transactions.
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-body padding-top-20 bg-white table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/transaction/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null,
		          null,
		          null,
		          null,
		        ]
		    });

		});

	</script>

	<script src="{{ Theme::asset('assets/js/classes/TransactionManager.js') }}" type="text/javascript"></script>

@stop