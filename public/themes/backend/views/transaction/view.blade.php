@extends('layouts.backend')

@section('title')
	Transaction - View
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	View Transaction
@stop

@section('page_description')
	View transaction information.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="add-room" method="post" enctype="multipart/form-data" data-op="add">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Room Name:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->room->name }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Room Booked:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->number_of_rooms }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Guest Name:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->first_name . ' ' . $booking->last_name }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Guest Email:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->email }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Guest Phone:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->phone_number }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Booking Date:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->created_at }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Check-In Date:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->checkin_date }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Check-Out Date:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->checkout_date }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Transaction ID:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->transaction->transaction_id }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Transaction Status:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ ucwords(Config::get('booking_states.' . $booking->transaction->status)) }}</label>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 padding-bottom-20">
							<label class="col-lg-2 control-label">Payment Method:</label>
							<div class="col-lg-8">
								<label class="control-label light-label">{{ $booking->transaction->type }}</label>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<a href="{{ URL::to('/') . '/backend/booking' }}" type="button" class="btn btn-success btn-icon-left margin-right-5 cancel"><i class="fa fa-check"></i> Done</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		maniac.loaddatatables();
	</script>

	<script src="{{ Theme::asset('assets/js/classes/booking/Booking.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/BookingManager.js') }}" type="text/javascript"></script>
@stop