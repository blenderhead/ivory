
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>404 Error</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ Theme::asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ Theme::asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/responsive.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="bg-grey-100 error-page overflow-hidden">
	<!-- BEGIN CONTENT -->
    <div class="wrapper">
		<div class="panel no-shadow bg-transparent panel-center">
			<div class="panel-body no-padding">
				<h1 class="font-size-70 margin-top-30 margin-bottom-10 color-blue-grey-600 animated fadeInDown"><span  data-toggle="counter" data-start="0" data-from="0" data-to="404" data-speed="600" data-refresh-interval="10"></span> <small class="color-blue-grey-600 display-block margin-top-5 font-size-20">page not found</small></h1>
				<p class="padding-30 color-grey-700 animated fadeInLeft">We're sorry but we can't seem find the page you requested.<br />You can search something else or read this text one more time.</p>
			</div>
			<div class="panel-footer padding-md bg-transparent animated fadeInUp">
				<a href="{{ URL::to('/') }}" class="btn btn-dark bg-light-green-500 color-white margin-right-15"><span class="glyphicon glyphicon-home margin-right-10"></span> Back to home </a>
			</div>
		</div><!-- /.panel -->
	</div><!-- /.wrapper -->
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="{{ Theme::asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/holder.js') }}"></script>
	<script src="{{ Theme::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/core.js" type="text/javascript') }}"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- counter -->
	<script src="{{ Theme::asset('assets/plugins/jquery-countTo/jquery.countTo.js') }}" type="text/javascript"></script>
	<!-- maniac -->
	<script src="{{ Theme::asset('assets/js/maniac.js" type="text/javascript') }}"></script>
	<script type="text/javascript">
		maniac.loadcounter();
	</script> 
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>