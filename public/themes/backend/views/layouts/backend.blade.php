
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>@yield('title')</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/jqueryui/jquery-ui.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/jqueryui/jquery-ui.theme.css') }}" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ Theme::asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/bootstrap-slider/css/slider.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ Theme::asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/responsive.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/custom.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->

	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";
	</script>

	@yield('styles')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header">
	<!-- BEGIN HEADER -->
	<header>
		<a href="{{ URL::route('dashboard.index') }}" class="logo"><i class="ion-ios-bolt"></i> <span>Ivory Dashboard</span></a>
		<nav class="navbar navbar-static-top">
			<a href="#" class="navbar-btn sidebar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
			
			<!--
            <div class="navbar-right">
				<form role="search" class="navbar-form pull-left" method="post" action="#">
					<div class="btn-inline">
						<input type="text" class="form-control padding-right-35" placeholder="Search..."/>
						<button class="btn btn-link no-shadow bg-transparent no-padding padding-right-10" type="button"><i class="ion-search"></i></button>
					</div>
				</form>
			</div>
			-->
        </nav>
    </header>
	<!-- END HEADER -->
		 
	<div class="wrapper">

		<!-- BEGIN LEFTSIDE -->
        <div class="leftside">

			<div class="sidebar">
				<!-- BEGIN RPOFILE -->
				<div class="nav-profile">
					<div class="thumb">

						<img src="@if($user->image){{ URL::to('/') . '/uploads/user/' . $user->image }}@else{{ Theme::asset('assets/img/avatar.jpg') }}@endif" class="img-circle" alt="" />

						<!--<span class="label label-danger label-rounded">3</span>-->
					</div>
					<div class="info">
						<a href="{{ URL::to('/') . '/backend/profile/edit?id=' . $user->id }}">{{ ucwords($user->first_name . ' ' . $user->last_name) }}</a>
						<ul class="tools list-inline">
							<li><a href="{{ URL::to('/') . '/backend/profile/edit?id=' . $user->id }}" data-toggle="tooltip" title="Edit Profile"><i class="ion-gear-a"></i></a></li>
						</ul>
					</div>
					<a href="{{ URL::route('auth.logout') }}" class="button"><i class="ion-log-out"></i></a>
				</div>
				<!-- END RPOFILE -->

				<!-- BEGIN NAV -->
				<div class="title">Navigation</div>
				
				<ul class="nav-sidebar">
					<li class="@if($current_route_name == 'dashboard.index'){{'active open'}}@endif">
                        <a href="{{ URL::route('dashboard.index') }}">
                            <i class="ion-home"></i> <span>Dashboard</span>
                        </a>
                    </li>

                    <!--
                    <li class="@if($current_route_name == 'booking.index'){{'active open'}}@endif"><a href="{{ URL::to('/') . '/backend/booking?date_start=0&date_end=0' }}"><i class="fa fa-book"></i><span>Booking Management</span></a></li>
                    <li class="@if($current_route_name == 'transaction.index'){{'active open'}}@endif"><a href="{{ URL::route('transaction.index') }}"><i class="fa fa-calculator"></i><span>Transaction Management</span></a></li>
                    -->

                    <li class="nav-dropdown @if($current_route_name == 'promo.edit' || $current_route_name == 'promo.add' || $current_route_name == 'policy.edit' || $current_route_name == 'room.index' || $current_route_name == 'room.add' || $current_route_name == 'room.edit' || $current_route_name == 'policy.index' || $current_route_name == 'policy.add' || $current_route_name == 'stock.index' || $current_route_name == 'promo.index' || $current_route_name == 'promo.add' || $current_route_name == 'banquet.index' || $current_route_name == 'banquet.add' || $current_route_name == 'banquet.edit'){{'active open'}}@endif">
                        <a href="#">
                            <i class="ion-clipboard"></i> <span>Room Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="{{ URL::route('room.index') }}">Rooms</a></li>
                            <li><a href="{{ URL::route('banquet.index') }}">Banquets</a></li>
                            <!--
                            <li><a href="{{ URL::route('policy.index') }}">Cancel Policies</a></li>
                            <li><a href="{{ URL::route('stock.index') }}">Price & Stocks</a></li>
                            <li><a href="{{ URL::route('promo.index') }}">Promotions</a></li>
                        	-->
                        </ul>
                    </li>
                    <!--
					<li class="@if($current_route_name == 'restriction.index'){{'active open'}}@endif">
						<a href="{{ URL::route('restriction.index') }}">
							<i class="fa fa-times"></i><span>Restictions Management</span>
						</a>
					</li>
					-->

					<!--
					<li class="@if($current_route_name == 'hotel.edit'){{'active open'}}@endif">
						<a href="{{ URL::route('hotel.edit') }}"><i class="fa fa-building-o"></i><span>Hotel Setting</span></a>
					</li>                    
					-->
                    <!--<li class="nav-dropdown">-->

                    <li class="nav-dropdown @if(
                    	$current_route_name == 'cms.gallery.index' || 
                    	$current_route_name == 'cms.gallery.add' || 
                    	$current_route_name == 'cms.gallery.edit' || 
                    	$current_route_name == 'cms.gallery-categories.index' || 
                    	$current_route_name == 'cms.gallery-categories.add' || 
                    	$current_route_name == 'cms.gallery-categories.edit' || 
                    	$current_route_name == 'cms.gallery.video.index' || 
                    	$current_route_name == 'cms.gallery.video.add' || 
                    	$current_route_name == 'cms.gallery.video.edit'
                    )

                    {{'active open'}}@endif">
                        <a href="#">
                            <i class="ion-compose"></i> <span>Gallery Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="{{ URL::route('cms.gallery-categories.index') }}">Gallery Categories</a></li>
                            <li><a href="{{ URL::route('cms.gallery.index') }}">Gallery</a></li>
                            <li><a href="{{ URL::route('cms.gallery.video.index') }}">Videos</a></li>
                        </ul>
                    </li>
                    <li class="@if($current_route_name == 'cms.review.index'){{'active open'}}@endif">
						<a href="{{ URL::route('cms.review.index') }}"><i class="fa fa-comment-o"></i><span>Review Management</span></a>
					</li>  
                    <li class="nav-dropdown @if($current_route_name == 'cafe.promo.index' || $current_route_name == 'cafe.promo.add' || $current_route_name == 'cafe.promo.edit'){{'active open'}}@endif">
                        <a href="#">
                            <i class="ion-wineglass"></i> <span>Cafe Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="{{ URL::route('cafe.promo.index') }}">Promo</a></li>
                        </ul>
                    </li>
                    <!--
                    <li class="@if($current_route_name == 'user.index'){{'active open'}}@endif">
                    	<a href="{{ URL::route('user.index') }}">
							<i class="ion-person"></i><span>Member Management</span>
						</a>
					</li>
					-->
                </ul>
				<!-- END NAV -->

			</div><!-- /.sidebar -->

        </div>
		<!-- END LEFTSIDE -->

		<!-- BEGIN RIGHTSIDE -->
        <div class="rightside">
			<!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
				<h1 class="page-title">@yield('page_title')<small>@yield('page_description')</small></h1>
			</div>
			<!-- END PAGE HEADING -->

            <div class="container-fluid">
				@yield('content')
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="{{ Theme::asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/holder.js') }}"></script>
	<script src="{{ Theme::asset('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/jqueryui/jquery-ui.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/core.js" type="text/javascript') }}"></script>
	<script src="{{ Theme::asset('assets/js/currency.js" type="text/javascript') }}"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- flot chart -->
	<!--
	<script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/flot/jquery.flot.grow.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	-->
	
	<!-- sparkline -->
	<script src="{{ Theme::asset('assets/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap slider -->
	<script src="{{ Theme::asset('assets/plugins/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
	
	<!-- datepicker -->
	<script src="{{ Theme::asset('assets/plugins/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
	
	<!-- vectormap -->
	<script src="{{ Theme::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en.js') }}" type="text/javascript"></script>
	
	<!-- counter -->
	<script src="{{ Theme::asset('assets/plugins/jquery-countTo/jquery.countTo.js') }}" type="text/javascript"></script>
	
	<!-- rickshaw -->
	<script src="{{ Theme::asset('assets/plugins/rickshaw/vendor/d3.v3.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/rickshaw/rickshaw.min.js') }}" type="text/javascript"></script>

	<!-- ion-rangeSlider -->
	<script src="{{ Theme::asset('assets/plugins/ion-rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
	
	<!-- knob -->
	<script src="{{ Theme::asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}" type="text/javascript"></script>

	<!-- bootstrap validator -->
	<script src="{{ Theme::asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.js') }}" type="text/javascript"></script>

	<!-- input mask -->
    <script src="{{ Theme::asset('assets/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/plugins/input-mask/jquery.inputmask.numeric.extensions.js') }}" type="text/javascript"></script>
	
	<!-- switchery -->
    <script src="{{ Theme::asset('assets/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>
	
	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- colorpicker -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
	
	<!-- iCheck -->
    <script src="{{ Theme::asset('assets/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
	
	<!-- maniac -->
	<script src="{{ Theme::asset('assets/js/maniac.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/js/functions.js') }}" type="text/javascript"></script>
	
	<!-- dashboard -->
	<script type="text/javascript">
		//maniac.loadchart();
		maniac.loadvectormap();
		maniac.loadbsslider();
		//maniac.loadrickshaw();
		maniac.loadcounter();
		maniac.loadprogress();
		maniac.loaddaterangepicker();
		showOpTooltip();
	</script> 

	@yield('scripts')

	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>