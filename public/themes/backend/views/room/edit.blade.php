@extends('layouts.backend')

@section('title')
	Room - Edit
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<!--<link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />-->
@stop

@section('page_title')
	Edit Room
@stop

@section('page_description')
	Enter room information, facility and capacity.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-room" method="post" enctype="multipart/form-data" data-op="edit">

						<div class="form-group no-margin-left no-margin-right">
							<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_name" class="col-lg-2 control-label">Room Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->name }}" type="text" class="form-control room_name" id="room_name" placeholder="room name" name="room_name">
								<span class="room_name_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_description" class="col-lg-2 control-label">Room Description <span class="required">*</span></label>
							<div class="col-lg-8">
								<textarea class="form-control room_description" id="room_description" name="room_description">{{ $room->description }}</textarea>
								<span class="room_desc_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>						

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_size" class="col-lg-2 control-label">Room Size <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->room_size }}" type="text" class="form-control room_size" id="room_size" placeholder="room size" name="room_size">
								<span class="room_size_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="number_of_beds" class="col-lg-2 control-label">Number of Beds <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->number_of_beds }}" type="text" class="form-control number_of_beds" id="number_of_beds" placeholder="number of beds" name="number_of_beds">
								<span class="num_beds_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="publish_price" class="col-lg-2 control-label">Publish Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->publish_price }}" type="text" class="form-control publish_price" id="publish_price" placeholder="published price" name="publish_price">
								<span class="pub_price_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="member_price" class="col-lg-2 control-label">Member Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->member_price }}" type="text" class="form-control member_price" id="member_price" placeholder="member price" name="member_price">
								<span class="member_price_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="adult_price" class="col-lg-2 control-label">Adult Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->adult_price }}" type="text" class="form-control adult_price" id="adult_price" placeholder="adult price" name="adult_price">
								<span class="adult_price_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="child_price" class="col-lg-2 control-label">Child Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->child_price }}" type="text" class="form-control child_price" id="child_price" placeholder="child price" name="child_price">
								<span class="child_price_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>												

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="adult_breakfast_price" class="col-lg-2 control-label">Adult Breakfast Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->adult_breakfast_price }}" type="text" class="form-control adult_breakfast_price" id="adult_breakfast_price" placeholder="adult breakfast price" name="adult_breakfast_price">
								<span class="adult_breakfast_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="child_breakfast_price" class="col-lg-2 control-label">Child Breakfast Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->child_breakfast_price }}" type="text" class="form-control child_breakfast_price" id="child_breakfast_price" placeholder="child breakfast price" name="child_breakfast_price">
								<span class="child_breakfast_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="include_breakfast" class="col-lg-2 control-label">Breakfast Included</label>
							<div class="col-lg-2">
								<select class="extra_breakfast" name="include_breakfast">
									<option value="0">No</option>
									<option value="1" {{ ($room->breakfast_included == 1) ? 'Selected' : '' }} >Yes</option>
								</select>							
							</div>
							<label for="person_breakfast" class="col-lg-2 control-label">For</label>
							<div class="col-lg-1">
								<input value="{{ $room->number_person_breakfast_included }}" type="text" class="form-control person_breakfast" id="person_breakfast" placeholder="" name="person_breakfast">
							</div>								
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="extra_bed_price" class="col-lg-2 control-label">Extra Bed Price <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->extrabed_price }}" type="text" class="form-control extra_bed_price" id="extra_bed_price" placeholder="extra bed price" name="extra_bed_price">
								<span class="extra_bed_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="extra_bed_number" class="col-lg-2 control-label">Maximum Number of Extra Beds <span class="required">*</span></label>
							<div class="col-lg-8">
								<input value="{{ $room->max_extrabed }}" type="text" class="form-control extra_bed_number" id="extra_bed_number" placeholder="extra bed maximum number" name="extra_bed_number">
								<span class="extra_bednum_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="extra_breakfast" class="col-lg-2 control-label">Extra Bed Get Breakfast</label>
							<div class="col-lg-4">
								<select class="extra_breakfast" name="extra_breakfast">
									<option value="0">No</option>
									<option value="1" {{ ($room->extrabed_get_breakfast == 1) ? 'Selected' : '' }} >Yes</option>
								</select>
							</div>						
						</div>
						-->
						
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="facilities" class="col-lg-2 control-label">Facility</label>
							<div class="col-lg-8">
								<div class="row" id="list_facility">
									@foreach($room->facilities as $index => $facility)
									<div class="col-lg-4  margin-bottom-5 facilities" id="fac-{{ $index }}">
										<div class="input-group">
											<input value="{{ $facility->name }}" name="facilities[{{ $index }}]" type="text" class="form-control no-border-right" placeholder="facility">
											<span class="input-group-btn"><button data-id_fac="fac-{{ $index }}" class="btn btn-warning delete-fac" type="button"><i class="fa fa-times"></i></button></span>
										</div>
									</div>
									@endforeach																			
								</div>
							</div>
							<div class="col-lg-2">
								<button type="button" id="add_facility" class="btn btn-primary btn-icon-left margin-right-5"><i class="fa fa-plus"></i>Add</button>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="capasities" class="col-lg-2 control-label">Capacity</label>
							<div class="col-lg-8" id="list_capacity">
							@foreach($room->capacities as $index => $capacity)
								<div class="row  margin-bottom-5 capacities" id="cap-{{ $index }}">
									<div class="col-lg-3 no-padding-right">
										<input value="{{ $capacity->adult }}" type="number" name="capacities[{{ $index }}][adult]" class="form-control" placeholder="adult">
									</div>
									<div class="col-lg-3 no-padding-right">
										<input value="{{ $capacity->child }}" type="number" name="capacities[{{ $index }}][child]" class="form-control" placeholder="child">
									</div>
									<div class="col-lg-3 no-padding-right">
										<input value="{{ $capacity->extrabed }}" type="number" name="capacities[{{ $index }}][bed]" class="form-control" placeholder="bed">
									</div>
									<div class="col-lg-3">
										<button type="button" data-id_cap="cap-{{ $index }}" class="btn btn-warning delete-cap"><i class="fa fa-times"></i></button>
									</div>
								</div>
								@endforeach							
							</div>
							<div class="col-lg-2">
								<button type="button" id="add_capacity" class="btn btn-primary btn-icon-left margin-right-5"><i class="fa fa-plus"></i>Add</button>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="profile_picture" class="col-lg-2 control-label">Room Image</label>
							<div class="col-lg-8">
								<input type="file" class="logo" id="room_image" name="room_image" data-op="edit">

								<div class="logo-container">
		                            <img width="70%" id="preview-normal" @if(Helper::getImage($room->photo_id)){{'src="' . URL::to('/') . '/uploads/room/' . Helper::getImage($room->photo_id) . '"'}}@endif />
		                            <input type="hidden" id="x" name="x" />
		                            <input type="hidden" id="y" name="y" />
		                            <input type="hidden" id="w" name="w" />
		                            <input type="hidden" id="h" name="h" />
		                        </div>
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="type" class="col-lg-2 control-label">Room Gallery</label>
							<div class="col-lg-8">
								<select class="gallery_id" name="gallery_id">
									@foreach($galleries as $gallery)
										<option @if($room->gallery_id == $gallery->id){{'selected'}}@endif value="{{ $gallery->id }}">{{ $gallery->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" class="room_id" name="id" value="{{ $room->id }}" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/room' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Done</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
	<script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
	<script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

	<!-- Currency Formater -->
	<script src="{{ Theme::asset('assets/plugins/jquery-currency/jquery.currency.js') }}" type="text/javascript"></script>

	<!--<script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>-->
	
	<script src="{{ Theme::asset('assets/js/errors/room_errors.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/room/Room.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/RoomManager.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			//resetFormElement($("#room_image"));
			
			$(".extra_breakfast, .gallery_id").selectpicker();

			$(".room_description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>
@stop