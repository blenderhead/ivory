@extends('layouts.backend')

@section('title')
	Testimoni - Edit
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.css') }}" rel="stylesheet" />

@stop

@section('page_title')
	Edit Testimoni
@stop

@section('page_description')
	Edit testimoni form.
@stop

@section('content')
	<div class="row">

		<div class="col-lg-12">

			<div class="panel">

				<div class="panel-body no-padding-left no-padding-right">
				
					<form class="form-horizontal" id="edit-testimoni" method="post" data-op="edit">

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="name" class="col-lg-2 control-label">Name <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control name" id="name" placeholder="Reviewer name" name="name" value="{{ $testimoni->name }}">
								<span class="name_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="email" class="col-lg-2 control-label">Review Link <span class="required">*</span></label>
							<div class="col-lg-8">
								<input type="text" class="form-control link" id="email" placeholder="Review link" name="link" value="{{ $testimoni->link }}">
								<span class="email_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
							</div>
						</div>

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="description" class="col-lg-2 control-label">Description <span class="required">*</span></label>
							<div class="col-lg-8">
								<textarea maxlength="140" class="form-control bs-texteditor description" rows="7" name="description">{{$testimoni->description}}</textarea>
							</div>
						</div>

						<!--
						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="assign_to" class="col-lg-2 control-label">Assign To</label>
							<div class="col-lg-8">
								<select class="assign_to" name="assign_to">
									<option value="">None</option>
									<?php $i = 0; ?>
									@foreach($home_galleries as $photo)
										<?php $i++; ?>
										<option value="{{ $photo->id }}" @if($testimoni->assign_to == $photo->id){{'selected'}}@endif>Home Slide #{{ $i }}</option>
									@endforeach
								</select>							
							</div>
						</div>
						-->

						<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
							<label for="room_size" class="col-lg-2 control-label">Publish? <span class="required">*</span></label>
							<div class="col-lg-8">
								<div class="checkbox checkbox-theme">
									<input type="checkbox" id="checkbox4" name="is_publish" @if($testimoni->is_publish){{'checked'}}@endif>
									<label for="checkbox4">Yes</label>
									<span class="publish_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
								</div>
							</div>
						</div>

						<div class="text-center margin-top-20 padding-top-20">
							<input type="hidden" id="testimoni-id" name="id" value="{{ $testimoni->id }}" />
							<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
							<a href="{{ URL::to('/') . '/backend/cms/testimoni' }}" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
						</div>

					</form>
				
				</div>
			
			</div>
			
		</div>

	</div><!-- /.row -->
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="{{ Theme::asset('assets/plugins/datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="{{ Theme::asset('assets/plugins/jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
    
    <script src="{{ Theme::asset('assets/js/errors/testi_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/testimoni/Testimoni.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/TestimoniManager.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.min.js') }}" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			/*
			$(".description").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});
			*/

			$(".assign_to").selectpicker();

		});
	</script>
@stop