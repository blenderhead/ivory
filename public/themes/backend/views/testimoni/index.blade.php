@extends('layouts.backend')

@section('title')
	Review - Index
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.css') }}" rel="stylesheet" />
@stop

@section('page_title')
	Manage Reviews
@stop

@section('page_description')
	Add, edit or delete reviews.
@stop

@section('content')
	<div class="row">

        <div class="col-lg-12">

			<div class="panel no-border ">

                <div class="panel-title">
					<button onClick="window.location='{{ URL::route('cms.review.add') }}'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-comment"></i> Add Review</button>
					<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-times"></i> Delete Selected</button>
				</div>

                <div class="panel-body padding-top-20 bg-white table_wrapper">

					{{ $tables->render() }}

            	</div>

        	</div>

    </div>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.table_wrapper table').dataTable({
		        "sPaginationType": "bootstrap",
		        "bProcessing": false,
		        "sAjaxSource": baseUrl + '/backend/cms/review/api',
		        "bServerSide": true,
		        "aoColumns": [
		          { "bSortable": false },
		          null,
		          null,
		          null,
		          null,
		          null
		        ]
		    });

		});
	</script>

	<script src="{{ Theme::asset('assets/js/classes/testimoni/Testimoni.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/classes/TestimoniManager.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/vendor/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
@stop