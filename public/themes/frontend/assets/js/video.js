$(document).ready(function() {
	
	$('#contentVideo .tab_content').hide(); // Initially hide all content
    $('#tabs_thumb li:first').attr('class', 'selected'); // Activate first tab
    $('#contentVideo div:first').fadeIn(); // Show first tab content
    $("#contentVideo").find("iframe").attr("src",$('#tabs_thumb li a:first').attr('href'));//untuk page gallery

	$('#tabs_thumb a').click(function(e) {
        e.preventDefault();
        $('#contentVideo .tab_content').hide(); //Hide all content
        $('#tabs_thumb li').attr('class', ''); //Reset class's
        $(this).parent().attr('class', 'selected'); // Activate this
        $('#' + $(this).attr('rel')).delay(100).fadeIn(); // Show content for current tab
        $("#contentVideo").find("iframe").attr("src", $(this).attr('href'));//untuk page gallery
        return false;
    });
});