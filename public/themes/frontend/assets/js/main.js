$(document).ready(function() {

    $(".room, .adult, .child").selectpicker();

    var today = new Date();

    /*
    $('.check-in-date').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: today,
    });

    $('.check-out-date').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: +1,
    });

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    
    $('.check-in-date').datepicker("setDate", today);
    
    $('.check-out-date').datepicker("setDate", tomorrow);

    $('.ui-datepicker-div').css('zIndex', 999999);
    */
    
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
            $(this).toggleClass('open');           
        }
    );

    $('.toggle_box .switch').click(function() {
        $(this).toggleClass('active')
        .parents('.toggle_box')
        .children('.content').slideToggle('fast');
    });

    $('.refresh_captcha').click(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: baseUrl + '/service/get_captcha',
            success: function(data) {
                if(!data.error)
                {
                    $('.captcha_image').attr('src', data.image);                    
                }
            },
        });
    });

    $(".content-desc").mCustomScrollbar({
        theme:"dark-thin",
        scrollInertia:400
    });
});

