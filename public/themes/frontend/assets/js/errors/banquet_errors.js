function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.name-error').removeClass('hide');
        $('.name-error').html(data.data.errors.name);
        $('.name').addClass("border-error");
    }
    else
    {
        $('.name-error').addClass('hide');
        $('.name-error').empty();
        $('.name').removeClass("border-error");
    }

    if(data.data.errors.email != undefined)
    {
        $('.email-error').removeClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').addClass("border-error");
    }
    else
    {
        $('.email-error').addClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').removeClass("border-error");
    }

    if(data.data.errors.phone != undefined)
    {
        $('.phone-error').removeClass('hide');
        $('.phone-error').html(data.data.errors.phone);
        $('.phone').addClass("border-error");
    }
    else
    {
        $('.phone-error').addClass('hide');
        $('.phone-error').html(data.data.errors.phone);
        $('.phone').removeClass("border-error");
    }

    if(data.data.errors.start_date != undefined)
    {
        $('.start-date-error').removeClass('hide');
        $('.start-date-error').html(data.data.errors.start_date);
        $('.check-in-date').addClass("border-error");
    }
    else
    {
        $('.start-date-error').addClass('hide');
        $('.start-date-error').html(data.data.errors.start_date);
        $('.check-in-date').removeClass("border-error");
    }

    if(data.data.errors.end_date != undefined)
    {
        $('.end-date-error').removeClass('hide');
        $('.end-date-error').html(data.data.errors.end_date);
        $('.check-out-date').addClass("border-error");
    }
    else
    {
        $('.end-date-error').addClass('hide');
        $('.end-date-error').html(data.data.errors.end_date);
        $('.check-out-date').removeClass("border-error");
    }
}