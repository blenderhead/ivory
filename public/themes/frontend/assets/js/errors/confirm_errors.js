function showInlineError(data)
{
    if(data.data.errors.invoice_id != undefined)
    {
        $('.invoice-error').removeClass('hide');
        $('.invoice-error').html(data.data.errors.invoice_id);
        $('.invoice_id').addClass("border-error");
    }
    else
    {
        $('.invoice-error').addClass('hide');
        $('.invoice-error').empty();
        $('.invoice_id').removeClass("border-error");
    }

    if(data.data.errors.transfer_date != undefined)
    {
        $('.transfer-date-error').removeClass('hide');
        $('.transfer-date-error').html(data.data.errors.transfer_date);
        $('.transfer_date').addClass("border-error");
    }
    else
    {
        $('.transfer-date-error').removeClass('hide');
        $('.transfer-date-error').empty();
        $('.transfer_date').removeClass("border-error");
    }

    if(data.data.errors.transfer_amount != undefined)
    {
        $('.transfer-amount-error').removeClass('hide');
        $('.transfer-amount-error').html(data.data.errors.transfer_amount);
        $('.transfer_amount').addClass("border-error");
    }
    else
    {
        $('.transfer-amount-error').removeClass('hide');
        $('.transfer-amount-error').empty();
        $('.transfer_amount').removeClass("border-error");
    }

    if(data.data.errors.transfer_account != undefined)
    {
        $('.transfer-account-error').removeClass('hide');
        $('.transfer-account-error').html(data.data.errors.transfer_account);
        $('.transfer_account').addClass("border-error");
    }
    else
    {
        $('.transfer-account-error').removeClass('hide');
        $('.transfer-account-error').empty();
        $('.transfer_account').removeClass("border-error");
    }

    if(data.data.errors.transfer_account_number != undefined)
    {
        $('.transfer-accountnum-error').removeClass('hide');
        $('.transfer-accountnum-error').html(data.data.errors.transfer_account);
        $('.transfer_account_number').addClass("border-error");
    }
    else
    {
        $('.transfer-accountnum-error').removeClass('hide');
        $('.transfer-accountnum-error').empty();
        $('.transfer_account_number').removeClass("border-error");
    }

    if(data.data.errors.email != undefined)
    {
        $('.email-error').removeClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').addClass("border-error");
    }
    else
    {
        $('.email-error').removeClass('hide');
        $('.email-error').empty();
        $('.email').removeClass("border-error");
    }

    if(data.data.errors.phone != undefined)
    {
        $('.phone-error').removeClass('hide');
        $('.phone-error').html(data.data.errors.phone);
        $('.phone').addClass("border-error");
    }
    else
    {
        $('.phone-error').removeClass('hide');
        $('.phone-error').empty();
        $('.phone').removeClass("border-error");
    }
}

function showError(data)
{
    /*
    var msg = '';

    if (data.error == '9107') 
    {
      msg = '<div><strong>' + data.message + '</strong></div>';

      for (var x in data.data.errors) {
        msg += '<div><strong><em>- ' + data.data.errors[x][0] + '</em></strong></div>';
      }

      bootbox.alert(msg, function() {});
    }
    else if (data.error == '1000') 
    {
      msg = '<div><p><strong>' + data.message + '</strong></p></div>';
      msg += '<div><strong><em>' + data.data.errors + '</em></strong></div>';

      bootbox.alert(msg, function() {});
    }
    else 
    {
      bootbox.alert(data.message, function() {});
    }
    */
    $('.error-system').removeClass('hide');
    $('.error-system').html(data.data.errors);
}