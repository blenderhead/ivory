function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.name-error').removeClass('hide');
        $('.name-error').html(data.data.errors.name);
        $('.name').addClass("border-error");
    }
    else
    {
        $('.name-error').addClass('hide');
        $('.name-error').empty();
        $('.name').removeClass("border-error");
    }

    if(data.data.errors.email != undefined)
    {
        $('.email-error').removeClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').addClass("border-error");
    }
    else
    {
        $('.email-error').addClass('hide');
        $('.email-error').empty();
        $('.email').removeClass("border-error");
    }

    if(data.data.errors.subject != undefined)
    {
        $('.subject-error').removeClass('hide');
        $('.subject-error').html(data.data.errors.subject);
        $('.subject').addClass("border-error");
    }
    else
    {
        $('.email-error').addClass('hide');
        $('.email-error').empty();
        $('.subject').removeClass("border-error");
    }

    if(data.data.errors.message != undefined)
    {
        $('.message-error').removeClass('hide');
        $('.message-error').html(data.data.errors.message);
        $('.message').addClass("border-error");
    }
    else
    {
        $('.captcha-error').addClass('hide');
        $('.captcha-error').empty();
        $('.captcha').removeClass("border-error");
    }

    if(data.data.errors.captcha != undefined)
    {
        $('.captcha-error').removeClass('hide');
        $('.captcha-error').html(data.data.errors.captcha);
        $('.captcha').addClass("border-error");
    }
    else
    {
        $('.captcha-error').addClass('hide');
        $('.captcha-error').empty();
        $('.captcha').removeClass("border-error");
    }
}

function showError(data)
{
    var msg = '';

    if (data.error == '9107') 
    {
      msg = '<div><strong>' + data.message + '</strong></div>';

      for (var x in data.data.errors) {
        msg += '<div><strong><em>- ' + data.data.errors[x][0] + '</em></strong></div>';
      }

      bootbox.alert(msg, function() {});
    }
    else if (data.error == '1000') 
    {
      msg = '<div><p><strong>' + data.message + '</strong></p></div>';
      msg += '<div><strong><em>' + data.data.errors + '</em></strong></div>';

      bootbox.alert(msg, function() {});
    }
    else 
    {
      bootbox.alert(data.message, function() {});
    }
}