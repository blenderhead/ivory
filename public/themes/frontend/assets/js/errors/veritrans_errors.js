function showInlineError(data)
{
    if(data.data.errors.card_owner != undefined)
    {
        $('.card-owner-error').removeClass('hide');
        $('.card-owner-error').html(data.data.errors.card_owner);
        $('.card_owner').addClass("border-error");
    }
    else
    {
        $('.card-owner-error').addClass('hide');
        $('.card-owner-error').empty();
        $('.card_owner').removeClass("border-error");
    }

    if(data.data.errors.card_number != undefined)
    {
        $('.card-number-error').removeClass('hide');
        $('.card-number-error').html(data.data.errors.card_number);
        $('.card_number').addClass("border-error");
    }
    else
    {
        $('.card-number-error').addClass('hide');
        $('.card-number-error').empty();
        $('.card_number').removeClass("border-error");
    }

    if(data.data.errors.card_exp_month != undefined)
    {
        $('.month-error').removeClass('hide');
        $('.month-error').html(data.data.errors.card_exp_month);
        $('.card_exp_month').addClass("border-error");
    }
    else
    {
        $('.month-error').addClass('hide');
        $('.month-error').empty();
        $('.card_exp_month').removeClass("border-error");
    }

    if(data.data.errors.card_exp_year != undefined)
    {
        $('.year-error').removeClass('hide');
        $('.year-error').html(data.data.errors.card_exp_month);
        $('.card_exp_year').addClass("border-error");
    }
    else
    {
        $('.year-error').addClass('hide');
        $('.year-error').empty();
        $('.card_exp_year').removeClass("border-error");
    }

    if(data.data.errors.card_cvv != undefined)
    {
        $('.cvv-error').removeClass('hide');
        $('.cvv-error').html(data.data.errors.card_cvv);
        $('.card_cvv').addClass("border-error");
    }
    else
    {
        $('.cvv-error').addClass('hide');
        $('.cvv-error').empty();
        $('.card_cvv').removeClass("border-error");
    }
}