function showInlineError(data)
{
    if(data.data.errors.first_name != undefined)
    {
        $('.first-name-error').removeClass('hide');
        $('.first-name-error').html(data.data.errors.first_name);
        $('.first_name').addClass("border-error");
    }
    else
    {
        $('.first-name-error').addClass('hide');
        $('.first-name-error').empty();
        $('.first_name').removeClass("border-error");
    }

    if(data.data.errors.email != undefined)
    {
        $('.email-error').removeClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').addClass("border-error");
    }
    else
    {
        $('.email-error').addClass('hide');
        $('.email-error').html(data.data.errors.email);
        $('.email').removeClass("border-error");
    }

    if(data.data.errors.phone_number != undefined)
    {
        $('.phone-error').removeClass('hide');
        $('.phone-error').html(data.data.errors.phone_number);
        $('.phone').addClass("border-error");
    }
    else
    {
        $('.phone-error').addClass('hide');
        $('.phone-error').html(data.data.errors.phone_number);
        $('.phone').removeClass("border-error");
    }
}