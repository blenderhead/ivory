$(document).ready(function() {

	$('#form-konfirmasi').submit(function(e) {

        e.preventDefault();
        
        var url = baseUrl + '/confirmation';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            /*
            beforeSend: function() {
                $('.booking-calendar').waitMe({
                    effect : 'stretch',
                    text : 'Processing...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.booking-calendar').waitMe('hide');
            },
            */
            success: function(data) {

                if(data.error)
                {
                    if(data.error == '9107')
                    {
                        showInlineError(data);
                    }
                    else
                    {
                        showError(data);
                    }
                    //
                }
                else
                {
                    bootbox.alert("Thank you. Your confirmation will be processed shortly", function() {
                        window.location.replace(baseUrl);
                    });
                }

            }
        });
    });
    
});