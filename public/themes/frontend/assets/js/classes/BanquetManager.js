function emptyForm()
{
    $('.title').val('mr');
    $('.name').val('');
    $('.email').val('');
    $('.phone').val('');
    $('.check-in-date').val('');
    $('.check-out-date').val('');
    $('.message').val('');
}

$(document).ready(function() {

	$('#banquet-enquiry').submit(function(e) {

        e.preventDefault();
        
        var url = baseUrl + '/banquet/enquery';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('#banquet-enquiry').waitMe({
                    effect : 'stretch',
                    text : 'Processing...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('#banquet-enquiry').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    $('.booking-info').html('Error occured. Please check your form again.');
                }
                else
                {
                    $('.booking-info').empty();
                    $('.mail-success').html('Thank you. Your enquiry is sent successfully');
                    emptyForm();
                }

            }
        });
    });
    
});