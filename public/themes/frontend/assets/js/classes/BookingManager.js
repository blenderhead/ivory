$(document).ready(function() {

	$('#add-booking').submit(function(e) {

        e.preventDefault();

        $('.booking-info').empty();
        
        var op = $(this).data('op');

        var url = baseUrl + '/booking';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('#add-booking').waitMe({
                    effect : 'stretch',
                    text : 'Processing...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('#add-booking').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    if(data.error == '1000')
                    {
                        $('.booking-info').html('Code is not acceptable.');
                    }
                    else
                    {
                        showInlineError(data);
                        $('.booking-info').html('Error occured. Please check your form again.');
                    }
                }
                else
                {
                    $('.booking-info').empty();
                    window.location.replace(data.data.redirect);
                }

            }
        });
    });
    
});