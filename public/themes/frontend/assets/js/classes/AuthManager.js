function showError(data)
{
    var msg = '';

    if (data.error == '9107') 
    {
      msg = '<div><strong>' + data.message + '</strong></div>';

      for (var x in data.data.errors) {
        msg += '<div><strong><em>- ' + data.data.errors[x][0] + '</em></strong></div>';
      }

      bootbox.alert(msg, function() {});
    }
    else if (data.error == '1000') 
    {
      msg = '<div><p><strong>' + data.message + '</strong></p></div>';
      msg += '<div><strong><em>' + data.data.errors + '</em></strong></div>';

      bootbox.alert(msg, function() {});
    }
    else 
    {
      bootbox.alert(data.message, function() {});
    }
}

$(document).ready(function() {
    
    $('#login-form').submit(function(e) {

        e.preventDefault();

        var form_data = new FormData(this);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/login',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    window.location.replace(baseUrl);
                }

            }
        });
    });

    $('#reset-form').submit(function(e) {

        e.preventDefault();

        var form_data = new FormData(this);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/forget_password',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    $('.reset-text').html('An email containing link to reset your password has been sent.');
                    $('.reset-body').hide();
                }

            }
        });
    });

    $('#reset-pass-form').submit(function(e) {

        e.preventDefault();

        var form_data = new FormData(this);
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseUrl + '/reset_password',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(data) {

                if(data.error)
                {
                    showError(data);
                }
                else
                {
                    $('.reset-text').html('Your password has been changed successfully.');
                    $('.reset-body').hide();
                }

            }
        });
    });

});