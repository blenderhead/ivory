function emptyForm()
{
    $('.name').val('');
    $('.email').val('');
    $('.subject').val('');
    $('.message').val('');
}

$(document).ready(function() {

	$('#contact').submit(function(e) {

        e.preventDefault();

        var url = baseUrl + '/contact-us';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.form-contact').waitMe({
                    effect : 'stretch',
                    text : 'Sending email...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.form-contact').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    //showError(data);
                }
                else
                {
                    //bootbox.alert('Thank you. Your email is sent successfully', function() {});
                    $('.mail-success').html('Thank you. Your email is sent successfully');
                    emptyForm();
                }

            }
        });
    });
    
});