$(document).ready(function() {

	$('#add-veritrans').submit(function(e) {

        e.preventDefault();
        
        var op = $(this).data('op');

        var url = baseUrl + '/send-secure-payment';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('.payment-block').waitMe({
                    effect : 'stretch',
                    text : 'Processing...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('.payment-block').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    //showError(data);
                }
                else
                {
                    window.location.replace(data.data.redirect);
                }

            }
        });
    });
    
});