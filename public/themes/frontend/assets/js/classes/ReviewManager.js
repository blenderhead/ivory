function emptyForm()
{
    $('.name').val('');
    $('.email').val('');
    $('.description').val('');
    $('.message').val('');
}

$(document).ready(function() {

	$('#add-review').submit(function(e) {

        e.preventDefault();
        
        var url = baseUrl + '/review';

        var form_data = new FormData(this);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: form_data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('#add-review').waitMe({
                    effect : 'stretch',
                    text : 'Processing...',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000',
                    sizeW : '',
                    sizeH : ''
                });
            },
            complete: function(){
                $('#add-review').waitMe('hide');
            },
            success: function(data) {

                if(data.error)
                {
                    showInlineError(data);
                    $('.booking-info').html('Error occured. Please check your form again.');
                }
                else
                {
                    $('.booking-info').empty();
                    $('.mail-success').html('Thank you. Your review is sent successfully');
                    emptyForm();
                }

            }
        });
    });
    
});