<div class="main-image site-width center">
	<div class="connected-carousels">
        <div class="stage">
            <div class="carousel carousel-stage">
                <ul>
                	<li>
	                    <img src="{{ URL::to('/') . '/uploads/defaults/placeholder.jpg' }}" />
	                </li>
                </ul>
            </div>
            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
        </div>
    </div>
</div>