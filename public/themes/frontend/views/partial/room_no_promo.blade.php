<article class="campaign">

    <div class="line1 clearfix">

        <div class="image">
            <!--<img @if(Helper::getImage($room->photo_id)){{'src="' . URL::to('/') . '/uploads/room/' . Helper::getImage($room->photo_id) . '"'}}@endif alt="{{ $room->name }}">-->
             <img @if(!$room->photos->isEmpty()){{'src="' . URL::to('/') . '/uploads/gallery/' . $room->photos->get(0)->file . '"'}}@endif alt="{{ $room->name }}">
        </div>
                                
        <div class="campaign_title">
            <h2 class="room_name">{{ $room->name }}</h2>
            <p>Room Size:&nbsp;{{$room->room_size}}m<sup>2</sup></p>
            <p class="breakfast_includ">Breakfast included for {{ $room->number_person_breakfast_included }} person</p>
            <p>Children allowed: 2 - 10 tahun</p>
            <p>Minimum stay: 1</p>
        </div>

        <div class="campaign_price">
            <p class="average">
                <p class="average">
                    <span class="price"> {{ Helper::moneyFormat(($room->room_price + $room->service_price + $room->tax_price) * $query['room']) }}</span><!--<span class="text">rata<sup>2</sup>/malam</span>-->
                    <span class="clearfix"></span>
                </p>

                <p class="total">Total for 1 room/night: {{ Helper::moneyFormat($room->room_price + $room->service_price + $room->tax_price) }}</p>
                <p><a class="btn-style-1 btn btn-icon-left margin-right-5" href="{{ URL::to('booking') }}?room_id={{ $room->id }}&query={{ base64_encode($query['stay_start_date']->toDateString().'|'.$query['stay_end_date']->toDateString().'|'.$query['room'].'|'.$query['adult'].'|'.$query['child']) }}" title="Book Now">Book Now</a></p>

            </p>

        </div>

        <div class="campaign_detail toggle_box clear padding-top-20">

            <p class="details_switch"><span class="switch"><i class="icon"></i>Room Details</span></p>
            
            <div class="content clearfix" style="display: none;">
                <h3 class="content-title">Facilities</h3>
                <ul class="facilities clearfix">
                    @foreach($room->facilities()->get() as $facility)
                        <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt="checked">{{ $facility->name }}</li>
                    @endforeach
                </ul>
            </div>

        </div>

        <div class="campaign_detail toggle_box clear">
            <p class="details_switch"><span class="switch"><i class="icon"></i>Cancellation Policy</span></p>

            <div class="content clearfix" style="display: none;">
                <ul class="cancellation_policy clearfix">
                    @foreach($room->cancellations()->get() as $cancellation)
                        @if($cancellation->level1Policy())
                            <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt="">{{ $cancellation->level1Policy()->name }}</li>
                        @endif
                        
                        @if($cancellation->level2Policy())
                            <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt="">{{ $cancellation->level2Policy()->name }}</li>
                        @endif

                        @if($cancellation->level3Policy())
                            <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt="">{{ $cancellation->level3Policy()->name }}</li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>

    </div>

</article>