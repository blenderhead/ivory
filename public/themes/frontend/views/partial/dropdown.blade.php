<li class="dropdown">
  <a href="#" class="dropdown-toggle site-color font-12" data-toggle="dropdown">{{ strtoupper($menu_name) }}</a>
  <ul class="site-dropdown dropdown-menu">
    @foreach($data as $key => $value)
    	<?php $key++; ?>
        <li class="@if($key != count($data)){{'border-1-white'}}@endif">
            <a href="{{ URL::to('/') . '/' . $route . '/'. $value->slug }}" class="font-12 new-site-color">{{ strtoupper($value->name) }}</a>
        </li>
    @endforeach
  </ul>
</li>