<div class="booking-calendar site-width center" @if($current_route_name == 'home' || $current_route_name == 'room' || $current_route_name == 'gallery' || $current_route_name == 'around-us' || $current_route_name == 'cafe.menu' || $current_route_name == 'cafe.moment'){{'style="margin-top:120px;"'}}@endif>

    <div class="calendar-wrapper">
        {{ Form::open(['route' => 'room.search', 'method' => 'GET']) }}
        <div class="row">

            <div class="col-md-2">
                
                <div class="form-group">
                    <label class="site-maroon font-12">CHECK-IN DATE</label>
                    <div class="input-group input-group site-maroon font-12">
                      <span class="input-group-addon"><img src="{{ Theme::asset('assets/img/cal.png') }}"></span>
                    @if(isset($query))
                        <input value="{{ $query['stay_start_date']->format('m/d/Y') }}" name="stay_start_date" type="text" class="form-control check-in-date">
                    @else
                        <input name="stay_start_date" type="text" class="form-control check-in-date">
                    @endif
                    </div>
                </div>


            </div>

            <div class="col-md-1">
                
                <div class="cal">
                    <img src="{{ Theme::asset('assets/img/cal.png') }}">
                </div>

            </div>

            <div class="col-md-2">
                
                <div class="form-group">

                    <label class="site-maroon font-12">CHECK-OUT DATE</label>

                    <div class="input-group input-group site-maroon font-12">
                      <span class="input-group-addon"><img src="{{ Theme::asset('assets/img/cal.png') }}"></span>
                        @if(isset($query))
                            <input value="" name="stay_end_date" type="text" class="form-control check-out-date">
                        @else
                            <input name="stay_end_date" type="text" class="form-control check-out-date">
                        @endif                      
                    </div>                    

                </div>

            </div>

            <div class="col-md-1">
                
                <div class="cal">
                    <img src="{{ Theme::asset('assets/img/cal.png') }}">
                </div>

            </div>

            <div class="col-md-4">
                
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail" class="site-maroon font-12">ROOM</label>
                            @if(isset($query))
                                {{ Form::selectRange('room', 1, 9, $query['room'], ['class' => 'form-control room']) }}
                            @else
                                {{ Form::selectRange('room', 1, 9, null, ['class' => 'form-control room']) }}
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail" class="site-maroon font-12">ADULT</label>
                            @if(isset($query))
                                {{ Form::selectRange('adult', 1, 9, $query['adult'], ['class' => 'form-control adult']) }}
                            @else
                                {{ Form::selectRange('adult', 1, 9, null, ['class' => 'form-control adult']) }}
                            @endif                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail" class="site-maroon font-12">CHILD</label>
                            @if(isset($query))
                                {{ Form::selectRange('child', 0, 9, $query['child'], ['class' => 'form-control adult']) }}
                            @else
                                {{ Form::selectRange('child', 0, 9, null, ['class' => 'form-control adult']) }}                            
                            @endif                            
                        </div>
                    </div>

                </div>

            </div>

            <div class="search col-md-2 pull-right">
                
                <div class="form-group pull-right">
                    <label></label>
                    <div class="search-room-button">
                        {{ Form::submit('Search', ['class' => 'btn-style-1 btn btn-icon-left margin-right-5', 'id' => 'search-room']) }}
                    </div>
                </div>

            </div>
            {{ Form::close() }}

        </div>

    </div>

<div>