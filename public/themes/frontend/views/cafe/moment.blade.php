@extends('layouts.frontend')

@section('title')
	Cafe Moment Gallery
@stop

@section('styles')
	<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/lightbox/css/lightbox.css') }}" />

	<style type="text/css">
		.photoset-grid {
			width: 960px;
			padding: 40px;
		}
	</style>
@stop

@section('content')
	
	@if(isset($response->data))
		@if(count($response->data) > 0)
			<div class="photoset-grid" data-layout="6">
				@foreach($response->data as $timeline)
					<a href="{{ $timeline->images->standard_resolution->url }}" data-lightbox="ivory" data-title="{{ $timeline->caption->text }}">
						<img width="100" height="100" style="display: inline-block; margin-bottom: 3px;" src="{{ $timeline->images->thumbnail->url }}">
					</a>
				@endforeach
			</div>
		@else
			<div class="photoset-grid" data-layout="6">
				<p>Recent tagged media unavailable</p>
			</div>
		@endif
	@else
		@include('partial.empty_gallery')
	@endif
	
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/lightbox/js/lightbox.js') }}" type="text/javascript"></script>
@stop