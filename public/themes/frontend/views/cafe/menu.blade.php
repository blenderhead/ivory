@extends('layouts.frontend')

@section('title')
	Cafe Menu Gallery
@stop

@section('styles')
	<style type="text/css">
        .socmed-footer {
          margin-top: 150px;
        }
    </style>
@stop

@section('content')
	
	@if($photos)

		<div class="main-image site-width center">
			<div class="image-holder width-full">
			
				<div class="connected-carousels">
			        <div class="stage">
			            <div class="carousel carousel-stage">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
			            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
			        </div>

			        <div class="navigation">
			            <a href="#" class="prev prev-navigation">&lsaquo;</a>
			            <a href="#" class="next next-navigation">&rsaquo;</a>
			            <div class="carousel carousel-navigation">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="100" height="49" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			        </div>
			    </div>

			</div>

			<div class="image-holder width-halft">
			    <div id="carousel-example-generic" class=" crousel-bootstrap carousel slide" data-ride="carousel">
			    	<ol class="carousel-indicators">
			    	@foreach($photos as $index => $photo)
			    		<li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" @if($index == 0) class="active" @endif></li>
			    	@endforeach
			    	</ol>
			    	<div class="carousel-inner" role="listbox">
			    		@foreach($photos as $index => $photo)
			    		<div class="item @if($index == 0) active @endif">
			    			<img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" class="img-crousel">
			    			<div class="carousel-caption">
			    			</div>
			    		</div>
			    		@endforeach
			    	</div>
			    	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    		<span class="sr-only">Previous</span>
			    	</a>
			    	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    		<span class="sr-only">Next</span>
			    	</a>
			    </div>					
			</div>
		</div>
	@else
		@include('partial.empty_gallery')
	@endif
	
@stop

@section('scripts')
@stop