@extends('layouts.frontend')

@section('title')
	Cafe Promo
@stop

@section('styles')
    <style type="text/css">
        .socmed-footer {
          margin-top: 150px;
        }
    </style>
@stop

@section('content')

	@if(!$promos->isEmpty())

		<div class="main-image site-width center">
            <div class="image-holder width-full">
                <div class="connected-carousels">
                    <div class="stage">
                        <div class="carousel carousel-stage">
                            <ul>
                                @foreach($promos as $promo)
                                    <li>
                                        <img @if(Helper::getImage($promo->photo_id)){{'src="' . URL::to('/') . '/uploads/cafe_promo/' . Helper::getImage($promo->photo_id) . '"'}}@endif width="960" height="465" />
                                        
                                        <!--
                                        <div class="around-overlay">

                                            <div class="row">

								                <div class="center-vertical">
								                    <p>{{ $promo->name }}</p>
								                </div>

								                <div class="col-md-6 info-right pull-right">
								                    <div class="center-vertical">
								                        <p>Only for:</p>
								                        <p class="font-20">{{ $promo->price }} IDR</p>
								                    </div>
								                </div>

								            </div>

                                        </div>
                                        -->
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
                        <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
                    </div>

                    <div class="navigation">
                        <a href="#" class="prev prev-navigation">&lsaquo;</a>
                        <a href="#" class="next next-navigation">&rsaquo;</a>
                        <div class="carousel carousel-navigation">
                            <ul>
                                @foreach($promos as $promo)
                                    <li>
                                        <img @if(Helper::getImage($promo->photo_id)){{'src="' . URL::to('/') . '/uploads/cafe_promo/' . Helper::getImage($promo->photo_id) . '"'}}@endif width="100" height="49" />
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="image-holder width-halft">
                <div id="carousel-example-generic" class=" crousel-bootstrap carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                    @foreach($promos as $index => $promo)
                        <li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" @if($index == 0) class="active" @endif></li>
                    @endforeach
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        @foreach($promos as $index => $promo)
                        <div class="item @if($index == 0) active @endif">
                            <img @if(Helper::getImage($promo->photo_id)){{'src="' . URL::to('/') . '/uploads/cafe_promo/' . Helper::getImage($promo->photo_id) . '"'}}@endif  class="img-crousel">
                            <!--
                            <div class="carousel-caption">
                                <p>{{ $promo->name }}</p>
                                <p>Only for:</p>
                                <p>{{ $promo->price }} IDR</p>
                            </div>
                            -->
                        </div>
                        @endforeach
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>                  
            </div>
        </div>
	@else
		@include('partial.empty_gallery')
	@endif

@stop

@section('scripts')
@stop