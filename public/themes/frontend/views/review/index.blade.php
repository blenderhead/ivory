@extends('layouts.frontend')

@section('title')
    Review Your Stay Here
@stop

@section('styles')
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/booking_form.css') }}">
    <link href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />

    <style type="text/css">
        .socmed-footer {
          margin-top: 80px;
        }
    </style>
@stop

@section('content')
    <div class="booking-calendar site-width center" style="padding-bottom: 20px;">

        <div class="col-lg-8" style="padding-left: 0px;">

            <form id="add-review" method="POST">

                <section class="booking_details block">

                    <h2 class="block_title">Review Form</h2>

                    <div class="form-group">
                        <label for="name" class="booking_label"><span class="required">*</span> <span class="required-info">required</span></label>
                    </div>

                    <p class="mail-success site-maroon"></p>

                    <div class="form-group">
                        <label for="title" class="booking_label">Title</label>
                        <select class="form-control title" name="title" style="width: 100px;">
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                        </select>   
                    </div>

                    <div class="form-group">
                        <label for="name" class="booking_label">Name <span class="required">*</span></label>
                        <input type="text" name="name" class="form-control name" placeholder="full name">
                        <span class="name-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="email" class="booking_label">Email <span class="required">*</span></label>
                        <input type="text" name="email" class="form-control email" placeholder="email">
                        <span class="email-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="description" class="booking_label">Your Review</label>
                        <textarea maxlength="140" class="form-control description" id="description" name="description" rows="3"></textarea>
                        <span class="description-error inline-error hide"></span>
                    </div>            

                    <div class="clear"></div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="captcha_image" src="{{ Captcha::img() }}" />
                                <a title="refresh captcha" class="refresh_captcha" href="#"><img width="15" height="15" src="{{ Theme::asset('assets/img/refresh.png') }}" /></a>
                            </div>
                            <div class="col-md-4 pull-right">
                                <input type="text" name="captcha" class="form-control captcha" placeholder="Enter capthca code">
                            </div>
                        </div>
                        <span class="captcha-error inline-error hide">error</span>
                    </div>

                </section>

                <div class="form-group">
                    <button type="submit" class="btn btn-style-1">Submit</button>
                    <span class="booking-info inline-error color-red-800"></span>
                </div>  

            </form>

        </div>

    </div>

    <div class="clear"></div>
@stop

@section('scripts')
    <script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/errors/review_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/ReviewManager.js') }}" type="text/javascript"></script>
@stop
