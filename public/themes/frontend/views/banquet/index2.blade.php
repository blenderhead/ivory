@extends('layouts.frontend')

@section('title')
	Room - {{ $room->name }}
@stop

@section('styles')
	<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.css') }}">
@stop

@section('content')
<section class="inner-content">
	<div class="row">
		<div class="col-md-8 width-full">
			<div class="holder">
				<div class="connected-carousels">
			        <div class="stage">
			            <div class="carousel carousel-stage">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="550" height="269" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			            <!--
			            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
			            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
			        	-->
			        </div>

			        <div class="navigation">
			            <a href="#" class="prev prev-navigation">&lsaquo;</a>
			            <a href="#" class="next next-navigation">&rsaquo;</a>
			            <div class="carousel carousel-navigation">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="100" height="49" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			        </div>
			    </div>		    
			</div>
		</div>
		<div class="col-md-8 col-xs8 width-halft">
		    <div id="carousel-example-generic" class=" crousel-bootstrap carousel slide" data-ride="carousel">
		    	<ol class="carousel-indicators">
		    	@foreach($photos as $index => $photo)
		    		<li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" @if($index == 0) class="active" @endif></li>
		    	@endforeach
		    	</ol>
		    	<div class="carousel-inner" role="listbox">
		    		@foreach($photos as $index => $photo)
		    		<div class="item @if($index == 0) active @endif">
		    			<img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" class="img-crousel">
		    			<div class="carousel-caption">
		    			</div>
		    		</div>
		    		@endforeach
		    	</div>
		    	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    		<span class="sr-only">Previous</span>
		    	</a>
		    	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    		<span class="sr-only">Next</span>
		    	</a>
		    </div>			
		</div>
		<div class="col-md-4 content-full">
			<div class="content-desc desc-text">
				<h3 class="room_name">{{ strtoupper($room->name) }}</h3>
				<p class="room_desc">{{ $room->description }}</p>
				<p>
                    <span class="bold"><strong>Room size</strong></span><br/>
                    <span class="they_say">{{ $room->size }}</span>
                </p>
                <p>
                    <span class="bold"><strong>Features</strong></span><br/>
                    <span class="they_say">{{ $room->setup }}</span>
                </p>
			</div>

			<a href="{{ URL::route('banquet.enquery') }}" class="btn btn-style-1 banquet-inquiry">MAKE BANQUET INQUIRY</a>
		</div>
		<div class="col-md-4 content-halft">
			<div class="content-desc desc-text">
				<h3 class="room_name">{{ strtoupper($room->name) }}</h3>
				<p class="room_desc">{{ $room->description }}</p>
				<p>
                    <span class="bold"><strong>Room size</strong></span><br/>
                    <span class="they_say">{{ $room->size }}</span>
                </p>
                <p>
                    <span class="bold"><strong>Features</strong></span><br/>
                    <span class="they_say">{{ $room->setup }}</span>
                </p>
			</div>

			<a href="{{ URL::route('banquet.enquery') }}" class="btn btn-style-1 banquet-inquiry">MAKE BANQUET INQUIRY</a>
		</div>		
	</div>
</section>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.js') }}"></script>
@stop