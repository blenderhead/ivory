@extends('layouts.frontend')

@section('title')
    Banquet Inquery
@stop

@section('styles')
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/booking_form.css') }}">
    <link href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
@stop

@section('content')
    <div class="booking-calendar site-width center" style="padding-bottom: 20px;">

        <div class="col-lg-8" style="padding-left: 0px;">

            <form id="banquet-enquiry" method="POST">

                <section class="booking_details block">

                    <h2 class="block_title">Banquet Room Inquery Form</h2>

                    <div class="form-group">
                        <label for="name" class="booking_label"><span class="required">*</span> <span class="required-info">required</span></label>
                    </div>

                    <p class="mail-success site-maroon"></p>
                    
                    <div class="form-group">
                        <label for="title" class="booking_label">Title</label>
                        <select class="form-control title" name="title" style="width: 100px;">
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                        </select>   
                    </div>

                    <div class="form-group">
                        <label for="name" class="booking_label">Name <span class="required">*</span></label>
                        <input type="text" name="name" class="form-control name" placeholder="full name">
                        <span class="name-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="email" class="booking_label">Email <span class="required">*</span></label>
                        <input type="text" name="email" class="form-control email" placeholder="email">
                        <span class="email-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="booking_label">Phone <span class="required">*</span></label>
                        <input type="text" name="phone" class="form-control phone" placeholder="mobile phone">
                        <span class="phone-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="room" class="booking_label">Room</label>
                        <select class="form-control" name="room">
                            @foreach($rooms as $room)
                                <option value="{{ $room->name }}">{{ $room->name }}</option>
                            @endforeach
                        </select>   
                    </div>

                    <div class="form-group">
                        <label for="start_date" class="booking_label">Booking Start Date <span class="required">*</span></label>
                        <input name="start_date" type="text" class="form-control check-in-date">
                        <span class="start-date-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="end_date" class="booking_label">Booking End Date <span class="required">*</span></label>
                        <input name="end_date" type="text" class="form-control check-out-date">
                        <span class="end-date-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="message" class="booking_label">Message</label>
                        <textarea class="form-control message" id="message" name="message" rows="7"></textarea>
                    </div>            

                    <div class="clear"></div>

                </section>

                <div class="form-group">
                    <button type="submit" class="btn btn-style-1">Submit</button>
                    <span class="booking-info inline-error color-red-800"></span>
                </div>  

            </form>

        </div>

    </div>
@stop

@section('scripts')
    <script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/errors/banquet_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/BanquetManager.js') }}" type="text/javascript"></script>
@stop
