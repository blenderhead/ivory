<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>@yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!--<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.css') }}">-->
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap-theme.min.css') }}">
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" />
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/carousel/jcarousel.connected-carousels.css') }}" />
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" />
        <link rel="stylesheet" href="{{ Theme::asset('assets/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ Theme::asset('assets/css/main.css') }}">
        <link rel="stylesheet" href="{{ Theme::asset('assets/css/responsive.css') }}">
        <link rel="stylesheet" href="{{ Theme::asset('assets/plugins/custom_scrollbar/jquery.mCustomScrollbar.css') }}" />

        <script src="{{ Theme::asset('assets/plugins/modernizr/modernizr-2.8.3.min.js') }}"></script>

        <script type="text/javascript">
            var baseUrl = "{{ URL::to('/') }}";
        </script>

        @yield('styles')

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="header">

            <div class="header-inner site-width center">

                <div class="row">

                    <div class="logo col-md-5 pull-left">
                        <a href="{{ URL::to('/') }}"><img src="{{ Theme::asset('assets/img/logo_new.png') }}"></a>
                        <div class="mobile-address">
                            <p>Jl. Bahureksa No.3 Bandung 40115, West Java, Indonesia</p>
                            <p>Reservation / Inquiry : +62-22-4203999, Fax : +62-22-84469390</p>    
                            <p>email: <a href="mailto:reservations@ivory-ayola.com">reservations@ivory-ayola.com</a></p> 
                        </div>
                    </div>

                    <div class="address col-md-5 pull-right">

                        <p>Jl. Bahureksa No.3 Bandung 40115, West Java, Indonesia</p>
                        <p>Reservation / Inquiry : +62-22-4203999, Fax : +62-22-84469390</p>    
                        <p>email: <a href="mailto:reservations@ivory-ayola.com">reservations@ivory-ayola.com</a></p>    

                    </div>

                </div>

                <div class="trip-container">
                    <div id="TA_linkingWidgetRedesign671" class="TA_linkingWidgetRedesign"><ul id="MVHpo3M03E" class="TA_links xEStAP"><li id="j2r3yeOLZA" class="mzXZVMO7avf"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/partner/tripadvisor_logo_115x18-15079-2.gif" alt="TripAdvisor"/></a></li></ul></div><script src="https://www.jscache.com/wejs?wtype=linkingWidgetRedesign&amp;uniq=671&amp;locationId=7347965&amp;lang=en_US&amp;border=true&amp;display_version=2"></script>
                </div>
                

            </div>
            

        </div>
            
        <div class="content site-width center">

            <div class="main site-width">

                <div class="main-menu site-width">

                    <nav class="navbar site-nav navbar-default" role="navigation">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">

                                <ul id="main-nav" class="nav navbar-nav">
                                    <li>
                                        <a href="{{ URL::to('/') }}" class="font-12 @if($current_route_name == 'home') {{ 'active' }} @endif">HOME</a>
                                    </li>
                                    
                                    @if(!$hotel_rooms->isEmpty())
                                    @include('partial.dropdown', array('menu_name' => 'rooms', 'route' => 'room', 'current_route' => $current_route_name, 'data' => $hotel_rooms))
                                    @else
                                    <li>
                                        <a href="#" class="new-site-color font-12 @if($current_route_name == 'room') {{ 'active' }} @endif">ROOMS</a>
                                    </li>
                                    @endif

                                    @if(!$banquets->isEmpty())
                                    @include('partial.dropdown', array('menu_name' => 'banquets', 'route' => 'banquet', 'current_route' => $current_route_name, 'data' => $banquets))
                                    @else
                                    <li>
                                        <a href="#" class="new-site-color font-12 @if($current_route_name == 'banquet') {{ 'active' }} @endif">BANQUETS</a>
                                    </li>
                                    @endif

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle new-site-color font-12" data-toggle="dropdown">GALLERY</a>
                                        <ul class="dropdown-menu">
                                            <li class="border-1-white"><a href="{{ URL::to('/') . '/gallery/photos' }}" class="font-12 white-important">PHOTOS</a></li>
                                            <li><a href="{{ URL::to('/') . '/gallery/videos' }}" class="font-12 white-important">VIDEOS</a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{ URL::to('/') . '/contact-us' }}" class="new-site-color font-12">CONTACT US</a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::to('/') . '/around-us' }}" class="new-site-color font-12 @if($current_route_name == 'around-us') {{ 'active' }} @endif">AROUND US</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle new-site-color font-12" data-toggle="dropdown">EVERJOY CAFE</a>
                                        <ul class="dropdown-menu">
                                            <li class="border-1-white"><a href="{{ URL::to('/') . '/cafe/promo' }}" class="font-12 white-important">PROMO</a></li>
                                            <li class="border-1-white"><a href="{{ URL::to('/') . '/cafe/menu' }}" class="font-12 white-important">MENU</a></li>
                                            <li><a href="{{ URL::to('/') . '/cafe/moment' }}" class="font-12 white-important">YOUR MOMENT WITH US</a></li>
                                        </ul>
                                    </li>
                                </ul>

                                <!--
                                <div class="col-sm-3 col-md-3">
                                    <form class="navbar-form" role="search">
                                        <div class="input-group stylish-input-group">
                                            <input type="text" class="form-control"  placeholder="Search" >
                                            <span class="input-group-addon">
                                                <button type="submit">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>  
                                            </span>
                                        </div>
                                    </form>
                                </div>-->
                            </div>
                        </div>
                    </nav>
                </div>                 

                @yield('content')

                <div class="socmed-footer site-width">

                    <div class="socmed-footer-inner">

                        <div class="row">

                            <div class="col-md-6 pull-left">

                                <div class="row">

                                    <div class="col-md-6 pull-left margin-bottom-10 socmed-icon"></a>
                                        <!--<a target="_blank" href="https://twitter.com/ivoryhotels"><img src="{{ Theme::asset('assets/img/twitter.png') }}" class="margin-right-5"></a>-->
                                        <a target="_blank" href="https://www.facebook.com/Ivory-By-Ayola-Hotel-Bandung-606847446109803/?fref=ts"><img src="{{ Theme::asset('assets/img/fb.png') }}" class="margin-right-5"></a>
                                        <a target="_blank" href="https://www.instagram.com/ivorybyayola/"><img src="{{ Theme::asset('assets/img/instagram.png') }}" class="margin-right-5"></a>
                                        <!--
                                        <a target="_blank" href="https://www.facebook.com/Ivory-By-Ayola-Hotel-Bandung-606847446109803/?fref=ts"><img src="{{ Theme::asset('assets/img/fb.png') }}" class="margin-right-5"></a>
                                        <a target="_blank" href="https://www.youtube.com/watch?v=Th4ydxnqvl8"><img src="{{ Theme::asset('assets/img/youtube.png') }}" class="margin-right-5"></a>
                                        -->
                                    </div>

                                </div>
                                
                            </div>

                            <div class="col-md-6 pull-right">

                                <div class="topotels-wrapper pull-right">
                                    <p>managed by <img src="{{ Theme::asset('assets/img/topotels.png') }}"> </p>
                                </div> 
                                
                            </div>

                        </div>

                    </div>
                
                </div>

            </div>

        </div>

        <script src="{{ Theme::asset('assets/plugins/jquery/jquery-1.11.3.min.js') }}"></script>
        <!--<script src="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.js') }}"></script>-->
        <script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ Theme::asset('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ Theme::asset('assets/plugins/carousel/jquery.jcarousel.js') }}" type="text/javascript"></script>
        <script src="{{ Theme::asset('assets/plugins/carousel/jcarousel.connected-carousels.js') }}" type="text/javascript"></script>
        <script src="{{ Theme::asset('assets/plugins/custom_scrollbar/jquery.mCustomScrollbar.concat.min.js') }}" type="text/javascript"></script>
        <script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
        <script src="{{ Theme::asset('assets/js/plugins.js') }}"></script>
        <script src="{{ Theme::asset('assets/js/main.js') }}"></script>

        @yield('scripts')

    </body>
</html>
