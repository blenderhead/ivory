@extends('layouts.frontend')

@section('title')
    Payment Process
@stop

@section('styles')
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/booking_form.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/transaction.css') }}">
@stop

@section('content')
    <div class="booking-calendar site-width center" style="padding-bottom: 20px;">

        @if($transaction->type == 'transfer')
            <section class="booking_details block">
                <h2 class="block_title">Thank You</h2>
                <p class="code">Invoice Number: {{ $transaction->transaction_id }}</p>
                <p class="note">
                    Informasi booking Anda telah kami terima dan akan kami proses lebih lanjut. Email mengenai detail booking Anda telah terkirim ke alamat yang telah Anda registrasi sebelumnya. Setelah anda menyelesaikan pembayaran, dan melakukan konfirmasi, tidak lebih dari  5-10 menit, anda akan menerima e-mail yang dilengkapi dengan detail pemesanan anda dan voucher hotel.
                    Mohon tunjukkan voucher hotel pada saat anda check-in. Terima kasih. Silahkan mengunjungi kami kembali.      
                </p>
            </section>
        @else
            @if($transaction->status == '2')
                <div class="col-lg-8" style="padding-left: 0px;">

                    <section class="booking_details payment-block block">

                        <h2 class="block_title">Credit Card Information</h2>

                        <form id="add-veritrans" method="POST" data-op="add">

                            <input type="hidden" name="transaction_id" value="{{ $transaction_id }}" />

                            <div class="form-group">
                                <label for="name" class="booking_label"><span class="required">*</span> <span class="required-info">required</span></label>
                            </div>

                            <div class="form-group">
                                <label for="card_owner" class="booking_label">Nama pemegang kartu kredit <span class="required">*</span></label>
                                <input type="text" name="card_owner" class="form-control card_owner" placeholder="credit card owner">
                                <span class="card-owner-error inline-error hide"></span>
                            </div>

                            <div class="form-group">
                                <label for="card_number" class="booking_label">Nomor kartu kredit <span class="required">*</span></label>
                                <input type="text" name="card_number" class="form-control card_number" placeholder="card number">
                                <span class="card-number-error inline-error hide"></span>
                            </div>

                            <div class="form-group">
                                <label for="email" class="booking_label">Masa berlaku <span class="required">*</span></label>
                                <div class="clear"></div>
                                <div class="col-lg-2" style="padding-left: 0px;">
                                    <input type="text" maxlength="2" size="2" name="card_exp_month" class="form-control card_exp_month" placeholder="MM">
                                </div>
                                <div class="col-lg-2" style="padding-left: 0px;">
                                    <input type="text" maxlength="4" size="4" name="card_exp_year" class="form-control card_exp_year" placeholder="YYYY">
                                </div>
                                <div class="clear"></div>
                                <p class="month-error inline-error hide"></p>
                                <p class="year-error inline-error hide"></p>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="booking_label">CVV <span class="required">*</span></label>
                                <div class="clear"></div>
                                <div class="col-lg-4" style="padding-left: 0px;">
                                    <input type="text" name="card_cvv" class="form-control card_cvv" placeholder="card security code">
                                </div>
                                <div class="clear"></div>
                                <span class="cvv-error inline-error hide"></span>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-style-1">Submit Payment</button>
                            </div>  

                        </form>
                        
                        <div class="clear"></div>

                    </section>
                </div>

                <div class="col-md-4 pull-right" style="padding-left: 0px; padding-right: 0px;">

                    <section class="booking_details block">

                        <h2 class="block_title">Total Pembayaran</h2>

                        <h4 class="site-maroon">{{ Helper::moneyFormat($transaction->total) }}</h4>

                    </section>

                </div>
            @elseif($transaction->status == '1')
                <section class="booking_details block">
                    <h2 class="block_title">Terima Kasih</h2>
                    <p class="code">Nomor Invoice: {{ $transaction->transaction_id }}</p>
                    <p class="note">
                        Invoice dan voucher anda telah dikirim ke alamat email yang telah anda registrasi sebelumnya.
                        Mohon tunjukkan voucher hotel pada saat anda check-in. Terima kasih. Silahkan mengunjungi kami kembali.      
                    </p>
                </section>
            @endif
            
        @endif

    </div>
@stop

@section('scripts')
    <script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/errors/veritrans_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/VeritransManager.js') }}" type="text/javascript"></script>
@stop
