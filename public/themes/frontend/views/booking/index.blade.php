@extends('layouts.frontend')

@section('title')
    Booking | {{ $room->name }}
@stop

@section('styles')
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/booking_form.css') }}">
    <link href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
@stop

@section('content')
    <div class="booking-calendar site-width center" style="padding-bottom: 20px;">

        <div class="col-lg-8" style="padding-left: 0px;">

            <form id="add-booking" method="POST" data-op="add">

                <input type="hidden" name="query_code" value="{{ $query['original_query'] }}" />
                <input type="hidden" name="room_id" value="{{ $room->id }}" />
                <input type="hidden" name="promotion_id" value="@if($room->promotions){{ $room->promotions->id }}@else{{'0'}}@endif" />
                <input type="hidden" name="checkin_date" value="{{ $query['stay_start_date']->format('Y-m-d') }}" />
                <input type="hidden" name="checkout_date" value="{{ $query['stay_end_date']->format('Y-m-d') }}" />
                <input type="hidden" name="number_of_rooms" value="{{ $query['room'] }}" />
                <input type="hidden" name="number_of_adults" value="{{ $query['adult'] }}" />
                <input type="hidden" name="number_of_children" value="{{ $query['child'] }}" />

                <section class="booking_details block">

                    <h2 class="block_title">Booking Form</h2>

                    <div class="form-group">
                        <label for="name" class="booking_label"><span class="required">*</span> <span class="required-info">required</span></label>
                    </div>

                    <div class="form-group">
                        <label for="first_name" class="booking_label">Title</label>
                        <select class="form-control title" name="title" style="width: 100px;">
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                        </select>   
                    </div>

                    <div class="form-group">
                        <label for="first_name" class="booking_label">First Name <span class="required">*</span></label>
                        <input type="text" name="first_name" class="form-control first_name" placeholder="your first name">
                        <span class="first-name-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="booking_label">Last Name</label>
                        <input type="text" name="last_name" class="form-control" placeholder="your last name">
                    </div>

                    <div class="form-group">
                        <label for="email" class="booking_label">Email <span class="required">*</span></label>
                        <input type="text" name="email" class="form-control email" placeholder="your email">
                        <span class="email-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="booking_label">Phone <span class="required">*</span></label>
                        <input type="text" name="phone_number" class="form-control phone" placeholder="your mobile phone number">
                        <span class="phone-error inline-error hide"></span>
                    </div>

                    <div class="form-group">
                        <label for="booking_code" class="booking_label">Member Code</label>
                        <input type="text" name="booking_code" class="form-control booking_code" placeholder="member/promo code">
                    </div>

                    <div class="form-group">
                        <label for="notes" class="booking_label">Additional Request</label>
                        <textarea class="form-control notes" id="notes" name="request_note" rows="7"></textarea>
                    </div>            

                    <div class="clear"></div>

                </section>

                <section class="booking_details block">
                    <h2 class="block_title">Cancellation Policy</h2>

                    <ul class="cancellation_policy clearfix">
                        @if(!$room->cancellations()->get()->isEmpty())
                            @foreach($room->cancellations()->get() as $cancellation)
                                @if($cancellation->level1Policy())
                                    <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt=""><span class="cancel_policy">{{ $cancellation->level1Policy()->name }}</span></li>
                                @endif
                                
                                @if($cancellation->level2Policy())
                                    <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt=""><span class="cancel_policy">{{ $cancellation->level2Policy()->name }}</span></li>
                                @endif

                                @if($cancellation->level3Policy())
                                    <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt=""><span class="cancel_policy">{{ $cancellation->level3Policy()->name }}</span></li>
                                @endif
                            @endforeach
                        @else
                            <li><img src="{{ Theme::asset('assets/img/checked.png') }}" alt=""><span class="cancel_policy">Full refundable</span></li>
                        @endif

                    </ul>
                </section>

                <section class="booking_details total-payment block">

                    <h2 class="block_title">Payment Information</h2>

                    <dl>
                        <dt>Room Rate</dt>

                        @if($room->promotions)
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->promotions->room_price) }}</dd>
                        @else
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->room_price) }}</dd>
                        @endif
                        
                        <dt>Service ({{ $hotel_setting->get(0)->service_charge }}%)</dt>
                        @if($room->promotions)
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->promotions->service_price) }}</dd>
                        @else
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->service_price) }}</dd>
                        @endif

                        <dt>Tax ({{ $hotel_setting->get(0)->tax_charge }}%)</dt>
                        @if($room->promotions)
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->promotions->tax_price) }}</dd>
                        @else
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->tax_price) }}</dd>
                        @endif

                        <dt>Sub Total</dt>
                        @if($room->promotions)
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->promotions->room_price + $room->promotions->service_price + $room->promotions->tax_price) }}</dd>
                        @else
                            <dd class="bold italic margin-bottom-10" style="text-align: right">{{ Helper::moneyFormat($room->room_price + $room->service_price + $room->tax_price) }}</dd>
                        @endif

                        <dt>Total Order</dt>
                        <dd class="bold italic margin-bottom-10" style="text-align: right">{{ $query['room'] }} Room(s)</dd>
                    </dl>

                    <dl id="total-payment-sidebar">
                        <dt>Total Payment</dt>
                        @if($room->promotions)
                            <dd style="text-align: right"><span class="bold italic remaining_amount_calculate">{{ Helper::moneyFormat($room->promotions->final_price) }}</span></dd>
                            <input type="hidden" name="total_order" value="{{ $room->promotions->final_price }}" />
                        @else
                            <dd style="text-align: right"><span class="bold italic remaining_amount_calculate">{{ Helper::moneyFormat($room->final_price) }}</span></dd>
                            <input type="hidden" name="total_order" value="{{ $room->final_price }}" />
                        @endif
                        
                    </dl>

                    <div class="clear"></div>

                </section>

                <section class="booking_details total-payment payment-method block">
                    <h2 class="block_title">Payment Method</h2>

                    <div class="form-group">
                        <label class="booking_label"><input name="payment_type" id="transfer" type="radio" value="transfer" checked><span class="transfer-method">Bank Transfer (Hanya Indonesia)</span></label>
                        <div class="payment_logo pull-right">
                            <img src="{{ Theme::asset('assets/img/bank_logo.png') }}" alt="Bank Transfer (Indonesia Only)">
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="form-group">
                        <label class="booking_label"><input name="payment_type" id="credit_card_veritrans" type="radio" value="credit_card_veritrans" ><span class="transfer-method">Credit Card</span></label>
                        <div class="payment_logo pull-right">
                            <img src="{{ Theme::asset('assets/img/visa-master.png') }}" alt="Kartu Kredit">
                        </div>
                        <div class="clear"></div>
                    </div>
                </section>

                <div class="form-group">
                    <button type="submit" class="btn btn-style-1">Submit</button>
                    <span class="booking-info inline-error color-red-800"></span>
                </div>  

            </form>

        </div>

        <div class="col-md-4 pull-right" style="padding-left: 0px; padding-right: 0px;">

            <section class="booking_details block">

                <h2 class="block_title">Order Detail</h2>

                <dl>
                    <dt>Check-in</dt>
                    <dd class="margin-bottom-10">{{ $query['stay_start_date']->format('j F Y') }}</dd>
                    
                    <dt>Check-out</dt>
                    <dd class="margin-bottom-10">{{ $query['stay_end_date']->format('j F Y') }}</dd>
                    
                    <dt>Room Name</dt>
                    <dd class="margin-bottom-10">{{ $room->name }}</dd>

                    @if($room->promotions)
                        <dt>Promotion</dt>
                        <dd class="margin-bottom-10">{{ $room->promotions->name }}</dd>
                    @endif
                </dl>

                <dl>
                    <dt>Breakfast</dt>
                        @if($room->breakfast_included)
                            <dd class="margin-bottom-10">Included for {{ $room->number_person_breakfast_included }} person</dd>
                        @else
                            <dd class="margin-bottom-10">Not Included</dd>
                        @endif

                    <dt>Total Room</dt>
                    <dd class="margin-bottom-10">{{ $query['room'] }} Room(s)</dd>
                    <dt>Total Person</dt>
                    <dd>{{ $query['adult'] }} Adult(s) @if($query['child']){{'+' . $query['child'] . ' Child' }}@endif</dd>
                </dl>

            </section>

        </div>

        <div class="clear"></div>

    </div>
@stop

@section('scripts')
    <script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/errors/booking_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/BookingManager.js') }}" type="text/javascript"></script>
@stop
