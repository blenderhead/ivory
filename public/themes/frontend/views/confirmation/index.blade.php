
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<title>Konfirmasi Pembayaran</title>
		<link href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
		<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.css') }}">
		<link rel="stylesheet" href="{{ Theme::asset('assets/css/confirmation/whmcs.css') }}">
		<link rel="stylesheet" href="{{ Theme::asset('assets/css/transaction.css') }}">

		<style type="text/css">
			
			.btn-style-1:hover {
  				color: #fff;
			}

		</style>

		<script type="text/javascript">
            var baseUrl = "{{ URL::to('/') }}";
        </script>
	</head>
	<body>
		<div id="whmcsheader">
			<div class="whmcscontainer">
				<div id="whmcsimglogo">
					<div class="logo-wrapper">
						<a href="{{ URL::to('/') }}"><img src="{{ Theme::asset('assets/img/logo-2.png') }}"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="whmcscontainer">
			<div class="contentpadded">
				<script type="text/javascript">
					
				</script>
				<h1 style="border-bottom:thin solid #525252;">Konfirmasi Pembayaran</h1>
				<p>&nbsp;</p>

				<div class="col-lg-12">

					<div class="alert alert-info">
						<strong>Penting!</strong> Mohon konfirmasi ini hanya dilakukan <strong>setelah</strong> Anda melakukan pembayaran.<br/>
						Isi data dengan benar untuk memudahkan kami memverifikasi konfirmasi Anda.
					</div>

					<div class="alert alert-danger error-system hide">
						
					</div>

					<div class="well">

						<div class="col-lg-6">

							<section class="booking_details block">

		                        <form id="form-konfirmasi" method="POST" data-op="add">

		                            <div class="form-group">
		                                <label for="name" class="booking_label"><span class="required">*</span> <span class="required-info">required</span></label>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_owner" class="booking_label">Nomor Invoice <span class="required">*</span></label>
		                                <input type="text" name="invoice_id" class="form-control invoice_id" placeholder="Masukkan nomor invoice">
		                                <span class="invoice-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Tanggal Transfer <span class="required">*</span></label>
		                                <input type="text" name="transfer_date" class="form-control transfer_date" placeholder="Masukkan tanggal transfer">
		                                <span class="transfer-date-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Bank Tujuan <span class="required">*</span></label>
		                                <div class="controls">
											<select id="transfer_destination" name="transfer_destination" class="inputDisabled">
												<option value="bca">BCA</option>
												<option value="mandiri">Bank Mandiri</option>
												<option value="bni">BNI</option>
												<option value="bri">BRI</option>
											</select>
										</div>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Jumlah Transfer <span class="required">*</span></label>
		                                <input type="text" name="transfer_amount" class="form-control transfer_amount" placeholder="Masukkan jumlah transfer">
		                                <span class="transfer-amount-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Nomor Rekening <span class="required">*</span></label>
		                                <input type="text" name="transfer_account_number" class="form-control transfer_account_number" placeholder="Masukkan nomor rekening">
		                                <span class="transfer-accountnum-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Nama Pemilik Rekening <span class="required">*</span></label>
		                                <input type="text" name="transfer_account" class="form-control transfer_account" placeholder="Masukkan nama pemilik rekening">
		                                <span class="transfer-account-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Email <span class="required">*</span></label>
		                                <input type="text" name="email" class="form-control email" placeholder="Masukkan email Anda">
		                                <span class="email-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <label for="card_number" class="booking_label">Nomor Telepon <span class="required">*</span></label>
		                                <input type="text" name="phone" class="form-control phone" placeholder="Masukkan nomor telepon Anda">
		                                <span class="phone-error inline-error hide"></span>
		                            </div>

		                            <div class="form-group">
		                                <button type="submit" class="btn btn-style-1">Submit</button>
		                            </div>  

		                        </form>
		                        
		                    </section>

						</div>

						<div class="clear"></div>	

					</div>

                </div>

				<div class="clear"></div>	

			</div>
		</div>

		<script src="{{ Theme::asset('assets/plugins/jquery/jquery-1.11.3.min.js') }}"></script>
		<script src="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.js') }}"></script>
		<script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
		<script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
		<script src="{{ Theme::asset('assets/js/errors/confirm_errors.js') }}"></script>
		<script src="{{ Theme::asset('assets/js/classes/ConfirmManager.js') }}"></script>

		<script type="text/javascript">
			$(document).ready(function() {

				var today = new Date();

			    $('.transfer_date').datepicker({
			        dateFormat: 'yy-mm-dd',
			        minDate: today,
			    });

			});
		</script>
	</body>
</html> 
