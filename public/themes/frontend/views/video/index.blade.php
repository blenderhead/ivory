@extends('layouts.frontend')

@section('title')
	Video Gallery
@stop

@section('styles')
	<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.css') }}">
	<link rel="stylesheet" href="{{ Theme::asset('assets/css/video.css') }}">
@stop

@section('content')
<section class="inner-content">
	<div class="row">
		<div class="col-md-7">
			<div id="contentVideo" class="">

				@foreach($videos as $key => $video)
					<div id="tab{{$key}}" class="tab_content video-full">
						<embed width="500" height="332" src="//www.youtube.com/v/{{ Helper::getYoutubeId($video->url) }}?version=3&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed>
                    </div>
				@endforeach
                
            </div>
		</div>
		<div class="col-md-5 video-navigation">
			<div class="content-desc desc-text">
				@foreach($videos as $key => $video)
					<ul id="tabs_thumb" class="they_say thumbs">
						<li>
		                    <a class="thumb" href="#" rel="tab{{$key}}">
		                    	<div class="row">
		                    		<div class="col-md-4">
		                    			<img width="80" height="53" src="http://img.youtube.com/vi/{{ Helper::getYoutubeId($video->url) }}/0.jpg" alt="" />
		                    		</div>
		                    		<div class="col-md-8">
		                    			<span>{{ $video->title }}</span>
		                    		</div>
		                    	</div>
		                 	</a>
		                </li>
	                </ul>
	            @endforeach
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/jquery-ui/jquery-ui.js') }}"></script>
	<script src="{{ Theme::asset('assets/js/video.js') }}"></script>
@stop