@extends('layouts.frontend')

@section('title')
	Room - {{ $room->name }}
@stop

@section('styles')
@stop

@section('content')
	<img @if(Helper::getImage($room->photo_id)){{'src="' . URL::to('/') . '/uploads/room/' . Helper::getImage($room->photo_id) . '"'}}@endif />
	@include('partial.search_room')
@stop

@section('scripts')
@stop