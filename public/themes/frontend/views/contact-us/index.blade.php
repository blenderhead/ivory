@extends('layouts.frontend')

@section('title')
	Contact Us
@stop

@section('styles')
	<link href="{{ Theme::asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="inner-content">
	<div class="row">
		<div class="col-md-5">
			<div class="contact-address">
				<p class="text-contact">Jl. Bahureksa No.3 Bandung 40115</p>
				<p class="text-contact"> West Java, Indonesia</p>
				<p class="text-contact">Reservation / Inquiry :</p>
				<p class="text-contact">Phone: +62-22-4203999</p>
				<p class="text-contact"> Fax : +62-22-84469390</p>    
				<p class="text-contact">email :  <a href="mailto:reservations@ivory-ayola.com">reservations@ivory-ayola.com</a></p> 			
			</div>
			<form id="contact" method="POST" data-op="add">
				<div class="form-contact">
					<p class="mail-success site-maroon"></p>

					<div class="form-group">
						<input type="text" name="name" class="form-control name" placeholder="Your Name">
						<span class="name-error inline-error hide">error</span>
					</div>
					<div class="form-group">
						<input type="text" name="email" class="form-control email" placeholder="Email">
						<span class="email-error inline-error hide">error</span>
					</div>
					<div class="form-group">
						<input type="text" name="subject" class="form-control subject" placeholder="Subject">
						<span class="subject-error inline-error hide">error</span>
					</div>
					<div class="form-group">
						<textarea class="form-control message" name="message" style="height:120px"></textarea>
						<span class="message-error inline-error hide">error</span>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<img class="captcha_image" src="{{ Captcha::img() }}" />
								<a title="refresh captcha" class="refresh_captcha" href="#"><img width="15" height="15" src="{{ Theme::asset('assets/img/refresh.png') }}" /></a>
							</div>
							<div class="col-md-6">
								<input type="text" name="captcha" class="form-control captcha" placeholder="Enter capthca code">
							</div>
						</div>
						<span class="captcha-error inline-error hide">error</span>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-style-1 submit-contact">Submit</button>
					</div>
					<div class="clearfix"></div>															
				</div>
			</form>
		</div>
		<div class="col-md-7 location-hotel">
			<iframe
			width="500"
			height="500"
			frameborder="0" style="border:0"
			src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDyeOV9HuuZqkYLO2PIoAzaVlE502WhHsM&q=Ivory+Hotel,Jl. Bahureksa No.3+Bandung" allowfullscreen>
			</iframe>			
		</div>
	</div>
</section>
@stop

@section('scripts')
	<script src="{{ Theme::asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/js/errors/contact_errors.js') }}" type="text/javascript"></script>
    <script src="{{ Theme::asset('assets/js/classes/ContactManager.js') }}" type="text/javascript"></script>
@stop