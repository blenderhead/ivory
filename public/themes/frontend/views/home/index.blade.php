@extends('layouts.frontend')

@section('title')
	Home
@stop

@section('styles')
	<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/slick/slick.css') }}">
	<link rel="stylesheet" href="{{ Theme::asset('assets/plugins/slick/slick-theme.css') }}">
	<link rel="stylesheet" href="{{ Theme::asset('assets/css/home.css') }}">

	<style type="text/css">
		blockquote {
			border: none;
		}

		.socmed-footer {
          margin-top: 80px;
        }
	</style>
@stop

@section('content')
	@if($photos)
		<div class="main-image site-width center">
			<div class="image-holder">
				<div class="connected-carousels">
			        <div class="stage">
			            <div class="carousel carousel-stage">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="960" height="465" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
			            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
			        </div>

			        <div class="navigation">
			            <a href="#" class="prev prev-navigation">&lsaquo;</a>
			            <a href="#" class="next next-navigation">&rsaquo;</a>
			            <div class="carousel carousel-navigation">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="100" height="49" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			        </div>
			    </div>
			    <div id="carousel-example-generic" class="carousel slide crousel-bootstrap" data-ride="carousel">
			    	<ol class="carousel-indicators">
			    	@foreach($photos as $index => $photo)
			    		<li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" @if($index == 0) class="active" @endif></li>
		    		@endforeach
			    	</ol>
			    	<div class="carousel-inner" role="listbox">
			    		@foreach($photos as $index => $photo)
			    		<div class="item @if($index == 0) active @endif">
			    			<img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" class="img-crousel">
			    			<div class="carousel-caption">
			    			</div>
			    		</div>
			    		@endforeach
			    	</div>
			    	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    		<span class="sr-only">Previous</span>
			    	</a>
			    	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    		<span class="sr-only">Next</span>
			    	</a>
			    </div>	
		   
			</div>
		</div>

		<div class="bookingbutton-home-wrapper">
			<iframe src="http://app-apac.thebookingbutton.com/properties/ivoryayolabdgdirect/booking_widget" height="200" width="250" frameborder="0" scrolling="no" allowtransparency="true"></iframe> 
		</div>

		<section role="complementary" class="simple white-back quotes no-fouc testi">

			<div class="col-lg-12 testi-wrapper">
				<div class="row">
					@foreach($testimonials as $testimonial)
					
						<div class="col-lg-3 col-md-4 col-xs-12">
							<blockquote>
								<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="{{ $testimonial->link }}" target="_blank">{{ $testimonial->description }}</a></p>
								<cite class="cite-text"><em>- {{ $testimonial->name }}</em>- <br />
							</blockquote>
						</div>
					
					@endforeach
				</div>
			</div>

			<!--
			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://andrehandoyo.com/hotel-review-ivory-by-ayola-bandung/" target="_blank">Review Ivory</a></p>
				<cite style="font-size: 14px;">Andre Handoyo<br />
			</blockquote>


			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://www.kopertraveler.id/ivory-by-ayola/" target="_blank">IVORY by Ayola</a></p>
				<cite style="font-size: 14px;">Koper Traveller<br />
			</blockquote>


			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://obendon.com/2015/08/03/hotel-ivory/" target="_blank">Ivory, Harmonisasi Perjalanan Rasa</a></p>
				<cite style="font-size: 14px;">Olive's Journey<br />
			</blockquote>


			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://www.len-diary.com/2016/03/ivory-by-ayola-hotel-bintang-tiga-plus.html?m=1" target="_blank">Ivory by Ayola Hotel Bintang Tiga Plus</a></p>
				<cite style="font-size: 14px;">Travel Diary<br />
			</blockquote>

			
			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://wiranurmansyah.com/feels-like-home-in-ivory-bandung" target="_blank">Feels Like Home in Ivory Bandung</a></p>
				<cite style="font-size: 14px;">Wira Nurmansyah<br />
			</blockquote>
			

			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://ririmaulidina.blogspot.co.id/2016/03/ivory-by-ayola-hotel-bandung.html" target="_blank">Ivory by Ayola Hotel, Bandung</a></p>
				<cite style="font-size: 14px;">The Pillow Talk<br />
			</blockquote>

			<blockquote>
				<p style="font-size: 14px;"><a style="color: #614216; text-decoration: none;" href="http://helenysm.com/2015/09/everjoy-ivory-by-ayola/" target="_blank">Everjoy ~ Ivory by Ayola</a></p>
				<cite style="font-size: 14px;">Helenysm<br />
			</blockquote>
			-->

		</section>

	@else
		@include('partial.empty_gallery')
	@endif

	{{-- @include('partial.search_room') --}}
	
@stop
	
@section('scripts')
	
	<script src="{{ Theme::asset('assets/plugins/slick/slick.min.js') }}" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {
			$('.carousel').jcarouselAutoscroll({
			    target: '+=1'
			});

			$('.carousel').jcarousel({
			    wrap: 'last'
			});

			/*
			$('.quotes').slick({
			  dots: true,
			  infinite: true,
			  autoplay: true,
			  autoplaySpeed: 6000,
			  speed: 800,
			  slidesToShow: 1,
			  adaptiveHeight: true,
			  arrows:false
			});
			*/

			$('.no-fouc').removeClass('no-fouc');
		});

	</script>

@stop