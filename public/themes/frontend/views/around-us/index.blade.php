@extends('layouts.frontend')

@section('title')
	Around Us Gallery
@stop

@section('styles')
@stop

@section('content')

	
	@if($photos)
		<div class="main-image site-width center">
			<div class="image-holder">
				<div class="connected-carousels">
			        <div class="stage">
			            <div class="carousel carousel-stage">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" />
				                        @if($photo->description)
					                        <div class="around-overlay">
			                                    <p>{{ $photo->description }}</p>
			                                </div>
			                            @endif
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			            <a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
			            <a href="#" class="next next-stage"><span>&rsaquo;</span></a>
			        </div>

			        <div class="navigation">
			            <a href="#" class="prev prev-navigation">&lsaquo;</a>
			            <a href="#" class="next next-navigation">&rsaquo;</a>
			            <div class="carousel carousel-navigation">
			                <ul>
			                	@foreach($photos as $photo)
				                    <li>
				                        <img src="{{ URL::to('/') . '/uploads/gallery/' . $photo->file }}" width="100" height="49" />
				                    </li>
				                @endforeach
			                </ul>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	@else
		@include('partial.empty_gallery')
	@endif

	{{-- @include('partial.search_room') --}}
@stop

@section('scripts')
@stop