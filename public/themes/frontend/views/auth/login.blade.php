
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>Ivory - Member Login</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<!--<link href="{{ Theme::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />-->
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="{{ Theme::asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />
	<!--<link href="{{ Theme::asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.css') }}" rel="stylesheet" />-->
	<!--<link href="{{ Theme::asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />-->
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="{{ Theme::asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<!--<link href="{{ Theme::asset('assets/css/responsive.css') }}" rel="stylesheet" />-->
	<link href="{{ Theme::asset('assets/css/login.css') }}" rel="stylesheet" />
	<!-- END THEME STYLES -->

	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";
	</script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="auth-page height-auto site-color">
	<!-- BEGIN CONTENT -->
	<div class="wrapper animated fadeInDown">
		<div class="panel overflow-hidden">
			
			<div class="site-logo padding-top-25 no-margin-bottom font-size-20 color-white text-center text-uppercase">
				<!--<i class="ion-log-in margin-right-5"></i> Sign In-->
				<a href="{{ URL::to('/') }}"><img width="50%" src="{{ Theme::asset('assets/img/logo-2.png') }}"></a>
			</div>

			<form id="login-form" method="post">

				<div class="alert site-logo text-center color-black no-radius no-margin padding-top-15 padding-bottom-30 padding-left-20 padding-right-20">Please sign in to member area</div>
				<div class="box-body padding-md">
				
					<div class="form-group">
						<input type="text" name="email" class="form-control input-lg" placeholder="Email" />
					</div>
					
					<div class="form-group">
						<input type="password" name="password" class="form-control input-lg" placeholder="Password"/>
					</div>        
					
					<div class="form-group margin-top-20">
						<input type="checkbox" class="js-switch" id="checkbox" checked /><label for="checkbox" class="font-size-12 normal margin-left-10">Remember Me</label>
						<a href="{{ URL::route('user.forget_password') }}" class="pull-right">Forget your password?</a>
					</div>       
					
					<button type="submit" class="btn btn-dark site-maroon padding-10 btn-block color-white"><i class="ion-log-in"></i> Sign in</button>  
				</div>
			</form>
			<!--<div class="panel-footer padding-md no-margin no-border bg-light-blue-500 text-center color-white">&copy; 2015.</div>-->
		</div>
	</div>
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="{{ Theme::asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<!--<script src="{{ Theme::asset('assets/plugins/bootstrap/js/holder.js') }}"></script>-->
	<!--<script src="{{ Theme::asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>-->
	<script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
	<!--<script src="{{ Theme::asset('assets/js/core.js" type="text/javascript') }}"></script>-->
	<!-- END CORE PLUGINS -->
	
	<!-- bootstrap validator -->
	<!--<script src="{{ Theme::asset('assets/plugins/bootstrapValidator/bootstrapValidator.min.js') }}" type="text/javascript"></script>-->
	
	<!-- switchery -->
	<!--<script src="{{ Theme::asset('assets/plugins/switchery/switchery.min.js') }}" type="text/javascript"></script>-->
	
	<!-- maniac -->
	<!--<script src="{{ Theme::asset('assets/js/maniac.js') }}" type="text/javascript"></script>-->

	<!--<script src="{{ Theme::asset('assets/js/functions.js') }}" type="text/javascript"></script>-->

	<script src="{{ Theme::asset('assets/js/classes/AuthManager.js') }}" type="text/javascript"></script>
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>