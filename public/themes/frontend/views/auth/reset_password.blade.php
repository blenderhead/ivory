
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>Ivory - Password Reset</title>
	
	<link href="{{ Theme::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/plugins/ionicons/css/ionicons.min.css') }}" rel="stylesheet" />

	<link href="{{ Theme::asset('assets/plugins/animate/animate.css') }}" rel="stylesheet" />

	<link href="{{ Theme::asset('assets/css/material.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/style.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/plugins.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/helpers.css') }}" rel="stylesheet" />
	<link href="{{ Theme::asset('assets/css/login.css') }}" rel="stylesheet" />


	<script type="text/javascript">
		var baseUrl = "{{ URL::to('/') }}";
	</script>

</head>

<body class="auth-page height-auto site-color">
	<div class="wrapper animated fadeInDown">
		<div class="panel overflow-hidden">
			
			<div class="site-logo padding-top-25 no-margin-bottom font-size-20 color-white text-center text-uppercase">
				<a href="{{ URL::to('/') }}"><img width="50%" src="{{ Theme::asset('assets/img/logo-2.png') }}"></a>
			</div>

			<form id="reset-pass-form" method="post">

				<div class="alert reset-text site-logo text-center color-black no-radius no-margin padding-top-15 padding-bottom-30 padding-left-20 padding-right-20">Please enter your new password.</div>
				
				<div class="box-body padding-md reset-body">
					
					<input type="hidden" name="token" value="{{$token}}" />

					<div class="form-group">
						<input type="password" name="password" class="form-control password" placeholder="New Password"/>
					</div>  
					
					<div class="form-group">
						<input type="password" name="password_confirm" class="form-control password_confirm" placeholder="Confirm Password"/>
					</div>  

					<button type="submit" class="btn btn-dark site-maroon padding-10 btn-block color-white"><i class="ion-log-in"></i> Reset Password</button>  
				</div>
			</form>
			<!--<div class="panel-footer padding-md no-margin no-border bg-light-blue-500 text-center color-white">&copy; 2015.</div>-->
		</div>
	</div>

	<script src="{{ Theme::asset('assets/plugins/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
	<script src="{{ Theme::asset('assets/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

	<script src="{{ Theme::asset('assets/js/classes/AuthManager.js') }}" type="text/javascript"></script>
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>