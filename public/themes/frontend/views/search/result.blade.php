@extends('layouts.frontend')

@section('title')
    Room - Search
@stop

@section('styles')
    <link rel="stylesheet" href="{{ Theme::asset('assets/css/room_search.css') }}">
@stop

@section('content')
    
    @include('partial.search_room')

    @if(!$rooms->isEmpty())
        @foreach($rooms as $room)

            @if(count($room->promotions) > 0)

                @foreach($room->promotions as $promotion)
                    @include('partial.room_with_promo')
                @endforeach

            @endif

            @include('partial.room_no_promo')

        @endforeach
    @else
        <p style="color: #fff; font-size: 13px;">We're sorry. The room criteria that you've search is not available yet.</p>
    @endif

@stop

@section('scripts')
@stop
