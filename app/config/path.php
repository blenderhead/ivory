<?php

return [
	
	'image_upload_root' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR,

	'default_image_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'defaults' . DIRECTORY_SEPARATOR,

	'user_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR,

    'gallery_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR,

    'gallery_thumbnail_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR,

    'testimoni_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'testimoni' . DIRECTORY_SEPARATOR,

	'room_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'room' . DIRECTORY_SEPARATOR,

	'banquet_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'banquet' . DIRECTORY_SEPARATOR,

	'cafe_promo_upload_path' => public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cafe_promo' . DIRECTORY_SEPARATOR,

	'booking_invoice' => storage_path() . DIRECTORY_SEPARATOR . 'booking_invoice' . DIRECTORY_SEPARATOR,

	'admin_booking_invoice' => storage_path() . DIRECTORY_SEPARATOR . 'booking_invoice' . DIRECTORY_SEPARATOR . 'admin_doc' . DIRECTORY_SEPARATOR
];
