<?php

    class RoleSeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $admin = new Role;
            $admin->name = 'Admin';
            $admin->save();

            $gm = new Role;
            $gm->name = 'General Manager';
            $gm->save();

            $member = new Role;
            $member->name = 'Member';
            $member->save(); 

            $admin_dashboard = new Permission;
            $admin_dashboard->name = 'access_admin_dashboard';
            $admin_dashboard->display_name = 'Admin Dashboard Permission';
            $admin_dashboard->save();

            $admin->perms()->sync(array($admin_dashboard->id));
            $gm->perms()->sync(array($admin_dashboard->id));

            $user = \Sentry::getUserProvider()->create([
                'email' => 'admin@ivory.dev',
                'password' => '12345678',
                'activated' => '1',
                'first_name' => 'Admin',
                'last_name' => 'Ivory',
                'role' => $admin->id
            ]);
    
            $user_entity = User::where('id','=',$user->id)->first();
            $user_entity->roles()->attach($admin->id);
        }

    }
