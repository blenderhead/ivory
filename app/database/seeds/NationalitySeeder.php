<?php

    class NationalitySeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $nationality = new Nationality();
            $nationality->flag = null;
            $nationality->save();
        }

    }
