<?php

    class CurrencySeeder extends Seeder
    {

        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $currency = new Currency();
            $currency->name = 'Rupiah';
            $currency->code = 'IDR';
            $currency->symbol = 'IDR';
            $currency->save();
        }

    }
