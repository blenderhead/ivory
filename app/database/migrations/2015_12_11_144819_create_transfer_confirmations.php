<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferConfirmations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfer_confirmations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('transaction_id');
			$table->date('transfer_date');
			$table->string('transfer_destination');
		    $table->integer('transfer_total');
		    $table->string('transfer_account');
		    $table->string('email');
		    $table->string('phone');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transfer_confirmations');
	}

}
