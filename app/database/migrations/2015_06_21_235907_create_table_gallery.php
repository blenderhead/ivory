<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGallery extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gallery', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('gallery_category_id')->unsigned();
		    $table->text('description')->nullable();
		    $table->tinyInteger('is_publish')->default(1);
		    $table->integer('created_by')->nullable();
		    $table->integer('updated_by')->nullable();
		    $table->timestamps();
		    $table->foreign('gallery_category_id')
		    	->references('id')
		    	->on('gallery_categories')
		    	->onDelete('restrict')
		    	->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gallery');
	}

}
