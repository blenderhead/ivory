<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManyColumnForSetActiveDayPromotion extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->tinyInteger('is_sunday')->nullable();
			$table->tinyInteger('is_monday')->nullable();
			$table->tinyInteger('is_tuesday')->nullable();
			$table->tinyInteger('is_wednesday')->nullable();
			$table->tinyInteger('is_thursday')->nullable();
			$table->tinyInteger('is_friday')->nullable();
			$table->tinyInteger('is_saturday')->nullable();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->dropColumn('is_sunday');
			$table->dropColumn('is_monday');
			$table->dropColumn('is_tuesday');
			$table->dropColumn('is_wednesday');
			$table->dropColumn('is_thursday');
			$table->dropColumn('is_friday');
			$table->dropColumn('is_saturday');
		});	
	}

}
