<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHotelAddTaxes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hotel', function(Blueprint $table)
		{
			$table->integer('tax_charge')->after('service_charge')->nullable();
			$table->string('manager_email')->after('email')->nullable();
			$table->integer('member_discount')->after('phone')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hotel', function(Blueprint $table)
		{
			$table->dropColumn('tax_charge');
			$table->dropColumn('manager_email');
			$table->dropColumn('member_discount');
		});
	}

}
