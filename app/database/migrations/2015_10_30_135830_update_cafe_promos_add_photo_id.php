<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCafePromosAddPhotoId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cafe_promos', function(Blueprint $table)
		{
			$table->integer('photo_id')->after('price');
			$table->dropColumn('image');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cafe_promos', function(Blueprint $table)
		{
			$table->dropColumn('photo_id');
			$table->string('image')->after('price');
		});
	}

}
