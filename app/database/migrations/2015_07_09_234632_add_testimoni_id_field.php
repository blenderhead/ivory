<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTestimoniIdField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE photos
				MODIFY gallery_id INT(10) UNSIGNED NULL');
			$table->integer('testimoni_id')->unsigned()->after('gallery_id')->nullable();
			$table->foreign('testimoni_id')
		    	->references('id')
		    	->on('testimonials')
		    	->onDelete('restrict')
		    	->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE photos
				MODIFY gallery_id INT(10) UNSIGNED NOT NULL');
		Schema::table('photos', function(Blueprint $table)
		{
			$table->dropForeign('photos_testimoni_id_foreign');
			$table->dropColumn('testimoni_id');
		});
	}

}
