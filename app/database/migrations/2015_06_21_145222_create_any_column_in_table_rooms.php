<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnyColumnInTableRooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms', function($table)
		{
			$table->integer('room_size')->after('description');
			$table->integer('adult_breakfast_price')->after('adult_price');
			$table->integer('child_breakfast_price')->after('child_price');
			DB::statement('ALTER TABLE rooms 
				CHANGE COLUMN number_person_breakfast_included 
				number_person_breakfast_included INT(11) NULL 
				AFTER breakfast_included');
			$table->timestamps();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms', function($table)
		{
			$table->dropColumn('room_size');
			$table->dropColumn('adult_breakfast_price');
			$table->dropColumn('child_breakfast_price');
			DB::statement('ALTER TABLE rooms 
				CHANGE COLUMN number_person_breakfast_included 
				number_person_breakfast_included INT(11) NOT NULL 
				AFTER breakfast_included');			
		});	
	}

}
