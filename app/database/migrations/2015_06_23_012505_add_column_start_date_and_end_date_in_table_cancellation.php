<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStartDateAndEndDateInTableCancellation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cancellations', function(Blueprint $table)
		{
		    $table->date('start_date')->nullable();
		    $table->date('end_date')->nullable();
		    $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cancellations', function(Blueprint $table)
		{
		    $table->dropColumn('start_date');
		    $table->dropColumn('end_date');
		    $table->dropColumn('created_at');
		    $table->dropColumn('updated_at');
		});		
	}

}
