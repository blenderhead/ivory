<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnyColumnInTablePromotions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->integer('day_in_advaces')->nullable()->after('discount_rate');
			$table->integer('early_days')->nullable()->after('day_in_advaces');
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->dropColumn('day_in_advaces');
			$table->dropColumn('early_days');
		});	
	}

}
