<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionConfirmationAddName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transfer_confirmations', function(Blueprint $table)
		{
			$table->string('transfer_account_number')->after('transfer_account');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transfer_confirmations', function(Blueprint $table)
		{
			$table->dropColumn('transfer_account_number');
		});
	}

}
