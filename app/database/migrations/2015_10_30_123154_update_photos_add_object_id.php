<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhotosAddObjectId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			$table->integer('object_id')->after('id');
			$table->string('title')->after('object_id')->nullable();
			$table->string('description')->after('title')->nullable();
			//$table->dropColumn('gallery_id');
			//$table->dropColumn('room_id');
			//$table->dropColumn('testimoni_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			$table->dropColumn('object_id');
			$table->dropColumn('title');
			$table->dropColumn('description');
			//$table->integer('gallery_id')->after('id');
			//$table->integer('room_id')->after('gallery_id');
			//$table->integer('testimoni_id')->after('room_id');
		});
	}

}
