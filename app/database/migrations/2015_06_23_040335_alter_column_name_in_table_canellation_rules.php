<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnNameInTableCanellationRules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cancellation_rules', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE cancellation_rules CHANGE COLUMN name name VARCHAR(60) NOT NULL AFTER id");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cancellation_rules', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE cancellation_rules CHANGE COLUMN name name VARCHAR(30) NOT NULL AFTER id");
		});
	}

}
