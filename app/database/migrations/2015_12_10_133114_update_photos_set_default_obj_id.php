<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhotosSetDefaultObjId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE photos CHANGE object_id object_id int default 0');
			DB::statement('ALTER TABLE photos CHANGE room_id room_id int default 0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('photos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE photos CHANGE object_id object_id int');
			DB::statement('ALTER TABLE photos CHANGE room_id room_id int');
		});
	}

}
