<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoomSetFieldsNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE rooms CHANGE extrabed_price extrabed_price int default NULL');
			DB::statement('ALTER TABLE rooms CHANGE max_extrabed max_extrabed int default NULL');
			DB::statement('ALTER TABLE rooms CHANGE extrabed_get_breakfast extrabed_get_breakfast int default NULL');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rooms', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE rooms CHANGE extrabed_price extrabed_price int');
			DB::statement('ALTER TABLE rooms CHANGE max_extrabed max_extrabed int');
			DB::statement('ALTER TABLE rooms CHANGE extrabed_get_breakfast extrabed_get_breakfast int');

		});
	}
}
