<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePromotionsRooms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('promotions_rooms', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('room_id')->unsigned()->index('room_ids');
		    $table->integer('promotion_id')->unsigned()->index('promotion_ids');

		    // Relation with Promotions
			$table->foreign('room_id')
			->references('id')
				->on('rooms')
			->onDelete('cascade')
			->onUpdate('no action');

			// Relation with Rooms
		    $table->foreign('promotion_id')
		    ->references('id')
		    	->on('promotions')
		    ->onDelete('cascade')
		    ->onUpdate('no action');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('promotions_rooms');
	}

}
