<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRestrictions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('restrictions', function(Blueprint $table)
		{
			//$table->renameColumn('date', 'start_date');
			DB::statement('ALTER TABLE restrictions CHANGE date start_date DATE');
			$table->date('end_date')->after('start_date');
			$table->text('description')->after('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('restrictions', function(Blueprint $table)
		{
			//$table->renameColumn('start_date', 'date');
			DB::statement('ALTER TABLE restrictions CHANGE start_date date DATE');
			$table->dropColumn('end_date');
			$table->dropColumn('description');
			$table->dropColumn('created_at');
			$table->dropColumn('updated_at');
		});
	}

}
