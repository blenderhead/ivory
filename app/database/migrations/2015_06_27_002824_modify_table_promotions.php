<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTablePromotions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE promotions
				CHANGE COLUMN discount_amount discount_amount INT(11) NULL DEFAULT "0" AFTER booking_end_date,
				CHANGE COLUMN discount_rate discount_rate INT(11) NULL DEFAULT "0" AFTER discount_amount,
				CHANGE COLUMN day_in_advaces day_in_advaces INT(11) NULL DEFAULT "0" AFTER discount_rate,
				CHANGE COLUMN early_days early_days INT(11) NULL DEFAULT "0" AFTER day_in_advaces,
				CHANGE COLUMN free_night free_night INT(11) NULL DEFAULT "0" AFTER early_days,
				CHANGE COLUMN min_number_of_nigth min_number_of_nigth INT(11) NULL DEFAULT "0" AFTER free_night,
				CHANGE COLUMN min_number_of_room min_number_of_room INT(11) NULL DEFAULT "0" AFTER min_number_of_nigth'
			);

			$table->string('name', 120)->after('type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{		
			DB::statement('ALTER TABLE promotions
				CHANGE COLUMN discount_amount discount_amount INT(11) NOT NULL AFTER booking_end_date,
				CHANGE COLUMN discount_rate discount_rate INT(11) NOT NULL AFTER discount_amount,
				CHANGE COLUMN day_in_advaces day_in_advaces INT(11) NOT NULL AFTER discount_rate,
				CHANGE COLUMN early_days early_days INT(11) NOT NULL AFTER day_in_advaces,
				CHANGE COLUMN free_night free_night INT(11) NOT NULL AFTER early_days,
				CHANGE COLUMN min_number_of_nigth min_number_of_nigth INT(11) NOT NULL AFTER free_night,
				CHANGE COLUMN min_number_of_room min_number_of_room INT(11) NOT NULL AFTER min_number_of_nigth'
			);

			$table->dropColumn('name');
			$table->dropColumn('created_at');
			$table->dropColumn('updated_at');
		});
	}

}
