<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnMaxNumberOfRoomsInTablePromotions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE promotions CHANGE COLUMN max_number_of_room min_number_of_room INT(11) NOT NULL AFTER min_number_of_nigth");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE promotions CHANGE COLUMN min_number_of_room max_number_of_room INT(11) NOT NULL AFTER min_number_of_nigth");
		});
	}

}
