<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyRelaitonBeetwenPromoitonAndCancellation extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->dropForeign('promotions_cancellation_id_foreign');
			$table->dropColumn('cancellation_id');
		});

		Schema::table('cancellations', function(Blueprint $table)
		{
			$table->integer('promotion_id')->unsigned()->nullable()->after('id');

			$table->foreign('promotion_id')
			->references('id')
				->on('promotions')
			->onDelete('cascade')
			->onUpdate('no action');
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promotions', function(Blueprint $table)
		{
			$table->integer('cancellation_id')->unsigned()->nullable()->after('id');

			$table->foreign('cancellation_id')
			->references('id')
				->on('cancelltions')
			->onDelete('cascade')
			->onUpdate('no action');			
		});

		Schema::table('cancellations', function(Blueprint $table)
		{
			$table->dropForeign('cancellations_promotion_id_foreign');
			$table->dropColumn('promotion_id');
		});	
	}

}
