<?php

	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class UpdateTestimonialsAddLink extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::table('testimonials', function(Blueprint $table)
			{
				$table->text('link')->after('email');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::table('videos', function(Blueprint $table)
			{
				$table->dropColumn('link');
			});
		}

	}
