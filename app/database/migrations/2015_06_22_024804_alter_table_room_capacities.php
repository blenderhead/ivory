<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRoomCapacities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE room_capacities
		CHANGE COLUMN adult adult INT(11) NOT NULL DEFAULT 0 AFTER room_id,
		CHANGE COLUMN child child INT(11) NOT NULL DEFAULT 0 AFTER adult,
		CHANGE COLUMN extrabed extrabed INT(11) NOT NULL DEFAULT 0 AFTER child');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE room_capacities
		CHANGE COLUMN adult adult INT(11) NOT NULL AFTER room_id,
		CHANGE COLUMN child child INT(11) NOT NULL AFTER adult,
		CHANGE COLUMN extrabed extrabed INT(11) NOT NULL AFTER child');
	}

}
