<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCancellationRules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/** 
		*
		* Create Table that can used
		*/
		Schema::create('cancellation_rules', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->string('name', 30);
		    $table->tinyInteger('level');
		    $table->integer('within_days');
		    $table->integer('percent_charged');
		    $table->integer('night_charged');
		});

		Schema::create('cancellations', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('level_1_cancellation_rule_id')->nullable()->unsigned();
		    $table->integer('level_2_cancellation_rule_id')->nullable()->unsigned();
		    $table->integer('level_3_cancellation_rule_id')->nullable()->unsigned();

		    $table->index('level_1_cancellation_rule_id');
		    $table->index('level_2_cancellation_rule_id');
		    $table->index('level_3_cancellation_rule_id');
		});

		Schema::create('cancellation_rooms', function(Blueprint $table)
		{
		    $table->increments('id');
		    $table->integer('room_id')->unsigned();
		    $table->integer('cancellation_id')->unsigned();

		    $table->index('room_id');
		    $table->index('cancellation_id');	    
		});


		/* Create Column cancellation_id for relation between promotion to cancellation*/
		Schema::table('promotions', function(Blueprint $table)
		{
		    $table->integer('cancellation_id')->unsigned()->nullable()->after('id');

		    $table->index('cancellation_id');
		});

		/**
		* Build Relations
		*/
		Schema::table('cancellations', function(Blueprint $table)
		{
		    $table->foreign('level_1_cancellation_rule_id')
		    ->references('id')
		    	->on('cancellation_rules')
		    ->onDelete('set null')
		    ->onUpdate('no action');

		    $table->foreign('level_2_cancellation_rule_id')
		    ->references('id')
		    	->on('cancellation_rules')
		    ->onDelete('set null')
		    ->onUpdate('no action');

		    $table->foreign('level_3_cancellation_rule_id')
		    ->references('id')
		    	->on('cancellation_rules')
		    ->onDelete('set null')
		    ->onUpdate('no action');		    		    
		});

		Schema::table('cancellation_rooms', function(Blueprint $table)
		{
		    $table->foreign('room_id')
		    ->references('id')
		    	->on('rooms')
		    ->onDelete('cascade')
		    ->onUpdate('no action');

		    $table->foreign('cancellation_id')
		    ->references('id')
		    	->on('cancellations')
		    ->onDelete('cascade')
		    ->onUpdate('no action');
		});

		Schema::table('promotions', function(Blueprint $table)
		{
		    $table->foreign('cancellation_id')
		    ->references('id')
		    	->on('cancellations')
		    ->onDelete('cascade')
		    ->onUpdate('no action');
		});				
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cancellation_rules');
		Schema::drop('cancellations');
		Schema::drop('cancellation_rooms');

		Schema::table('promotions', function(Blueprint $table)
		{
			$table->dropForeign('promotions_cancellation_id');
			$table->dropColumn('cancellation_id');

		});
	}

}
