<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBanquetAddPhotoId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banquets', function(Blueprint $table)
		{
			$table->integer('photo_id')->after('setup');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banquets', function(Blueprint $table)
		{
			$table->dropColumn('photo_id');
		});
	}

}
