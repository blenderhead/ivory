<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnTimestampsInTableRoomFacilitiesAndRoomCapacities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('room_facilities', function($table)
		{
			$table->timestamps();
		});

		Schema::table('room_capacities', function($table)
		{
			$table->timestamps();
		});				
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('room_facilities', function($table)
		{
			 $table->dropColumn(array('created_at', 'updated_at'));
		});

		Schema::table('room_capacities', function($table)
		{
			 $table->dropColumn(array('created_at', 'updated_at'));
		});	
	}

}
