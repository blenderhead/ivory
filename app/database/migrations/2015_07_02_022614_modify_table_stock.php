<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTableStock extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('stock', function(Blueprint $table)
		{
			$table->integer('price')->after('date');
			$table->integer('sold')->nullable()->after('stock');
			$table->tinyInteger('stop_sell')->nullable()->after('sold');
			$table->tinyInteger('stop_promotion')->nullable()->after('stop_sell');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('stock', function(Blueprint $table)
		{
			$table->dropColumn('price');
			$table->dropColumn('sold');
			$table->dropColumn('stop_sell');
			$table->dropColumn('stop_promotion');
			$table->dropColumn('created_at');
			$table->dropColumn('updated_at');
		});
	}

}
