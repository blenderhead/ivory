<?php

	class Cancellation extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'cancellations';

		/**
		* Relation to model Room
		*/
		public function rooms()
		{
			return $this->belongsToMany('Room', 'cancellation_rooms', 'cancellation_id', 'room_id');
		}

		/**
		* Relation to model Room
		*/
		public function promotion()
		{
			return $this->belongsTo('Promo');
		}		

		/**
		* Get Cancellation Rule level 1
		*
		* @return object orm cancellation rule level 1
		*/
		public function level1Policy()
		{
			return Cancellation\Rule::where('id', $this->level_1_cancellation_rule_id)->first();
		}

		/**
		* This method get Cancellation Rule level 2
		*
		* @return object orm cancellation rule level 2
		*/
		public function level2Policy()
		{
			return Cancellation\Rule::where('id', $this->level_2_cancellation_rule_id)->first();
		}

		/**
		* This method get Cancellation Rule level 3
		*
		* @return object orm cancellation rule level 3
		*/
		public function level3Policy()
		{
			return Cancellation\Rule::where('id', $this->level_3_cancellation_rule_id)->first();
		}						

		/**
		* For create cancellation
		*
		* @param array $data is data from input user
		* @return object orm cancellation has created
		*/
		public function createCancellation($data)
		{	
			$this->start_date = $data['start_date'];
			$this->end_date = $data['end_date'];
			$this->level_1_cancellation_rule_id = $data['level_1_policy'];
			$this->level_2_cancellation_rule_id = $data['level_2_policy'];
			$this->level_3_cancellation_rule_id = $data['level_3_policy'];

			$this->save();

			/* Assign cancellation to rooms */
			$this->rooms()->attach($data['room_ids']);

			return $this;
		}

		/**
		* For update cancellation
		*
		* @param array $data is data from input user
		* @return object orm cancellation has created
		*/
		public function updateCancellation($data)
		{	
			$this->start_date = $data['start_date'];
			$this->end_date = $data['end_date'];
			$this->level_1_cancellation_rule_id = $data['level_1_policy'];
			$this->level_2_cancellation_rule_id = $data['level_2_policy'];
			$this->level_3_cancellation_rule_id = $data['level_3_policy'];

			$this->save();

			/* Assign cancellation to rooms */
			$this->rooms()->sync($data['room_ids']);

			return $this;
		}

		/**
		* This method for check the room has any cancellation or not.
		* 
		* @param array $rooms is object orm room
		* @return object orm room with not have cancellation
		*/
		public static function checkCancellation($rooms)
		{
			$room_not_have_cancellation = [];

			foreach ($rooms as $room) {

				if (!$room->cancellations->contains(24)) {
					array_push($room_not_have_cancellation, $room);
				}
			}

			return $room_not_have_cancellation;
		}

		/**
		* This method for create promotion costum cancellaiton
		* 
		* @param int $level_1 this cancellation id level 1
		* @param int $level_2 this cancellation id level 2
		* @param int $level_3 this cancellation id level 3
		* @return object ORM cancellation created
		*/
		public function createCancellationPromotion($promotion_id, $level_1, $level_2 = NULL, $level_3 = NULL)
		{
			$this->promotion_id = $promotion_id;
			$this->start_date = NULL;
			$this->end_date = NULL;
			$this->level_1_cancellation_rule_id = $level_1;
			$this->level_2_cancellation_rule_id = $level_2;
			$this->level_3_cancellation_rule_id = $level_3;

			$this->save();

			return $this;
		}

		/**
		* This method for update promotion costum cancellaiton
		* 
		* @param int $level_1 this cancellation id level 1
		* @param int $level_2 this cancellation id level 2
		* @param int $level_3 this cancellation id level 3
		* @return object ORM cancellation updateed
		*/
		public function updateCancellationPromotion($promotion_id, $level_1, $level_2 = NULL, $level_3 = NULL)
		{
			$this->promotion_id = $promotion_id;
			$this->start_date = NULL;
			$this->end_date = NULL;
			$this->level_1_cancellation_rule_id = $level_1;
			$this->level_2_cancellation_rule_id = $level_2;
			$this->level_3_cancellation_rule_id = $level_3;

			$this->save();

			return $this;
		}	
	}