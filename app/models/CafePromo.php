<?php
	
	namespace  Ivory\Backend\Models;

	use Cviebrock\EloquentSluggable\SluggableInterface;
	use Cviebrock\EloquentSluggable\SluggableTrait;

	use Eloquent;

	class CafePromo extends Eloquent implements SluggableInterface
	{
		use SluggableTrait;

    	protected $sluggable = [
        	'build_from' => 'name',
        	'save_to'    => 'slug',
        	'on_update' => true
    	];

		/**
		* Used Table
		*/		
		protected $table = 'cafe_promos';

		public static function setActive($promo_id)
		{
			CafePromo::where('id',$promo_id)->update(array(
				'status' => 1
			));

			CafePromo::whereNotIn('id', array($promo_id))->update(array(
				'status' => 0
			));

			return 1;
		}

		public static function getActivePromo()
		{
			$promo = CafePromo::where('status',1)->get();
			return $promo;
		}
	}