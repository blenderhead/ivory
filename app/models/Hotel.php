<?php

	class Hotel extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'hotel';

		public function facilities()
		{
			return $this->hasMany('HotelFacility');
		}

		public function services()
		{
			return $this->hasMany('HotelService');
		}

		public static function getHotelId()
		{
			$id = 0;
			$hotel = Hotel::all();
			
			if(!$hotel->isEmpty())
			{
				$hotel->each(function($h) use (&$id) {
					$id = $h->id;
				});
			}

			return $id;
		}
	}