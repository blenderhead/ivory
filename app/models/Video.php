<?php

    namespace Ivory\Backend\Models;

    use Eloquent;

    class Video extends Eloquent 
    {
        protected $table = 'videos';
    }