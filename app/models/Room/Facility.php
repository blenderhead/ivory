<?php

	namespace Room;

	use Eloquent;

	/**
	* 
	*/
	class Facility extends Eloquent
	{

		/**
		* Used Table
		*/
		protected $table = 'room_facilities';


		/**
		* Relation to model Room
		*/
		public function room()
		{
			return $this->belongsTo('Room');
		}

		/**
		* For Create facility
		*
		* @param array $facilities is array facilities
		* @param int $room_id is room id		
		* @return object this model
		*/
		public function createFacility($room_id, $facilities)
		{
			foreach ($facilities as $facility) {
				if ($facility != "") {
					
					$obj_facility = new Facility();

					$obj_facility->room_id = $room_id;
					$obj_facility->name = $facility;

					$obj_facility->save();
				}
			}

			return $this;
		}

		/**
		* For update facility
		*
		* @param array $facilities is array facilities
		* @param int $room_id is room id		
		* @return object this model
		*/
		public function updateFacility($room_id, $facilities)
		{
			/* Delete Existing Capacities */
			$this->where('room_id', '=', $room_id)->delete();

			/*Create again capacity*/
			$this->createFacility($room_id, $facilities);

			return $this;				
		}						

	}