<?php

	namespace Room;

	use Eloquent;

	/**
	* 
	*/
	class Capacity extends Eloquent
	{

		/**
		* Used Table
		*/
		protected $table = 'room_capacities';

		/**
		* Relation to model Room
		*/
		public function room()
		{
			return $this->belongsTo('Room');
		}

		/**
		* For Create capacity
		*
		* @param array $capacities is array capacities
		* @param int $room_id is room id
		* @return object this model
		*/
		public function createCapacity($room_id, $capacities)
		{
			foreach ($capacities as $capacity) {
				
				$obj_capacity = new Capacity();
				
				$obj_capacity->room_id = $room_id;
				$obj_capacity->adult = $capacity['adult'];
				$obj_capacity->child = $capacity['child'];
				$obj_capacity->extrabed = $capacity['bed'];

				$obj_capacity->save();
			}

			return $this;		
		}

		/**
		* For update capacity
		*
		* @param array $capacities is array capacities
		* @param int $room_id is room id
		* @return object this model
		*/
		public function updateCapacity($room_id, $capacities)
		{
			/* Delete Existing Capacities */
			$this->where('room_id', '=', $room_id)->delete();

			/*Create again capacity*/
			$this->createCapacity($room_id, $capacities);

			return $this; 		
		}					

	}