<?php

	use Carbon\Carbon;

	class Booking extends Eloquent
	{
		protected $table = 'bookings';

		public function room()
		{
			return $this->belongsTo('Room');
		}

		public function transaction()
		{
			return $this->hasOne('Transaction');
		}

		public static function decryptQuery($query) {


			$encryptedQuery = base64_decode($query);
			$encryptedQuery = explode('|', $encryptedQuery);
			$newquery = [];

			foreach ($encryptedQuery as $key => $value) {

				switch ($key) {
					case 0 :
						$newquery['stay_start_date'] = Carbon::createFromFormat('Y-m-d', $value);
						break;
					case 1 :
						$newquery['stay_end_date'] = Carbon::createFromFormat('Y-m-d', $value);
						break;
					case 2 :
						$newquery['room'] = $value;
						break;
					case 3 :
						$newquery['adult'] = $value;
						break;																		
					case 4 :
						$newquery['child'] = $value;
						break;					
				}
			}

			$newquery['original_query'] = $query;
			
			return $newquery;
		}

		public static function getBookingState($state)
		{
			$result = null;

			switch ($state) 
			{
				case 'done':
					$state_code = '1';
					break;

				case 'pending':
					$state_code = '2';
					break;

				case 'new':
					$state_code = '3';
					break;

				case 'cancel':
					$state_code = '4';
					break;
			}

			$bookings = Booking::all();

			if(!$bookings->isEmpty())
			{
				$new_result = $bookings->filter(function($booking) use ($state_code) {
					if($booking->transaction->status == $state_code)
					{
						return true;
					}

					return false;
				});

				$result = $new_result;
			}

			return $result;
		}

		public static function getNewBookings()
		{
			$new_bookings = Booking::where('is_new',1)->get();
			return $new_bookings;
		}
	}