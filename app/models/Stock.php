<?php
	
	use Carbon\Carbon;
	use Restriction;
	use Illuminate\Database\Eloquent\Collection;

	class Stock extends Eloquent
	{
		protected $table = 'stock';

		/**
		 * Constructor
		 * @param int $room_id room_id
		 * @param date $date date format Y-m-d
		 */
		public function __construct($room_id = NULL, $date = NULL) {
			$this->room_id = $room_id;
			$this->date = $date;
		}

		/**
		* Relation to model Promotion
		*/
		public function room()
		{
			return $this->belongsTo('Room');
		}

		/**
		 * This for create dummy data
		 * @return Object
		 */
		public function createDummyData($date)
		{
			$this->sold = 0;
			$this->stop_sell = 0;
			$this->stop_promotion = 0;
			$this->price = 0;
			$this->member_price = 0;
			$this->stock = 0;
			$this->date = $date;

			return $this;		
		}

		/**
		 * This for create new stock
		 * @param  array $data stock
		 * @return this
		 */
		public function saveStock($dataStock)
		{
			$this->stock = $dataStock['stock'];
			$this->price = $dataStock['price'];
			$this->member_price = $dataStock['member_price'];
			$this->stop_sell = isset($dataStock['stopsell']) ? $dataStock['stopsell'] : 0;
			$this->stop_promotion = isset($dataStock['stoppromo']) ? $dataStock['stoppromo'] : 0;
			$this->save();
		}

		public function getStockRoom($room, $data)
		{

			$stocks = $room->stock()
			->where('date', '>', $data['stay_start_date']->toDateString())
			->where('date', '<=', $data['stay_end_date']->toDateString())
			->where('stock', '>=', $data['room'])
			->get()
			->sortBy('date');

			$stocks = $this->checkStockAvailable($stocks->keyBy('date'), $data);

			return $stocks;
		}

		private function checkStockAvailable($stocks, $data)
		{
			if ($stocks->isEmpty()) {
				return new Collection();
			}

			$now = Carbon::createFromFormat('Y-m-d', $stocks->first()->date);

			if (count($stocks) != $data['stay_start_date']->diffInDays($data['stay_end_date'])) {
				return new Collection;
			}

			/*
			if (Restriction::checkRestriction($data['stay_start_date']->format('Y-m-d'), $data['stay_end_date']->format('Y-m-d'))->isEmpty() == FALSE) {
				return new Collection;
			}
			*/

			foreach ($stocks as $stock) {

				if ($stock->stock == 0) {
					return new Collection();
				}

				if ($stock->stop_sell == 1) {
					return new Collection();
				}
			}

			return $stocks;
		}

		public function decrease($numberStock = 1)
		{
			$this->stock = $this->stock - $numberStock;
			$this->save();

			return $this;
		}		

		public static function updateStock($date_start, $date_end, $amount, $op_type = 'substract')
		{
			$stock = Stock::where('date','>=',$date_start)->where('date','<=',$date_end)->get();

			if(!$stock->isEmpty())
			{
				$stock->each(function($s) use ($amount, $op_type) {
					$stock = $s->stock;

					switch($op_type)
					{
						case 'add':
							$s->stock = $stock + $amount;
							break;

						case 'substract':
							$s->stock = $stock - $amount;
							break;

						default:
							$s->stock = $stock - $amount;
							break;
					}
					
					$s->save();
				});
			}
		}

		public static function promoIsOn($date)
		{
			$stock = Stock::where('date',$date)->first();

			if($stock)
			{
				if($stock->stop_promotion)
				{
					return false;
				}
			}

			return true;
		}
	}