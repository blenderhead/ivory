<?php

	class Restriction extends Eloquent 
	{
		protected $table = 'restrictions';

		public static function checkRestriction($checkin_date, $checkout_date)
		{
			$range = [$checkin_date, $checkout_date];
			//$restrictions = Restriction::where('start_date','>=',"2016-01-08")->where('end_date','<=',"2016-01-09")->get();
			$restrictions = Restriction::whereBetween('start_date', $range)->orwhereBetween('end_date', $range)->get();
			return $restrictions;
		}
	}