<?php
namespace  Ivory\Backend\Models;

use Config;
use Eloquent;

class Gallery extends Eloquent {

    protected $table = 'gallery';
    /**
     * Gallery associations
     * @return Gallery display gallery relations
     */
    public function galleryCategories()
    {
        return $this->belongsTo('Ivory\Backend\Models\GalleryCategories','gallery_category_id');
    }

    public function photos() 
    {
        return $this->hasMany('Ivory\Backend\Models\Photos');
    }

    // CRUD
    /**
     * Get all Gallery
     * @return GalleryCategories
     */
    public static function getAllGalleries()
    {
        $galleries = Gallery::all();
        return $galleries;
    }

    public function createGallery($input, $photos = array()) 
    {
        $this->name = $input['name'];
        $this->description = $input['description'];
        $this->gallery_category_id = $input['category'];
        $this->is_publish = $input['publish'];
        $this->save();
        return $this->id;

        /*
        if($this->save()) {
            if(count($photos) > 0 ) {
                foreach ($photos['files'] as $index => $file) {
                    $photos = new Photos();
                    $photos->gallery_id = $this->id;
                    $photos->file = $file;
                    $photos->destination = Config::get('path.gallery_upload_path');
                    $photos->type = 'gallery';
                    try{
                        $photos->save();
                    }catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            }
            return true;
        }
        return false;   
        */
    }
    
    public function editGallery($input, $photos = array()) {
        $id = $input['_id'];

        if($id === NULL) return FALSE;

        $gallery = Gallery::find($id);
        $gallery->name = $input['name'];
        $gallery->description = $input['description'];
        $gallery->gallery_category_id = $input['gallery_category_id'];
        $gallery->is_publish = $input['is_publish'];
        if($gallery->save()) {
            if(count($photos) > 0 ) {
                foreach ($photos['files'] as $index => $file) {
                    $photos = new Photos();
                    $photos->gallery_id = $gallery->id;
                    $photos->file = $file;
                    $photos->destination = 'uploads/gallery/';
                    $photos->type = 'gallery';
                    try{
                        $photos->save();
                    }catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            }
            return true;
        }
        return false;    
    }

    public function deleteGallery($id = NULL) {

        if($id === NULL) return FALSE;

        $gallery = Gallery::find($id);
        if($gallery->delete()) return true;
        else return false;
    }

    public static function setPublished($gallery_id)
    {
        Gallery::where('id',$gallery_id)->update(array(
            'is_publish' => 1
        ));

        /*
        Gallery::whereNotIn('id', array($gallery_id))->update(array(
            'is_publish' => 0
        ));
        */

        //return 1;
    }

    public static function getGalleryPhotosByCategory($category_id)
    {
        $gallery_ids = array();
        $gallery_photos = array();

        $galleries = Gallery::where('gallery_category_id',$category_id)->where('is_publish',1)->get();

        if(!$galleries->isEmpty())
        {
            $galleries->each(function($gallery) use (&$gallery_ids) {
                array_push($gallery_ids, $gallery->id);
            });

            $photos = Photos::whereIn('object_id',$gallery_ids)->get();

            if(!$photos->isEmpty())
            {
                $gallery_photos = $photos;
            }
        }

        return $gallery_photos;
    }

    public static function getGalleryPhotosById($id)
    {
        $gallery_ids = array();
        $gallery_photos = array();

        $galleries = Gallery::where('id',$id)->where('is_publish',1)->get();

        if(!$galleries->isEmpty())
        {
            $galleries->each(function($gallery) use (&$gallery_ids) {
                array_push($gallery_ids, $gallery->id);
            });

            $photos = Photos::whereIn('object_id',$gallery_ids)->get();

            if(!$photos->isEmpty())
            {
                $gallery_photos = $photos;
            }
        }

        return $gallery_photos;
    }

    public static function getGalleryDataByCategory($category_id)
    {
        $result = null;

        $galleries = Gallery::where('gallery_category_id',$category_id)->where('is_publish',1)->get();

        if(!$galleries->isEmpty())
        {
            $result = $galleries;
        }

        return $result;
    }
}