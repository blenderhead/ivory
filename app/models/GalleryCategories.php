<?php

    namespace Ivory\Backend\Models;

    use Cviebrock\EloquentSluggable\SluggableInterface;
    use Cviebrock\EloquentSluggable\SluggableTrait;

    use Eloquent;

    class GalleryCategories extends Eloquent implements SluggableInterface
    {
        use SluggableTrait;

        protected $sluggable = [
            'build_from' => 'name',
            'save_to'    => 'slug',
            'on_update' => true
        ];
        
        /**
         * Gallery associations
         * @return Gallery display gallery relations
         */
        public function galleries()
        {
            return $this->hasMany('models\Gallery');
        }

        // CRUD
        /**
         * Get all Gallery categories
         * @return GalleryCategories
         */
        public static function getAllCategories()
        {
            $categories = GalleryCategories::all();
            return $categories;
        }

        public function createCategories($input) 
        {
            $this->name = strtolower($input['gallery_name']);
            $this->description = $input['description'];
            if($this->save()) {
                return true;
            }
            return false;   
        }
        
        public function editCategories($input) 
        {
            $id = $input['_id'];

            if($id === NULL) return FALSE;

            $category = GalleryCategories::find($id);
            $category->name = strtolower($input['gallery_name']);
            $category->description = $input['description'];
            $category->save();

            return $category;   
        }

        public function deleteCategories($id = NULL) {

            for($i=0;$i<count($id);$i++)
            {
                $category = GalleryCategories::find($id[$i]);
                $category->delete();
            }
             
        }

        public static function getCategoryIdByName($category_name)
        {
            $id = null;

            $category = GalleryCategories::where('name',$category_name)->first();

            if($category)
            {
                $id = $category->id;
            }
            
            return $id;
        }
    }