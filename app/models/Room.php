<?php
	
	use Cviebrock\EloquentSluggable\SluggableInterface;
	use Cviebrock\EloquentSluggable\SluggableTrait;

	use Ivory\Backend\Models\Photos;

	class Room extends Eloquent implements SluggableInterface
	{
		use SluggableTrait;

    	protected $sluggable = [
        	'build_from' => 'name',
        	'save_to'    => 'slug',
        	'on_update' => true
    	];


    	public function __toString()
    	{
    		return $this->name;
    	}

		/**
		* Used Table
		*/		
		protected $table = 'rooms';

		/**
		* Relation to model Hotel
		*/
		public function hotel()
		{
			return $this->belongsTo('Hotel');
		}

		/**
		* Relation to model Photo
		*/
		public function photo()
		{
			return $this->hasOne('Ivory\Backend\Models\Photos');
		}

		/**
		* Relation to model Facility
		*/
		public function facilities()
		{
			return $this->hasMany('Room\Facility');
		}

		/**
		* Relation to model Capacity
		*/
		public function capacities()
		{
			return $this->hasMany('Room\Capacity');
		}

		/**
		* Relation to model Stock
		*/
		public function stock()
		{
			return $this->hasMany('Stock');
		}

		/**
		* Relation to model Booking
		*/
		public function bookings()
		{
			return $this->hasMany('Booking');
		}

		/**
		* Relation to model Promotion
		*/
		public function promotions()
		{
			return $this->belongsToMany('Promo', 'promotions_rooms', 'room_id', 'promotion_id');
		}

		/**
		* Relation to model PromotionRoom
		*/
		public function cancellations()
		{
			return $this->belongsToMany('Cancellation', 'cancellation_rooms');
		}

		protected $fillable = array('photo_id');

		/**
		* For create room
		*
		* @param array $data is data from input user
		* @return bool is createded or not;
		*/
		public function createRoom($data)
		{
			$this->hotel_id = Hotel::getHotelId() ? Hotel::getHotelId() : 0;
			$this->name = $data['room_name'];
			$this->description = $data['room_description'];
			$this->room_size = $data['room_size'];
			$this->number_of_beds = $data['number_of_beds'];
			$this->publish_price = $data['publish_price'];
			$this->member_price = $data['member_price'];
			$this->adult_price = $data['adult_price'];
			$this->child_price = $data['child_price'];
			$this->adult_breakfast_price = $data['adult_breakfast_price'];
			$this->child_breakfast_price = $data['child_breakfast_price'];
			$this->breakfast_included = $data['include_breakfast'];
			$this->number_person_breakfast_included = $data['number_person_breakfast_included'];
			//$this->extrabed_price = $data['extra_bed_price'];
			//$this->max_extrabed = $data['extra_bed_number'];
			//$this->extrabed_get_breakfast = $data['extra_breakfast'];

			//$this->photo_id = Helper::processRoomImage($data['room_image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.room_upload_path'), null, 'room');
			
			$this->gallery_id = $data['gallery_id'];

			if($this->save()) 
			{
				return true;
			}

			return false;	
		}

		/**
		* For update room
		*
		* @param array $data is data from input user
		* @return bool is updated or not;
		*/
		public function updateRoom($data)
		{
			$room = $this->find($data['id']);

			/**
			* Set Value Room
			*/
			$room->name = $data['room_name'];
			$room->description = $data['room_description'];
			$room->room_size = $data['room_size'];
			$room->number_of_beds = $data['number_of_beds'];
			$room->publish_price = $data['publish_price'];
			$room->member_price = $data['member_price'];
			$room->adult_price = $data['adult_price'];
			$room->child_price = $data['child_price'];
			$room->adult_breakfast_price = $data['adult_breakfast_price'];
			$room->child_breakfast_price = $data['child_breakfast_price'];
			$room->breakfast_included = $data['include_breakfast'];
			$room->number_person_breakfast_included = $data['number_person_breakfast_included'];
			//$room->extrabed_price = $data['extra_bed_price'];
			//$room->max_extrabed = $data['extra_bed_number'];
			//$room->extrabed_get_breakfast = $data['extra_breakfast'];
			//$room->photo_id = Helper::processRoomImage($data['room_image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.room_upload_path'), $room->photo_id, 'room');
			$room->gallery_id = $data['gallery_id'];
			$room->save();

			return $room;
		}

		/**
		* This method from delete room
		*
		* @param array data from view
		* @return bool has deleted or not
		*/
		public function deleteRoom($room_id)
		{
			$room = $this->find($room_id);

			$room_photo = Helper::getImage($room->photo_id);

			if($room_photo)
			{
				$delete_photo = File::delete(Config::get('path.room_upload_path') . $room_photo);
				Photos::where('id',$room->photo_id)->delete();
			}

			if ($room->delete()) {
				return true;
			}

			return false;
		}


		public function getRoomByCapacity($data, $room_id = NULL)
		{
			$rooms = Room::join('room_capacities', 'rooms.id', '=','room_capacities.room_id')
			->where('room_capacities.adult', '=', $data['adult'])
			->where('room_capacities.child', '=', $data['child']);
			
			if ($room_id != NULL) {
				$rooms = $rooms->where('rooms.id', '=', $room_id);	
			}

			$rooms = $rooms->select('rooms.*', 'room_capacities.adult', 'room_capacities.child')
			->groupBy('rooms.id');

			if ($room_id != NULL) {
				$rooms = $rooms->first();
			} else {
				$rooms = $rooms->get();	
			}
			

			return $rooms;
		}

		/**
		* Method untuk kalkulasi semua harga
		* methid ini harus di eksekusi secara berurutan 
		* 1. Kalkulasi harga final yang di bayar oleh user
		* 2. kalkulasi harga service
		* 3. kalkulasi harga pajak
		* 4. kalkulasi harga room tanpa pajak dan service
		*/
		public function calculatePrice($data)
		{
			$this->__set('room_price', $this->calculateRoomPrice());
			$this->__set('service_price', $this->calculateServicePrice());
			$this->__set('tax_price', $this->calculateTaxPrice());
			$this->__set('price_withhout_tax_and_service', $this->calculatePriceWithoutTaxAndService());
			$this->__set('avarage_price_per_night', $this->calculateAvaragePricePerNight());
			$this->__set('final_service_price', $this->calculateFinalServicePrice());
			$this->__set('final_tax_price', $this->calculateFinalTaxPrice());
			$this->__set('final_price', $this->calculateFinalPrice($data['room']));
		}

		public function calculateAvaragePricePerNight()
		{
			return $this->final_price/count($this->stocks);
		}

		public function calculateRoomPrice()
		{
			$finalPrice = 0;

			foreach ($this->stocks as $stock) 
			{
				$finalPrice += $stock->price;
			}

			return $finalPrice;
		}

		/**
		* kalkulasi harga final yang dibayar oleh user
		* 
		*/
		public function calculateFinalPrice($numberOfRoom)
		{
			$finalPrice = 0;

			foreach ($this->stocks as $stock) 
			{
				$finalPrice += $stock->price;
			}

			//return $finalPrice * $numberOfRoom;
			//return $finalPrice;
			return ($this->room_price + $this->service_price + $this->tax_price) * $numberOfRoom;
		}

		/**
		* kalkulasi harga service
		* 
		*/
		public function calculateServicePrice()
		{
			$service_charge = $this->hotel()->first()->service_charge;
			return $this->room_price * ($service_charge/100);
		}

		public function calculateFinalServicePrice()
		{
			$service_price = $this->calculateServicePrice();
			return $this->room_price + $service_price;
		}

		/**
		* kalkulasi harga pajak
		* 
		*/
		public function calculateTaxPrice()
		{
			$tax_charge = $this->hotel()->first()->tax_charge;
			return $this->calculateFinalServicePrice() * ($tax_charge/100);
		}

		public function calculateFinalTaxPrice()
		{
			$tax_price = $this->calculateTaxPrice();
			return $this->calculateFinalServicePrice() + $tax_price;
		}

		/**
		* kalkulasi harga final tanpa pajak dan serivce
		* 
		*/
		public function calculatePriceWithoutTaxAndService()
		{
			return ($this->final_price - $this->service_price) - $this->tax_price;
		}
	}