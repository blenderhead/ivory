<?php

	class HotelService extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'hotel_services';

		public function hotel()
		{
			return $this->belongsTo('Hotel');
		}

		public static function createServices($hotel_id, $services)
		{
			if($services)
			{
				foreach ($services as $service) 
				{
					if ($service != "") 
					{
						$hotel_service = new HotelService();
						$hotel_service->hotel_id = $hotel_id;
						$hotel_service->name = $service;
						$hotel_service->save();
					}
				}
			}
		}

		public static function updateServices($hotel_id, $services)
		{
			HotelService::where('hotel_id', '=', $hotel_id)->delete();
			HotelService::createServices($hotel_id, $services);
		}		
	}