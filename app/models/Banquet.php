<?php
	
	use Cviebrock\EloquentSluggable\SluggableInterface;
	use Cviebrock\EloquentSluggable\SluggableTrait;

	class Banquet extends Eloquent implements SluggableInterface
	{
		use SluggableTrait;

    	protected $sluggable = [
        	'build_from' => 'name',
        	'save_to'    => 'slug',
        	'on_update' => true
    	];

		/**
		* Used Table
		*/		
		protected $table = 'banquets';
	}