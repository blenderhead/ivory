<?php

	class Transaction extends Eloquent
	{
		protected $table = 'transactions';

		public function booking()
		{
			return $this->belongsTo('Booking');
		}

		public function generateTransactionId()
        {
            $id = mt_rand(10000000,99999999);

            $check = Transaction::where('transaction_id',$id)->get();

            while(!$check->isEmpty())
            {
                $id = mt_rand(10000000,99999999);
            }

            return $id;
        }
	}