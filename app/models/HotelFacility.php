<?php

	class HotelFacility extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'hotel_facilities';

		public function hotel()
		{
			return $this->belongsTo('Hotel');
		}

		public static function createFacility($hotel_id, $facilities)
		{
			if($facilities)
			{
				foreach ($facilities as $facility) 
				{
					if ($facility != "") 
					{
						$hotel_facility = new HotelFacility();
						$hotel_facility->hotel_id = $hotel_id;
						$hotel_facility->name = $facility;
						$hotel_facility->save();
					}
				}
			}
		}

		public static function updateFacility($hotel_id, $facilities)
		{
			HotelFacility::where('hotel_id', '=', $hotel_id)->delete();
			HotelFacility::createFacility($hotel_id, $facilities);
		}		
	}