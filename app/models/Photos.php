<?php

namespace Ivory\Backend\Models;

use Eloquent;

class Photos extends Eloquent {
    /**
     * Gallery associations
     * @return Gallery display gallery relations
     */
    public function gallery()
    {
        return $this->belongsTo('models\Gallery', 'gallery_id');
    }

    /**
     * Testimoni associations
     * @return Testimoni display testimoni relations
     */
    public function testimoni()
    {
        return $this->belongsTo('models\Testimoni', 'testimoni_id');
    }
}