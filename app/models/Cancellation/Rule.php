<?php
	
	namespace Cancellation;
	
	use Eloquent;

	class Rule extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'cancellation_rules';

		/**
		* Relation to model Cancellation
		*/
		public function cancellation()
		{
			return $this->belongsTo('Cancellation');
		}		
	}