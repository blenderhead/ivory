<?php
	
	namespace Cancellation;
	
	use Eloquent;

	class Room extends Eloquent
	{
		/**
		* Used Table
		*/		
		protected $table = 'cancellation_rooms';


		public function room()
		{
			return $this->belongsTo('Room', 'room_id');
		}

		public function cancellaiton()
		{
			return $this->belongsTo('Cancellation', 'cancellation_id');
		}					
	}