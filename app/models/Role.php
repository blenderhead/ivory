<?php

	use Zizaco\Entrust\EntrustRole;

	class Role extends EntrustRole
	{
		public static function getRoleId($role_name)
		{
			$result = 0;

			$role = Role::where('name',$role_name)->first();

			if($role)
			{
				$result = $role->id;
			}

			return $result;
		}
	}