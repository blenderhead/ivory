<?php
	
	use Carbon\Carbon;
	use Illuminate\Database\Eloquent\Collection;

	class Promo extends Eloquent {

		/**
		* Used Table
		*/		
		protected $table = 'promotions';

		/**
		* This relation with cancellation
		*/
		public function cancellation()
		{
			return $this->hasOne('Cancellation', 'promotion_id');
		}

		/**
		* This relatoin with rooms
		*/
		public function rooms()
		{
			return $this->belongsToMany('Room', 'promotions_rooms', 'promotion_id', 'room_id');
		}

		/**
		* Create promotion
		*
		* @param array $data is data from input user
		* @return object orm promotion created
		*/
		public function createPromotion($data)
		{	
			/* Selection value fee promotion */
			switch ($data['benifit_value']) {
				case '0':
					$this->discount_amount = NULL;
					$this->discount_rate = $data['value_discount'];
					$this->free_night = NULL;
					break;
				case '1':
					$this->discount_amount = $data['value_discount'];
					$this->discount_rate = NULL;
					$this->free_night = NULL;
					break;
				case '2':
					$this->discount_amount = NULL;
					$this->discount_rate = NULL;
					$this->free_night = $data['value_discount'];
					break;										
			}

			/* selection value within day */
			switch ($data['promo_type']) {
				case '1':
					$this->day_in_advaces = $data['whitin_day'];
					$this->early_days = NULL;
					break;
				case '2':
					$this->day_in_advaces = NULL;
					$this->early_days = $data['early_day'];
					break;					
				default:
					$this->day_in_advaces = NULL;
					$this->early_days = NULL;
					break;
			}

			/* set value */
			$this->type = $data['promo_type'];
			$this->name = $data['promotion_name'];
			$this->stay_start_date = $data['stay_start_date'];
			$this->stay_end_date = $data['stay_end_date'];
			$this->booking_start_date = $data['booking_start_date'];
			$this->booking_end_date = $data['booking_end_date'];
			$this->min_number_of_nigth = $data['min_stay_duration'];
			$this->min_number_of_room = $data['min_book_room'];
			$this->is_sunday = $data['is_sunday'];
			$this->is_monday = $data['is_monday'];
			$this->is_tuesday = $data['is_tuesday'];
			$this->is_wednesday = $data['is_wednesday'];
			$this->is_thursday = $data['is_thursday'];
			$this->is_friday = $data['is_friday'];
			$this->is_saturday = $data['is_saturday'];
			$this->save();

			return $this;
		}

		/**
		* Update Promotion 
		* 
		* @param array $data is data from input user
		* @return object orm promotion updated
		*/
		public function updatePromotion($data)
		{
			/* Selection value fee promotion */
			switch ($data['benifit_value']) {
				case '0':
					$this->discount_amount = NULL;
					$this->discount_rate = $data['value_discount'];
					$this->free_night = NULL;
					break;
				case '1':
					$this->discount_amount = $data['value_discount'];
					$this->discount_rate = NULL;
					$this->free_night = NULL;
					break;
				case '2':
					$this->discount_amount = NULL;
					$this->discount_rate = NULL;
					$this->free_night = $data['value_discount'];
					break;										
			}

			/* selection value within day */
			switch ($data['promo_type']) {
				case '1':
					$this->day_in_advaces = $data['whitin_day'];
					$this->early_days = NULL;
					break;
				case '2':
					$this->day_in_advaces = NULL;
					$this->early_days = $data['early_day'];
					break;					
				default:
					$this->day_in_advaces = NULL;
					$this->early_days = NULL;
					break;
			}

			/* set value */
			$this->type = $data['promo_type'];
			$this->name = $data['promotion_name'];
			$this->stay_start_date = $data['stay_start_date'];
			$this->stay_end_date = $data['stay_end_date'];
			$this->booking_start_date = $data['booking_start_date'];
			$this->booking_end_date = $data['booking_end_date'];
			$this->min_number_of_nigth = $data['min_stay_duration'];
			$this->min_number_of_room = $data['min_book_room'];
			$this->is_sunday = $data['is_sunday'];
			$this->is_monday = $data['is_monday'];
			$this->is_tuesday = $data['is_tuesday'];
			$this->is_wednesday = $data['is_wednesday'];
			$this->is_thursday = $data['is_thursday'];
			$this->is_friday = $data['is_friday'];
			$this->is_saturday = $data['is_saturday'];
			$this->save();			

			return $this;
		}

		/**
		* Delete Promotion 
		*
		* @param int $id is id promotion 
		* @return void
		*/
		public function deletePromotion($id)
		{
			$this->find($id);
			$this->delete();
		}

		public function getSinglePromotionRoom($room, $query, $promo_id)
		{
			$filter_day = $this->getFilterDay($query)->toArray();
			$now = Carbon::now();

			$promotions = $room->promotions()

			// Filter Stay Date
			->where('stay_start_date', '<=', $query['stay_start_date']->toDateString())
			->where('stay_end_date', '>=', $query['stay_end_date']->toDateString())

			// Filter Booking Date
			->where('booking_start_date', '<=', $now->toDateString())
			->where('booking_end_date', '>=', $now->toDateString())
			->where('promotions.id', '=', $promo_id)
			->where($filter_day)
			->get();

			$promotions = $this->filterType($promotions, $query);
			$promotions = $this->filterNigthAndRoom($promotions, $query);
			
			return $promotions->first();	
		}

		public function getPromotionRoom($room, $query)
		{
			$filter_day = $this->getFilterDay($query)->toArray();
			$now = Carbon::now();

			$promotions = $room->promotions()

			// Filter Stay Date
			->where('stay_start_date', '<=', $query['stay_start_date']->toDateString())
			->where('stay_end_date', '>=', $query['stay_end_date']->toDateString())

			// Filter Booking Date
			->where('booking_start_date', '<=', $now->toDateString())
			->where('booking_end_date', '>=', $now->toDateString())

			->where($filter_day)
			->get();

			$promotions = $this->filterType($promotions, $query);
			$promotions = $this->filterNigthAndRoom($promotions, $query);

			return $promotions;		
		}

		public function getFilterDay($query)
		{	

			$filter_day = new Collection;
			$stay_start_date  = Carbon::createFromFormat('Y-m-d', $query['stay_start_date']->toDateString());

			for ($stay_start_date; $query['stay_end_date']->gte($stay_start_date); $stay_start_date->addDay()) { 
				
				$filter_day->put('is_'.strtolower($query['stay_end_date']->format('l')), 1);

			}

			return $filter_day;
		}

		private function filterType($promotions, $query)
		{
			

			$promotions = $promotions->filter(function($promotion) use ($query){
				$now = Carbon::now();

				if ($promotion['type'] == 1) {
					if ($now->addDays($promotion['day_in_advaces'])->lte($query['stay_start_date'])) {
						return $promotion;
					}
				} elseif($promotion['type'] == 2) {
					if ($now->addDays($promotion['early_days'])->gte($query['stay_start_date'])) {
						return $promotion;
					}					
				} else {
					return $promotion;
				}
				
			});

			return $promotions;
		}

		private function filterNigthAndRoom($promotions, $query)
		{
			$promotions = $promotions->filter(function($promotion) use ($query){

				if ($query['stay_start_date']->diffInDays($query['stay_end_date']) >= $promotion['min_number_of_nigth'] 
					&& $query['room'] >= $promotion['min_number_of_room']) {
					return $promotion;
				} 
				
			});

			return $promotions;			
		}				

		public function calculatePrice($room)
		{
			/* Selection value fee promotion */
			if ($this->discount_amount) {
				
				$this->calculatePricePromoDiscountAmount($room);

			} elseif ($this->discount_rate) {

				$this->calculatePricePromoDiscountRate($room);

			} else {
				$this->calculatePricePromoDiscountNight($room);
			} 
		}

		private function calculatePricePromoDiscountAmount($room)
		{
			$this->__set('room_price', $room->room_price * $this->discount_amount/100);
			$this->__set('service_price', $room->service_price * $this->discount_amount/100);
			$this->__set('tax_price', $room->tax_price * $this->discount_amount/100);
			$this->__set('final_price', $room->final_price - ($room->final_price * ($this->discount_amount/100)));

			/*
			$this->__set('room_price', ($room->room_price - $room->discount_amount) * $this->discount_rate/100);
			$this->__set('final_price', $room->final_price - $room->discount_amount);
			$this->__set('service_price', $this->final_price * ($room->hotel()->first()->service_charge/100));
			$this->__set('tax_price', $this->final_price * ($room->hotel()->first()->tax_charge/100));
			$this->__set('price_withhout_tax_and_service', ($this->final_price - $this->service_price) - $this->tax);
			$this->__set('avarage_price_per_night', $this->final_price/count($room->stocks));
			*/
		}

		private function calculatePricePromoDiscountRate($room)
		{
			$this->__set('room_price', $room->room_price * $this->discount_rate/100);
			$this->__set('service_price', $room->service_price * $this->discount_rate/100);
			$this->__set('tax_price', $room->tax_price * $this->discount_rate/100);
			$this->__set('final_price', $room->final_price - ($room->final_price * ($this->discount_rate/100)));

			//$this->__set('final_price', $room->final_price - ($room->final_price * ($this->discount_rate/100)));
			//$this->__set('service_price', $this->final_price * ($room->hotel()->first()->service_charge/100));
			//$this->__set('tax_price', $this->final_price * ($room->hotel()->first()->tax_charge/100));
			//$this->__set('price_withhout_tax_and_service', ($this->final_price - $this->service_price) - $this->tax);
			//$this->__set('avarage_price_per_night', $this->final_price/count($room->stocks));
		}

		private function calculatePricePromoDiscountNight($room)
		{
			$this->__set('service_price', $room->service_price * ($room->hotel()->first()->service_charge/100));
			$this->__set('tax_price', $this->tax_price * ($room->hotel()->first()->tax_charge/100));
			$this->__set('price_withhout_tax_and_service', ($this->final_price - $this->service_price) - $this->tax);
			$this->__set('avarage_price_per_night', $this->final_price/count($room->stocks));
			$this->__set('final_price', $room->final_price - ($room->avarage_price_per_night * $this->free_night));

			/*
			$this->__set('final_price', $room->final_price - ($room->avarage_price_per_night * $this->free_night));
			$this->__set('service_price', $this->final_price * ($room->hotel()->first()->service_charge/100));
			$this->__set('tax_price', $this->final_price * ($room->hotel()->first()->tax_charge/100));
			$this->__set('price_withhout_tax_and_service', ($this->final_price - $this->service_price) - $this->tax);
			$this->__set('avarage_price_per_night', $this->final_price/count($room->stocks));
			*/
		}				
		
	}