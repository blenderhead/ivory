<?php
namespace  Ivory\Backend\Models;

use Config;
use Eloquent;

class Testimoni extends Eloquent {

    protected $table = 'testimonials';

    public function photos() 
    {
        return $this->hasOne('Ivory\Backend\Models\Photos');
    }

    // CRUD
    /**
     * Get all Testimoni
     * @return Testimoni
     */
    public static function getAllTestimonies()
    {
        $testimonies = Testimoni::all();
        return $testimonies;
    }

    public function createTestimoni($input, $photos = array()) {
        $fields = \Schema::getColumnListing($this->table);

        foreach ($fields as $key => $field) {
            if($field == 'id'||$field == 'testimoni_id') continue;
            $this->$field = $input[$field];
        }
        if($this->save()) {
            if(count($photos) > 0 ) {
                foreach ($photos['files'] as $index => $file) {
                    $photos = new Photos();
                    $photos->testimoni_id = $this->id;
                    $photos->file = $file;
                    $photos->destination = Config::get('path.testimoni_upload_path');
                    $photos->type = 'testimoni';
                    try{
                        $photos->save();
                    }catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            }
            return true;
        }
        return false;   
    }
    
    public function editTestimoni($input, $photos = array()) {
        $id = $input['_id'];

        if($id === NULL) return FALSE;

        $testimoni = Testimoni::find($id);
        $fields = \Schema::getColumnListing($this->table);
        foreach ($fields as $key => $field) {
            if($field == 'id'||$field == 'testimoni_id') continue;
            $testimoni->$field = $input[$field];
        }

        if($testimoni->save()) {
            if(is_array($photos) ) {
                $photo = new Photos();
                $photo->testimoni_id = $testimoni->id;
                $photo->file = $photos['files'][0];
                $photo->destination = $photos['destination'];
                $photo->type = 'testimoni';
                try
                {
                    $photo->save();
                }
                catch (\Exception $e) {
                   return $e->getMessage();
                }
            }
            return true;
        }
        return false;    
    }

    public function deleteTestimoni($id = NULL) {

        if($id === NULL) return FALSE;

        $testimoni = Testimoni::find($id);
        if($testimoni->delete()) return true;
        else return false;
    }
}