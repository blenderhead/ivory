<?php
	
	class IvoryCustomValidator extends \Illuminate\Validation\Validator
	{
		public function validateDateGreaterThan($attribute, $value, $parameters)
		{
			$this->requireParameterCount(1, $parameters, 'greater than');
			$value = strtotime($value);
			$other = strtotime(Input::get($parameters[0]));

			if($value > $other || $value == $other)
			{
				return TRUE;
			}
		}

		public function validateDateLessThan($attribute, $value, $parameters)
		{
			$this->requireParameterCount(1, $parameters, 'less than');
			$value = strtotime($value);
			$other = strtotime(Input::get($parameters[0]));

			if($value < $other || $value == $other)
			{
				return TRUE;
			}
		}

		protected function replaceDateLessThan($message, $attribute, $rule, $parameters)
		{
		    return str_replace(':other', 'end date', $message);
		}

		protected function replaceDateGreaterThan($message, $attribute, $rule, $parameters)
		{
		    return str_replace(':other', 'start date', $message);
		}

		protected function validateStocksRoom($attribute, $value, $parameters)
		{

			foreach ($value as $index => $dataStock) {

				if ($dataStock['stock'] == "" || $dataStock['price'] == "") {
					return FALSE;
				}

				if (!is_numeric($dataStock['price']) || !is_numeric($dataStock['stock'])) {
					return FALSE;
				}

				return TRUE;
			}
		}	

		protected function validateCaptcha($attribute, $value, $parameters)
	    {
	        return Captcha::check($value);
	    }
	}	
