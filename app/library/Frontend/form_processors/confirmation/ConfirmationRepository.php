<?php
	
	namespace Ivory\Frontend;

	use Input;

	class ConfirmationRepository extends BaseRepository
	{
		private $invoice_id;
		private $transfer_date;
		private $transfer_destination;
		private $transfer_amount;
		private $transfer_account_number;
		private $transfer_account;
		private $email;
		private $phone;
		
		public function getInput()
		{
			$this->invoice_id = Input::get('invoice_id');
			$this->transfer_date = Input::get('transfer_date');
			$this->transfer_destination = Input::get('transfer_destination');
			$this->transfer_amount = Input::get('transfer_amount');
			$this->transfer_account_number = Input::get('transfer_account_number');	
			$this->transfer_account = Input::get('transfer_account');	
			$this->email = Input::get('email');
			$this->phone = Input::get('phone');	
		}

		public function setValidationData()
		{
			$this->data = array(
				'invoice_id' => $this->invoice_id,
				'transfer_date' => $this->transfer_date,
				'transfer_destination' => $this->transfer_destination,
				'transfer_amount' => $this->transfer_amount,
				'transfer_account_number' => $this->transfer_account_number,
				'transfer_account' => $this->transfer_account,
				'email' => $this->email,
				'phone' => $this->phone,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'invoice_id' => 'required',
				'transfer_date' => 'required|date_format:Y-m-d',
				'transfer_destination' => 'required',
				'transfer_amount' => 'required|numeric',
				'transfer_account_number' => 'required',
				'transfer_account' => 'required',
				'email' => 'required|email',
				'phone' => 'required',
			);
		}
	}