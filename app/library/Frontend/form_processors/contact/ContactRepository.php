<?php
	
	namespace Ivory\Frontend;

	use Input;

	class ContactRepository extends BaseRepository
	{
		private $name;
		private $email;
		private $subject;
		private $captcha;
		private $message;
		
		public function getInput()
		{
			$this->name = Input::get('name');
			$this->email = Input::get('email');
			$this->subject = Input::get('subject');	
			$this->captcha = Input::get('captcha');
			$this->message = Input::get('message');
		}

		public function setValidationData()
		{
			$this->data = array(
				'name' => $this->name,
				'email' => $this->email,
				'subject' => $this->subject,
				'captcha' => $this->captcha,
				'message' => $this->message,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'subject' => 'required',
				'captcha' => 'required|captcha',
				'message' => 'required'
			);
		}
	}