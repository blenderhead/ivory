<?php
	
	namespace Ivory\Frontend;

	use Input;

	class BookingRepository extends BaseRepository
	{
		private $room_id;
		private $promotion_id;
		private $title;
		private $first_name;
		private $last_name;
		private $email;
		private $phone_number;
		private $booking_code;
		private $request_note;
		private $total_order;
		private $payment_type;
		private $checkin_date;
		private $checkout_date;
		private $number_of_rooms;
		private $number_of_adults;
		private $number_of_children;
		
		public function getInput()
		{
			$this->room_id = Input::get('room_id');
			$this->query_code = Input::get('query_code');
			$this->promotion_id = Input::get('promotion_id');
			$this->title = Input::get('title');	
			$this->first_name = Input::get('first_name');
			$this->last_name = Input::get('last_name');
			$this->email = Input::get('email');
			$this->phone_number = Input::get('phone_number');
			$this->booking_code = Input::get('booking_code');
			$this->request_note = Input::get('request_note');
			$this->total_order = Input::get('total_order');
			$this->payment_type = Input::get('payment_type');	
			$this->checkin_date = Input::get('checkin_date');
			$this->checkout_date = Input::get('checkout_date');
			$this->number_of_rooms = Input::get('number_of_rooms');
			$this->number_of_adults = Input::get('number_of_adults');
			$this->number_of_children = Input::get('number_of_children');
		}

		public function setValidationData()
		{
			$this->data = array(
				'room_id' => $this->room_id,
				'query_code' => $this->query_code,
				'promotion_id' => $this->promotion_id,
				'title' => $this->title,
				'first_name' => $this->first_name,
				'last_name' => $this->last_name,
				'email' => $this->email,
				'phone_number' => $this->phone_number,
				'booking_code' => $this->booking_code,
				'request_note' => $this->request_note,
				'total_order' => $this->total_order,
				'payment_type' => $this->payment_type,
				'checkin_date' => $this->checkin_date,
				'checkout_date' => $this->checkout_date,
				'number_of_rooms' => $this->number_of_rooms,
				'number_of_adults' => $this->number_of_adults,
				'number_of_children' => $this->number_of_children,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'room_id' => 'required',
				'query_code' => 'required',
				'title' => 'required',
				'first_name' => 'required',
				'email' => 'required|email',
				'phone_number' => 'required',
				'payment_type' => 'required',
			);
		}
	}