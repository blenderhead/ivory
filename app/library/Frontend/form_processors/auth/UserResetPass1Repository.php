<?php
	
	namespace Ivory\Frontend;

	use Input;

	class UserResetPass1Repository extends BaseRepository
	{
		private $email;
		
		public function getInput()
		{
			$this->email = Input::get('email');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'email' => $this->email,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'email' => 'required|email',
			);
		}
	}