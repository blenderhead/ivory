<?php
	
	namespace Ivory\Frontend;

	use Input;

	class UserResetPass2Repository extends BaseRepository
	{
		private $token;
		private $password;
		private $password_confirm;
		
		public function getInput()
		{
			$this->token = Input::get('token');
			$this->password = Input::get('password');
			$this->password_confirm = Input::get('password_confirm');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'token' => $this->token,
	            'password' => $this->password,
	            'password_confirm' => $this->password_confirm,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'token' => 'required',
	            'password' => 'required',
	            'password_confirm' => 'required|same:password',
			);
		}
	}