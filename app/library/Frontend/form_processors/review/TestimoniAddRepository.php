<?php
	
	namespace Ivory\Frontend;

	use Input;

	class TestimoniAddRepository extends BaseRepository
	{
		private $name;
		private $email;
		private $captcha;
		private $description;

		public function getInput()
		{
			$this->name = Input::get('name');
			$this->email = Input::get('email');
			$this->captcha = Input::get('captcha');
			$this->description = Input::get('description');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'name' => $this->name,
	            'email' => $this->email,
	            'captcha' => $this->captcha,
	            'description' => $this->description,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'name' => 'required',
	            'email' => 'required|email',
	            'captcha' => 'required|captcha',
	            'description' => 'required|max:140'
			);
		}
	}