<?php
	
	namespace Ivory\Frontend;

	use Input;

	class PaymentRepository extends BaseRepository
	{
		private $transaction_id;
		private $card_owner;
		private $card_number;
		private $card_exp_month;
		private $card_exp_year;
		private $card_cvv;
		
		public function getInput()
		{
			$this->transaction_id = Input::get('transaction_id');
			$this->card_owner = Input::get('card_owner');
			$this->card_number = Input::get('card_number');	
			$this->card_exp_month = Input::get('card_exp_month');
			$this->card_exp_year = Input::get('card_exp_year');
			$this->card_cvv = Input::get('card_cvv');
		}

		public function setValidationData()
		{
			$this->data = array(
				'transaction_id' => $this->transaction_id,
				'card_owner' => $this->card_owner,
				'card_number' => $this->card_number,
				'card_exp_month' => $this->card_exp_month,
				'card_exp_year' => $this->card_exp_year,
				'card_cvv' => $this->card_cvv
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'transaction_id' => 'required',
				'card_owner' => 'required',
				'card_number' => 'required|numeric',
				'card_exp_month' => 'required|numeric',
				'card_exp_year' => 'required|numeric',
				'card_cvv' => 'required|numeric',
			);
		}
	}