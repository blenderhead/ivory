<?php
	
	namespace Ivory\Frontend;

	use Input;

	class RoomSearchRepository extends BaseRepository
	{
		private $stay_start_date;
		private $stay_end_date;
		private $room;
		private $adult;
		private $child;
		
		public function getInput()
		{
			$this->stay_start_date = date('Y-m-d', strtotime(Input::get('stay_start_date')));
			$this->stay_end_date = date('Y-m-d', strtotime(Input::get('stay_end_date')));
			$this->room = Input::get('room');
			$this->adult = Input::get('adult');
			$this->child = Input::get('child');		
		}

		public function setValidationData()
		{
			$this->data = array(
				'stay_start_date' => $this->stay_start_date,
				'stay_end_date' => $this->stay_end_date,
				'room' => $this->room,
				'adult' => $this->adult,
				'child' => $this->child,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'stay_start_date' => 'required',
				'stay_end_date' => 'required',
				'room' => 'required|numeric',
				'adult' => 'numeric',
				'child' => 'numeric',
			);
		}
	}