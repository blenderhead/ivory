<?php
	
	namespace Ivory\Frontend;

	use Input;

	class BanquetEnquiryRepository extends BaseRepository
	{
		private $title;
		private $name;
		private $email;
		private $phone;
		private $room;
		private $start_date;
		private $end_date;
		private $message;
		
		public function getInput()
		{
			$this->title = Input::get('title');
			$this->name = Input::get('name');
			$this->email = Input::get('email');
			$this->phone = Input::get('phone');	
			$this->room = Input::get('room');	
			$this->start_date = Input::get('start_date');
			$this->end_date = Input::get('end_date');
			$this->message = Input::get('message');
		}

		public function setValidationData()
		{
			$this->data = array(
				'title' => $this->title,
				'name' => $this->name,
				'email' => $this->email,
				'room' => $this->room,
				'phone' => $this->phone,
				'start_date' => $this->start_date,
				'end_date' => $this->end_date,
				'message' => $this->message,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'name' => 'required',
				'email' => 'required|email',
				'phone' => 'required',
				'room' => 'required',
				'start_date' => 'required',
				'end_date' => 'required',
				'message' => 'required'
			);
		}
	}