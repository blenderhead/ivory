<?php
	
	namespace Ivory\Frontend;

	use Config;
	use Mail;

	use Transaction;
	use TransferConfirmation;

	class ConfirmationProcessor extends BaseProcessor
	{

		public function process($data)
		{
			try 
			{
				$transaction_data = Transaction::where('transaction_id',$data['invoice_id'])->first();

				if(!$transaction_data)
				{
					throw new \Exception("Invoice ID not found", 1000);
				}
				
				if($transaction_data->confirmed)
				{
					throw new \Exception("You already confirmed this transaction", 1000);
				}

				$confirmation = new TransferConfirmation();
				$confirmation->transaction_id = $data['invoice_id'];
				$confirmation->transfer_date = $data['transfer_date'];
				$confirmation->transfer_destination = $data['transfer_destination'];
				$confirmation->transfer_total = $data['transfer_amount'];
				$confirmation->transfer_account = $data['transfer_account'];
				$confirmation->transfer_account_number = $data['transfer_account_number'];
				$confirmation->email = $data['email'];
				$confirmation->phone = $data['phone'];
				$confirmation->save();

				$transaction_data->confirmed = 1;
				$transaction_data->save();

				return TRUE;
			} 
			catch (\Exception $e) 
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}