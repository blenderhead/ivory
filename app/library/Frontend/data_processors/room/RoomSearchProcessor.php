<?php
	
	namespace Ivory\Frontend;

	use Room;
	use Promo;
	use Stock;
	use Ivory\Backend\Models\Gallery;
	use DB;
	use Illuminate\Database\Eloquent\Collection;
	use Carbon\Carbon;

	class RoomSearchProcessor extends BaseProcessor
	{

		public function process($data)
		{
			try {

				$data['stay_start_date'] = Carbon::createFromFormat('Y-m-d', $data['stay_start_date']);
				$data['stay_end_date'] = Carbon::createFromFormat('Y-m-d', $data['stay_end_date']);

				$rooms = new Room;
				$rooms = $rooms->getRoomByCapacity($data);

				foreach ($rooms  as $key => $room) {

					$stocks = new Stock;
					$stocks = $stocks->getStockRoom($room, $data);
					
					if ($stocks->isEmpty()) {
						$rooms->forget($key);
						continue;
					}

					$promotions = new Promo;
					$promotions = $promotions->getPromotionRoom($room, $data);

					$room->__set('promotions', $promotions);	

					$room->__set('stocks', $stocks);
					
					$room->calculatePrice($data);

					$photos = Gallery::getGalleryPhotosById($room->gallery_id);

					$room->__set('photos', $photos);

					foreach ($room->promotions as $promotion) {
						$promotion->calculatePrice($room);
					}

					if(!Stock::promoIsOn($data['stay_start_date']->format('Y-m-d')))
					{
						$room->promotions = null;		
					}
				}

				$this->output = [
					'rooms' => $rooms,
					'query' => $data
					];

				return TRUE;

			} catch (Exception $e) {
				
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}