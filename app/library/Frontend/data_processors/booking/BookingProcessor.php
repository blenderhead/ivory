<?php
	
	namespace Ivory\Frontend;

	use Config;
	use Mail;

	use Booking;
	use Transaction;
	use Room;
	use Stock;
	use User;
	use Hotel;

	use Helper;
	use Request;
	use DB;

	class BookingProcessor extends BaseProcessor
	{

		public function process($data)
		{
			DB::beginTransaction();

			try 
			{
				$hotel = Hotel::all();

				$query = Booking::decryptQuery($data['query_code']);

				$room = new Room;
				$room = $room->getRoomByCapacity($query, $data['room_id']);
				
				$stocks = new Stock;
				$stocks = $stocks->getStockRoom($room, $query);

				if ($stocks->isEmpty()) {
					//throw new \Exception("Mohon maaf stock room sudah habis. Silahkan booking pada tanggal yang lain.");
					throw new \Exception("We're sorry, but the room's stock is empty. Please choose another date.");
				}

				/*
				foreach ($stocks as $stock) {
					$stock->decrease();
				}
				*/

				if($data['booking_code'])
				{
					$user = User::where('member_code',$data['booking_code'])->first();

					if(!$user)
					{
						throw new \Exception("Code is not acceptable.");
					}
				}

				$booking = new Booking();
				$booking->user_id = 0;
				$booking->room_id = $data['room_id'];
				$booking->promotion_id = $data['promotion_id'];
				$booking->currency_id = 0;
				$booking->nationality_id = 0;
				$booking->checkin_date = $data['checkin_date'];
				$booking->checkout_date = $data['checkout_date'];
				$booking->title = $data['title'];
				$booking->first_name = $data['first_name'];
				$booking->last_name = $data['last_name'];
				$booking->email = $data['email'];
				$booking->phone_number = $data['phone_number'];
				$booking->booking_code = $data['booking_code'];
				$booking->number_of_rooms = $data['number_of_rooms'];
				$booking->number_of_adults = $data['number_of_adults'];
				$booking->number_of_children = $data['number_of_children'];
				$booking->request_note = $data['request_note'];
				$booking->is_cancel = 0;
				$booking->is_new = 1;
				$booking->save();

				Stock::updateStock($booking->checkin_date, $booking->checkout_date,$booking->number_of_rooms);

				$transaction = new Transaction();
				$transaction->transaction_id = $transaction->generateTransactionId();
				$transaction->type = $data['payment_type'];
				$transaction->booking_id = $booking->id;
				$transaction->total = $data['total_order'];
				$transaction->discount = $data['booking_code'] ? $hotel->get(0)->member_discount : 0;
				$transaction->status = '2';
				$transaction->confirmed = 0;
				$transaction->save();

				$room_data = Room::find($data['room_id']);

				$this->output = $transaction->transaction_id;

				if($data['payment_type'] == 'transfer')
				{
					//$admin = Config::get('manager_emails.booking_manager');
					//$admin_bcc = Config::get('manager_emails.customer_service');

					$admin = $hotel->get(0)->email;
					$admin_bcc = $hotel->get(0)->manager_email;

					$user_data = array(
		                'to_admin' => $admin,
		                'to_admin_cc' => $admin_bcc,
		                'to_user' => $booking->email,
		                'room' => $room_data,
		                'booking' => $booking,
		                'transaction' => $transaction,
		                'title' => $data['title'],
		                'name' => $booking->first_name . ' ' . $booking->last_name,
		                'payment_method' => 'Bank Transfer',
		                'status' => 'Unpaid'
		            );

		            Mail::send('emails.booking.admin_notification', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_admin'], 'Ivory Booking Admin');
		                $message->subject('New Booking (Invoice #' . $user_data['transaction']->transaction_id . ')');
		            });

		            Mail::send('emails.booking.admin_notification', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_admin_cc'], 'Ivory Booking Admin');
		                $message->subject('New Booking (Invoice #' . $user_data['transaction']->transaction_id . ')');
		            });

		            Mail::send('emails.booking.transfer_notification', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_user'], 'Ivory Booking Admin');
		                $message->subject('Ivory Booking (Invoice #' . $user_data['transaction']->transaction_id . ')');
		            });
				}

				DB::commit();
				return TRUE;

			} 
			catch (\Exception $e) 
			{
				DB::rollback();
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}