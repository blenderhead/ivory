<?php
	
    namespace Ivory\Frontend;

    use Config;
    use Mail;

    use User;

    use Helper;

	class UserResetPass1Processor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $user = User::where('email',$data['email'])->first();

                if(!$user)
                {
                    $this->error = 'User not found';
                    return FALSE;
                }

                $reset_token = Helper::generateToken();

                $user->reset_password_code = $reset_token;
                $user->save();

                $origin = Config::get('manager_emails.customer_service');

                $user_data = array(
                	'from' => $origin,
	                'to' => $data['email'],
	                'name' => $user->first_name,
	                'full_name' => $user->first_name . ' ' . $user->last_name, 
	                'reset_token' => $reset_token
	            );

				$invoice_path = Config::get('path.booking_invoice');

	            Mail::send('emails.auth.reminder', $user_data, function($message) use ($user_data) {
	            	$message->from($user_data['from'], 'Customer Service');
	                $message->to($user_data['to'], $user_data['full_name']);
	                $message->subject('Reset Password Request');
	            });

                return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}