<?php
	
    namespace Ivory\Frontend;

    use Config;
    use Mail;

    use User;

    use Cartalyst\Sentry\Hashing\NativeHasher;

    use Helper;

	class UserResetPass2Processor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $user = User::where('reset_password_code',$data['token'])->first();

                if(!$user)
                {
                    $this->error = 'Token mismatch';
                    return FALSE;
                }

                $user->reset_password_code = null;

                $hasher = new NativeHasher();
                $user->password = $hasher->hash($data['password']);
                $user->save();

                return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}