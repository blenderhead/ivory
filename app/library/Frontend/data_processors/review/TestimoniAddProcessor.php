<?php
	
	namespace Ivory\Frontend;

	use \Ivory\Backend\Models\Testimoni;
	
	class TestimoniAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$testimoni = new Testimoni();
				$testimoni->name = $data['name'];
				$testimoni->email = $data['email'];
				$testimoni->description = strip_tags($data['description']);
				$testimoni->is_publish = 0;
				$testimoni->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}