<?php
	
	namespace Ivory\Frontend;

	use Config;
	use Mail;

	use Booking;
	use Transaction;

	use PDF;

	use Helper;

	class BanquetEnquiryProcessor extends BaseProcessor
	{

		public function process($data)
		{
			try 
			{
				$destination = Config::get('manager_emails.booking_manager');
				$destination_bcc = Config::get('manager_emails.customer_service');

				$user_data = array(
	                'to' => $destination,
	                'to_admin_cc' => $destination_bcc,
	                'title' => $data['title'],
	                'name' => $data['name'],
	                'from' => $data['email'],
	                'phone' => $data['phone'],
	                'room' => $data['room'],
	                'start_date' => $data['start_date'],
	                'end_date' => $data['end_date'],
	                'body' => $data['message']
	            );

	            Mail::send('emails.banquet.enquiry', $user_data, function($message) use ($user_data) {
	            	$message->from($user_data['from'], $user_data['name']);
	                $message->to($user_data['to'], 'Customer Service');
	                $message->subject(ucwords($user_data['room']) . ' Enquiry (' . $user_data['start_date'] . ' to '. $user_data['start_date'] . ')');
	            });

	            Mail::send('emails.banquet.enquiry', $user_data, function($message) use ($user_data) {
	            	$message->from($user_data['from'], $user_data['name']);
	                $message->to($user_data['to_admin_cc'], 'Customer Service');
	                $message->subject(ucwords($user_data['room']) . ' Enquiry (' . $user_data['start_date'] . ' to '. $user_data['start_date'] . ')');
	            });

				return TRUE;
			} 
			catch (\Exception $e) 
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}