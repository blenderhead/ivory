<?php
	
	namespace Ivory\Frontend;

	use Config;
	use Mail;

	use Booking;
	use Transaction;
	use Stock;
	use Room;
	use Hotel;

	use PDF;

	use Helper;

	class PaymentProcessor extends BaseProcessor
	{

		public function process($data)
		{
			try 
			{
				$transaction_id = base64_decode($data['transaction_id']);
				$transaction = Transaction::where('transaction_id',$transaction_id)->first();
				$transaction->status = '1';
				$transaction->confirmed = 1;
				$transaction->save();

				$booking = Booking::find($transaction->booking_id);
				$room = Room::find($booking->room_id);

				Stock::updateStock($booking->checkin_date, $booking->checkout_date,$booking->number_of_rooms);

				//$destination = Config::get('manager_emails.booking_manager');
				//$destination_bcc = Config::get('manager_emails.customer_service');

				$hotel = Hotel::all();

				$destination = $hotel->get(0)->email;
				$destination_bcc = $hotel->get(0)->manager_email;

				$user_data = array(
	                'to_admin' => $destination,
	                'to_admin_cc' => $destination_bcc,
	                'to_user' => $booking->email,
	                'title' => $booking->title,
	                'room' => $room,
	                'booking' => $booking,
	                'transaction' => $transaction,
	                'name' => $booking->first_name . ' ' . $booking->last_name,
	                'payment_method' => 'Credit Card',
	                'status' => 'Paid'
	            );

				$invoice_path = Config::get('path.booking_invoice');
				$admin_invoice_path = Config::get('path.admin_booking_invoice');

				$user_invoice_title = 'Ivory Booking (Invoice #' . $user_data['transaction']->transaction_id . ')';
				$admin_invoice_title = 'New Booking (Invoice #' . $user_data['transaction']->transaction_id . ')';

				$pdf_filename = $transaction_id;

				PDF::loadView('pdf.invoice', $user_data)
    				->setPaper('a4')
    				->setOrientation('portrait')
    				->save($invoice_path.$pdf_filename);

    			PDF::loadView('pdf.admin_invoice', $user_data)
    				->setPaper('a4')
    				->setOrientation('portrait')
    				->save($admin_invoice_path.$pdf_filename);

	            Mail::send('emails.booking.admin_notification', $user_data, function($message) use ($user_data, $admin_invoice_path, $admin_invoice_title, $pdf_filename) {
	                $message->to($user_data['to_admin'], 'Ivory Booking Admin');
	                $message->subject($admin_invoice_title);
	                //$message->attach($admin_invoice_path.$pdf_filename);
	            });

	            Mail::send('emails.booking.admin_notification', $user_data, function($message) use ($user_data, $admin_invoice_path, $admin_invoice_title, $pdf_filename) {
	                $message->to($user_data['to_admin_cc'], 'Ivory Booking Admin');
	                $message->subject($admin_invoice_title);
	                //$message->attach($admin_invoice_path.$pdf_filename);
	            });

	            Mail::send('emails.booking.user_notification', $user_data, function($message) use ($user_data, $invoice_path, $user_invoice_title, $pdf_filename) {
	                $message->to($user_data['to_user'], 'Ivory Booking Admin');
	                $message->subject($user_invoice_title);
	                $message->attach($invoice_path.$pdf_filename);
	            });

				$this->output = $transaction->transaction_id;

				return TRUE;

			} 
			catch (\Exception $e) 
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}