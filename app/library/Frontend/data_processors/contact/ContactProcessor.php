<?php
	
	namespace Ivory\Frontend;

	use Config;
	use Mail;

	use Booking;
	use Transaction;

	use PDF;

	use Helper;

	class ContactProcessor extends BaseProcessor
	{

		public function process($data)
		{
			try 
			{
				$destination = Config::get('manager_emails.reservation_service');

				$user_data = array(
	                'to' => $destination,
	                'name' => $data['name'],
	                'from' => $data['email'],
	                'subject' => $data['subject'],
	                'body' => $data['message']
	            );

				$invoice_path = Config::get('path.booking_invoice');

	            Mail::send('emails.contact.contact_us', $user_data, function($message) use ($user_data) {
	            	$message->from($user_data['from'], $user_data['name']);
	                $message->to($user_data['to'], 'Customer Service');
	                $message->subject($user_data['subject']);
	            });

				return TRUE;

			} 
			catch (\Exception $e) 
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}