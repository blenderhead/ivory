<?php
	
	namespace Ivory\Frontend;
	
	use Validator;
	
	abstract class BaseRepository
	{
		protected $data;
		protected $rules;
		protected $errors;
		protected $fields;

		abstract public function getInput();

		abstract public function setValidationData();

		abstract public function setValidationRules();

		public function getFields()
		{
			$this->fields = array();

			foreach($this->data as $key => $value)
			{
				if($key != 'token')
				{
					array_push($this->fields, $key);
				}
			}

			return $this->fields;
		}

		public function validate()
		{
			$this->getInput();
			$this->setValidationData();
			$this->setValidationRules();

			$validation = Validator::make($this->data, $this->rules);

			if($validation->fails())
			{
				$this->errors = $validation->errors();
				return FALSE;
			}

			return TRUE;
		}

		public function getErrors()
		{
			return $this->errors;
		}

		public function getData()
		{
			return $this->data;
		}

	}