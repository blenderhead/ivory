<?php

	class UploadCreator
	{
		public function createDirScturct()
        {
            $image_upload_root = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;

            $default_image_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'defaults' . DIRECTORY_SEPARATOR;

            $user_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR;

            $gallery_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR;

            $gallery_thumbanail_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR;

            $testimoni_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'testimoni' . DIRECTORY_SEPARATOR;

            $room_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'room' . DIRECTORY_SEPARATOR;

            $banquet_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'banquet' . DIRECTORY_SEPARATOR;

            $cafe_promo_upload_path = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'cafe_promo' . DIRECTORY_SEPARATOR;

            $booking_invoice = storage_path() . DIRECTORY_SEPARATOR . 'booking_invoice' . DIRECTORY_SEPARATOR;

            $admin_booking_invoice = storage_path() . DIRECTORY_SEPARATOR . 'booking_invoice' . DIRECTORY_SEPARATOR . 'admin_doc' . DIRECTORY_SEPARATOR;

            if (!File::exists($image_upload_root))
            {
                File::makeDirectory($image_upload_root, $mode = 0777, true, true);
            }

            if (!File::exists($default_image_path))
            {
                File::makeDirectory($default_image_path, $mode = 0777, true, true);
            }

            if (!File::exists($user_upload_path))
            {
                File::makeDirectory($user_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($gallery_upload_path))
            {
                File::makeDirectory($gallery_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($gallery_thumbanail_path))
            {
                File::makeDirectory($gallery_thumbanail_path, $mode = 0777, true, true);
            }

            if (!File::exists($testimoni_upload_path))
            {
                File::makeDirectory($testimoni_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($room_upload_path))
            {
                File::makeDirectory($room_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($banquet_upload_path))
            {
                File::makeDirectory($banquet_upload_path, $mode = 0777, true, true);
            }

            if (!File::exists($cafe_promo_upload_path))
            {
                File::makeDirectory($cafe_promo_upload_path, $mode = 0777, true, true);
            }
            
            if (!File::exists($booking_invoice))
            {
                File::makeDirectory($booking_invoice, $mode = 0777, true, true);
            }

            if (!File::exists($admin_booking_invoice))
            {
                File::makeDirectory($admin_booking_invoice, $mode = 0777, true, true);
            }
        }
	}