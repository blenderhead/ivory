<?php

	class Helper
	{
		public static function processImage($image, $x_coord, $y_coord, $width, $height, $rw, $path)
        {
            if($image) 
            {
                $image_processor = new ImageProcessor($image, $path);
                $image_processor->upload();
                $image_processor->crop($x_coord, $y_coord, $width, $height, $rw);
                $filename = $image_processor->getFilename();           
                return $filename;
            }
            else
            {
                return '';
            }
        }

        public static function updateImage($image, $x_coord, $y_coord, $width, $height, $rw, $path, $old_image)
        {   
            if($image) 
            {
                $destination = $path;

                $image_processor = new ImageProcessor($image, $destination);
                $image_processor->upload();
  
                $image_processor->crop($x_coord, $y_coord, $width, $height, $rw);
                $filename = $image_processor->getFilename();

                File::delete($path . $old_image);

                return $filename;
            }
            else
            {
                return $old_image;
            }
        }

        public static function deleteImage($filename)
        {
            File::delete($filename);
        }

        public static function processRoomImage($image, $x_coord, $y_coord, $width, $height, $rw, $path, $photo_id, $type)
        {
            if($photo_id)
            {
                $old_filename = Helper::getImage($photo_id);

                $filename = Helper::updateImage($image, $x_coord, $y_coord, $width, $height, $rw, $path, $old_filename);
                $photo = Ivory\Backend\Models\Photos::find($photo_id);
                $photo->file = $filename;
                $photo->save();
            }
            else
            {
                $filename = Helper::processImage($image, $x_coord, $y_coord, $width, $height, $rw, $path);

                if($filename)
                {
                    $photo = new Ivory\Backend\Models\Photos();
                    $photo->file = $filename;
                    $photo->destination = $path;
                    $photo->type = $type;
                    $photo->save();
                    $photo_id = $photo->id;
                }
            }

            return $photo_id;
        }

        public static function getImage($id)
        {
            $filename = null;

            $image = Ivory\Backend\Models\Photos::find($id);

            if($image)
            {
                $filename = $image->file;
            }

            return $filename;
        }

        public static function getImageTitle($id)
        {
            $title = null;

            $image = Ivory\Backend\Models\Photos::find($id);

            if($image)
            {
                $title = $image->title;
            }

            return $title;
        }

        public static function getImageDescription($id)
        {
            $description = null;

            $image = Ivory\Backend\Models\Photos::find($id);

            if($image)
            {
                $description = $image->description;
            }

            return $description;
        }

        public static function displayRestrictionType($res_id)
        {
            switch($res_id)
            {
                case '0':
                    return 'No Departure';
                    break;

                case '1':
                    return 'No Arrival';
                    break;
            }
        }

        public static function moneyFormat($amount, $symbol = 'IDR')
        {
            return $symbol . ' ' . number_format($amount,0,',',',');
        }

        public static function formatPaymentMethod($payment_method)
        {
            switch($payment_method)
            {
                case 'transfer':
                    return 'Bank Transfer';
                    break;

                case 'credit_card_veritrans':
                    return 'Credit Card';
                    break;
            }
        }

        public static function formatPaymentStatus($status)
        {
            switch($status)
            {
                case '1':
                    return 'Paid';
                    break;

                case '2':
                    return 'Pending';
                    break;

                case '4':
                    return 'Canceled';
                    break;
            }
        }

        public static function generatePassword($length)
        {
            $passwd = str_random($length);
            return $passwd;
        }

        public static function generateCode()
        {
            $code = mt_rand(10000000,99999999);

            $check = User::where('member_code',$code)->get();

            while(!$check->isEmpty())
            {
                $code = mt_rand(10000000,99999999);
            }

            return $code;
        }

        public static function generateToken($length = 10)
        {
            return hash('sha256', Str::random($length),false);
        }

        public static function getYoutubeId($url)
        {
            $id = '';

            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) 
            {
                $id = $match[1];
            }

            return $id;
        }
	}