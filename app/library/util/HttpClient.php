<?php
	
	class HttpClient
	{
		protected $http_client;

		public function __construct()
		{
			$this->http_client = new \GuzzleHttp\Client();
		}

		public function get($url, $query)
		{
			$response = $this->http_client->get($url, $query);

			return $response;
		}

		public function post($url, $data)
		{
			$response = $this->http_client->post($url, $data);
			return $response;
		}

		public function delete($url, $data)
		{
			
		}

		public function head($url)
		{
			
		}

		public function options($url)
		{
			
		}

		public function patch($data)
		{

		}

		public function put($url, $data)
		{

		}

		public function getResponse($output = 'object')
		{
			$response = $this->response;

			if($output == 'json')
			{
				return json_decode((string) $response->getBody());
			}

			return $response->getBody();
		}
	}