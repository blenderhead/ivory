<?php
	
	class ImageProcessor
	{
		private $file_object;

		private $destinantion;

		private $filename;

		private $upload_status;

		private $file_size;

		public function __construct($file_object, $destination)
		{
			$this->file_object = $file_object;
			$this->destination = $destination;
		}

		public function upload()
		{
			$this->filename = md5(time() . $this->file_object->getClientOriginalName()) . '.' . $this->file_object->getClientOriginalExtension();
			if($this->file_object->move($this->destination, $this->filename)) return true;

			return false;
		}

		public function setFilename($filename)
		{	
			$this->filename = $filename;
		}
		
		public function getFilename()
		{
			return $this->filename;
		}

		public function getFilePath()
		{
			return $this->destination . $this->filename;
		}

		public function crop($x, $y, $w, $h, $rw)
		{
			$img_width = Image::make($this->getFilePath())->width();
			//var_dump($img_size);
            $multiply = $img_width / $rw;
            $crop_x = (int) ($multiply * $x);
            $crop_y = (int) ($multiply * $y);
            $crop_w = (int) ($multiply * $w);
            $crop_h = (int) ($multiply * $h);

			$img = Image::make($this->destination . $this->filename)->crop($crop_w, $crop_h, $crop_x, $crop_y)->save();

			if(!$img)
			{
				return FALSE;
			}
			
			return TRUE;
		}

		public function getFileSize()
		{
			$this->file_size = Image::make($this->getFilePath())->filesize();
			return $this->file_size;
		}

		public function getFileType()
		{
			$this->file_type = Image::make($this->getFilePath())->mime();
			return $this->file_type;
		}

		public function makeThumbnail($path)
		{
			Image::make($this->getFilePath())->fit(80,60)->save($path . $this->filename);
		}

		public function getFileUrl($path, $size = 'normal')
		{
			$base_url = URL::to('/') . $path;

			if($size == 'thumbnail')
			{
				return $base_url . 'thumbnails' . '/' . $this->filename;
			}

			return $base_url . $this->filename;
		}
	}