<?php
	
	namespace Ivory\Backend;
	
	class BaseProcessor
	{
		protected $error;

		protected $output;

		public function getError()
        {
        	return $this->error;
        }

        public function getoutput()
        {
        	return $this->output;
        }
	}