<?php
	
	namespace Ivory\Backend;

	use Input;

	class HotelEditRepository extends BaseRepository
	{
		private $hotel_id;
		private $name;
		private $description;
		private $star;
		private $address;
		private $type;
		private $phone;
		private $fax;
		private $email;
		private $manager_email;
		private $number_of_room;
		private $number_of_floor;
		private $facilities;
		private $services;
		private $checkin_time;
		private $checkout_time;
		private $tax;
		private $service;
		private $member_discount;
		
		public function getInput()
		{
			$this->hotel_id = Input::get('hotel_id');
			$this->name = Input::get('name');
			$this->description = Input::get('description');
			$this->star = Input::get('star');
			$this->address = Input::get('address');
			$this->type = Input::get('type');
			$this->phone = Input::get('phone');
			$this->fax = Input::get('fax');
			$this->email = Input::get('email');
			$this->manager_email = Input::get('manager_email');
			$this->number_of_room = Input::get('number_of_room');
			$this->number_of_floor = Input::get('number_of_floor');
			$this->facilities = Input::get('facilities');
			$this->services = Input::get('services');
			$this->checkin_time = Input::get('checkin_time');
			$this->checkout_time = Input::get('checkout_time');
			$this->tax = Input::get('tax');
			$this->service = Input::get('service');
			$this->member_discount = Input::get('member_discount');
		}

		public function setValidationData()
		{
			$this->data = array(
				'hotel_id' => $this->hotel_id,
				'name' => $this->name,
	            'description' => $this->description,
	            'star' => $this->star,
	            'address' => $this->address,
	            'type' => $this->type,
	            'phone' => $this->phone,
	            'fax' => $this->fax,
	            'email' => $this->email,
	            'manager_email' => $this->manager_email,
	            'number_of_room' => $this->number_of_room,
	            'number_of_floor' => $this->number_of_floor,
	            'facilities' => $this->facilities,
	            'services' => $this->services,
	            'checkin_time' => $this->checkin_time,
	            'checkout_time' => $this->checkout_time,
	            'tax' => $this->tax,
	            'service' => $this->service,
	            'member_discount' => $this->member_discount
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'name' => 'required',
	            'phone' => 'required|numeric',
	            'fax' => 'numeric',
	            'email' => 'required|email',
	            'manager_email' => 'required|email',
	            'number_of_room' => 'numeric',
	            'number_of_floor' => 'numeric',
	            'tax' => 'numeric',
	            'service' => 'numeric',
	            'member_discount' => 'numeric',
			);
		}
	}