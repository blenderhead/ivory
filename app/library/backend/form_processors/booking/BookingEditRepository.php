<?php
	
	namespace Ivory\Backend;

	use Input;

	class BookingEditRepository extends BaseRepository
	{
		private $token;
		private $_id;
		
		public function getInput()
		{
			$this->token = Input::get('token');
			$this->_id = Input::get('_id');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'token' => $this->token,
	            '_id' => $this->_id,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            
			);
		}
	}