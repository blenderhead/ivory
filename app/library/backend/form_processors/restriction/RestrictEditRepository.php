<?php
	
	namespace Ivory\Backend;

	use Input;

	class RestrictEditRepository extends BaseRepository
	{
		private $res_id;
		private $start_date;
		private $end_date;
		private $type;
		private $description;
		
		public function getInput()
		{
			$this->res_id = Input::get('res_id');
			$this->start_date = Input::get('start_date');
			$this->end_date = Input::get('end_date');
			$this->type = Input::get('type');
			$this->description = Input::get('description');
		}

		public function setValidationData()
		{
			$this->data = array(
				'res_id' => $this->res_id,
	            'start_date' => $this->start_date,
	            'end_date' => $this->end_date,
	            'type' => $this->type,
	            'description' => $this->description
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'res_id' => 'required',
	            'start_date' => 'required|date_format:"Y-m-d"|date_less_than:end_date',
	            'end_date' => 'required|date_format:"Y-m-d"|date_greater_than:start_date',
			);
		}
	}