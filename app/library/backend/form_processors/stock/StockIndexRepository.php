<?php
	
	namespace Ivory\Backend;

	use Input;
	use Validator;

	class StockIndexRepository extends BaseRepository
	{
		protected $room_id;
		protected $mass_stock;
		protected $mass_price;
		protected $mass_stopsell;
		protected $mass_stoppromo;
		protected $stocks = [];

		public function getInput()
		{
			$this->room_id = Input::get('room_id');
			$this->mass_stock = Input::get('mass_stock');
			$this->mass_price = Input::get('mass_price');
			$this->mass_stopsell = Input::get('mass_stopsell');
			$this->mass_stoppromo = Input::get('mass_stoppromo');
			$this->stocks = Input::get('rooms');
		}

		public function setValidationData()
		{			
			$this->data = array(
				'room_id' => $this->room_id,
				'mass_stock' => $this->mass_stock,
				'mass_price' => $this->mass_price,
				'mass_stopsell' => $this->mass_stopsell,
				'mass_stoppromo' => $this->mass_stoppromo,
				'stocks' => $this->stocks,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'room_id' => 'required|numeric',
				'mass_stock' => 'numeric',
				'mass_price' => 'numeric',
				'mass_stopsell' => 'boolean',
				'mass_stoppromo' => 'boolean',
				'stocks' => 'array|stocks_room',
			);
		}
	}