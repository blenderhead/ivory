<?php
	
	namespace Ivory\Backend;

	use Input;
	use Validator;

	class StockSearchRepository extends BaseRepository
	{
		protected $room_id;
		protected $start_date;
		protected $end_date;

		/**
		 * This override method parent to costom validation 
		 * @return boolean [description]
		 */
		public function validate()
		{	

			if (!$this->isActionSearch()) {
				return TRUE; 
			}

			$this->getInput();
			$this->setValidationData();
			$this->setValidationRules();

			$validation = Validator::make($this->data, $this->rules);

			if($validation->fails())
			{
				$this->errors = $validation->errors();
				return FALSE;
			}

			return TRUE;
		}		

		public function getInput()
		{
			$this->room_id = Input::get('room_id');
			$this->start_date = Input::get('start_date');
			$this->end_date = Input::get('end_date');
		}

		public function setValidationData()
		{
			$this->data = array(
				'room_id' => $this->room_id,
				'start_date' => $this->start_date,
				'end_date' => $this->end_date,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'room_id' => 'required|numeric',
				'start_date' => 'required|date',
				'end_date' => 'required|date',
			);
		}

		public function isActionSearch()
		{
			if (Input::get('search') == 'submit') {
				return TRUE;
			}
			return FALSE;			
		}
	}