<?php
	
	namespace Ivory\Backend;

	use Input;

	class BanquetAddRepository extends BaseRepository
	{
		private $room_name;
		private $room_description;
		private $room_size;
		private $room_setup;
		private $gallery_id;

		/*
		private $room_image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_w;
		private $img_h;
		private $img_rw;
		*/
		
		public function getInput()
		{
			$this->room_name = Input::get('room_name');
			$this->room_description = Input::get('room_description');
			$this->room_size = Input::get('room_size');
			$this->room_setup = Input::get('room_setup');
			$this->room_image = Input::file('room_image');
			$this->gallery_id = Input::get('gallery_id');

			/*
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
            */
		}

		public function setValidationData()
		{
			$this->data = array(
	            'room_name' => $this->room_name,
	            'room_description' => $this->room_description,
	            'room_size' => $this->room_size,
	            'room_setup' => $this->room_setup,
	            'room_image' => $this->room_image,
	            'gallery_id' => $this->gallery_id,

	            /*
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
	            */
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'room_name' => 'required|unique:banquets,name',
				'room_description' => 'required',
				'room_size' => 'required',
				'room_setup' => 'required',
				//'room_image' => 'required|image'
			);
		}
	}