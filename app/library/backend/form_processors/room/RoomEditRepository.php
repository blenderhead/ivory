<?php
	
	namespace Ivory\Backend;

	use Input;

	class RoomEditRepository extends BaseRepository
	{
		private $id;
		private $room_name;
		private $room_description;
		private $room_size;
		private $number_of_beds;
		private $publish_price;
		private $member_price;
		private $adult_price;
		private $child_price;
		private $adult_breakfast_price;
		private $child_breakfast_price;
		private $include_breakfast;
		private $extra_bed_price;
		private $extra_bed_number;
		private $extra_breakfast;
		private $facilities = [];
		private $capacities = [];	

		/*
		private $room_image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_w;
		private $img_h;
		private $img_rw;	
		*/
		
		private $gallery_id;

		public function getInput()
		{
			$this->id = Input::get('id');
			$this->room_name = Input::get('room_name');
			$this->room_description = Input::get('room_description');
			$this->room_size = Input::get('room_size');
			$this->number_of_beds = Input::get('number_of_beds');
			$this->publish_price = Input::get('publish_price');
			$this->member_price = Input::get('member_price');
			$this->adult_price = Input::get('adult_price');
			$this->child_price = Input::get('child_price');
			$this->adult_breakfast_price = Input::get('adult_breakfast_price');
			$this->child_breakfast_price = Input::get('child_breakfast_price');
			$this->include_breakfast = Input::get('include_breakfast');
			$this->number_person_breakfast_included = Input::get('person_breakfast');
			$this->extra_bed_price = Input::get('extra_bed_price');
			$this->extra_bed_number = Input::get('extra_bed_number');
			$this->extra_breakfast = Input::get('extra_breakfast');
			$this->facilities = Input::get('facilities');
			$this->capacities = Input::get('capacities');			
			$this->gallery_id = Input::get('gallery_id');

			/*
			$this->room_image = Input::file('room_image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
            */
		}

		public function setValidationData()
		{
			$this->data = array(
				'id' => $this->id,
	            'room_name' => $this->room_name,
	            'room_description' => $this->room_description,
	            'room_size' => $this->room_size,
	            'number_of_beds' => $this->number_of_beds,
	            'publish_price' => $this->publish_price,
	            'member_price' => $this->member_price,
	            'adult_price' => $this->adult_price,
	            'child_price' => $this->child_price,
	            'adult_breakfast_price' => $this->adult_breakfast_price,
	            'child_breakfast_price' => $this->child_breakfast_price,
	            'number_person_breakfast_included' => $this->number_person_breakfast_included,
	            'include_breakfast' => $this->include_breakfast,
	            'extra_bed_price' => $this->extra_bed_price,
	            'extra_bed_number' => $this->extra_bed_number,
	            'extra_breakfast' => $this->extra_breakfast,
	            'facilities' => $this->facilities,
	            'capacities' => $this->capacities,	      
	            'gallery_id' => $this->gallery_id
	            /*
	            'room_image' => $this->room_image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,      
	            */
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required|numeric',
				'room_name' => 'required|unique:rooms,name,' . $this->id,
				'room_description' => 'required|min:100',
				'room_size' => 'required',
				'number_of_beds' => 'required|numeric|min:1',
				'publish_price' => 'required|numeric|min:4',
				'member_price' => 'required|numeric|min:4',
				'adult_price' => 'required|numeric|min:4',
				'child_price' => 'required|numeric|min:4',
				'adult_breakfast_price' => 'required|numeric|min:3',
				'child_breakfast_price' => 'required|numeric|min:3',
				'include_breakfast' => 'required|numeric',
				'gallery_id' => 'required'
				//'room_image' => 'image',
				//'extra_bed_price' => 'required|numeric|min:3',
				//'extra_bed_number' => 'required|numeric|max:2',
				//'extra_breakfast' => 'required|numeric',
			);
		}
	}