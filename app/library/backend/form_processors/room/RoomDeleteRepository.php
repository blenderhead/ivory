<?php
	
	namespace Ivory\Backend;

	use Input;

	class RoomDeleteRepository extends BaseRepository
	{
		private $room_id;

		public function getInput()
		{
			$this->room_id = Input::get('id');
		}

		public function setValidationData()
		{
			$this->data = array(
				'room_id' => $this->room_id,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'room_id' => 'required',
			);
		}
	}