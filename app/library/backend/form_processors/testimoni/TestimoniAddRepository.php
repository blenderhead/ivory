<?php
	
	namespace Ivory\Backend;

	use Input;

	class TestimoniAddRepository extends BaseRepository
	{
		private $name;
		private $link;
		private $description;
		private $is_publish;
		private $assign_to;

		public function getInput()
		{
			$this->name = Input::get('name');
			$this->link = Input::get('link');
			$this->description = Input::get('description');
			$this->is_publish = Input::get('is_publish');
			$this->assign_to = Input::get('assign_to');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'name' => $this->name,
	            'link' => $this->link,
	            'description' => $this->description,
	            'is_publish' => $this->is_publish,
	            'assign_to' => $this->assign_to,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'name' => 'required',
	            'link' => 'required',
	            'description' => 'required|max:140'
	            //'is_publish' => 'required',
			);
		}
	}