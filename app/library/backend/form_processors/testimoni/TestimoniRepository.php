<?php

namespace Ivory\Backend;

abstract class TestimoniRepository extends BaseRepository {
    protected $token;
    protected $_id;
    protected $name;
    protected $job_position;
    protected $description;
    protected $company;
    protected $is_publish;
    protected $created_by;
    protected $updated_by;
    protected $created_at;
    protected $updated_at;

    public function getInput(){}
    public function setValidationData(){}
    public function setValidationRules(){}
}