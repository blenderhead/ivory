<?php
	
	namespace Ivory\Backend;

	use Input;

	class PolicyEditRepository extends BaseRepository
	{
		private $id;
		private $room_ids = [];
		private $start_date;
		private $end_date;
		private $level_1_policy;
		private $level_2_policy = NULL;
		private $level_3_policy = NULL;
		
		public function getInput()
		{
			$this->id = Input::get('id');
			$this->room_ids = Input::get('room_ids');
			$this->start_date = Input::get('start_date');
			$this->end_date = Input::get('end_date');
			$this->level_1_policy = Input::get('level_1_policy');
			$this->level_2_policy = (Input::get('level_2_policy') == "") ? NULL : Input::get('level_2_policy') ;
			$this->level_3_policy = (Input::get('level_3_policy') == "") ? NULL : Input::get('level_3_policy') ;
		}

		public function setValidationData()
		{
			$this->data = array(
				'id' => $this->id,
				'room_ids' => $this->room_ids,
				'start_date' => $this->start_date,
				'end_date' => $this->end_date,
				'level_1_policy' => $this->level_1_policy,
				'level_2_policy' => $this->level_2_policy,
				'level_3_policy' => $this->level_3_policy,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required|numeric',
				'room_ids' => 'required|array',
				//'start_date' => 'required|date',
				//'end_date' => 'required|date|after:start_date',
				'level_1_policy' => 'required|numeric',
				'level_2_policy' => 'numeric',
				'level_3_policy' => 'numeric',		            
			);
		}
	}