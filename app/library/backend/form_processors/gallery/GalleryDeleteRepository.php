<?php
    
    namespace Ivory\Backend;

    use Input;

    class GalleryDeleteRepository extends BaseRepository
    {
        private $id;
        
        public function getInput()
        {
            $this->id = Input::get('id');
        }

        public function setValidationData()
        {
            $this->data = array(
                'id' => $this->id,
            );
        }

        public function setValidationRules()
        {
            $this->rules = array(
                'id' => 'required'
            );
        }
    }