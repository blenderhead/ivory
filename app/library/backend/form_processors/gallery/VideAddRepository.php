<?php
	
	namespace Ivory\Backend;

	use Input;

	class VideAddRepository extends BaseRepository
	{
		private $title;
		private $description;
		private $url;
		private $is_publish;
		
		public function getInput()
		{
			$this->title = Input::get('title');
			$this->description = Input::get('description');
			$this->is_publish = Input::get('publish');
			$this->url = Input::get('url');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'title' => $this->title,
	            'description' => $this->description,
	            'publish' => $this->is_publish,
	            'url' => $this->url
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
		        'title' => 'required',
		        'url' => 'required',
			);
		}
	}