<?php
	
	namespace Ivory\Backend;

	use Input;

	class GalleryUpMetaRepository extends BaseRepository
	{
		private $photo_id;
		private $photo_title;
		private $photo_description;

		public function getInput()
		{
			$this->photo_id = Input::get('photo_id');
			$this->photo_title = Input::get('photo_title');
			$this->photo_description = Input::get('photo_description');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'photo_id' => $this->photo_id,
	            'photo_title' => $this->photo_title,
	            'photo_description' => $this->photo_description,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array();
		}
	}