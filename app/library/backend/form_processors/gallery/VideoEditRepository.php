<?php
	
	namespace Ivory\Backend;

	use Input;

	class VideoEditRepository extends BaseRepository
	{
		private $id;
		private $title;
		private $description;
		private $url;
		private $is_publish;
		
		public function getInput()
		{
			$this->id = Input::get('id');
			$this->title = Input::get('title');
			$this->description = Input::get('description');
			$this->is_publish = Input::get('publish');
			$this->url = Input::get('url');
		}

		public function setValidationData()
		{
			$this->data = array(
				'id' => $this->id,
	            'title' => $this->title,
	            'description' => $this->description,
	            'publish' => $this->is_publish,
	            'url' => $this->url
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required',
		        'title' => 'required',
		        'url' => 'required',
			);
		}
	}