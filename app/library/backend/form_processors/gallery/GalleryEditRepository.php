<?php
	
	namespace Ivory\Backend;

	use Input;

	class GalleryEditRepository extends BaseRepository
	{
		private $id;
		private $name;
		private $description;
		private $is_publish;
		private $gallery_category_id;

		public function getInput()
		{
			$this->id = Input::get('id');
			$this->name = Input::get('name');
			$this->description = Input::get('description');
			$this->is_publish = Input::get('publish');
			$this->gallery_category_id = Input::get('category');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'id' => $this->id,
	            'name' => $this->name,
	            'description' => $this->description,
	            'publish' => $this->is_publish,
	            'category' => $this->gallery_category_id
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required',
		        'name' => 'required|min:3',
		        'category' => 'required',
		        'publish' => 'required',
			);
		}
	}