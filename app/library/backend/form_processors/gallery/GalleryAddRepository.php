<?php
	
	namespace Ivory\Backend;

	use Input;

	class GalleryAddRepository extends BaseRepository
	{
		private $name;
		private $description;
		private $is_publish;
		private $gallery_category_id;

		public function getInput()
		{
			$this->name = Input::get('name');
			$this->description = Input::get('description');
			$this->is_publish = Input::get('publish');
			$this->gallery_category_id = Input::get('category');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'name' => $this->name,
	            'description' => $this->description,
	            'publish' => $this->is_publish,
	            'category' => $this->gallery_category_id
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
		        'name' => 'required|min:3',
		        'category' => 'required',
		        'publish' => 'required',
			);
		}
	}