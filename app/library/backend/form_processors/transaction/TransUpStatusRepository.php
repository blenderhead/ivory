<?php
	
	namespace Ivory\Backend;

	use Input;

	class TransUpStatusRepository extends BaseRepository
	{
		private $id;
		private $status;
		
		public function getInput()
		{
			$this->id = Input::get('id');
			$this->status = Input::get('status');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'id' => $this->id,
	            'status' => $this->status,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required',
				'status' => 'required',
			);
		}
	}