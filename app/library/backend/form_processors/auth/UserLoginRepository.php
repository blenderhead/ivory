<?php
	
	namespace Ivory\Backend;

	use Input;

	class UserLoginRepository extends BaseRepository
	{
		private $email;
		private $password;
		
		public function getInput()
		{
			$this->email = Input::get('email');
			$this->password = Input::get('password');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'email' => $this->email,
	            'password' => $this->password,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'email' => 'required|email',
	            'password' => 'required'
			);
		}
	}