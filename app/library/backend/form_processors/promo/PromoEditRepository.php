<?php
	
	namespace Ivory\Backend;

	use Input;

	class PromoEditRepository extends BaseRepository
	{
		private $id;
		private $room_ids = NULL;
		private $promo_type;
		private $early_day;
		private $whitin_day;
		private $booking_start_date;
		private $booking_end_date;
		private $stay_start_date;
		private $stay_end_date;
		private $is_sunday;
		private $is_monday;
		private $is_tuesday;
		private $is_wednesday;
		private $is_thursday;
		private $is_friday;
		private $is_saturday;
		private $min_stay_duration;
		private $min_book_room;
		private $value_discount;
		private $benifit_value;
		private $costum_policy;
		private $level_1_policy;
		private $level_2_policy;
		private $level_3_policy;
		private $promotion_name;

		
		public function getInput()
		{
			$this->id = Input::get('id');
			$this->room_ids = Input::get('room_ids');
			$this->promo_type = Input::get('promo_type');
			$this->early_day = Input::get('early_day');
			$this->whitin_day = Input::get('whitin_day');
			$this->booking_start_date = Input::get('booking_start_date');
			$this->booking_end_date = Input::get('booking_end_date');
			$this->stay_start_date = Input::get('stay_start_date');
			$this->stay_end_date = Input::get('stay_end_date');
			$this->is_sunday = Input::get('is_sunday');
			$this->is_monday = Input::get('is_monday');
			$this->is_tuesday = Input::get('is_tuesday');
			$this->is_wednesday = Input::get('is_wednesday');
			$this->is_thursday = Input::get('is_thursday');
			$this->is_friday = Input::get('is_friday');
			$this->is_saturday = Input::get('is_saturday');
			$this->min_stay_duration = Input::get('min_stay_duration');
			$this->min_book_room = Input::get('min_book_room');
			$this->value_discount = Input::get('value_discount');
			$this->benifit_value = Input::get('benifit_value');
			$this->costum_policy = Input::get('costum_policy');
			$this->level_1_policy = Input::get('level_1_policy');
			$this->level_2_policy = $this->emptyStringToNull(Input::get('level_2_policy'));
			$this->level_3_policy = $this->emptyStringToNull(Input::get('level_3_policy'));
			$this->promotion_name = Input::get('promotion_name');
		}

		public function setValidationData()
		{
			$this->data = array(
				'id' => $this->id,
				'room_ids' => $this->room_ids,
				'promo_type' => $this->promo_type,
				'early_day' => $this->early_day,
				'whitin_day' => $this->whitin_day,
				'booking_start_date' => $this->booking_start_date,
				'booking_end_date' => $this->booking_end_date,
				'stay_start_date' => $this->stay_start_date,
				'stay_end_date' => $this->stay_end_date,
				'is_sunday' => $this->is_sunday,
				'is_monday' => $this->is_monday,
				'is_tuesday' => $this->is_tuesday,
				'is_wednesday' => $this->is_wednesday,
				'is_thursday' => $this->is_thursday,
				'is_friday' => $this->is_friday,
				'is_saturday' => $this->is_saturday,
				'min_stay_duration' => $this->min_stay_duration,
				'min_book_room' => $this->min_book_room,
				'value_discount' => $this->value_discount,
				'benifit_value' => $this->benifit_value,
				'costum_policy' => $this->costum_policy,
				'level_1_policy' => $this->level_1_policy,
				'level_2_policy' => $this->level_2_policy,
				'level_3_policy' => $this->level_3_policy,
				'promotion_name' => $this->promotion_name,				
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'numeric|required',
				'room_ids' => 'array',
				'promo_type' => 'numeric',
				'early_day' => 'min:1|numeric',
				'whitin_day' => 'min:1|numeric',
				'booking_start_date' => 'date|required',
				'booking_end_date' => 'date|required|after:booking_start_date',
				'stay_start_date' => 'date|required',
				'stay_end_date' => 'date|required|after:stay_start_date',
				'is_sunday' => 'numeric',
				'is_monday' => 'numeric',
				'is_tuesday' => 'numeric',
				'is_wednesday' => 'numeric',
				'is_thursday' => 'numeric',
				'is_friday' => 'numeric',
				'is_saturday' => 'numeric',
				'min_stay_duration' => 'min:1|required|numeric',
				'min_book_room' => 'min:1|required|numeric',
				'value_discount' => 'min:1|required|numeric',
				'benifit_value' => 'numeric',
				'level_1_policy' => 'required|numeric',
				'level_2_policy' => 'numeric',
				'level_3_policy' => 'numeric',
				'promotion_name' => 'required',	            
			);
		}
	}