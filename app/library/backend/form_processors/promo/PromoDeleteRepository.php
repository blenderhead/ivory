<?php
	
	namespace Ivory\Backend;

	use Input;

	class PromoDeleteRepository extends BaseRepository 
	{
		private $promotion_id;

		public function getInput()
		{
			$this->promotion_id = Input::get('id');
		}

		public function setValidationData()
		{
			$this->data = array(
				'promotion_id' => $this->promotion_id,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'promotion_id' => 'required',
			);
		}
	}
	