<?php
	
	namespace Ivory\Backend;

	use Input;

	class CafePromoAddRepository extends BaseRepository
	{
		private $promo_name;
		private $promo_description;
		private $promo_price;
		private $promo_status;

		private $promo_image;
		private $img_coord_x;
		private $img_coord_y;
		private $img_w;
		private $img_h;
		private $img_rw;
		
		public function getInput()
		{
			$this->promo_name = Input::get('promo_name');
			$this->promo_description = Input::get('promo_description');
			$this->promo_price = Input::get('promo_price');
			$this->promo_status = Input::get('promo_status');
			$this->promo_image = Input::file('promo_image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'promo_name' => $this->promo_name,
	            'promo_description' => $this->promo_description,
	            'promo_price' => $this->promo_price,
	            'promo_status' => $this->promo_status,
	            'promo_image' => $this->promo_image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'promo_name' => 'required|min:8|unique:cafe_promos,name',
				'promo_description' => 'required',
				'promo_price' => 'required|numeric|min:2',
				'promo_image' => 'required|image',
			);
		}
	}