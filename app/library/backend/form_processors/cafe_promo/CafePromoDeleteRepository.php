<?php
	
	namespace Ivory\Backend;

	use Input;

	class CafePromoDeleteRepository extends BaseRepository
	{
		private $room_id;

		public function getInput()
		{
			$this->promo_id = Input::get('id');
		}

		public function setValidationData()
		{
			$this->data = array(
				'promo_id' => $this->promo_id,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'promo_id' => 'required',
			);
		}
	}