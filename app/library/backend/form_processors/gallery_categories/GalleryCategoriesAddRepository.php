<?php
	
	namespace Ivory\Backend;

	use Input;

	class GalleryCategoriesAddRepository extends BaseRepository
	{
		private $name;
		private $description;
		
		public function getInput()
		{
			$this->name = Input::get('gallery_name');
			$this->description = Input::get('description');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'gallery_name' => $this->name,
	            'description' => $this->description
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
		        'gallery_name' => 'required|unique:gallery_categories,name',
			);
		}
	}