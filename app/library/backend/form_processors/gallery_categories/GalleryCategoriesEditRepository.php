<?php
	
	namespace Ivory\Backend;

	use Input;

	class GalleryCategoriesEditRepository extends BaseRepository
	{
		private $id;
		private $name;
		private $description;

		public function getInput()
		{
			$this->id = Input::get('id');
			$this->name = Input::get('gallery_name');
			$this->description = Input::get('description');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'id' => $this->id,
	            'gallery_name' => $this->name,
	            'description' => $this->description
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'id' => 'required',
		        'gallery_name' => 'required|unique:gallery_categories,name,' . $this->id,
			);
		}
	}