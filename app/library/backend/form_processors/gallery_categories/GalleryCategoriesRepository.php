<?php

namespace Ivory\Backend;

abstract class GalleryCategoriesRepository extends BaseRepository {
    protected $token;
    protected $_id;
    protected $name;
    protected $description;

    public function getInput(){}
    public function setValidationData(){}
    public function setValidationRules(){}
}