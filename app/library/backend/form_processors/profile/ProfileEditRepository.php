<?php
	
	namespace Ivory\Backend;

	use Input;

	class ProfileEditRepository extends BaseRepository
	{
		private $user_id;
		private $first_name;
		private $last_name;
		private $email;
		private $password;
		private $password_confirm;
		private $address;
		private $phone;
		private $image;

		private $img_coord_x;
		private $img_coord_y;
		private $img_w;
		private $img_h;
		private $img_rw;
		
		public function getInput()
		{
			$this->user_id = Input::get('user_id');
			$this->first_name = Input::get('first_name');
			$this->last_name = Input::get('last_name');
			$this->email = Input::get('email');
			$this->password = Input::get('password');
			$this->password_confirm = Input::get('password_confirm');
			$this->address = Input::get('address');
			$this->phone = Input::get('phone');
			$this->image = Input::file('image');
			$this->img_coord_x = Input::get('x');
            $this->img_coord_y = Input::get('y');
            $this->img_w = Input::get('w');
            $this->img_h = Input::get('h');
            $this->img_rw = Input::get('rw');
		}

		public function setValidationData()
		{
			$this->data = array(
				'user_id' => $this->user_id,
	            'first_name' => $this->first_name,
	            'last_name' => $this->last_name,
	            'email' => $this->email,
	            'password' => $this->password,
	            'password_confirm' => $this->password_confirm,
	            'address' => $this->address,
	            'phone' => $this->phone,
	            'image' => $this->image,
	            'img_coord_x' => $this->img_coord_x,
	            'img_coord_y' => $this->img_coord_y,
	            'img_w' => $this->img_w,
	            'img_h' => $this->img_h,
	            'img_rw' => $this->img_rw,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'user_id' => 'required',
	            'first_name' => 'required',
	            'email' => 'required|email|unique:users,email,' . $this->user_id,
	            'password' => 'same:password_confirm',
	            'password_confirm' => 'same:password',
	            //'member_code' => 'required|unique:users,member_code,' . $this->user_id
			);
		}
	}