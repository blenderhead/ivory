<?php
	
	namespace Ivory\Backend;

	use Promo;
	use Cancellation;
	use DB;

	class PromoEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			DB::beginTransaction();
			try
			{
				$promo = Promo::find($data['id']);
				$promo = $promo->updatePromotion($data);

				/* if user choose costum cancellation, create cancellation costum */
				if ($data['costum_policy'] == 1) {
					/* if promotion has cancellation */
					if (count($promo->cancellation)) {

						$cancellation = $promo->cancellation;
						$cancellation = $cancellation->updateCancellationPromotion(
							$promo->id,
							$data['level_1_policy'],
							$data['level_2_policy'],
							$data['level_3_policy']
						);
					/* if promotion not have cancellation */
					} else {

						$cancellation = new Cancellation();
						$cancellation = $cancellation->createCancellationPromotion(
							$promo->id,
							$data['level_1_policy'],
							$data['level_2_policy'],
							$data['level_3_policy']
						);						
					}

				} else {
					/* if promotion has cancellation */
					if (count($promo->cancellation)) {
						$promo->cancellation->delete();
					}
				}

				/* Sync data room id and promotion */
				$promo->rooms()->sync($data['room_ids']);

				DB::commit();
				return TRUE;
			}
			catch(\Exception $e)
			{
				DB::rollback();
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}