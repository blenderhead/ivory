<?php
	
	namespace Ivory\Backend;

	use Promo;

	class PromoDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['promotion_id']);$i++)
				{
					$room = Promo::find($data['promotion_id'][$i]);
					$room->deletePromotion($data['promotion_id'][$i]);
				}
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}