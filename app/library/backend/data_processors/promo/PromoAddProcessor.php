<?php
	
	namespace Ivory\Backend;

	use Promo;
	use Cancellation;
	use DB;

	class PromoAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			DB::beginTransaction();
			try
			{
				$promo = new Promo();
				$promo = $promo->createPromotion($data);

				/* if user choose costum cancellation, create cancellation costum */
				if ($data['costum_policy'] == 1) {
					
					$cancellation = new Cancellation();
					$cancellation = $cancellation->createCancellationPromotion(
						$promo->id,
						$data['level_1_policy'],
						$data['level_2_policy'],
						$data['level_3_policy']
					);
				}

				/* Sync data room id and promotion */
				$promo->rooms()->sync($data['room_ids']);

				DB::commit();
				return TRUE;
			}
			catch(\Exception $e)
			{
				DB::rollback();
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}