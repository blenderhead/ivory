<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Promo;

	use Datatable;

	class PromoDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Promo::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="promo" value="' . $model->id . '">';
	                })
	                ->addColumn('promotion', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('room_name', function($model) {
	                	$output = '';

	                    foreach($model->rooms as $index => $room)
	                    {
	                    	$output .= $index+1 . ' ' . $room->name  . '<br>';
	                    }

	                    return $output;
	                })
	                ->addColumn('booking_date', function($model) {
	                    return $model->booking_start_date . ' - ' . $model->booking_end_date;
	                })
	                ->addColumn('stay_date', function($model) {
	                    return $model->stay_start_date . ' - ' . $model->stay_end_date;
	                })
	                ->addColumn('cancellation_policy', function($model) {
	                    if(is_object($model->cancellation)) 
	                    {
	                    	return '<a type="button" data-toggle="tooltip" data-placement="bottom" title="1. ' . $model->cancellation->level1Policy()->name . '|2. ' . is_object($model->cancellation->level2Policy()) ? $model->cancellation->level2Policy()->name : '-' . '| 3. ' . is_object($model->cancellation->level3Policy()) ? $model->cancellation->level3Policy()->name : '-' . '">Costum Cancellations</a>';
						}
						else 
						{
							return 'Defult Cancellation';
						}
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                   
	                    $action .= '<a href="' . URL::to('/') . '/backend/promo/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
					   	$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';

	                    return $action;
	                })
	                ->searchColumns('promotion', 'room_name')
	                ->orderColumns('room_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}