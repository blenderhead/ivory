<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Ivory\Backend\Models\Gallery;

	use Datatable;

	class GalTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Gallery::getAllGalleries();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="gal" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('gallery_name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('is_publish', function($model) {
	                    return $model->is_publish ? 'Yes' : 'No';
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/cms/gallery/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
	                    $action .= '<a href="' . URL::to('/') . '/backend/cms/gallery/manage_photo?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-camera"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('category_name', 'id')
	                ->orderColumns('category_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}