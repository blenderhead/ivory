<?php
    
    namespace Ivory\Backend;

    use Ivory\Backend\Models\Video;
   
    class VideoDeleteProcessor extends BaseProcessor
    {
        public function process($data)
        {
            try
            {
                for($i=0;$i<count($data['id']);$i++)
                {
                    $video = Video::find($data['id'][$i]);
                    $video->delete();
                }
                
                return TRUE;
            }
            catch(\Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
        }
    }