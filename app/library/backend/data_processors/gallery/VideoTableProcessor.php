<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Ivory\Backend\Models\Video;

	use Datatable;

	class VideoTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Video::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="video" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('title', function($model) {
	                    return $model->title;
	                })
	                ->addColumn('is_publish', function($model) {
	                    return $model->is_publish ? 'Yes' : 'No';
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/cms/gallery/video/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('title', 'id')
	                ->orderColumns('id')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}