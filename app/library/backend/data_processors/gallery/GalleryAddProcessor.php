<?php
	
	namespace Ivory\Backend;
	
	use \Ivory\Backend\Models\Gallery;

	class GalleryAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$gallery = new Gallery();
				$gallery->name = $data['name'];
		        $gallery->description = $data['description'];
		        $gallery->gallery_category_id = $data['category'];
		        $gallery->is_publish = $data['publish'] ? 1 : 0;
		        $gallery->save();
		        //$gallery->is_publish = $data['publish'] ? Gallery::setPublished($gallery->id) : 0;
		        //$gallery->is_publish = $data['publish'];

		        $this->output = $gallery->id;

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}