<?php
	
	namespace Ivory\Backend;
	
	use \Ivory\Backend\Models\Gallery;

	class GalleryEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$gallery = Gallery::find($data['id']);
				$gallery->name = $data['name'];
		        $gallery->description = $data['description'];
		        $gallery->gallery_category_id = $data['category'];
		        $gallery->save();
		        $gallery->is_publish = $data['publish'] ? Gallery::setPublished($gallery->id) : 0;

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}