<?php
    
    namespace Ivory\Backend;

    use Config;

    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\Photos;
    
    use File;

    class GalleryDeleteProcessor extends BaseProcessor
    {
        public function process($data)
        {
            try
            {
                for($i=0;$i<count($data['id']);$i++)
                {
                    $gallery = Gallery::find($data['id'][$i]);

                    $photos = Photos::where('object_id','=',$gallery->id)->get();

                    if(!$photos->isEmpty())
                    {
                        foreach($photos as $i=>$row) 
                        {
                            $galleries_path = Config::get('path.gallery_upload_path');
                            $galleries_thumb_path = Config::get('path.gallery_thumbnail_path');

                            File::delete($galleries_path . $row->file);
                            File::delete($galleries_thumb_path . $row->file);

                            Photos::where('id',$row->id)->delete();
                        }
                    }

                    $gallery->delete();
                }
                
                return TRUE;
            }
            catch(\Exception $e)
            {
                $this->error = $e->getMessage();
                return FALSE;
            }
        }
    }