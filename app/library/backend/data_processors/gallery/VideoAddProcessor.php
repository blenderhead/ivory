<?php
	
	namespace Ivory\Backend;
	
	use \Ivory\Backend\Models\Video;

	class VideoAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$gallery = new Video();
				$gallery->title = $data['title'];
		        $gallery->description = $data['description'];
		        $gallery->url = $data['url'];
		        $gallery->is_publish = $data['publish'] ? 1 : 0;
		        $gallery->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}