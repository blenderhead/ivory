<?php
	
	namespace Ivory\Backend;
	
	use \Ivory\Backend\Models\Photos;

	class GalleryUpMetaProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$photo = Photos::find($data['photo_id']);
				$photo->title = $data['photo_title'];
		        $photo->description = $data['photo_description'];
		        $photo->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}