<?php
	
	namespace Ivory\Backend;
	use Cancellation;

	class PolicyEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$cancelltion =  Cancellation::find($data['id']);
				$cancelltion->updateCancellation($data);
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}