<?php
	
	namespace Ivory\Backend;
	use Cancellation;

	class PolicyDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					$cancellation = Cancellation::find($data['id'][$i]);
					$cancellation->delete();
				}
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}