<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Cancellation;
	use Room;

	use Datatable;

	class PolicyDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Cancellation::where('promotion_id', '=', NULL)->get();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="policy" value="' . $model->id . '">';
	                })
	                /*
	                ->addColumn('date', function($model) {
	                    return $model->start_date . '-' . $model->end_date;
	                })
	                */
	                ->addColumn('room_name', function($model) {
	                    if($model->rooms->count() == Room::all()->count())
	                    {
	                    	return 'All Rooms';
	                    }
						else
						{
							foreach($model->rooms as $room)
							{
								return "$room->name <br>";
							}
						}
	                })
	                ->addColumn('cancellation_policy', function($model) {
	                	$output = '';
	                    $output .= 'Level 1 : ' . $model->level1Policy()->name . '<br>';
						$output .= 'Level 2 : ' . (!is_object($model->level2Policy())) ? '-' : $model->level2Policy()->name . '<br>';
						$output .= 'Level 3 : ' . (!is_object($model->level3Policy())) ? '-' : $model->level3Policy()->name . '<br>';
						return $output;
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';

	                    $action .= '<a href="' . URL::to('/') . '/backend/policy/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';

	                    return $action;
	                })
	                ->searchColumns('date', 'room_name')
	                ->orderColumns('date')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}