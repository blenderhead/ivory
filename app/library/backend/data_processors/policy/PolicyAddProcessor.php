<?php
	
	namespace Ivory\Backend;

	use Cancellation;

	class PolicyAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$cancellation = new Cancellation();
				$cancellation->createCancellation($data);
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}