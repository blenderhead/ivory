<?php
	
	namespace Ivory\Backend;

	use Cartalyst\Sentry\Hashing\NativeHasher;

	use User;

	use Config;
	use Helper;

	class ProfileEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
	            $user = User::find($data['user_id']);
				$user->first_name = $data['first_name'];
				$user->last_name = $data['last_name'];
				$user->email = $data['email'];
				//$user->member_code = $data['member_code'];
				$user->address = $data['address'];
				$user->phone = $data['phone'];

				$user->image = Helper::updateImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.user_upload_path'), $user->image);

				if($data['password'])
				{
					$hasher = new NativeHasher();
					$user->password = $hasher->hash($data['password']);
				}

				$user->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}