<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Ivory\Backend\Models\GalleryCategories;

	use Datatable;

	class GalCatTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = GalleryCategories::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="cat" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('category_name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('description', function($model) {
	                    return $model->description;
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/cms/gallery-categories/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id .'"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('category_name', 'id')
	                ->orderColumns('category_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}