<?php
	
	namespace Ivory\Backend;

	use Ivory\Backend\Models\GalleryCategories;
	
	class GalleryCategoriesProcessor extends BaseProcessor
	{
		public function process($action, $data)
		{
			try
			{
				$category = new GalleryCategories();
				
				switch ($action) {
					case 'create':
						$category->createCategories($data);
						break;					
					case 'edit':
						$category->editCategories($data);
						break;
					case 'delete':
						$category->deleteCategories($data);
						break;	
				}

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}