<?php
	
	namespace Ivory\Backend;

	use Config;
	use URL;

	use Transaction;

	use Datatable;

	use Helper;

	class TransactionDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Transaction::orderBy('created_at','DESC')->get();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('transaction_id', function($model) {
	                    return $model->transaction_id;
	                })
	                ->addColumn('payment_type', function($model) {
	                    return Helper::formatPaymentMethod($model->type);
	                })
	                ->addColumn('booking_id', function($model) {
	                    return '<a class="booking_link" href="' . URL::to('/') . '/backend/booking/view?id=' . $model->booking_id . '">' . $model->booking_id . '</a>';
	                })
	                ->addColumn('total', function($model) {
	                    return Helper::moneyFormat($model->total - $model->discount);
	                })
	                ->addColumn('status', function($model) {
	                    return ucwords(Config::get('booking_states.' . $model->status));
	                })
	                ->addColumn('confirmation', function($model) {
	                	if($model->type == 'credit_card_veritrans')
	                	{
	                		$html = '';
	                    	$html .= '<span>' . ucwords(Config::get('confirmation_status.' . $model->confirmed)) . '</span>&nbsp;&nbsp;<a href="' . URL::to('/') . '/backend/transaction/download_invoice?file=' . $model->transaction_id . '" data-id="' . $model->id . '">[Download Invoice]</a>';
	                		return $html;
	                		//return ucwords(Config::get('confirmation_status.' . $model->confirmed));
	                	}
	                    else
	                    {
	                    	if($model->confirmed == 0)
	                    	{
	                    		return ucwords(Config::get('confirmation_status.' . $model->confirmed));
	                    	}
	                    	else
	                    	{
	                    		$html = '';

	                    		if($model->status == '1')
	                    		{
	                    			$html .= '<span>' . ucwords(Config::get('confirmation_status.' . $model->confirmed)) . '</span>&nbsp;&nbsp;<a href="' . URL::to('/') . '/backend/confirmation/view?id=' . $model->transaction_id . '" data-id="' . $model->id . '">[View Confirmation]</a></span>&nbsp;&nbsp;<a href="' . URL::to('/') . '/backend/transaction/download_invoice?file=' . $model->transaction_id . '" data-id="' . $model->id . '">[Download Invoice]</a>';
	                    		}
	                    		else
	                    		{
	                    			$html .= '<span>' . ucwords(Config::get('confirmation_status.' . $model->confirmed)) . '</span>&nbsp;&nbsp;<a href="' . URL::to('/') . '/backend/confirmation/view?id=' . $model->transaction_id . '" data-id="' . $model->id . '">[View Confirmation]</a></span>';
	                    		}

	                    		return $html;
	                    	}
	                    }
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    if($model->status != '4')
	                    {
	                    	$action .= '<select class="update_status" data-id="' . $model->id . '">';
		                    $action .= '<option value="" selected>-- Select New Status --</option>';

		                    if($model->type == 'credit_card_veritrans')
		                    {
								$action .= '<option value="4">Cancel</option>';
		                    }
		                    else
		                    {
		                    	$action .= '<option value="1">Done</option>';
								$action .= '<option value="4">Cancel</option>';
		                    }
							
							$action .= '</select>';
	                    }
	                                        	
	                    return $action;
	                })
	                ->searchColumns('transaction_id', 'payment_type')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}