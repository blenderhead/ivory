<?php
	
	namespace Ivory\Backend;

	use Config;
	use Mail;

	use Transaction;
	use Booking;
	use Stock;
	use Room;
	use Hotel;

	use PDF;
	use Helper;

	class TransUpStatusProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$transaction = Transaction::find($data['id']);
								
				$booking = Booking::find($transaction->booking_id);

				$room = Room::find($booking->room_id);

				//$destination = Config::get('manager_emails.booking_manager');
				//$destination_bcc = Config::get('manager_emails.customer_service');

				$hotel = Hotel::all();

				$destination = $hotel->get(0)->email;
				$destination_bcc = $hotel->get(0)->manager_email;

				$user_data = array(
	                'to_admin' => $destination,
	                'to_admin_cc' => $destination_bcc,
	                'to_user' => $booking->email,
	                'title' => $booking->title,
	                'room' => $room,
	                'booking' => $booking,
	                'title' => $booking->title,
	                'transaction' => $transaction,
	                'name' => $booking->first_name . ' ' . $booking->last_name,
	                'payment_method' => Helper::formatPaymentMethod($transaction->type),
                	'status' => Helper::formatPaymentStatus($data['status'])
	            );

				$invoice_path = Config::get('path.booking_invoice');
				$admin_invoice_path = Config::get('path.admin_booking_invoice');
				$pdf_filename = $transaction->transaction_id;

				PDF::loadView('pdf.admin_invoice', $user_data)
					->setPaper('a4')
					->setOrientation('portrait')
					->save($admin_invoice_path.$pdf_filename);

				if($data['status'] == '1')
				{
					if($transaction->type == 'transfer' && $transaction->status !='1')
					{
						//Stock::updateStock($booking->checkin_date, $booking->checkout_date,$booking->number_of_rooms);

						PDF::loadView('pdf.invoice', $user_data)
		    				->setPaper('a4')
		    				->setOrientation('portrait')
		    				->save($invoice_path.$pdf_filename);

			            Mail::send('emails.booking.user_notification', $user_data, function($message) use ($user_data, $invoice_path, $pdf_filename) {
			                $message->to($user_data['to_user'], 'Ivory Booking Admin');
			                $message->subject('Payment Received Notification (#' . $user_data['transaction']->transaction_id . ')');
			                $message->attach($invoice_path.$pdf_filename);
			            });
					}
				}
				else if($data['status'] == '4')
				{
					Mail::send('emails.booking.user_cancel_notification', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_user'], 'Ivory Booking Admin');
		                $message->subject('Booking Cancellation Notification (Invoice #' . $user_data['transaction']->transaction_id . ')');
		            });

					Mail::send('emails.booking.admin_user_cancel', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_admin'], 'Ivory Booking Admin');
		                $message->subject('Booking Cancellation Notification (Invoice #' . $user_data['transaction']->transaction_id . ')');
		                //$message->attach($admin_invoice_path.$pdf_filename);
		            });

		            Mail::send('emails.booking.admin_user_cancel', $user_data, function($message) use ($user_data) {
		                $message->to($user_data['to_admin_cc'], 'Ivory Booking Admin');
		                $message->subject('Booking Cancellation Notification (Invoice #' . $user_data['transaction']->transaction_id . ')');
		                //$message->attach($admin_invoice_path.$pdf_filename);
		            });
				}
				
				$transaction->status = $data['status'];
				$transaction->save();
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}