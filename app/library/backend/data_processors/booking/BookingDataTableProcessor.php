<?php
	
	namespace Ivory\Backend;

	use URL;
	use Config;
	use DB;

	use Booking;

	use Datatable;

	class BookingDataTableProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				if(($data['date_start'] == '0' && $data['date_end'] == '0') || ($data['date_start'] == '' && $data['date_end'] == ''))
				{
					$query = Booking::orderBy('created_at','DESC')->get();
				}
				else
				{
					$query = Booking::whereRaw('? >= DATE(created_at)', array($data['date_start']))->whereRaw('? <= DATE(created_at)', array($data['date_end']))->orderBy('created_at','DESC')->get();
				}

				//var_dump($query);

				$data = Datatable::collection($query);
	            
	            $this->output = $data
	            	->addColumn('', function($model) {
	                    return '<input type="checkbox" class="booking" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->id . '</strong>';
	                	}

	                    return $model->id;
	                })
	                ->addColumn('room_name', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->room->name . '</strong>';
	                	}

	                    return $model->room->name;
	                })
	                ->addColumn('guest_name', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->first_name . ' ' . $model->last_name . '</strong>';
	                	}

	                    return $model->first_name . ' ' . $model->last_name;
	                })
	                ->addColumn('checkin_date', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->checkin_date . '</strong>';
	                	}

	                    return $model->checkin_date;
	                })
	                ->addColumn('checkout_date', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->checkout_date . '</strong>';
	                	}

	                    return $model->checkout_date;
	                })
	                ->addColumn('number_of_rooms', function($model) {

	                	if($model->is_new)
	                	{
	                		return '<strong>' . $model->number_of_rooms . '</strong>';
	                	}

	                    return $model->number_of_rooms;
	                })
	                ->addColumn('member_code', function($model) {
	                	if($model->booking_code)
	                	{
	                		if($model->is_new)
	                		{
	                			return '<strong>' . $model->booking_code . '</strong>';
	                		}

	                		return $model->booking_code;
	                	}
	                    else
	                    {
	                    	return '-';
	                    }
	                })
	                ->addColumn('status', function($model) {

	                	if($model->is_new)
                		{
                			return '<strong>' . ucwords(Config::get('booking_states.' . $model->transaction->status)) . '</strong>';
                		}

	                    return ucwords(Config::get('booking_states.' . $model->transaction->status));
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a title="View detail" href="' . URL::to('/') . '/backend/booking/view?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-zoom-in"></i></a>';
						//$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id  . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('id', 'room_name', 'booking_date')
	                ->orderColumns('room_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}