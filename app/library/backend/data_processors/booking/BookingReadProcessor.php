<?php
	
	namespace Ivory\Backend;

	use Booking;
	
	class BookingReadProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					$booking = Booking::find($data['id'][$i]);
					$booking->is_new = 0;
					$booking->save();
				}

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}