<?php
	
	namespace Ivory\Backend;

	class BookingDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}