<?php
	
	namespace Ivory\Backend;

	use Config;
	use File;

	use Banquet;
	use Helper;

	use Ivory\Backend\Models\Photos;

	class BanquetDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['room_id']);$i++)
				{
					$room = Banquet::find($data['room_id'][$i]);

					$room_photo = Helper::getImage($room->photo_id);

					if($room_photo)
					{
						$delete_photo = File::delete(Config::get('path.banquet_upload_path') . $room_photo);
						Photos::where('id',$room->photo_id)->delete();
					}

					$room->delete();
				}
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}