<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;

	use Banquet;

	use Datatable;

	class BanquetDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Banquet::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="room" value="' . $model->id . '">';
	                })
	                ->addColumn('room_name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/banquet/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id  . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('room_name')
	                ->orderColumns('room_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}