<?php
	
	namespace Ivory\Backend;

	use Config;

	use Banquet;
	use Helper;

	class BanquetAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$banquet = new Banquet();
				$banquet->name = $data['room_name'];
				$banquet->description = $data['room_description'];
				$banquet->size = $data['room_size'];
				$banquet->setup = $data['room_setup'];
				$banquet->gallery_id = $data['gallery_id'];
				//$banquet->photo_id = Helper::processRoomImage($data['room_image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.banquet_upload_path'), null, 'banquet');
				$banquet->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}