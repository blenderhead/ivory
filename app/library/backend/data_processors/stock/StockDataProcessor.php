<?php
	
	namespace Ivory\Backend;

	use Stock;
	use Room;
	
	class StockDataProcessor extends BaseProcessor
	{	
		/**
		 * Default interval date for generate days in
		 * page index of manage stock
		 * @var integer
		 */
		private $defaultIntervalDate = 20;

		/**
		 * Interval Start Date
		 * @var Date
		 */
		private $start_date;

		/**
		 * Interval end date
		 * @var Date
		 */
		private $end_date;

		/**
		 * This object room
		 * @var room
		 */
		private $room;


		public function __construct($room, $start_date = NULL, $end_date = NULL) {

			if (($start_date != NULL) && ($end_date != NULL)) {
				$this->start_date = $start_date;
				$this->end_date = $end_date;
			} else {
				$this->start_date = date('Y-m-d');
				$this->end_date = date('Y-m-d', strtotime('+'.$this->defaultIntervalDate.' days'));
			}

			$this->room = $room;
		}

		/**
		 * For proccess stock data
		 * @return boolean [description]
		 */
		public function process($data)
		{
			try
			{
				foreach ($data['stocks'] as $date => $dataStock) {
					
					$stock = Stock::where('date', '=', $date)
					->where('room_id', '=', $this->room)
					->first();
					
					// if notthing data stock
					if ($stock === NULL) {
						$newStock = new Stock($this->room, $date);
						$newStock->saveStock($dataStock);

					} else {
						$stock->saveStock($dataStock);
					}	

				} 

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}

		/**
		 * Get stock datas for some interval date given
		 * or use default intervalDate
		 * @param  Object $room       Object Room
		 * @param  Date   $start_date Interval start date
		 * @param  Date   $end_date   Interval end date
		 * @return Object             
		 */
		public function getStockDatas()
		{

			$stockAvailable = $this->room->stock()
			->whereBetween('date', array(
				$this->start_date,
				$this->end_date,
				))
			->get()
			->keyBy('date');		

			while ($this->start_date <= $this->end_date) {
	
				if ($stockAvailable->get($this->start_date) === NULL) {

					$stock = new Stock();
					$stock->createDummyData($this->start_date);

					$stockAvailable->put($this->start_date, $stock);
				}

				// start_date +1 days
				$this->start_date = date('Y-m-d', strtotime($this->start_date.' +1days'));
			}

			return $stockAvailable->sortBy('date');
		}
	}