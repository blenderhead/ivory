<?php
	
	namespace Ivory\Backend;

	use Restriction;

	class RestrictDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					Restriction::where('id',$data['id'][$i])->delete();
				}

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}