<?php
	
	namespace Ivory\Backend;

	use Restriction;

	class RestrictAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$restriction = new Restriction;
				$restriction->start_date = $data['start_date'];
				$restriction->hotel_id = 1;
				$restriction->end_date = $data['end_date'];
				$restriction->type = $data['type'];
				$restriction->description = $data['description'];
				$restriction->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}