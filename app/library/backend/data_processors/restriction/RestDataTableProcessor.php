<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;

	use Restriction;

	use Datatable;

	use Helper;

	class RestDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Restriction::orderBy('created_at','DESC')->get();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="res" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('date_start', function($model) {
	                    return $model->start_date;
	                })
	                ->addColumn('date_end', function($model) {
	                    return $model->end_date;
	                })
	                ->addColumn('type', function($model) {
	                    return Helper::displayRestrictionType($model->type);
	                })
	                 ->addColumn('description', function($model) {
	                    return str_limit($model->description, 50, '...');
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';

	                    $action .= '<a href="' . URL::to('/') . '/backend/restrictions/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    
	                    return $action;
	                })
	                ->searchColumns('name', 'date_start', 'date_end')
	                ->orderColumns('id')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}