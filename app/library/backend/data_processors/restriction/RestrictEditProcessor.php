<?php
	
	namespace Ivory\Backend;

	use Restriction;

	class RestrictEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$restriction = Restriction::find($data['res_id']);
				$restriction->start_date = $data['start_date'];
				$restriction->end_date = $data['end_date'];
				$restriction->type = $data['type'];
				$restriction->description = $data['description'];
				$restriction->save();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}