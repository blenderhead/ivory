<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;

	use Room;

	use Datatable;

	class RoomDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Room::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="room" value="' . $model->id . '">';
	                })
	                ->addColumn('room_name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('publish_price', function($model) {
	                    return $model->publish_price;
	                })
	                ->addColumn('adult_breakfast_price', function($model) {
	                    return $model->adult_breakfast_price;
	                })
	                ->addColumn('child_breakfast_price', function($model) {
	                    return $model->child_breakfast_price;
	                })
	                ->addColumn('breakfast_included', function($model) {
	                    if($model->breakfast_included == 1)
						{
							return 'Yes, for ' . $model->number_person_breakfast_included . ' person';
						}
						else
						{
							return 'No';
						}
	                })
	                /*
	                ->addColumn('extrabed_price', function($model) {
	                    return $model->extrabed_price;
	                })
	                */
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/room/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id  . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('id', 'room_name')
	                ->orderColumns('room_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}