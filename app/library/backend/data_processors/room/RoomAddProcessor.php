<?php
	
	namespace Ivory\Backend;

	use Room;
	use DB;

	class RoomAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			DB::beginTransaction();
			try
			{
				/* Room */
				$room = new Room();
				$room->createRoom($data);
				
				/* Facility */
				$facility = new Room\Facility();
				$facility->createFacility($room->id, $data['facilities']);	

				/* Capacity */
				$capacity = new Room\Capacity();
				$capacity->createCapacity($room->id, $data['capacities']);

				DB::commit();
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();

				DB::rollback();
				return FALSE;
			}
		}
	}