<?php
	
	namespace Ivory\Backend;

	use Room;
	use DB;

	class RoomEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			DB::beginTransaction();

			try
			{
				/* Room */
				$room = new Room();
				$room = $room->updateRoom($data);

				/* Facility */
				$facility = new Room\Facility();
				$facility->updateFacility($room->id, $data['facilities']);	

				/* Capacity */
				$capacity = new Room\Capacity();
				$capacity->updateCapacity($room->id, $data['capacities']);				

				DB::commit();
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();

				DB::rollback();
				return FALSE;
			}
			
		}
	}