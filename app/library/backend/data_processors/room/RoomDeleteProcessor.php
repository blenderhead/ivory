<?php
	
	namespace Ivory\Backend;

	use Room;

	class RoomDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['room_id']);$i++)
				{
					$room = new Room();
					$room->deleteRoom($data['room_id'][$i]);
				}
			
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}