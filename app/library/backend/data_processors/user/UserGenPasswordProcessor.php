<?php
	
	namespace Ivory\Backend;

	use Helper;

	class UserGenPasswordProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$this->output = Helper::generatePassword(8);

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}