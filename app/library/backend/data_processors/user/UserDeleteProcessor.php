<?php
	
	namespace Ivory\Backend;

	use Config;

	use User;
	use AssignedRole;

	use Helper;

	class UserDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					$user = User::find($data['id'][$i]);

					User::where('id','=',$user->id)->delete();

					AssignedRole::where('user_id','=',$user->id)->delete();

					Helper::deleteImage(Config::get('path.user_upload_path') . $user->image);
				}
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}