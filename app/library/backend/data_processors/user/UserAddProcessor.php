<?php
	
	namespace Ivory\Backend;

	use User;
	use Role;

	use Config;
	use Sentry;	
	use Helper;

	class UserAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$user = Sentry::getUserProvider()->create([
					'first_name' => $data['first_name'],
					'last_name' => $data['last_name'],
	                'email' => $data['email'],
	                'password' => $data['password'],
	                'member_code' => $data['member_code'],
	                'address' => $data['address'],
	                'phone' => $data['phone'],
	                'activated' => TRUE,
	                'role' => Role::getRoleId('Member'),
	                'image' => Helper::processImage($data['image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.user_upload_path'))
	            ]);
	    
	            $user_entity = User::where('id','=',$user->id)->first();
	            $user_entity->roles()->attach(Role::getRoleId('Member'));

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}