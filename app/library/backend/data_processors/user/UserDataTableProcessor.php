<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;

	use Datatable;

	class UserDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = User::getMembers();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="user" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('member_code', function($model) {
	                    return $model->member_code;
	                })
	                ->addColumn('name', function($model) {
	                    return $model->first_name . ' ' . $model->last_name;
	                })
	                ->addColumn('email', function($model) {
	                    return $model->email;
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    $action .= '<a href="' . URL::to('/') . '/backend/user/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('name', 'member_code', 'email')
	                ->orderColumns('name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}