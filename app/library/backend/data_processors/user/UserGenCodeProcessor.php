<?php
	
	namespace Ivory\Backend;

	use Helper;

	class UserGenCodeProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$this->output = Helper::generateCode();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}