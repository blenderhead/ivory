<?php
	
	namespace Ivory\Backend;

	use Ivory\Backend\Models\Testimoni;
	use Ivory\Backend\Models\Photos;
	
	class TestimoniAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$testimoni = new Testimoni();
				$testimoni->name = $data['name'];
				$testimoni->link = $data['link'];
				$testimoni->description = strip_tags($data['description']);
				//$testimoni->assign_to = $data['assign_to'] ? $data['assign_to'] : 0;
				$testimoni->is_publish = $data['is_publish'] ? 1 : 0;
				$testimoni->save();
				
				if($data['assign_to'])
				{
					$check_testi = Testimoni::where('assign_to',$data['assign_to'])->where('id','<>',$testimoni->id)->first();

					if($check_testi)
					{
						$check_testi->assign_to = 0;
						$check_testi->save();
					}

					$photo = Photos::find($data['assign_to']);
					$photo->title = $data['name'];
					$photo->description = $data['description'];
					$photo->save();
				}

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}