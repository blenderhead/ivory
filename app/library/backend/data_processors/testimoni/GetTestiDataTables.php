<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;
	use Ivory\Backend\Models\Testimoni;
	use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;

	use Datatable;

	class GetTestiDataTables extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = Testimoni::getAllTestimonies();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="testi" value="' . $model->id . '">';
	                })
	                ->addColumn('id', function($model) {
	                    return $model->id;
	                })
	                ->addColumn('name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('link', function($model) {
	                    return $model->link;
	                })
	                /*
	                ->addColumn('pos', function($model) {

	                	$home_gallery_id = GalleryCategories::getCategoryIdByName('home');
            			$home_galleries = Gallery::getGalleryPhotosByCategory($home_gallery_id);

            			$i = 0;

            			$pos = 'None';

            			foreach($home_galleries as $gallery)
            			{
            				$i++;

            				if($model->assign_to == $gallery->id)
            				{
            					$pos = 'Home Slide #' . $i;
            				}
            			}

            			return $pos;

	                })
	                */
	                ->addColumn('status', function($model) {
	                    switch($model->is_publish)
	                    {
	                    	case 0:
	                    		return 'Unpublished';
	                    		break;

	                    	case 1:
	                    		return 'Published';
	                    		break;
	                    }
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/cms/review/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id  . '"><i class="glyphicon glyphicon-remove"></i></button>';

	                    return $action;
	                })
	                ->searchColumns('name', 'id')
	                ->orderColumns('id')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}