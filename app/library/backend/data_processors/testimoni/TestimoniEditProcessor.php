<?php
	
	namespace Ivory\Backend;

	use Ivory\Backend\Models\Testimoni;
	use Ivory\Backend\Models\Photos;

	class TestimoniEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$testimoni = Testimoni::find($data['id']);
				$testimoni->name = $data['name'];
				$testimoni->link = $data['link'];
				$testimoni->description = strip_tags($data['description']);
				//$testimoni->assign_to = $data['assign_to'];
				$testimoni->is_publish = $data['is_publish'] ? 1 : 0;;
				$testimoni->save();

				/*
				if($data['assign_to'])
				{
					if($data['assign_to'] != $testimoni->assign_to)
					{
						$check_testi = Testimoni::where('assign_to',$data['assign_to'])->first();

						if($check_testi)
						{
							if($testimoni->assign_to)
							{
								// from a slide to non empty slide
								$photo_source = Photos::find($check_testi->assign_to);
								$title = $photo_source->title;
								$description = $photo_source->description;
								$photo_source->title = $data['name'];
								$photo_source->description = $data['description'];
								$photo_source->save();

								$check_testi->assign_to = $testimoni->assign_to;
								$check_testi->save();

								$photo_dest = Photos::find($testimoni->assign_to);
								$photo_dest->title = $title;
								$photo_dest->description = $description;
								$photo_dest->save();

								$testimoni->assign_to = $data['assign_to'];
								$testimoni->save();
							}
							else
							{
								// from no slide to non empty slide
								$photo_dest = Photos::find($data['assign_to']);
								$photo_dest->title = $data['name'];
								$photo_dest->description = $data['description'];
								$photo_dest->save();

								$check_testi->assign_to = 0;
								$check_testi->save();

								$testimoni->assign_to = $data['assign_to'];
								$testimoni->save();
							}
						}
						else
						{
							if($testimoni->assign_to)
							{
								// from a slide to empty slide
								$photo_source = Photos::find($testimoni->assign_to);
								$photo_source->title = '';
								$photo_source->description = '';
								$photo_source->save();

								$photo_dest = Photos::find($data['assign_to']);
								$photo_dest->title = $data['name'];
								$photo_dest->description = $data['description'];
								$photo_dest->save();

								$testimoni->assign_to = $data['assign_to'];
								$testimoni->save();
							}
							else
							{
								// from no slide to empty slide
								$photo_dest = Photos::find($data['assign_to']);
								$photo_dest->title = $data['name'];
								$photo_dest->description = $data['description'];
								$photo_dest->save();

								$testimoni->assign_to = $data['assign_to'];
								$testimoni->save();
							}
						}
					}
					else
					{
						$photo_dest = Photos::find($data['assign_to']);
						$photo_dest->title = $data['name'];
						$photo_dest->description = $data['description'];
						$photo_dest->save();
					}
				}
				else
				{
					if($testimoni->assign_to)
					{
						$photo_dest = Photos::find($testimoni->assign_to);
						$photo_dest->title = '';
						$photo_dest->description = '';
						$photo_dest->save();

						$testimoni->assign_to = 0;
						$testimoni->save();
					}
					else
					{
						// from no slide to no slide
						$testimoni->assign_to = 0;
						$testimoni->save();
					}
				}
				*/

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}