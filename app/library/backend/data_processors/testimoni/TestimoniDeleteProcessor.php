<?php
	
	namespace Ivory\Backend;

	use Ivory\Backend\Models\Testimoni;
	use Ivory\Backend\Models\Photos;

	use File;

	class TestimoniDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['id']);$i++)
				{
					$testimoni = Testimoni::find($data['id'][$i]);
					
					$photo = Photos::find($testimoni->assign_to);

					if($photo)
					{
						$photo->title = '';
						$photo->description = '';
						$photo->save();
					}

					$testimoni->delete();
				}
				
				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}