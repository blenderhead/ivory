<?php
	
	namespace Ivory\Backend;

	use Hotel;
	use HotelFacility;
	use HotelService;

	class HotelEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
	            $hotel_setting = Hotel::find($data['hotel_id']);

	            if(!$hotel_setting)
	            {
					$hotel_setting = new Hotel();
	            }

	            $hotel_setting->name = $data['name'];
				//$hotel_setting->description = $data['description'];
				//$hotel_setting->star = $data['star'];
				$hotel_setting->address = $data['address'];
				//$hotel_setting->type = $data['type'];
				$hotel_setting->phone = $data['phone'];
				$hotel_setting->fax = $data['fax'];
				$hotel_setting->email = $data['email'];
				$hotel_setting->manager_email = $data['manager_email'];
				//$hotel_setting->number_of_room = $data['number_of_room'];
				//$hotel_setting->number_of_floor = $data['number_of_floor'];
				//$hotel_setting->checkin_time = $data['checkin_time'];
				//$hotel_setting->checkout_time = $data['checkout_time'];
				$hotel_setting->tax_charge = $data['tax'];
				$hotel_setting->service_charge = $data['service'];
				$hotel_setting->member_discount = $data['member_discount'];
				$hotel_setting->save();

				//HotelFacility::updateFacility($hotel_setting->id, $data['facilities']);
				//HotelService::updateServices($hotel_setting->id, $data['services']);

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}