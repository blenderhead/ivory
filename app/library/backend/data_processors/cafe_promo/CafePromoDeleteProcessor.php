<?php
	
	namespace Ivory\Backend;

	use Config;
	use File;
	
	use Ivory\Backend\Models\CafePromo;
	use Ivory\Backend\Models\Photos;

	use Helper;

	class CafePromoDeleteProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				for($i=0;$i<count($data['promo_id']);$i++)
				{
					$promo = CafePromo::find($data['promo_id'][$i]);

					$promo_photo = Helper::getImage($promo->photo_id);

					if($promo_photo)
					{
						$delete_photo = File::delete(Config::get('path.cafe_promo_upload_path') . $promo_photo);
						Photos::where('id',$promo->photo_id)->delete();
					}

					$promo->delete();
				}
			
			return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}