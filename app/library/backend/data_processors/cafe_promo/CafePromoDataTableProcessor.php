<?php
	
	namespace Ivory\Backend;

	use URL;

	use User;
	use Role;

	use Ivory\Backend\Models\CafePromo;

	use Datatable;

	class CafePromoDataTableProcessor extends BaseProcessor
	{
		public function process()
		{
			try
			{
				$query = CafePromo::all();
            
	            $data = Datatable::collection($query);
	            
	            $this->output = $data
	                ->addColumn('', function($model) {
	                    return '<input type="checkbox" class="promo" value="' . $model->id . '">';
	                })
	                ->addColumn('promo_name', function($model) {
	                    return $model->name;
	                })
	                ->addColumn('status', function($model) {

	                	switch($model->status)
	                	{
	                		case '0':
	                			return 'Non active';
	                			break;

	                		case '1':
	                			return 'Active';
	                			break;
	                	}
	                })
	                ->addColumn('action', function($model) {
	                    $action = '';
	                    
	                    $action .= '<a href="' . URL::to('/') . '/backend/cafe/promo/edit?id=' . $model->id . '" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="' . $model->id . '"><i class="glyphicon glyphicon-pencil"></i></a>';
						$action .= '<button type="button" class="btn btn-warning btn-circle delete" data-id="' . $model->id  . '"><i class="glyphicon glyphicon-remove"></i></button>';
	                    return $action;
	                })
	                ->searchColumns('promo_name')
	                ->orderColumns('promo_name')
	                ->make();

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}