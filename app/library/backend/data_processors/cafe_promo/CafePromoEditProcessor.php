<?php
	
	namespace Ivory\Backend;

	use Config;
	
	use Ivory\Backend\Models\CafePromo;

	use Helper;

	class CafePromoEditProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
				$promo = CafePromo::find($data['id']);
				$promo->name = $data['promo_name'];
				$promo->description = $data['promo_description'];
				$promo->price = $data['promo_price'];
				$promo->photo_id = Helper::processRoomImage($data['promo_image'], $data['img_coord_x'], $data['img_coord_y'], $data['img_w'], $data['img_h'], $data['img_rw'], Config::get('path.cafe_promo_upload_path'), $promo->photo_id, 'cafe-promo');
				$promo->status = $data['promo_status'] ? 1 : 0;
				$promo->save();

				//$promo->status = $data['promo_status'] ? CafePromo::setActive($promo->id) : 0;

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
			
		}
	}