<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function getIndex()
	{
		//$url = 'https://www.youtube.com/watch?v=lfy9ZqKUJjU';
		//var_dump(Helper::getYoutubeId($url));

		$curl = new anlutro\cURL\cURL;
		$access_token = Config::get('instagram.access_token');
	    $response = $curl->get("https://api.instagram.com/v1/tags/ivorybyayola/media/recent?access_token=$access_token");
	    $rsp = json_decode($response->body);

	    if(isset($rsp->data))
	    {
	    	foreach($rsp->data as $timeline)
	    	{
	    		var_dump($timeline);
	    		//echo '<img src="' . $timeline->images->thumbnail->url . '" />';
	    	}
	    }
	}
}
