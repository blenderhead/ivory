<?php
	
	namespace Ivory\Backend;

	use View;

	use Hotel;
	
	use Format;
	use Message;

	class HotelController extends BaseController 
	{
		public function getEdit()
		{
			$data['hotel'] = Hotel::all()->first();
			return View::make('hotel.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new HotelEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new HotelEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
