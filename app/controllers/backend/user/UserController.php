<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
    use Input;
    use User;
    use Datatable;
    use Format;
	use Message;

	class UserController extends BaseController 
	{
		public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Member Code', 'Name', 'Email', 'Actions')
                    ->setUrl(URL::route('user.api'))
                    ->noScript();

			//$data['users'] = User::getMembers();

			return View::make('user.index', $data);
		}

        public function getMemberDataTables()
        {
            $processor = new UserDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
			return View::make('user.add');
		}

		public function postCreate()
		{
			$form_processor = new UserAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['user'] = User::find($id);
			return View::make('user.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new UserEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new UserDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function postGeneratePassword()
        {
            $processor = new UserGenPasswordProcessor();

            if($processor->process() == FALSE)
            {
                $message = new Message(1000, $processor->getError());
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
            }

            return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('password', $processor->getOutput()), 200);
        }

        public function postGenerateCode()
        {
            $processor = new UserGenCodeProcessor();

            if($processor->process() == FALSE)
            {
                $message = new Message(1000, $processor->getError());
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
            }

            return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('code', $processor->getOutput()), 200);
        }
	}
