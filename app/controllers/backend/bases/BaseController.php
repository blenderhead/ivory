<?php
	
	namespace Ivory\Backend;

	use Controller;
	use View;
	use Route;

	use Hotel;
	
	use Sentry;
	use Theme;

	class BaseController extends Controller 
	{
		public function __construct()
		{
			if(Sentry::check())
			{
				$user = Sentry::getUser();
				View::share('user', $user);
			}

			View::share('hotel_setting', Hotel::all());
			View::share('current_route_name', Route::currentRouteName());
			Theme::init('backend');
		}
	}
