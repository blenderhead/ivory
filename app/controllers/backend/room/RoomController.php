<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;

	use Format;
	use Message;
	use Room;
	use Input;

    use Ivory\Backend\Models\Gallery;

    use Datatable;

	class RoomController extends BaseController 
	{
		public function getIndex()
		{
			$data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'Room Name', 'Publish Price', 'Adult Breakfast Price', 'Child Breakfast Price', 'Breakfast Included', 'Actions')
                    ->setUrl(URL::route('room.api'))
                    ->noScript();

			//$data['rooms'] = Room::all();

			return View::make('room.index', $data);
		}

        public function getRoomDataTables()
        {
            $processor = new RoomDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            $data['galleries'] = Gallery::all();
			return View::make('room.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new RoomAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new RoomAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
			$room_id = Input::get('id');

			$data['room'] = Room::find($room_id);

            $data['galleries'] = Gallery::all();

			return View::make('room.edit', $data);			
		}

		public function postEdit()
		{
			$form_processor = new RoomEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new RoomEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}

		public function postDelete()
		{
			$form_processor = new RoomDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new RoomDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}
	}
