<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
	use Format;
	use Message;
    use Input;

    use Datatable;

    use Ivory\Backend\Models\GalleryCategories;

	class GalleryCategoriesController extends BaseController 
	{
		public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Category Name', 'Description', 'Actions')
                    ->setUrl(URL::route('cms.gallery-categories.api'))
                    ->noScript();

			//$data['categories'] = GalleryCategories::getAllCategories();
            
			return View::make('gallery_categories.index', $data);
		}

        public function getGalCatDataTables()
        {
            $processor = new GalCatTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
			return View::make('gallery_categories.add');
		}

		public function postCreate()
		{
			$form_processor = new GalleryCategoriesAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                $processor = new GalleryCategoriesProcessor();

                if($processor->process('create',$data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['category'] = GalleryCategories::find($id);
            
            return View::make('gallery_categories.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new GalleryCategoriesEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new GalleryCategoriesProcessor();

                if($processor->process('edit',$data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
            $processor = new GalleryCategoriesProcessor();

            if($processor->process('delete',Input::get('id')) == FALSE)
            {
                $message = new Message(1000, $processor->getError());
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
            }

            return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
		}
	}
