<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;

	use Format;
	use Message;
	use Room;
	use Input;
	use Cancellation\Rule;
	use Cancellation;

    use Datatable;

	class PolicyController extends BaseController 
	{
		public function getIndex()
		{
			$data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'Room Name', 'Cancellation Policy', 'Actions')
                    ->setUrl(URL::route('policy.api'))
                    ->noScript();

			//$data['policies'] =  Cancellation::where('promotion_id', '=', NULL)->get();
			
			return View::make('policy.index', $data);
		}

        public function getPolicyDataTables()
        {
            $processor = new PolicyDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
			$data['rooms'] = Room::all();
			$data['cancellations']['level_1'] = Rule::where('level', '=', 1)->get();
			$data['cancellations']['level_2'] = Rule::where('level', '=', 2)->get();

			return View::make('policy.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new PolicyAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PolicyAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
			$id = Input::get('id');

			$data['cancellation'] = Cancellation::find($id);

			$data['rooms'] = Room::all();
			$data['cancellation_rules']['level_1'] = Rule::where('level', '=', 1)->get();
			$data['cancellation_rules']['level_2'] = Rule::where('level', '=', 2)->get();

			return View::make('policy.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new PolicyEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PolicyEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}

		public function postDelete()
		{
			$form_processor = new PolicyDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new PolicyDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }				
		}
	}
