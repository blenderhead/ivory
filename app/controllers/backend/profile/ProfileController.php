<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
    use Input;
    use User;
    use Datatable;
    use Format;
	use Message;

	class ProfileController extends BaseController 
	{
		public function getEdit()
		{
            $id = Input::get('id');
            $data['user'] = User::find($id);
			return View::make('profile.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new ProfileEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new ProfileEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
