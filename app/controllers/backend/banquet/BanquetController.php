<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;

	use Format;
	use Message;
	use Banquet;
	use Input;

    use Ivory\Backend\Models\Gallery;

    use Datatable;

	class BanquetController extends BaseController 
	{
		public function getIndex()
		{
			$data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'Room Name', 'Actions')
                    ->setUrl(URL::route('banquet.api'))
                    ->noScript();

			//$data['rooms'] = Room::all();

			return View::make('banquet.index', $data);
		}

        public function getBanquetDataTables()
        {
            $processor = new BanquetDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            $data['galleries'] = Gallery::all();
			return View::make('banquet.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new BanquetAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BanquetAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
			$room_id = Input::get('id');

			$data['room'] = Banquet::find($room_id);

            $data['galleries'] = Gallery::all();

			return View::make('banquet.edit', $data);			
		}

		public function postEdit()
		{
			$form_processor = new BanquetEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new BanquetEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}

		public function postDelete()
		{
			$form_processor = new BanquetDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new BanquetDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}
	}
