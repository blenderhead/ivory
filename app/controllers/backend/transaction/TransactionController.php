<?php
	
	namespace Ivory\Backend;

    use Config;
    use URL;
	use View;
    use Input;
    use Response;

    use Booking;

    use Datatable;

	use Format;
	use Message;

	class TransactionController extends BaseController 
	{
		public function getIndex()
		{
			$data['tables'] = Datatable::table()
                    ->addColumn('Invoice No', 'Payment Type', 'Booking ID', 'Total', 'Status', 'Confirmed', 'Actions')
                    ->setUrl(URL::route('transaction.api'))
                    ->noScript();

			return View::make('transaction.index', $data);
		}

        public function getTransactionDataTables()
        {
            $processor = new TransactionDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function postUpdateStatus()
		{
			$form_processor = new TransUpStatusRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new TransUpStatusProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getDownloadInvoice()
        {
            $filename = Input::get('file');

            $invoice_path = Config::get('path.admin_booking_invoice');

            $file = $invoice_path . $filename;

            $headers = array(
                'Content-Type: application/pdf',
            );

            return Response::download($file, $filename, $headers);
        }
	}
