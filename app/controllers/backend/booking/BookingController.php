<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
    use Input;

    use Booking;

    use Datatable;

	use Format;
	use Message;

	class BookingController extends BaseController 
	{
		public function getIndex()
		{
            $data['date_start'] = Input::get('date_start');
            $data['date_end'] = Input::get('date_end');

			$data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Room Name', 'Guest Name', 'Check-In Date', 'Check-Out Date', 'Number of Rooms', 'Member Code', 'Status', 'Actions')
                    //->setUrl(URL::route('booking.api'))
                    ->setUrl(URL::to('/') . '/backend/booking/api?date_start=' . $data['date_start'] . '&date_end=' . $data['date_end'])
                    ->noScript();

			return View::make('booking.index', $data);
		}

        public function getBookingDataTables()
        {
            $data['date_start'] = Input::get('date_start');
            $data['date_end'] = Input::get('date_end');

            $processor = new BookingDataTableProcessor();
            $processor->process($data);
            return $processor->getOutput();
        }

		public function getView()
		{
			$id = Input::get('id');
            $booking = Booking::find($id);
            $booking->is_new = 0;
            $booking->save();
			$data['booking'] = $booking;
			return View::make('booking.view', $data);
		}

		public function postMarkAsRead()
		{
			$form_processor = new BookingReadRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BookingReadProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
