<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;

	use Format;
	use Message;
	use Room;
	use Input;

    use Datatable;

    use Ivory\Backend\Models\CafePromo;

	class CafePromoController extends BaseController 
	{
		public function getIndex()
		{
			$data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'Promo Name', 'Status', 'Actions')
                    ->setUrl(URL::route('room.api'))
                    ->noScript();

			//$data['rooms'] = Room::all();

			return View::make('cafe_promo.index', $data);
		}

        public function getCafePromoDataTables()
        {
            $processor = new CafePromoDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
			return View::make('cafe_promo.add');
		}

		public function postCreate()
		{
			$form_processor = new CafePromoAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new CafePromoAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
			$promo_id = Input::get('id');

			$data['promo'] = CafePromo::find($promo_id);

			return View::make('cafe_promo.edit', $data);			
		}

		public function postEdit()
		{
			$form_processor = new CafePromoEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new CafePromoEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}

		public function postDelete()
		{
			$form_processor = new CafePromoDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                
                $processor = new CafePromoDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}
	}
