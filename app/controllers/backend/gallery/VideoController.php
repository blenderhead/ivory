<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
	use Format;
	use Message;
    use ImageProcessor;
    use Input;
    use File;
    use Request;
    use Session;
    use Response;
    use Redirect;

    use Datatable;

    use \Ivory\Backend\Models\Video;

	class VideoController extends BaseController 
	{
        public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Title', 'Published', 'Actions')
                    ->setUrl(URL::route('cms.gallery.video.api'))
                    ->noScript();

			//$data['galleries'] = Gallery::getAllGalleries();
            
			return View::make('video.index', $data);
		}

        public function getVideoDataTables()
        {
            $processor = new VideoTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            return View::make('video.add');
		}

		public function postCreate()
        {
            $form_processor = new VideAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new VideoAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                if (Session::has('photos'))
                {
                    Session::forget('photos');
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('gallery_id', $processor->getOutput()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['video'] = Video::find($id);
            return View::make('video.edit', $data);
		}

		public function postEdit()
		{
            $form_processor = new VideoEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new VideoEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new VideoDeleteRepository();
            
            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                $processor = new VideoDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
