<?php
	
	namespace Ivory\Backend;

    use Config;
    use URL;
	use View;
	use Format;
	use Message;
    use ImageProcessor;
    use Input;
    use File;
    use Request;
    use Session;
    use Response;
    use Redirect;

    use Datatable;

    use \Ivory\Backend\Models\GalleryCategories;
    use \Ivory\Backend\Models\Gallery;
    use \Ivory\Backend\Models\Photos;
    use Ivory\Backend\Models\Testimoni;

	class GalleryImageController extends BaseController 
	{
        public function postUpload()
        {
            $this->layout = NULL;

            $files = Input::file('files');
            $gallery_id = Input::get('gallery_id');
            
            $image_manager = new ImageProcessor($files[0], Config::get('path.gallery_upload_path'));
            $image_manager->upload();
            $image_manager->makeThumbnail(Config::get('path.gallery_thumbnail_path'));

            $gallery_photo = new Photos();
            $gallery_photo->object_id = $gallery_id;
            $gallery_photo->title = '';
            $gallery_photo->description = '';
            $gallery_photo->file = $image_manager->getFilename();
            $gallery_photo->destination = Config::get('path.gallery_upload_path');
            $gallery_photo->type = 'gallery';
            $gallery_photo->save();

            echo json_encode(array(
                'files' => array(
                    '0' => array(
                        'name' => $image_manager->getFilename(),
                        'size' => $image_manager->getFileSize(),
                        'type' => $image_manager->getFileType(),
                        'url' => $image_manager->getFileUrl('/uploads/slideshow/'),
                        'thumbnailUrl' => $image_manager->getFileUrl('/uploads/gallery/', 'thumbnail'),
                        'deleteUrl' => URL::to('/') . '/backend/cms/gallery/delete_photo?photo_id=' . $gallery_photo->id . '&filename=' . $image_manager->getFilename(),
                        'deleteType' => 'GET',
                        'photo_id' => $gallery_photo->id,
                        'photo_title' => $gallery_photo->title,
                        'photo_description' => $gallery_photo->description
                    )
                )
            ));
            
        }

        public function getDeletePhoto()
        {
            $this->layout = NULL;

            $photo_id = Input::get('photo_id');
            $filename = Input::get('filename');

            Testimoni::where('assign_to',$photo_id)->update(array(
                'assign_to' => 0
            ));

            Photos::where('id','=',$photo_id)->delete();

            $galleries_path = Config::get('path.gallery_upload_path');
            $galleries_thumb_path = Config::get('path.gallery_thumbnail_path');
            File::delete($galleries_path . $filename);
            File::delete($galleries_thumb_path . $filename);

            echo json_encode(array(
                $filename => TRUE
            ));
        }

        public function getGalleryPhoto()
        {
            $this->layout = NULL;

            $files = array();

            $gallery_id = Input::get('gallery_id');

            $photos = Photos::where('object_id','=',$gallery_id)->get();

            if(!$photos->isEmpty())
            {
                $photos->each(function($photo) use (&$files) {
                    $image_manager = new ImageProcessor(NULL, Config::get('path.gallery_upload_path'));

                    $image_manager->setFilename($photo->file);

                    $data = array(
                        'name' => $photo->url,
                        'size' => $image_manager->getFileSize(),
                        'type' => $image_manager->getFileType(),
                        'url' => URL::to('/') . '/uploads/galleries/' . $photo->file,
                        'thumbnailUrl' => URL::to('/') . '/uploads/gallery/thumbnails/' . $photo->file,
                        'deleteUrl' => URL::to('/') . '/backend/cms/gallery/delete_photo?photo_id=' . $photo->id . '&filename=' . $photo->file,
                        'deleteType' => 'GET',
                        'photo_id' => $photo->id,
                        'photo_title' => $photo->title,
                        'photo_description' => $photo->description
                    );

                    array_push($files, $data);
                });

                echo json_encode(array(
                    'files' => $files
                ));
            }
        }
	}
