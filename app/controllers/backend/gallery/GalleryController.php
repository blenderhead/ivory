<?php
	
	namespace Ivory\Backend;

    use URL;
	use View;
	use Format;
	use Message;
    use ImageProcessor;
    use Input;
    use File;
    use Request;
    use Session;
    use Response;
    use Redirect;

    use Datatable;

    use \Ivory\Backend\Models\GalleryCategories;
    use \Ivory\Backend\Models\Gallery;
    use \Ivory\Backend\Models\Photos;

	class GalleryController extends BaseController 
	{
        public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Gallery Name', 'Published', 'Actions')
                    ->setUrl(URL::route('cms.gallery.api'))
                    ->noScript();

			//$data['galleries'] = Gallery::getAllGalleries();
            
			return View::make('gallery.index', $data);
		}

        public function getGalDataTables()
        {
            $processor = new GalTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            $data['categories'] = GalleryCategories::getAllCategories()->lists('name','id');
            
			return View::make('gallery.add', $data);
		}

		public function postCreate()
        {
            $form_processor = new GalleryAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new GalleryAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                if (Session::has('photos'))
                {
                    Session::forget('photos');
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('gallery_id', $processor->getOutput()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['gallery'] = Gallery::find($id);
            $data['categories'] = GalleryCategories::getAllCategories()->lists('name','id');
            
            return View::make('gallery.edit', $data);
		}

		public function postEdit()
		{
            $form_processor = new GalleryEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new GalleryEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new GalleryDeleteRepository();
            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                $processor = new GalleryDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getManagePhoto()
        {
            $data['gallery_id'] = Input::get('id');
            return View::make('gallery.manage_photo', $data);
        }

        public function postUpdateMetadata()
        {
            $form_processor = new GalleryUpMetaRepository();
            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                $processor = new GalleryUpMetaProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
	}
