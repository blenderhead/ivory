<?php
	
	namespace Ivory\Backend;

    use URL;

	use View;

	use Format;
	use Message;
	use Input;
	use Room;
	use Promo;
	use Cancellation\Rule;

    use Datatable;

	class PromoController extends BaseController 
	{
		public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'Promotion', 'Room Name', 'Booking Date', 'Stay Date', 'Cancellation Policy', 'Operations')
                    ->setUrl(URL::route('promo.api'))
                    ->noScript();

			$data['promotions'] = Promo::all();

			return View::make('promo.index', $data);
		}

        public function getPromoDataTables()
        {
            $processor = new PromoDataTableProcessor();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
			$data['rooms'] = Room::all();
			$data['cancellations']['level_1'] = Rule::where('level', '=', 1)->get();
			$data['cancellations']['level_2'] = Rule::where('level', '=', 2)->get();

			return View::make('promo.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new PromoAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PromoAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
			$id = Input::get('id');
			$data['promotion'] = Promo::find($id);

			$data['rooms'] = Room::all();
			$data['cancellations']['level_1'] = Rule::where('level', '=', 1)->get();
			$data['cancellations']['level_2'] = Rule::where('level', '=', 2)->get();

			return View::make('promo.edit', $data);
		}

		public function postEdit()
		{
			$form_processor = new PromoEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PromoEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }			
		}

		public function postDelete()
		{
			$form_processor = new PromoDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PromoDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }				
		}
	}
