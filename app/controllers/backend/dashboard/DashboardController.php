<?php
	
	namespace Ivory\Backend;

	use View;

	use User;
	use Booking;

	class DashboardController extends BaseController 
	{
		public function getIndex()
		{
			$data['members'] = User::getMembers() ? User::getMembers()->count() : '0';
			$data['new_bookings'] = Booking::getNewBookings() ? Booking::getNewBookings()->count() : '0';

			return View::make('dashboard.index', $data);
		}
	}
