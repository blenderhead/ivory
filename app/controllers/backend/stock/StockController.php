<?php
	
	namespace Ivory\Backend;

	use View;

	use Format;
	use Message;
	use Room;
	use Redirect;

	class StockController extends BaseController 
	{
		public function getIndex()
		{
			$data = [];
			$room;
			$start_date = NULL;
			$end_date = NULL;	

			$form_processor = new StockSearchRepository;
            if($form_processor->validate() == FALSE)
            {
            	$errors = $form_processor->getErrors();

            	return Redirect::route('stock.index')
            	->withErrors($errors)
            	->withInput();
            } 
            else 
            {
            	$rooms = Room::all()->keyBy('id');

            	if ($form_processor->isActionSearch()) {

					$data = $form_processor->getData();

					$room = $rooms->get($data['room_id']);
					
					$start_date = $data['start_date'];
					$end_date = $data['end_date'];

				} else {
					
					$room = $rooms->first();
				}
            }

            /*Check has have room ?*/
            if ($room == NULL) {
            	return Redirect::route('room.index')
            	->withErrors('Before you manage stocks and prices, please create room before.')
            	;
            }

			$stockDatas = new StockDataProcessor($room, $start_date, $end_date);
			$stockDatas = $stockDatas->getStockDatas();       

			return View::make('stock.index')
			->with('rooms', $rooms)
			->with('room', $room)
			->with('stockDatas', $stockDatas)
			;
		}

		public function postIndex()
		{
			$form_processor = new StockIndexRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
            	$data = $form_processor->getData();
            	$processor = new StockDataProcessor($data['room_id']);

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
