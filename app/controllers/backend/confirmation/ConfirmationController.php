<?php
	
	namespace Ivory\Backend;

	use View;
    use Input;

    use TransferConfirmation;

	class ConfirmationController extends BaseController 
	{
		public function getConfirmation()
		{ 
            $transaction_id = Input::get('id');
            $confirmation = TransferConfirmation::where('transaction_id',$transaction_id)->first();
            $data['confirmation'] = $confirmation;
			return View::make('confirmation.view', $data);
		}
	}
