<?php
	
	namespace Ivory\Backend;

    use Config;
    use URL;
	use View;
    use Format;
    use Message;
    use ImageProcessor;
    use Input;
    use File;
    use Request;
    use Session;
    use Response;

    use Datatable;

    use \Ivory\Backend\Models\Testimoni;
    use \Ivory\Backend\Models\Photos;
    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;

	class TestimoniController extends BaseController 
	{
		public function getIndex()
		{
            $data['tables'] = Datatable::table()
                    ->addColumn('<input type="checkbox" id="selectall">', 'ID', 'Name', 'Link', 'Status', 'Action')
                    ->setUrl(URL::route('cms.review.api'))
                    ->noScript();

			//$data['testimonies'] = Testimoni::getAllTestimonies();
			return View::make('testimoni.index', $data);
		}

        public function getTestiDataTables()
        {
            $processor = new GetTestiDataTables();
            $processor->process();
            return $processor->getOutput();
        }

		public function getCreate()
		{
            $home_gallery_id = GalleryCategories::getCategoryIdByName('home');
            $data['home_galleries'] = Gallery::getGalleryPhotosByCategory($home_gallery_id);
			return View::make('testimoni.add', $data);
		}

		public function postCreate()
		{
			$form_processor = new TestimoniAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new TestimoniAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function getEdit()
		{
            $id = Input::get('id');
            $data['testimoni'] = Testimoni::find($id);
            $home_gallery_id = GalleryCategories::getCategoryIdByName('home');
            $data['home_galleries'] = Gallery::getGalleryPhotosByCategory($home_gallery_id);
			return View::make('testimoni.edit',$data);
		}

		public function postEdit()
		{
			$form_processor = new TestimoniEditRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new TestimoniEditProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

		public function postDelete()
		{
			$form_processor = new TestimoniDeleteRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();
                $processor = new TestimoniDeleteProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function clearPhotoSession($photo) {
            if (Session::has($photo))
            {
                Session::forget($photo);
            }
        }
        public function upload() {
            $this->clearPhotoSession('photos');

            $file = Input::file('file');
            $destination = Config::get('path.testimoni_upload_path');
            $photos = array(
                'destination'=>$destination,
                'files'=>array()
            );

            $image = new ImageProcessor($file,$destination);
            if($image->upload()) {
                $photos['files'][] = $image->getFilename();
            }
            // store photos's result to Session.
            Session::put('photos', $photos);
            
        }

        public function getAllPhotos() {
            $testimoni_id = Input::get('id');
            if(! $testimoni_id) {
                return Format::apiResponse(0, Message::getSuccessMessage(), 
                    Format::ioObject('errors', array('Testimoni id can not be empty.')), 200);
            }
            $photo = Testimoni::find($testimoni_id)->photos;
            $res = array();

            if($photo) {
                $path = public_path() . DIRECTORY_SEPARATOR .
                    $photo->destination. 
                    $photo->file;
                $filesize = 0;
                if (file_exists($path) && is_file($path)) {
                    $filesize = filesize($path);
                }
                $res[] = array(
                    'id'=>$photo->id,
                    'destination'=>$photo->destination,
                    'file'=>$photo->file,
                    'size'=>$filesize
                );
            }
        
            return Response::json($res);

        }

        public function deletePhoto($id = NULL , $name = NULL) {
            if(!$id || !$name) {
                return Format::apiResponse(0, Message::getSuccessMessage(), 
                    Format::ioObject('errors', array(
                        'Testimoni id or File name can not be empty.')
                    ), 200);
            }

            $photos = Photos::where('testimoni_id','=', $id)
                ->where('type','=', 'testimoni')
                ->where('file','=', $name);

            if($photos->first()) {
                try{
                    $path = public_path();
                    $file = '/'.$photos->first()->destination . $photos->first()->file;

                    if(File::exists($path.$file)) {
                        File::delete($path.$file);
                        $photos->delete();
                    }
                    return 'Image success deleted from database.';
                }catch (\Exception $e) {
                    return 'Image not found in database.';
                }
            }
            return 'Image removed.';
        }
	}
