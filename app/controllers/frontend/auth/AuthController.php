<?php
    
    namespace Ivory\Frontend;

    use Input;
    use View;
    use Redirect;

    use Sentry;

    use Message;
    use Format;

	class AuthController extends BaseController 
	{
		public function getLogin()
		{
            if(Sentry::check())
            {
                return Redirect::route('home');
            }

			return View::make('auth.login');
		}

		public function postLogin()
		{
			$form_processor = new UserLoginRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserLoginProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}

        public function getForgetPassword()
        {
            return View::make('auth.forget_password');
        }

        public function postForgetPassword()
        {
            $form_processor = new UserResetPass1Repository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserResetPass1Processor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function getResetPassword()
        {
            $data['token'] = Input::get('token');
            return View::make('auth.reset_password', $data);
        }

        public function postResetPassword()
        {
            $form_processor = new UserResetPass2Repository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserResetPass2Processor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

		public function getLogout()
		{
			Sentry::logout();
            return Redirect::route('home');
		}
	}
