<?php
    
    namespace Ivory\Frontend;

    use App;
    
    use View;

    use Room;
    use Ivory\Backend\Models\Gallery;

	class RoomController extends BaseController 
	{
		public function getRoom($room_slug)
		{
			$room = Room::findBySlug($room_slug);

			if($room)
			{
				$data['room'] = $room;
				$data['photos'] = Gallery::getGalleryPhotosById($room->gallery_id);
				return View::make('room.index2', $data);
			}

			App::abort(404);
		}

		public function search()
		{
			$from_processor = new RoomSearchRepository();

			if ($from_processor->validate() == FALSE) 
			{
				return Redirect::back()->withError($from_processor->getErrors());
			} 
			else 
			{
				$data = $from_processor->getData();
				$data_processor = new RoomSearchProcessor();

				if ($data_processor->process($data) == FALSE) 
				{
					return Redirect::back()->withError($data_processor->getError());
				}
				
				return View::make('search.result', $data_processor->getOutput());
			}
		}
	}
