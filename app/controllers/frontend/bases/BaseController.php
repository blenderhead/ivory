<?php
	
	namespace Ivory\Frontend;

	use View;
	use Controller;
	use Route;

	use Sentry;
	use Theme;

	use Room;
	use Banquet;
	use Hotel;

	class BaseController extends Controller 
	{
		public function __construct()
		{
			if(Sentry::check())
			{
				$user = Sentry::getUser();
				View::share('user', $user);
			}

			View::share('hotel_setting', Hotel::all());
			View::share('hotel_rooms', Room::all());
			View::share('banquets', Banquet::all());
			View::share('current_route_name', Route::currentRouteName());
			Theme::init('frontend');
		}
	}
