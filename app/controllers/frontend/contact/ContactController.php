<?php

    namespace Ivory\Frontend;

    use App;

    use View;

    use Message;
    use Format;

	class ContactController extends BaseController 
	{
		public function getContact()
		{
			return View::make('contact-us.index');
		}

		public function postContact()
		{
			$form_processor = new ContactRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new ContactProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array(), 200));
            }
		}
	}