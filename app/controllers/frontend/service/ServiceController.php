<?php
    
    namespace Ivory\Frontend;

	use Captcha;

	class ServiceController extends BaseController 
	{
		public function getCaptcha()
		{
			return json_encode(array(
				'image' => Captcha::img()
			));
		}
	}
