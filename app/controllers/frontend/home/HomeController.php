<?php
    
    namespace Ivory\Frontend;

    use App;
    use View;
    use Room;

    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;
    use Ivory\Backend\Models\Testimoni;

	class HomeController extends BaseController 
	{
		public function getIndex()
		{
			$gallery_cat = GalleryCategories::findBySlug('home');

			if($gallery_cat)
			{
				$data['testimonials'] = Testimoni::where('is_publish',1)->get();
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('home.index', $data);
			}
			
			App::abort(404);
		}
	}
