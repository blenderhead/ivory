<?php
    
    namespace Ivory\Frontend;

    use URL;
    use Request;
    use Input;

    use App;
    
    use View;

    use Banquet;
    use Ivory\Backend\Models\Gallery;

    use Message;
    use Format;

	class BanquetController extends BaseController 
	{
		public function getRoom($room_slug)
		{
			$room = Banquet::findBySlug($room_slug);

			if($room)
			{
				$data['room'] = $room;
				$data['photos'] = Gallery::getGalleryPhotosById($room->gallery_id);
				return View::make('banquet.index2', $data);
			}

			App::abort(404);
		}

		public function getEnquery()
		{
			$data['rooms'] = Banquet::all();
			return View::make('banquet.enquery', $data);
		}

		public function postEnquery()
		{
			$form_processor = new BanquetEnquiryRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BanquetEnquiryProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                $redirect = URL::to('/') . '/booking/transaction?id=' . base64_encode($processor->getOutput());

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('redirect', array($redirect), 200));
            }
		}
	}
