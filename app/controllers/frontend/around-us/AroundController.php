<?php
    
    namespace Ivory\Frontend;

    use App;

    use View;

    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;

	class AroundController extends BaseController 
	{
		public function getGallery()
		{
			$gallery_cat = GalleryCategories::findBySlug('around-us');

			if($gallery_cat)
			{
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('around-us.index2', $data);
			}
			
			App::abort(404);
		}
	}
