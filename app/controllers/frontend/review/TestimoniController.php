<?php
	
	namespace Ivory\Frontend;

    use Config;
    use URL;
	use View;
    use Format;
    use Message;
    use ImageProcessor;
    use Input;
    use File;
    use Request;
    use Session;
    use Response;

    use Datatable;

    use \Ivory\Backend\Models\Testimoni;
    use \Ivory\Backend\Models\Photos;


	class TestimoniController extends BaseController 
	{
		public function getCreate()
		{
			return View::make('review.index');
		}

		public function postCreate()
		{
			$form_processor = new TestimoniAddRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new TestimoniAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
