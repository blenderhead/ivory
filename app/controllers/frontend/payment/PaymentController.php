<?php

    namespace Ivory\Frontend;

    use URL;
    use View;
    use Request;
    use Input;

    use App;
    use Booking;
    use Room;
    use Stock;
    use Promo;
    use Transaction;

    use Message;
    use Format;

	class PaymentController extends BaseController 
	{
		public function postSendSecurePayment()
		{
			$form_processor = new PaymentRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PaymentProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                $redirect = URL::to('/') . '/booking/transaction?id=' . base64_encode($processor->getOutput());

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('redirect', array($redirect), 200));
            }
		}
	}