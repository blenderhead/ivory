<?php
    
    namespace Ivory\Frontend;

    use App;

    use View;

    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;
    use Ivory\Backend\Models\Video;

	class GalleryController extends BaseController 
	{
		public function getPhotoGallery()
		{
			$gallery_cat = GalleryCategories::findBySlug('main');

			if($gallery_cat)
			{
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('gallery.index', $data);
			}
			
			App::abort(404);
		}

		public function getVideoGallery()
		{
			$videos = Video::where('is_publish',true)->get();

			if(!$videos->isEmpty())
			{
				$data['videos'] = $videos;
				return View::make('video.index', $data);
			}
			
			App::abort(404);
		}
	}
