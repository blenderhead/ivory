<?php

    namespace Ivory\Frontend;

    use URL;
    use View;
    use Request;
    use Input;

    use App;
    use Booking;
    use Room;
    use Stock;
    use Promo;
    use Transaction;

    use Message;
    use Format;

	class BookingController extends BaseController 
	{
		public function getBooking()
		{
				
			$room_id = Request::get('room_id');
			$promo_id = Request::get('promo_id');
			$query = Request::get('query');

			$query = Booking::decryptQuery($query);

			$room = new Room;
			$room = $room->getRoomByCapacity($query, $room_id);

			$stocks = new Stock;
			$stocks = $stocks->getStockRoom($room, $query);

			$promotion = new Promo;
			$promotion = $promotion->getSinglePromotionRoom($room, $query, $promo_id);

			if (($promotion == NULL || $promotion == FALSE) AND $promo_id > 0) {
				App::abort(404);
			}

			$room->__set('promotions', $promotion);
			$room->__set('stocks', $stocks);

			$room->calculatePrice($query);

			if ($promotion) {
				$promotion->calculatePrice($room);
			}

			return View::make('booking.index', [
				'room' => $room,
				'query' => $query,
			]);
			
		}

		public function postBooking()
		{
			$form_processor = new BookingRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new BookingProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                $redirect = URL::to('/') . '/booking/transaction?id=' . base64_encode($processor->getOutput());

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('redirect', array($redirect), 200));
            }
		}

		public function getHandleTransaction()
		{
			$transaction_id = base64_decode(Input::get('id'));
			$data['transaction'] = Transaction::where('transaction_id',$transaction_id)->first();
			$data['transaction_id'] = Input::get('id');
			return View::make('booking.transaction', $data);
		}
	}