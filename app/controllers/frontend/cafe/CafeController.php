<?php
    
    namespace Ivory\Frontend;

    use App;
    use Config;
    use View;

    use Ivory\Backend\Models\CafePromo;
    use Ivory\Backend\Models\Gallery;
    use Ivory\Backend\Models\GalleryCategories;

	class CafeController extends BaseController 
	{
		public function getPromo()
		{
			$data['promos'] = CafePromo::getActivePromo();
			return View::make('cafe.promo', $data);

			/*
			$gallery_cat = GalleryCategories::findBySlug('cafe-promo');

			if($gallery_cat)
			{
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('cafe.promo', $data);
			}
			
			App::abort(404);
			*/
		}

		public function getMenu()
		{
			$gallery_cat = GalleryCategories::findBySlug('cafe-menu');

			if($gallery_cat)
			{
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('cafe.menu', $data);
			}
			
			App::abort(404);
		}

		public function getMoment()
		{
			/*
			$gallery_cat = GalleryCategories::findBySlug('cafe-moment');

			if($gallery_cat)
			{
				$data['photos'] = Gallery::getGalleryPhotosByCategory($gallery_cat->id);
				return View::make('cafe.moment', $data);
			}
			
			App::abort(404);
			*/

			$curl = new \anlutro\cURL\cURL;
			$access_token = Config::get('instagram.access_token');
			$tag = Config::get('instagram.tag_search');
		    $response = $curl->get("https://api.instagram.com/v1/tags/$tag/media/recent?access_token=$access_token&count=1000");
		    $data['response'] = json_decode($response->body);
		    return View::make('cafe.moment', $data);
		}
	}
