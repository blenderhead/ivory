<?php
    
    namespace Ivory\Frontend;

    use View;
    use Input;

    use Message;
    use Format;

	class ConfirmationController extends BaseController 
	{
		public function getIndex()
		{
			$data['invoice_id'] = Input::get('id');
			return View::make('confirmation.index', $data);
		}

		public function postSave()
		{
			$form_processor = new ConfirmationRepository();

            if($form_processor->validate() == FALSE)
            {
                $errors = $form_processor->getErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new ConfirmationProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
		}
	}
