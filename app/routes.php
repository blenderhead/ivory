<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::controller('home', 'HomeController');

Route::group(array('namespace' => 'Ivory\Frontend'), function() {  

    Route::get('/', array(
        'as' => 'home',
        'uses' => 'HomeController@getIndex'
    ));

    /*
    Route::get('/search', array(
        'as' => 'room.search',
        'uses' => 'RoomController@search'
    ));
    */

    Route::get('/room/{slug}', array(
        'as' => 'room',
        'uses' => 'RoomController@getRoom'
    ));

    Route::get('/banquet/enquery', array(
        'as' => 'banquet.enquery',
        'uses' => 'BanquetController@getEnquery'
    ));

    Route::post('/banquet/enquery', array(
        'as' => 'banquet.enquery',
        'uses' => 'BanquetController@postEnquery'
    ));

    Route::get('/banquet/{slug}', array(
        'as' => 'banquet',
        'uses' => 'BanquetController@getRoom'
    ));

    /*
    Route::get('/review', array(
        'as' => 'review.get',
        'uses' => 'TestimoniController@getCreate'
    ));

    Route::post('/review', array(
        'as' => 'review.save',
        'uses' => 'TestimoniController@postCreate'
    ));
    */

    Route::group(array('prefix' => 'gallery'), function() {

        Route::get('/photos', array(
            'as' => 'gallery.photos',
            'uses' => 'GalleryController@getPhotoGallery'
        ));   

        Route::get('/videos', array(
            'as' => 'gallery.videos',
            'uses' => 'GalleryController@getVideoGallery'
        )); 

    });    

    Route::get('/around-us', array(
        'as' => 'around-us',
        'uses' => 'AroundController@getGallery'
    ));

    Route::get('/contact-us', array(
        'as' => 'contact-us',
        'uses' => 'ContactController@getContact'
    ));  

    Route::post('/contact-us', array(
        'as' => 'contact-us.save',
        'uses' => 'ContactController@postContact'
    ));

    /*
    Route::group(array('prefix' => 'booking'), function() {

        Route::get('/', array(
            'as' => 'user.booking',
            'uses' => 'BookingController@getBooking'
        ));   

        Route::post('/', array(
            'as' => 'user.booking',
            'uses' => 'BookingController@postBooking'
        ));   

        Route::get('/transaction', array(
            'as' => 'user.booking.success',
            'uses' => 'BookingController@getHandleTransaction'
        )); 

    });        

    Route::group(array('prefix' => 'confirmation'), function() {

        Route::get('/',array(
            'as' => 'confirmation.index',
            'uses' => 'ConfirmationController@getIndex'
        ));

        Route::post('/',array(
            'as' => 'transaction.save',
            'uses' => 'ConfirmationController@postSave'
        ));

    }); 
    
    Route::post('/send-secure-payment', array(
        'as' => 'veritrans.send-secure-payment',
        'uses' => 'PaymentController@postSendSecurePayment'
    ));
    */

    /*
    Route::group(array('prefix' => 'login'), function() {

        Route::get('/', array(
            'as' => 'user.login',
            'uses' => 'AuthController@getLogin'
        ));

        Route::post('/', array(
            'as' => 'user.do.login',
            'uses' => 'AuthController@postLogin'
        ));

    });
    */
    
    /*
    Route::get('/forget_password', array(
        'as' => 'user.forget_password',
        'uses' => 'AuthController@getForgetPassword'
    )); 

    Route::post('/forget_password', array(
        'as' => 'user.do.forget_password',
        'uses' => 'AuthController@postForgetPassword'
    ));  

    Route::get('/reset_password', array(
        'as' => 'user.reset_password',
        'uses' => 'AuthController@getResetPassword'
    ));

    Route::post('/reset_password', array(
        'as' => 'user.do.reset_password',
        'uses' => 'AuthController@postResetPassword'
    )); 
    */

    /*
    Route::get('/logout', array(
        'as' => 'user.logout',
        'uses' => 'AuthController@getLogout'
    ));  
    */

    Route::group(array('prefix' => 'cafe'), function() {

        Route::get('/promo', array(
            'as' => 'cafe.promo',
            'uses' => 'CafeController@getPromo'
        ));

        Route::get('/menu', array(
            'as' => 'cafe.menu',
            'uses' => 'CafeController@getMenu'
        ));

        Route::get('/moment', array(
            'as' => 'cafe.moment',
            'uses' => 'CafeController@getMoment'
        ));

    });

    Route::group(array('prefix' => 'service'), function() {

        Route::get('/get_captcha', array(
            'as' => 'service.get_captcha',
            'uses' => 'ServiceController@getCaptcha'
        )); 

    });

});

Route::group(array('prefix' => 'backend', 'namespace' => 'Ivory\Backend'), function() {

	Route::get('/login', array(
	    'as' => 'auth.login',
	    'uses' => 'AuthController@getLogin'
	));

	Route::post('/login', array(
	    'as' => 'auth.do.login',
	    'uses' => 'AuthController@postLogin'
	));

	Route::get('logout', array(
	    'as' => 'auth.logout',
	    'uses' => 'AuthController@getLogout'
	));

});

Route::group(array('prefix' => 'backend', 'namespace' => 'Ivory\Backend', 'before' => 'admin'), function() {

    Route::group(array('prefix' => 'profile'), function() {

        Route::get('/edit',array(
            'as' => 'profile.edit',
            'uses' => 'ProfileController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'profile.edit.save',
            'uses' => 'ProfileController@postEdit'
        ));

    });

	Route::group(array('prefix' => 'dashboard'), function() {

        Route::get('/',array(
            'as' => 'dashboard.index',
            'uses' => 'DashboardController@getIndex'
        ));

    });

    Route::group(array('prefix' => 'room'), function() {

        Route::get('/',array(
            'as' => 'room.index',
            'uses' => 'RoomController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'room.add',
            'uses' => 'RoomController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'room.add.save',
            'uses' => 'RoomController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'room.edit',
            'uses' => 'RoomController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'room.edit.save',
            'uses' => 'RoomController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'room.delete',
            'uses' => 'RoomController@postDelete'
        ));

        Route::get('/api', array(
            'as' => 'room.api',
            'uses' => 'RoomController@getRoomDataTables'
        ));

    });

    Route::group(array('prefix' => 'banquet'), function() {

        Route::get('/',array(
            'as' => 'banquet.index',
            'uses' => 'BanquetController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'banquet.add',
            'uses' => 'BanquetController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'banquet.add.save',
            'uses' => 'BanquetController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'banquet.edit',
            'uses' => 'BanquetController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'banquet.edit.save',
            'uses' => 'BanquetController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'banquet.delete',
            'uses' => 'BanquetController@postDelete'
        ));

        Route::get('/api', array(
            'as' => 'banquet.api',
            'uses' => 'BanquetController@getBanquetDataTables'
        ));

    });

    Route::group(array('prefix' => 'policy'), function() {

        Route::get('/',array(
            'as' => 'policy.index',
            'uses' => 'PolicyController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'policy.add',
            'uses' => 'PolicyController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'policy.add.save',
            'uses' => 'PolicyController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'policy.edit',
            'uses' => 'PolicyController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'policy.edit.save',
            'uses' => 'PolicyController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'policy.delete',
            'uses' => 'PolicyController@postDelete'
        ));

        Route::get('/api', array(
            'as' => 'policy.api',
            'uses' => 'PolicyController@getPolicyDataTables'
        ));
    });

    Route::group(array('prefix' => 'promo'), function() {

        Route::get('/',array(
            'as' => 'promo.index',
            'uses' => 'PromoController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'promo.add',
            'uses' => 'PromoController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'promo.add.save',
            'uses' => 'PromoController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'promo.edit',
            'uses' => 'PromoController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'promo.edit.save',
            'uses' => 'PromoController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'promo.delete',
            'uses' => 'PromoController@postDelete'
        ));

        Route::get('/api', array(
            'as' => 'promo.api',
            'uses' => 'PromoController@getPromoDataTables'
        ));
    });

    Route::group(array('prefix' => 'stock'), function() {

        Route::get('/',array(
            'as' => 'stock.index',
            'uses' => 'StockController@getIndex'
        ));

        Route::post('/',array(
            'as' => 'stock.index.post',
            'uses' => 'StockController@postIndex'
        ));         

    });

    Route::group(array('prefix' => 'restrictions'), function() {

        Route::get('/',array(
            'as' => 'restriction.index',
            'uses' => 'RestrictController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'restriction.add',
            'uses' => 'RestrictController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'restriction.add.save',
            'uses' => 'RestrictController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'restriction.edit',
            'uses' => 'RestrictController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'restriction.edit.save',
            'uses' => 'RestrictController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'restriction.delete',
            'uses' => 'RestrictController@postDelete'
        ));

        Route::get('/api', array(
            'as' => 'restriction.api',
            'uses' => 'RestrictController@getRestDataTables'
        ));
        
    });

    Route::group(array('prefix' => 'hotel'), function() {

        Route::get('/edit',array(
            'as' => 'hotel.edit',
            'uses' => 'HotelController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'hotel.edit.save',
            'uses' => 'HotelController@postEdit'
        ));

    });

    Route::group(array('prefix' => 'booking'), function() {

        Route::get('/',array(
            'as' => 'booking.index',
            'uses' => 'BookingController@getIndex'
        ));

        Route::get('/view',array(
            'as' => 'booking.edit',
            'uses' => 'BookingController@getView'
        ));

        Route::post('/mark_as_read',array(
            'as' => 'booking.mark_as_read',
            'uses' => 'BookingController@postMarkAsRead'
        ));

        Route::get('/api', array(
            'as' => 'booking.api',
            'uses' => 'BookingController@getBookingDataTables'
        ));

    }); 

    Route::group(array('prefix' => 'transaction'), function() {

        Route::get('/',array(
            'as' => 'transaction.index',
            'uses' => 'TransactionController@getIndex'
        ));

        Route::post('/update_status',array(
            'as' => 'transaction.update_status',
            'uses' => 'TransactionController@postUpdateStatus'
        ));

         Route::get('/download_invoice',array(
            'as' => 'transaction.download_invoice',
            'uses' => 'TransactionController@getDownloadInvoice'
        ));

        Route::get('/api', array(
            'as' => 'transaction.api',
            'uses' => 'TransactionController@getTransactionDataTables'
        ));

    }); 

    Route::group(array('prefix' => 'confirmation'), function() {

        Route::get('/view',array(
            'as' => 'confirmation.view',
            'uses' => 'ConfirmationController@getConfirmation'
        ));

    }); 

    Route::group(array('prefix' => 'cms'), function() {

        Route::group(array('prefix' => 'gallery'), function() {

            Route::get('/',array(
                'as' => 'cms.gallery.index',
                'uses' => 'GalleryController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'cms.gallery.add',
                'uses' => 'GalleryController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'cms.gallery.add.save',
                'uses' => 'GalleryController@postCreate'
            ));

            Route::get('/edit',array(
                'as' => 'cms.gallery.edit',
                'uses' => 'GalleryController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'cms.gallery.edit.save',
                'uses' => 'GalleryController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'cms.gallery.delete',
                'uses' => 'GalleryController@postDelete'
            ));

            Route::post('/upload',array(
                'as' => 'cms.gallery.upload',
                'uses' => 'GalleryImageController@postUpload'
            ));

            Route::get('delete_photo', array(
                'as' => 'cms.gallery.delete_photo',
                'uses' => 'GalleryImageController@getDeletePhoto'
            ));

            Route::get('gallery_photo', array(
                'as' => 'cms.gallery.gallery_photo',
                'uses' => 'GalleryImageController@getGalleryPhoto'
            ));

            Route::get('manage_photo', array(
                'as' => 'cms.gallery.manage_photo',
                'uses' => 'GalleryController@getManagePhoto'
            ));

            Route::post('update_metadata', array(
                'as' => 'cms.gallery.update_metadata',
                'uses' => 'GalleryController@postUpdateMetadata'
            ));

            Route::get('/api',array(
                'as' => 'cms.gallery.api',
                'uses' => 'GalleryController@getGalDataTables'
            ));

            Route::group(array('prefix' => 'video'), function() {

                Route::get('/',array(
                    'as' => 'cms.gallery.video.index',
                    'uses' => 'VideoController@getIndex'
                ));

                Route::get('/add',array(
                    'as' => 'cms.gallery.video.add',
                    'uses' => 'VideoController@getCreate'
                ));

                Route::post('/add',array(
                    'as' => 'cms.gallery.video.add.save',
                    'uses' => 'VideoController@postCreate'
                ));

                Route::get('/edit',array(
                    'as' => 'cms.gallery.video.edit',
                    'uses' => 'VideoController@getEdit'
                ));

                Route::post('/edit',array(
                    'as' => 'cms.gallery.video.edit.save',
                    'uses' => 'VideoController@postEdit'
                ));

                Route::post('/delete',array(
                    'as' => 'cms.gallery.video.delete',
                    'uses' => 'VideoController@postDelete'
                ));

                Route::get('/api',array(
                    'as' => 'cms.gallery.video.api',
                    'uses' => 'VideoController@getVideoDataTables'
                ));
            });
        });

        Route::group(array('prefix' => 'gallery-categories'), function() {

            Route::get('/',array(
                'as' => 'cms.gallery-categories.index',
                'uses' => 'GalleryCategoriesController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'cms.gallery-categories.add',
                'uses' => 'GalleryCategoriesController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'cms.gallery-categories.save',
                'uses' => 'GalleryCategoriesController@postCreate'
            ));

            Route::get('/edit',array(
                'as' => 'cms.gallery-categories.edit',
                'uses' => 'GalleryCategoriesController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'cms.gallery-categories.edit',
                'uses' => 'GalleryCategoriesController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'cms.gallery-categories.delete',
                'uses' => 'GalleryCategoriesController@postDelete'
            ));

            Route::get('/api',array(
                'as' => 'cms.gallery-categories.api',
                'uses' => 'GalleryCategoriesController@getGalCatDataTables'
            ));

        });
        
        Route::group(array('prefix' => 'review'), function() {

            Route::get('/',array(
                'as' => 'cms.review.index',
                'uses' => 'TestimoniController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'cms.review.add',
                'uses' => 'TestimoniController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'cms.review.add.save',
                'uses' => 'TestimoniController@postCreate'
            ));

            Route::get('/edit',array(
                'as' => 'cms.review.edit',
                'uses' => 'TestimoniController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'cms.review.edit.save',
                'uses' => 'TestimoniController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'cms.review.delete',
                'uses' => 'TestimoniController@postDelete'
            ));

            Route::post('/upload',array(
                'as' => 'cms.review.upload',
                'uses' => 'TestimoniController@upload'
            ));

            Route::any('/get-all-photos',array(
                'as' => 'cms.review.getAllPhotos',
                'uses' => 'TestimoniController@getAllPhotos'
            ));
            
            Route::get('/{id}/delete-photo/{name}',array(
                'as' => 'cms.review.deletePhoto',
                'uses' => 'TestimoniController@deletePhoto'
            ));

            Route::get('/api',array(
                'as' => 'cms.review.api',
                'uses' => 'TestimoniController@GetTestiDataTables'
            ));

        });

    });
    
    Route::group(array('prefix' => 'cafe'), function() {

        Route::group(array('prefix' => 'promo'), function() {

            Route::get('/',array(
                'as' => 'cafe.promo.index',
                'uses' => 'CafePromoController@getIndex'
            ));

            Route::get('/add',array(
                'as' => 'cafe.promo.add',
                'uses' => 'CafePromoController@getCreate'
            ));

            Route::post('/add',array(
                'as' => 'cafe.promo.add.save',
                'uses' => 'CafePromoController@postCreate'
            ));

            Route::get('/edit',array(
                'as' => 'cafe.promo.edit',
                'uses' => 'CafePromoController@getEdit'
            ));

            Route::post('/edit',array(
                'as' => 'cafe.promo.edit.save',
                'uses' => 'CafePromoController@postEdit'
            ));

            Route::post('/delete',array(
                'as' => 'cafe.promo.delete',
                'uses' => 'CafePromoController@postDelete'
            ));

            Route::get('/api',array(
                'as' => 'cafe.promo.api',
                'uses' => 'CafePromoController@getCafePromoDataTables'
            ));

        });

    });

    Route::group(array('prefix' => 'user'), function() {

        Route::get('/',array(
            'as' => 'user.index',
            'uses' => 'UserController@getIndex'
        ));

        Route::get('/add',array(
            'as' => 'user.add',
            'uses' => 'UserController@getCreate'
        ));

        Route::post('/add',array(
            'as' => 'user.add.save',
            'uses' => 'UserController@postCreate'
        ));

        Route::get('/edit',array(
            'as' => 'user.edit',
            'uses' => 'UserController@getEdit'
        ));

        Route::post('/edit',array(
            'as' => 'user.edit.save',
            'uses' => 'UserController@postEdit'
        ));

        Route::post('/delete',array(
            'as' => 'user.delete',
            'uses' => 'UserController@postDelete'
        ));

        Route::post('/gen_passwd',array(
            'as' => 'user.gen_passwd',
            'uses' => 'UserController@postGeneratePassword'
        ));

        Route::post('/gen_code',array(
            'as' => 'user.gen_code',
            'uses' => 'UserController@postGenerateCode'
        ));

        Route::get('/api', array(
            'as' => 'user.api',
            'uses' => 'UserController@getMemberDataTables'
        ));
    });

});