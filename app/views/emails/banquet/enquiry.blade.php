<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

</head>

<body>
	<h2>Banquet Enquiry ({{$room}})</h2>
	<p><strong>From:</strong> {{ ucwords($title) }}. {{ $name }}</p>
	<p><strong>Phone:</strong> {{ $phone }}</p>
	<p><strong>Room:</strong> {{ $room }}</p>
	<p><strong>Start Date:</strong> {{ $start_date }}</p>
	<p><strong>End Date:</strong> {{ $end_date }}</p>
	<p><strong>Message: </strong><br><br>{{ $body }}</p>
</body>

</html>