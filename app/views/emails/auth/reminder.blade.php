<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

</head>

<body>
	<h2>Hello {{ $name }},</h2>
	<p>You are requesting a password reset.</p>
	<p><a href="{{ URL::to('/') . '/reset_password?token=' . $reset_token }}">Click here</a> to complete the process</p>
</body>

</html>